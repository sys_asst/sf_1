<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/popup'); ?>

<div class="row-fluid">
	<div class="span12 page-content">
		<?php
			$this->widget('bootstrap.widgets.TbAlert', array(
				'id'=>'statusMsg',
				'block'=>false, // display a larger alert block?
				'fade'=>true, // use transitions?
				'closeText'=>'x', // close link text - if set to false, no close link is displayed
				'alerts'=>array(// configurations per alert type
					'success'=>array('block'=>false, 'fade'=>true, 'closeText'=>'&times;'), // success, info, warning, error or danger
					'error'=>array('block'=>false, 'fade'=>true, 'closeText'=>'&times;'),
					'info'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'),
					'warning'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'),
				),
			));
		?>

        <div class="row">
            <?php if (null != $this->menu) {
                $this->widget('bootstrap.widgets.TbMenu', array(
                    'type'=>'pills',
                    'stacked'=>false,
                    'items'=>$this->menu,
                    'encodeLabel'=>false,
                    'htmlOptions'=>array('class'=>'pull-right'),
                ));
            }
            ?>
        </div>

		<?php echo $content; ?>
	</div>
</div>
<?php $this->endContent(); ?>