<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div class="row-fluid">
	<div class="span2 page-content">
		<?php 
			$this->widget('bootstrap.widgets.TbMenu', array(
				'type'=>'list', // '', 'tabs', 'pills' (or 'list')
				'stacked'=>false, // whether this is a stacked menu
				'encodeLabel'=>false,
				'items'=>array(
					array('label'=> Yii::app()->fa->getIcon('home', Yii::t('messages', 'Home')), 'active' => in_array($this->getAction()->getId(), array('index')), 'url'=>array('/site/index')),
					array('label'=> Yii::app()->fa->getIcon('desktop', Yii::t('messages', 'Dashboard')), 'visible' => Yii::app()->user->checkAccess('Dashboard.Dashboard'), 'active' => in_array(Yii::app()->controller->id, array('dashboard')), 'url'=>array('/dashboard/dashboard')),
					array('label'=> Yii::app()->fa->getIcon('group', Yii::t('messages', 'Users')), 'visible' => Yii::app()->user->checkAccess('User.Admin'), 'active' => in_array(Yii::app()->controller->id, array('user')), 'url'=>array('/user/admin')),
					array('label'=> Yii::app()->fa->getIcon('check', Yii::t('messages', 'Roles')), 'visible' => Yii::app()->user->checkAccess('Erbac.Authitem.Admin'), 'active' => in_array(Yii::app()->controller->id, array('authItem')) && CAuthItem::TYPE_ROLE == Tk::get('type'), 'url' => array('/erbac/authItem/admin', 'type'=>CAuthItem::TYPE_ROLE)),
					array('label' => Yii::app()->fa->getIcon('list', Yii::t('messages', 'Permissions')), 'visible' => Yii::app()->user->getState('is_super_admin'), 'active' => in_array(Yii::app()->controller->id, array('authItem')) && CAuthItem::TYPE_OPERATION == Tk::get('type'), 'url' => array('/erbac/authItem/admin', 'type'=>CAuthItem::TYPE_OPERATION)),
                    array('label'=> Yii::app()->fa->getIcon('pencil', Yii::t('messages', 'Forms')), 'visible' => Yii::app()->user->checkAccess('Records.Create'), 'active' => in_array(Yii::app()->controller->id, array('records')) && (!in_array(Yii::app()->controller->action->id, array('ListReview', 'ReviewRecords'))), 'url'=>array('/records/admin')),
                    array('label'=> Yii::app()->fa->getIcon('comment', Yii::t('messages', 'Review')), 'visible' => Yii::app()->user->checkAccess('Records.ListReview'), 'active' => (in_array(Yii::app()->controller->action->id, array('ListReview', 'ReviewRecords'))), 'url'=>array('/records/ListReview')),
                    array('label'=> Yii::app()->fa->getIcon('check-square', Yii::t('messages', 'AG Review')), 'visible' => Yii::app()->user->checkAccess('AgreviewerAssignment.AGReview'), 'active' => (in_array(Yii::app()->controller->action->id, array('AGReview'))), 'url'=>array('/agreviewerAssignment/AGReview')),
                    array('label'=> Yii::app()->fa->getIcon('cog', Yii::t('messages', 'Reports')), 'visible' => Yii::app()->user->checkAccess('Report.ViewReport'), 'active' => in_array(Yii::app()->controller->id, array('report')), 'url'=>array('/report/ViewReport')),

                    array('label'=>Yii::t('messages', 'Settings')),
                    array('label'=> Yii::app()->fa->getIcon('flag', Yii::t('messages', 'Department')), 'visible' => Yii::app()->user->checkAccess('Department.Admin'), 'active' => in_array(Yii::app()->controller->id, array('department')), 'url'=>array('/department/admin')),
                    array('label'=> Yii::app()->fa->getIcon('suitcase', Yii::t('messages', 'Commune')), 'visible' => Yii::app()->user->checkAccess('Commune.Admin'), 'active' => in_array(Yii::app()->controller->id, array('commune')), 'url'=>array('/commune/admin')),
                    array('label'=> Yii::app()->fa->getIcon('umbrella', Yii::t('messages', 'Indicator')), 'visible' => Yii::app()->user->checkAccess('Indicator.Admin'), 'active' => in_array(Yii::app()->controller->id, array('indicator')), 'url'=>array('/indicator/admin')),
                    array('label'=> Yii::app()->fa->getIcon('thumb-tack', Yii::t('messages', 'Data Point')), 'visible' => Yii::app()->user->checkAccess('IndDataPoint.Admin'), 'active' => in_array(Yii::app()->controller->id, array('indDataPoint')), 'url'=>array('/indDataPoint/admin')),
                    array('label'=> Yii::app()->fa->getIcon('language', Yii::t('messages', 'Language')), 'visible' => Yii::app()->user->getState('is_super_admin'), 'active' => in_array(Yii::app()->controller->id, array('languages')), 'url'=>array('/languages/admin')),
                    array('label'=> Yii::app()->fa->getIcon('book', Yii::t('messages', 'Add Words')), 'visible' => Yii::app()->user->getState('is_super_admin'), 'active' => in_array(Yii::app()->controller->id, array('words')), 'url'=>array('/words/admin')),
                    array('label'=> Yii::app()->fa->getIcon('bullhorn', Yii::t('messages', 'Translation')), 'visible' => Yii::app()->user->getState('is_super_admin'), 'active' => in_array(Yii::app()->controller->id, array('dWords')), 'url'=>array('/dWords/admin')),
                    array('label'=> Yii::app()->fa->getIcon('download', Yii::t('messages', 'Get IPTT')), 'visible' => Yii::app()->user->getState('is_super_admin'), 'active' => (in_array(Yii::app()->controller->action->id, array('getpisttemplate'))), 'url'=>array('/report/getpisttemplate')),
                    array('label'=> Yii::app()->fa->getIcon('cog', Yii::t('messages', 'Reviewer Assign')), 'visible' => Yii::app()->user->getState('is_super_admin'), 'active' => (in_array(Yii::app()->controller->action->id, array('reviewRoleAssignment'))), 'url'=>array('/reviewRoleAssignment/admin')),
                    array('label'=> Yii::app()->fa->getIcon('check-square', Yii::t('messages', 'AG Reviewer Assign')), 'visible' => Yii::app()->user->getState('is_super_admin'), 'active' => (in_array(Yii::app()->controller->action->id, array('agreviewerAssignment'))), 'url'=>array('/agreviewerAssignment/admin')),
                    array('label'=> Yii::app()->fa->getIcon('cogs', Yii::t('messages', 'Mail Notifications')), 'visible' => Yii::app()->user->getState('is_super_admin'), 'active' => (in_array(Yii::app()->controller->action->id, array('sysMailNotifications'))), 'url'=>array('/sysMailNotifications/1')),
                    //array('label'=> Yii::app()->fa->getIcon('group', Yii::t('messages', 'Data Entry Form')), 'visible' => Yii::app()->user->checkAccess('StaffCategory.Admin'), 'active' => in_array(Yii::app()->controller->id, array('staffCategory')), 'url'=>array('/staffCategory/admin')),

				),
			)); 
		?>
	</div>
	<div class="span10 page-content">
		<?php
			$this->widget('bootstrap.widgets.TbAlert', array(
				'id'=>'statusMsg',
				'block'=>false, // display a larger alert block?
				'fade'=>true, // use transitions?
				'closeText'=>'x', // close link text - if set to false, no close link is displayed
				'alerts'=>array(// configurations per alert type
					'success'=>array('block'=>false, 'fade'=>true, 'closeText'=>'&times;'), // success, info, warning, error or danger
					'error'=>array('block'=>false, 'fade'=>true, 'closeText'=>'&times;'),
					'info'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'),
					'warning'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'),
				),
			));
		?>
		
		<?php if (null != $this->title):?>
			<div class="page-header">
				<h3><?php echo $this->title ?> <small><?php echo $this->titleDescription?></small></h3>
			</div>
		<?php endif;?>
		
		<div class="row">
			<?php if (null != $this->menu) {
					$this->widget('bootstrap.widgets.TbMenu', array(
						'type'=>'pills',
						'stacked'=>false,
						'items'=>$this->menu,
						'encodeLabel'=>false,
						'htmlOptions'=>array('class'=>'pull-right'),
					));
				}
			?>
		</div>
		
		<?php echo $content; ?>

        <!-- <div class="site_by">
            <?php //echo 'Site By '.CHtml::link(CHtml::image(Yii::app()->request->baseUrl."/images/nile_point.jpg","Nile Point"),'http://www.nilepoint.com/',array('target'=>'_blank')).' Nile Point Consulting LLC.'; ?>
        </div> -->
	</div>

</div>
<?php $this->endContent(); ?>