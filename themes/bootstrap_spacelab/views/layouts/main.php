﻿<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>

	<?php Yii::app()->tk->registerMainScripts(); ?>
</head>

<body>

<div class="container" id="page">

	<?php
	$module = is_object(Yii::app()->controller->module) ? '/' . Yii::app()->controller->module->id . '/' : '';
	$thisAction = $module . Yii::app()->controller->id . '/' . $this->getAction()->getId();
	$languages = array();
	
	/*foreach (Yii::app()->params['lang'] as $key => $langInfo) {
		$languages[] = array('label' => '<img src="'. Yii::app()->tk->getImagePath() . $langInfo['flagName'] . '"/> ' . Yii::t('messages', $langInfo['name']), 'url' => array($thisAction . '?lang=' . $langInfo['identifier']));
	}*/
    $languages = array();
    foreach (Yii::app()->params['lang'] as $key => $langInfo) {
        $languages[] = array('label' => '<img src="'. Yii::app()->tk->getImagePath() . $langInfo['flagName'] . '"/> ' . Yii::t('messages', $langInfo['name']), 'url' => array('/Site/ChangeLang/', 'lang'=>$langInfo['identifier']));
    }
	
	$this->widget('bootstrap.widgets.TbNavbar',array(
		'type'=>'null',
		'collapse'=>true,
        'brand'=>CHtml::image(Yii::app()->getBaseUrl().'/images/logo_kl.png'),
		'fixed' => 'top',
		'items'=>array(
			array(
				'class'=>'bootstrap.widgets.TbMenu',
				'encodeLabel'=>false,
				'items'=>array(
					array('label'=> Yii::app()->fa->getIcon('home', Yii::t('messages', 'Home')), 'url'=>array('/site/index')),
					//array('label'=> Yii::app()->fa->getIcon('desktop', Yii::t('messages', 'Dashboard')), 'url'=>array('/dashboard/dashboard')),
					/*array('label'=> Yii::app()->fa->getIcon('wrench', Yii::t('messages', 'System')), 'active' => in_array(Yii::app()->controller->id, array('authItem', 'user')),
						'items' => array (
							array('label' => Yii::app()->fa->getIcon('check', Yii::t('messages', 'Manage Permissions')), 'visible' => Yii::app()->user->getState('is_super_admin'), 'url' => array('/erbac/authItem/admin', 'type'=>CAuthItem::TYPE_OPERATION)),
							array('label' => Yii::app()->fa->getIcon('check', Yii::t('messages', 'Manage Roles')), 'visible' => Yii::app()->user->checkAccess('Erbac.Authitem.Admin'), 'url' => array('/erbac/authItem/admin', 'type'=>CAuthItem::TYPE_ROLE)),
							array('label' => Yii::app()->fa->getIcon('group', Yii::t('messages', 'Manage System Users')), 'active'=> in_array(Yii::app()->controller->id, array('user')), 'visible' => Yii::app()->user->checkAccess('User.Admin'), 'url' => array('/user/admin')),
						)
					),*/
				),
			),
			array(
				'class'=>'bootstrap.widgets.TbMenu',
				'htmlOptions'=>array('class'=>'pull-right'),
				'encodeLabel'=>false,
				'items'=>array(
					'---',
					array('label'=>Yii::app()->user->getState('firstName'), 'url'=>'#', 'items'=>array(
						array('label'=>Yii::app()->fa->getIcon('user', Yii::t('messages', 'My Account')), 'visible' => Yii::app()->user->checkAccess("User.MyAccount"), 'url'=>array('/user/myAccount')),						
						array('label'=>Yii::app()->fa->getIcon('lock', Yii::t('messages', 'Change Password')), 'visible' => Yii::app()->user->checkAccess("User.ChangePassword"), 'url'=>array('/user/changePassword')),
						array('label'=>Yii::app()->fa->getIcon('flag', Yii::t('messages', 'Language')), 'url'=>'#',
							'items' => $languages
						),
						array('label'=>Yii::app()->fa->getIcon('power-off', Yii::t('messages', 'Logout')), 'url'=>array('/site/logout'))
					)),
				),
			),
		),
	)); ?>

    <!-- <div class="page-content sponsers">
        <?php //echo CHtml::image(Yii::app()->request->baseUrl."/images/hiti.jpg","Hiti"); ?>
        <?php //echo CHtml::image(Yii::app()->request->baseUrl."/images/care.jpg","CARE"); ?>
        <?php //echo CHtml::image(Yii::app()->request->baseUrl."/images/acf.jpg","ACF"); ?>
        <?php //echo CHtml::image(Yii::app()->request->baseUrl."/images/wfp.jpg","WFP"); ?>
        <?php //echo CHtml::image(Yii::app()->request->baseUrl."/images/korelavi.jpg","WFP"); ?>
        <?php //echo CHtml::image(Yii::app()->request->baseUrl."/images/usaid.jpg","USAID"); ?>
    </div> -->
    <div class="page-content sponsers logo-align">
        <table>
            <tr>
                <td style="text-align: center;vertical-align: text-bottom"><div class="row logo-align"><?php echo CHtml::image(Yii::app()->request->baseUrl."/images/usaid.jpg","USAID"); ?>
                    <?php echo CHtml::image(Yii::app()->request->baseUrl."/images/minister.gif","Minister"); ?>
                    <?php echo CHtml::image(Yii::app()->request->baseUrl."/images/care.gif","Care"); ?>
                    <?php echo CHtml::image(Yii::app()->request->baseUrl."/images/wfm.jpg","WFP"); ?>
                    <?php echo CHtml::image(Yii::app()->request->baseUrl."/images/acf.jpg","ACF").'<br />'; ?></div></td>
            </tr>
            <!-- <tr>
                <td class="sep" style="text-align: center;vertical-align: text-bottom">
                        <?php //echo CHtml::image(Yii::app()->request->baseUrl."/images/korelavi_2.jpg","Kore Lavi"); ?>
                    </td>
            </tr> -->
        </table>
    </div>

    <div class="clearfix"></div>

	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>

	<div class="footer">
        <?php echo CHtml::link(CHtml::image(Yii::app()->request->baseUrl."/images/nile_point.png","Nile Point"),'http://www.nilepoint.com/',array('target'=>'_blank')).'<br />'; ?>
		<?php echo Yii::app()->params['copyRight']; ?>
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>
