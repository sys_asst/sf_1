<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	<?php Yii::app()->tk->registerMainScripts(); ?>
</head>

<body>
	<div class="container" id="page">
		<div class="row-fluid">
			<div class="span12">
				<div class="login-page-content">
					<?php echo $content ?>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
