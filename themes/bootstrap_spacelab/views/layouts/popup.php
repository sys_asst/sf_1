﻿<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>

	<?php Yii::app()->tk->registerMainScripts(); ?>
    <style>
        .container, .navbar-static-top .container, .navbar-fixed-top .container, .navbar-fixed-bottom .container {
            width: 99%;
        }
    </style>
</head>


<body>

<div class="container" id="page" style="padding: .2em 0 0 0;">

	<?php
	$module = is_object(Yii::app()->controller->module) ? '/' . Yii::app()->controller->module->id . '/' : '';
	$thisAction = $module . Yii::app()->controller->id . '/' . $this->getAction()->getId();
	$languages = array();
	
	/*foreach (Yii::app()->params['lang'] as $key => $langInfo) {
		$languages[] = array('label' => '<img src="'. Yii::app()->tk->getImagePath() . $langInfo['flagName'] . '"/> ' . Yii::t('messages', $langInfo['name']), 'url' => array($thisAction . '?lang=' . $langInfo['identifier']));
	}*/
    $languages = array();
    foreach (Yii::app()->params['lang'] as $key => $langInfo) {
        $languages[] = array('label' => '<img src="'. Yii::app()->tk->getImagePath() . $langInfo['flagName'] . '"/> ' . Yii::t('messages', $langInfo['name']), 'url' => array('/Site/ChangeLang/', 'lang'=>$langInfo['identifier']));
    }
	
	?>
	<?php echo $content; ?>

    <div class="footer">
        <?php echo CHtml::link(CHtml::image(Yii::app()->request->baseUrl."/images/nile_point.png","Nile Point"),'http://www.nilepoint.com/',array('target'=>'_blank')).'<br />'; ?>
        <?php echo Yii::app()->params['copyRight']; ?>
    </div><!-- footer -->

</div><!-- page -->

</body>
</html>
