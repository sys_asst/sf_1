#!/usr/bin/php
<?php
// ini_set('error_reporting', E_ALL);
// ini_set('display_errors', 1);
// This was added just to avoid waringins on php newer versions
date_default_timezone_set('Asia/Colombo');
// change the following paths if necessary
$yii=dirname(__FILE__).'/framework/yii.php';
//$yii=dirname(__FILE__).'/../../../yii/yii-1.1.15.022a51/framework/yii.php';
$config=dirname(__FILE__).'/protected/config/console.php';

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',true);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);
//define('YII_ENABLE_ERROR_HANDLER', false);

require_once($yii);
//print_r(Yii);exit;
Yii::createConsoleApplication($config)->run();
