<?php

/**
 * This is the model class for table "ms_area".
 *
 * The followings are the available columns in table 'ms_area':
 * @property integer $id
 * @property integer $reagon_id
 * @property integer $distributor_id
 * @property string $code
 * @property string $name
 * @property string $addedon
 * @property integer $added_by
 * @property integer $status
 * @property integer $updated_by
 * @property string $updatedon
 *
 *
 * The followings are the available model relations:
 * @property DArea[] $dAreas
 * @property DRegions[] $dRegions
 * @property MsRegions $reagon
 * @property Users $addedBy
 * @property Users $updatedBy
 * @property Distributors $distributor
 * @property Salesreps[] $salesreps
 */
class Area extends CActiveRecord
{
    const STATUS_NOACTIVE=0;
    const STATUS_ACTIVE=1;
    const STATUS_BANNED=-1;

    //TODO: Delete for next version (backward compatibility)
    const STATUS_BANED=-1;

    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ms_area';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, reagon_id, code,distributor_id', 'required'),
			array('reagon_id, added_by, status, updated_by', 'numerical', 'integerOnly'=>true),
			array('code', 'length', 'max'=>10),
			array('name', 'length', 'max'=>255),
            array('updatedon', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, reagon_id, distributor_id, code, name, addedon, added_by, status, updated_by, updatedon,distributor_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'dAreas' => array(self::HAS_MANY, 'Area', 'area_id'),
			'dRegions' => array(self::HAS_MANY, 'Regions', 'area_id'),
			'reagon' => array(self::BELONGS_TO, 'Regions', 'reagon_id'),
			'addedBy' => array(self::BELONGS_TO, 'Users', 'added_by'),
			'updatedBy' => array(self::BELONGS_TO, 'Users', 'updated_by'),
            'distributor' => array(self::BELONGS_TO, 'Distributors', 'distributor_id'),
            'salesreps' => array(self::HAS_MANY, 'Salesreps', 'area_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'reagon_id' => 'Reagon',
            'distributor_id' => 'Distributor',
			'code' => 'Code',
			'name' => 'Name',
			'addedon' => 'Addedon',
			'added_by' => 'Added By',
			'status' => 'Status',
			'updated_by' => 'Updated By',
			'updatedon' => 'Updatedon',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
                (strlen($this->status) < 1) ? $this->status = Area::STATUS_ACTIVE : '';

		$criteria->compare('id',$this->id);
		$criteria->compare('reagon_id',$this->reagon_id);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('addedon',$this->addedon,true);
        $criteria->compare('distributor_id',$this->distributor_id);
		$criteria->compare('added_by',$this->added_by);
		$criteria->compare('status',$this->status);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('updatedon',$this->updatedon,true);
        $criteria->compare('distributor_id',$this->distributor_id,true);


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Area the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * Returns record status of particular record
     * @param $type
     * @param null $code
     * @return bool
     */
    public static function itemAlias($type,$code=NULL) {
        $_items = array(
            'ItemStatus' => array(
                self::STATUS_NOACTIVE => 'Not active',
                self::STATUS_ACTIVE => 'Active',
                self::STATUS_BANNED => 'Banned',
            ),
            'UserDef' => array(
                '0' =>'Non - User Defined',
                '1' =>'User Defined'
            )
        );
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

    public function getDistricts($id)
    {
        $values = '';
        $connection = Yii::app()->db;
        $sql = "SELECT A.`description` FROM `d_area` AS DR INNER JOIN `ms_district` AS A ON A.`id` = DR.`district_id` AND DR.`area_id` = '{$id}'";

        $command = $connection->createCommand($sql);
        $row = $command->queryAll();
        $values = '';
        if(sizeof($row))
        {
            foreach($row as $data)
            {
                $values .= (strlen($values)) ? ', '.$data['description'] : $data['description'] ;
            }
        }

        return strlen($values) ? $values : 'n/a';
    }
}
