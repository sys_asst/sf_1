<?php

/**
 * This is the model class for table "towns".
 *
 * The followings are the available columns in table 'towns':
 * @property integer $id
 * @property string $province
 * @property string $district
 * @property string $dsDivision
 * @property integer $ds_divid
 * @property string $town
 * @property string $addedOn
 * @property string $lastUpdatedOn
 * @property integer $disabled
 * @property integer $disabled_by
 * @property string $disabled_on
 * @property integer $province_id
 * @property integer $district_id
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property DRoute[] $dRoutes
 * @property DealersEdit[] $dealersEdits
 * @property MsDsdivisions $dsDiv
 * @property MsProvinces $province0
 * @property MsDistrict $district0
 */
class Towns extends CActiveRecord
{
    const STATUS_NOACTIVE=0;
    const STATUS_ACTIVE=1;
    const STATUS_BANNED=-1;

    //TODO: Delete for next version (backward compatibility)
    const STATUS_BANED=-1;

    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'towns';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('dsDivision, lastUpdatedOn', 'required'),
			array('disabled, disabled_by, province_id, district_id, status,ds_divid', 'numerical', 'integerOnly'=>true),
			array('province, district, town, addedOn, disabled_on', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, province, district, dsDivision, town, addedOn, lastUpdatedOn, disabled, disabled_by, disabled_on, province_id, district_id,ds_divid', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'dRoutes' => array(self::HAS_MANY, 'DRoute', 'gndiv_id'),
            'dealersEdits' => array(self::HAS_MANY, 'DealersEdit', 'townId'),
			'district0' => array(self::BELONGS_TO, 'District', 'district_id'),
			'province0' => array(self::BELONGS_TO, 'Provinces', 'province_id'),
            'dsDiv' => array(self::BELONGS_TO, 'Dsdivisions', 'ds_divid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'province' => 'Province',
			'district' => 'District',
			'dsDivision' => 'Ds Division',
            'ds_divid' => 'Ds Divid',
			'town' => 'Town',
			'addedOn' => 'Added On',
			'lastUpdatedOn' => 'Last Updated On',
			'disabled' => 'Disabled',
			'disabled_by' => 'Disabled By',
			'disabled_on' => 'Disabled On',
			'province_id' => 'Province',
			'district_id' => 'District',
            'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('province',$this->province,true);
		$criteria->compare('district',$this->district,true);
		$criteria->compare('dsDivision',$this->dsDivision,true);
		$criteria->compare('town',$this->town,true);
		$criteria->compare('addedOn',$this->addedOn,true);
		$criteria->compare('lastUpdatedOn',$this->lastUpdatedOn,true);
		$criteria->compare('disabled',$this->disabled);
		$criteria->compare('disabled_by',$this->disabled_by);
		$criteria->compare('disabled_on',$this->disabled_on,true);
		$criteria->compare('province_id',$this->province_id);
        $criteria->compare('status',$this->status);
		$criteria->compare('district_id',$this->district_id);
		$criteria->compare('ds_divid',$this->ds_divid);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Towns the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * @param $type
     * @param null $code
     * @return bool
     */
    public static function itemAlias($type,$code=NULL) {
        $_items = array(
            'ItemStatus' => array(
                self::STATUS_NOACTIVE => 'Not active',
                self::STATUS_ACTIVE => 'Active',
                self::STATUS_BANNED => 'Banned',
            ),
            'Level' => array(
                '1' =>'Segment',
                '2' =>'Category',
                '3' =>'Web Category'
            ),
        );
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

    /**
     * Get Towns against the given dsDivision
     * @param int $ds_id
     * @return mixed
     */
    public static function getTowns($ds_id)
    {
        $sql = "SELECT * FROM `towns` AS T WHERE T.`ds_divid` = '{$ds_id}'
            AND T.`status` = '".self::STATUS_ACTIVE."' ORDER BY T.`town`";
        $records = Tk::sql($sql);

        return $records;
    }
}
