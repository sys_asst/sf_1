<?php

/**
 * This is the model class for table "ms_designations".
 *
 * The followings are the available columns in table 'ms_designations':
 * @property integer $id
 * @property string $code
 * @property string $name
 * @property integer $status
 * @property string $added_on
 * @property integer $added_by
 *
 * The followings are the available model relations:
 * @property Users $addedBy
 * @property Salesreps[] $salesreps
 */
class Designations extends CActiveRecord
{
    const STATUS_NOACTIVE=0;
    const STATUS_ACTIVE=1;
    const STATUS_BANNED=-1;

    //TODO: Delete for next version (backward compatibility)
    const STATUS_BANED=-1;

    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ms_designations';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('status, added_by', 'numerical', 'integerOnly'=>true),
			array('code', 'length', 'max'=>5),
			array('name', 'length', 'max'=>255),
			array('added_on', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, code, name, status, added_on, added_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'addedBy' => array(self::BELONGS_TO, 'Users', 'added_by'),
			'salesreps' => array(self::HAS_MANY, 'Salesreps', 'designation_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'code' => 'Code',
			'name' => 'Name',
			'status' => 'Status',
			'added_on' => 'Added On',
			'added_by' => 'Added By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('added_on',$this->added_on,true);
		$criteria->compare('added_by',$this->added_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Designations the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * Returns record status of particular record
     * @param $type
     * @param null $code
     * @return bool
     */
    public static function itemAlias($type,$code=NULL) {
        $_items = array(
            'ItemStatus' => array(
                self::STATUS_NOACTIVE => 'Not active',
                self::STATUS_ACTIVE => 'Active',
                self::STATUS_BANNED => 'Banned',
            ),
            'Type' => array(
                'distType' =>'DISTRIBU',
            ),
        );
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }
}
