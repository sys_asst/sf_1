<?php

/**
 * This is the model class for table "dealercomments".
 *
 * The followings are the available columns in table 'dealercomments':
 * @property integer $id
 * @property integer $reasonId
 * @property string $dealerId
 * @property string $comment
 * @property string $commentDate
 * @property string $srCode
 * @property double $latitude
 * @property double $longitude
 */
class Dealercomments extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'dealercomments';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('reasonId, dealerId, commentDate, srCode, latitude, longitude', 'required'),
			array('reasonId', 'numerical', 'integerOnly'=>true),
			array('latitude, longitude', 'numerical'),
			array('dealerId', 'length', 'max'=>10),
			array('srCode', 'length', 'max'=>50),
			array('comment, distributor_id', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, reasonId, dealerId, comment, commentDate, srCode, latitude, longitude', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'dealer' => array(self::BELONGS_TO, 'Dealers', 'dealerId'),
            'reason' => array(self::BELONGS_TO, 'Dealercommentreasons', 'reasonId'),
            'salesref' => array(self::BELONGS_TO, 'Salesreps', 'srCode'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'reasonId' => 'Reason',
			'dealerId' => 'Dealer',
			'comment' => 'Comment',
			'commentDate' => 'Comment Date',
			'srCode' => 'Sr Code',
			'latitude' => 'Latitude',
			'longitude' => 'Longitude',
            'distributor_id'=>'Distributor'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('reasonId',$this->reasonId);
		$criteria->compare('dealerId',$this->dealerId,true);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('commentDate',$this->commentDate,true);
		$criteria->compare('srCode',$this->srCode,true);
		$criteria->compare('latitude',$this->latitude);
		$criteria->compare('longitude',$this->longitude);

        $criteria->order = 'id desc';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Dealercomments the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function getSalesRefName() {
        $categoryOptions = Salesreps::getSalesRefs();
        return isset($categoryOptions[$this->srCode]) ?
            $categoryOptions[$this->srCode] : "n/a";//"unknown category ({$this->parent_cat_id})"
    }

    public function getComentReasontext() {
        $categoryOptions = Dealercommentreasons::getCommentReasons();
        return isset($categoryOptions[$this->reasonId]) ?
            $categoryOptions[$this->reasonId] : "n/a";//"unknown category ({$this->parent_cat_id})"
    }
    
    public static function getNoofComments($sr_code, $from_date, $to_date)
    {
        $sql = "SELECT COUNT(0) AS `coments` FROM `dealercomments` AS DC WHERE DC.`srCode` = '{$sr_code}' AND DC.`commentDate` BETWEEN '{$from_date} 00:00:00' AND '{$to_date} 23:59:59'";
        $records = Tk::sql($sql);

        if(sizeof($records))
        {
            return $records[0]['coments'];
        } else {
            return 0;
        }
    }
}
