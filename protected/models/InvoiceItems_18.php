<?php

/**
 * This is the model class for table "invoice_items".
 *
 * The followings are the available columns in table 'invoice_items':
 * @property integer $id
 * @property integer $invoice_id
 * @property integer $status_id
 * @property integer $products_id
 * @property integer $quantity
 * @property integer $pending_quantity
 * @property string $unit_price
 * @property integer $createdby
 * @property string $createdtime
 * @property integer $updatedby
 * @property string $updatedtime
 */
class InvoiceItems extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'invoice_items';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('invoice_id, status_id, products_id, quantity, pending_quantity, createdby, updatedby', 'numerical', 'integerOnly'=>true),
			array('unit_price', 'length', 'max'=>11),
			array('createdtime, updatedtime', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, invoice_id, status_id, products_id, quantity, pending_quantity, unit_price, createdby, createdtime, updatedby, updatedtime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    
                     'products' => array(self::BELONGS_TO, 'Products', 'products_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'invoice_id' => 'Invoice No',
			'status_id' => 'Status',
			'products_id' => 'Product Name',
			'quantity' => 'Invoice Quantity',
			'pending_quantity' => 'Pending Quantity',
			'unit_price' => 'Unit Price',
			'createdby' => 'Created by',
			'createdtime' => 'Created time',
			'updatedby' => 'Updated by',
			'updatedtime' => 'Updated time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('invoice_id',$this->invoice_id);
		$criteria->compare('status_id',$this->status_id);
		$criteria->compare('products_id',$this->products_id);
		$criteria->compare('quantity',$this->quantity);
		$criteria->compare('pending_quantity',$this->pending_quantity);
		$criteria->compare('unit_price',$this->unit_price,true);
		$criteria->compare('createdby',$this->createdby);
		$criteria->compare('createdtime',$this->createdtime,true);
		$criteria->compare('updatedby',$this->updatedby);
		$criteria->compare('updatedtime',$this->updatedtime,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return InvoiceItems the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
