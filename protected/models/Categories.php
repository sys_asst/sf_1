<?php

/**
 * This is the model class for table "categories".
 *
 * The followings are the available columns in table 'categories':
 * @property integer $id
 * @property string $category
 * @property integer $parent_cat_id
 * @property integer $category_level
 * @property integer $status
 * @property string $addedOn
 * @property string $lastUpdatedOn
 */
class Categories extends CActiveRecord
{

    const STATUS_NOACTIVE=0;
    const STATUS_ACTIVE=1;
    const STATUS_BANNED=-1;

    //TODO: Delete for next version (backward compatibility)
    const STATUS_BANED=-1;

    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'categories';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('category, category_level', 'required'),
			array('parent_cat_id, category_level, status', 'numerical', 'integerOnly'=>true),
			array('category', 'length', 'max'=>80),
			array('addedOn', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, category, parent_cat_id, category_level, status, addedOn, lastUpdatedOn', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'labels' => array(self::MANY_MANY, 'LeavesLabels', 'leaf_level_id_fk'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'category' => 'Name',
			'parent_cat_id' => 'Parent Category',
			'category_level' => 'Category Level',
			'status' => 'Status',
			'addedOn' => 'Added On',
			'lastUpdatedOn' => 'Last Updated On',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
                (strlen($this->status) < 1) ? $this->status = Categories::STATUS_ACTIVE : '';

		$criteria->compare('id',$this->id);
		$criteria->compare('category',$this->category,true);
		$criteria->compare('parent_cat_id',$this->parent_cat_id);
		$criteria->compare('category_level',$this->category_level);
		$criteria->compare('status',$this->status);
		$criteria->compare('addedOn',$this->addedOn,true);
		$criteria->compare('lastUpdatedOn',$this->lastUpdatedOn,true);

        $criteria->order = 'id desc';

        $criteria->addCondition('status > -1');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Categories the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * Returns record status of particular record
     * @param $type
     * @param null $code
     * @return bool
     */
    public static function itemAlias($type,$code=NULL) {
        $_items = array(
            'ItemStatus' => array(
                self::STATUS_NOACTIVE => 'Not active',
                self::STATUS_ACTIVE => 'Active',
                self::STATUS_BANNED => 'Banned',
            ),
            'Level' => array(
                '1' =>'Segment',
                '2' =>'Category',
                '3' =>'Web Category'
            ),
        );
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

    /**
     * Get categories list
     * @return mixed
     */
    public static function getCategories() {
        $categoryArray = CHtml::listData(Categories::model()->findAll(array('condition'=>'status = 1 ','order'=>'category')), 'id', 'category');
        return $categoryArray;
    }

    /**
     * Get Parent category text
     * @return string
     */
    public function getParentCategoryText() {
        $categoryOptions = Categories::getCategories();
        return isset($categoryOptions[$this->parent_cat_id]) ?
            $categoryOptions[$this->parent_cat_id] : "ANTON";//"unknown category ({$this->parent_cat_id})"
    }

    /**
     * Get Level text
     * @return string
     */
    public function getLevelText() {
        $levelOptions = Categories::itemAlias('Level');
        return isset($levelOptions[$this->category_level]) ?
            $levelOptions[$this->category_level] : "ANTON";//"unknown level ({$this->category_level})"
    }

    /**
     * Get status text
     * @return string
     */
    public function getStatusText() {
        $levelOptions = Categories::itemAlias('ItemStatus');
        return isset($levelOptions[$this->status]) ?
            $levelOptions[$this->status] : "ANTON";//"unknown level ({$this->status})"
    }

    /**
     * Get Category Text
     * @param $id
     * @return string
     */
    public static function getCategoryText($id) {
        $levelOptions = Categories::getCategories();
        return isset($levelOptions[$id]) ?
            $levelOptions[$id] : "ANTON";//"unknown index ({$id})"
    }

    /**
     * Get Category by given level
     * @param $levelId
     * @return array
     */
    public static function getCategoriesByLevel($levelId) {
        $categoryArray = CHtml::listData(Categories::model()->findAll(array('condition'=>'status=:status AND category_level=:levelid','order'=>'category','params'=>array(':status'=>1,':levelid'=>$levelId))), 'id', 'category');
        return $categoryArray;
    }

    /**
     * Get child category by given parent id
     * @param $parentId
     * @return mixed
     */
    public static function getChildCategory($parentId)
    {
        $categoryArray = CHtml::listData(Categories::model()->findAll(array('condition'=>'status=:status AND parent_cat_id=:parentid','order'=>'category','params'=>array(':status'=>1,':parentid'=>$parentId))), 'id', 'category');
        return $categoryArray;
    }

    /**
     * Suggest category name for text box
     * @param $keyword
     * @param int $limit
     * @return array
     */
    public function suggestCategory($keyword, $limit=20)
    {
        $criteria=array(
            'select'=>'DISTINCT(category) AS category',
            'condition'=>'category ILIKE :keyword status = 1',
            'order'=>'category',
            'limit'=>$limit,
            'params'=>array(
                ':keyword'=>"$keyword%",

            )
        );
        $models=$this->findAll($criteria);
        $suggest=array();
        foreach($models as $model) {
            $suggest[] = array(
                'value'=>$model->lastname,
                'label'=>$model->lastname,
            );
        }
        return $suggest;
    }

    public static function getLeafValues($catid)
    {
        $connection = Yii::app()->db;
        $sql = "SELECT *, (SELECT label FROM `leaf_level_attribute_labels` WHERE id = `label_id_fk` AND `status` = '1') AS `label`,
                                 (SELECT `abbreviation` FROM `ms_unit` WHERE id = `unit_id` AND `status` = '1') AS `unit` FROM `leaves_labels` WHERE `leaf_level_id_fk` = '{$catid}' AND `label_id_fk` > 0 ORDER BY `type` ASC";
        $command = $connection->createCommand($sql);
        $row = $command->queryAll();

        $sqlData = new CArrayDataProvider(array($row));
        $leaflevels = $sqlData->getData();
        return $leaflevels;
    }
}
