<?php

/**
 * This is the model class for table "ms_district".
 *
 * The followings are the available columns in table 'ms_district':
 * @property integer $id
 * @property integer $province_id
 * @property string $code
 * @property string $description
 * @property integer $added_by
 * @property string $added_on
 * @property integer $deleted
 * @property string $deleted_on
 *
 * The followings are the available model relations:
 * @property MsCities[] $msCities
 * @property MsProvinces $province
 * @property Towns[] $towns
 */
class District extends CActiveRecord
{
    const STATUS_NOACTIVE=0;
    const STATUS_ACTIVE=1;
    const STATUS_BANNED=-1;

    //TODO: Delete for next version (backward compatibility)
    const STATUS_BANED=-1;

    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ms_district';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('province_id, added_by, deleted, status', 'numerical', 'integerOnly'=>true),
			array('code', 'length', 'max'=>5),
			array('description', 'length', 'max'=>255),
			array('added_on, deleted_on', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, province_id, code, description, added_by, added_on, deleted, deleted_on, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'country' => array(self::BELONGS_TO, 'Country', 'country_id'),
			'province' => array(self::BELONGS_TO, 'Provinces', 'province_id'),
			'towns' => array(self::HAS_MANY, 'Towns', 'district_id'),
            'addedBy' => array(self::BELONGS_TO, 'Users', 'added_by'),
            'dAreas' => array(self::HAS_MANY, 'DArea', 'district_id'),
            'msDsdivisions' => array(self::HAS_MANY, 'MsDsdivisions', 'district_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'province_id' => 'Province',
			'code' => 'Code',
			'description' => 'Description',
			'added_by' => 'Added By',
			'added_on' => 'Added On',
			'deleted' => 'Deleted',
			'deleted_on' => 'Deleted On',
            'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('province_id',$this->province_id);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('added_by',$this->added_by);
		$criteria->compare('added_on',$this->added_on,true);
		$criteria->compare('deleted',$this->deleted);
        $criteria->compare('status',$this->status);
		$criteria->compare('deleted_on',$this->deleted_on,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return District the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * Returns record status of particular record
     * @param $type
     * @param null $code
     * @return bool
     */
    public static function itemAlias($type,$code=NULL) {
        $_items = array(
            'ItemStatus' => array(
                self::STATUS_NOACTIVE => 'Not active',
                self::STATUS_ACTIVE => 'Active',
                self::STATUS_BANNED => 'Banned',
            ),
            'Level' => array(
                '1' =>'Segment',
                '2' =>'Category',
                '3' =>'Web Category'
            ),
        );
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }
}
