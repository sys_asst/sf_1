<?php

/**
 * This is the model class for table "dealers".
 *
 * The followings are the available columns in table 'dealers':
 * @property integer $id
 * @property string $dealerCode
 * @property string $dealerName
 * @property string $phoneOffice
 * @property integer $phoneResidence
 * @property integer $creditDays
 * @property double $creditAmount
 * @property string $address1
 * @property string $address2
 * @property string $address3
 * @property string $townId
 * @property double $latitude
 * @property double $longitude
 * @property string $distributor
 * @property string $addedOn
 * @property string $lastUpdatedOn
 * @property integer $disabled
 * @property integer $disabled_by
 * @property string $disabled_on
 * @property string $email
 */
class Dealers extends CActiveRecord
{
    public $jsonHeader = 'Content-type: application/json';

    const STATUS_DISABLED = 1;
    const STATUS_ACTIVE = 0;
    const STATUS_BANNED = -1;

    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'dealers';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('dealerCode, dealerName, townId, latitude, longitude, lastUpdatedOn', 'required'),
			array('phoneResidence, creditDays, disabled, disabled_by', 'numerical', 'integerOnly'=>true),
			array('creditAmount, latitude, longitude', 'numerical'),
			array('dealerCode', 'length', 'max'=>10),
                        array('srcode', 'length', 'max'=>5),
			array('dealerName', 'length', 'max'=>100),
			array('phoneOffice', 'length', 'max'=>25),
			array('distributor', 'length', 'max'=>50),
            array('contactPerson', 'length', 'max'=>100),
            array('email', 'length', 'max'=>100),
			array('address1, address2, address3, addedOn, disabled_on', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, dealerCode, dealerName, phoneOffice, phoneResidence, contactPerson, creditDays, creditAmount, address1, address2, address3, townId, latitude, email, longitude, distributor, addedOn, lastUpdatedOn, disabled, disabled_by, disabled_on,srcode', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'distributor0' => array(self::BELONGS_TO, 'Distributors', '', 'foreignKey' => array('distributor'=>'distId')),
            'dealernos' => array(self::HAS_ONE, 'DealerNo', 'id','foreignKey' => array('dealerId'=>'id')),
            //'rl_status' => array(self::BELONGS_TO, 'RelatedFields', '', 'foreignKey' => array('status_id'=>'related_id'),'condition'=>'related_text = "ActiveServices_status"',
            'town' => array(self::BELONGS_TO, 'Towns', 'townId'),
            'srep' => array(self::BELONGS_TO, 'Salesreps', 'srcode','foreignKey' => array('srcode'=>'srEmpNo')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'dealerCode' => 'Dealer Code',
			'dealerName' => 'Dealer Name',
			'phoneOffice' => 'Phone Office',
			'phoneResidence' => 'Phone Residence',
                        'contactPerson' => 'Contact Person Name',
			'creditDays' => 'Credit Days',
			'creditAmount' => 'Credit Amount',
			'address1' => 'Address1',
			'address2' => 'Address2',
			'address3' => 'Address3',
			'townId' => 'Town',
			'latitude' => 'Latitude',
			'longitude' => 'Longitude',
			'distributor' => 'Distributor',
			'addedOn' => 'Added On',
			'lastUpdatedOn' => 'Last Updated On',
			'disabled' => 'Status',
			'disabled_by' => 'Disabled By',
			'disabled_on' => 'Disabled On',
                        'email'=>'E-mail',
                        'srcode'=>'Sales Rep'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
                (strlen($this->disabled) < 1) ? $this->disabled = Dealers::STATUS_ACTIVE : '';

		$criteria->compare('id',$this->id);
		$criteria->compare('dealerCode',$this->dealerCode,true);
		$criteria->compare('dealerName',$this->dealerName,true);
		$criteria->compare('phoneOffice',$this->phoneOffice,true);
		$criteria->compare('phoneResidence',$this->phoneResidence);
        $criteria->compare('contactPerson',$this->contactPerson,true);
		$criteria->compare('creditDays',$this->creditDays);
		$criteria->compare('creditAmount',$this->creditAmount);
		$criteria->compare('address1',$this->address1,true);
		$criteria->compare('address2',$this->address2,true);
		$criteria->compare('address3',$this->address3,true);
		$criteria->compare('townId',$this->townId,true);
		$criteria->compare('latitude',$this->latitude);
		$criteria->compare('longitude',$this->longitude);
		$criteria->compare('distributor',$this->distributor,true);
		$criteria->compare('addedOn',$this->addedOn,true);
		$criteria->compare('lastUpdatedOn',$this->lastUpdatedOn,true);
		$criteria->compare('disabled',$this->disabled);
		$criteria->compare('disabled_by',$this->disabled_by);
		$criteria->compare('disabled_on',$this->disabled_on,true);
                $criteria->compare('email',$this->email,true);
                $criteria->compare('srcode',$this->srcode,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Dealers the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function jsonEncode($array) {
        header($this->jsonHeader);
        echo json_encode($array, JSON_NUMERIC_CHECK);
        Yii::app()->end();
    }

    public static function getDealers() {
        $categoryArray = CHtml::listData(Dealers::model()->findAll(array('condition'=>'disabled = 0 ','order'=>'dealerName')), 'id', 'dealerName');
        return $categoryArray;
    }
    
    public static function getDealerDistributor($dealer_id)
    {
        $connection = Yii::app()->db;
        $id = 0;
        $sql = "SELECT DT.`id` FROM `distributors` AS DT WHERE DT.`distId` = (SELECT D.`distributor` FROM `dealers` AS D WHERE D.`id` = '{$dealer_id}')";
        $command = $connection->createCommand($sql);
        $data = $command->queryRow();
        if(sizeof($data))
        {
            $id = $data['id'];
        }

        return $id;
    }
    
    public static function itemAlias($type,$code=NULL) {
        $_items = array(
            'ItemStatus' => array(
                self::STATUS_ACTIVE => 'Active',
                self::STATUS_BANNED => 'Banned',
                self::STATUS_DISABLED => 'In-Active',
            )
        );

        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

    public static function getDealersByDist($distributors)
    {
        $sql = "SELECT * FROM `dealers` AS D WHERE D.`distributor` IN({$distributors}) AND D.`disabled` = '".Dealers::STATUS_ACTIVE."' ORDER BY D.`dealerName`";
        $records = Tk::sql($sql);

        return $records;
    }

    public static function getDealersByRef($srcode)
    {
        $distId = 0;
        $sql = $sql = "SELECT S.`distributor` FROM `salesreps` AS S WHERE S.`srEmpNo` = '{$srcode}'";
        $records = Tk::sql($sql);

        if(sizeof($distId))
        {
            $distId = $records[0]['distributor'];
        }

        return $distId;
    }
}
