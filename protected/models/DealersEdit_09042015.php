<?php

/**
 * This is the model class for table "dealers_edit".
 *
 * The followings are the available columns in table 'dealers_edit':
 * @property integer $id
 * @property integer $dealer_id
 * @property string $dealerCode
 * @property string $dealerName
 * @property string $phoneOffice
 * @property string $contactPerson
 * @property integer $phoneResidence
 * @property integer $creditDays
 * @property double $creditAmount
 * @property string $address1
 * @property string $address2
 * @property string $address3
 * @property integer $townId
 * @property double $latitude
 * @property double $longitude
 * @property string $distributor
 * @property string $addedOn
 * @property string $lastUpdatedOn
 * @property integer $disabled
 * @property integer $disabled_by
 * @property string $disabled_on
 *
 * The followings are the available model relations:
 * @property Towns $town
 * @property Dealers $dealer
 */
class DealersEdit extends CActiveRecord
{
    const STATUS_NOACTIVE=0;
    const STATUS_ACTIVE=1;
    const STATUS_BANNED=-1;

    //TODO: Delete for next version (backward compatibility)
    const STATUS_BANED=-1;
    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'dealers_edit';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('dealerCode, dealerName, townId, latitude, longitude, lastUpdatedOn', 'required'),
			array('dealer_id, phoneResidence, creditDays, townId, disabled, disabled_by', 'numerical', 'integerOnly'=>true),
			array('creditAmount, latitude, longitude, status', 'numerical'),
			array('dealerCode', 'length', 'max'=>10),
			array('dealerName, contactPerson, email', 'length', 'max'=>100),
			array('phoneOffice', 'length', 'max'=>25),
			array('distributor', 'length', 'max'=>50),
			array('address1, address2, address3, addedOn, disabled_on', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, dealer_id, dealerCode, dealerName, phoneOffice, contactPerson, phoneResidence, creditDays, creditAmount, address1, address2, address3, townId, latitude, longitude, distributor, addedOn, lastUpdatedOn, disabled, disabled_by, disabled_on, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'town' => array(self::BELONGS_TO, 'Towns', 'townId'),
			'dealer' => array(self::BELONGS_TO, 'Dealers', 'dealer_id'),
            'distributor0' => array(self::BELONGS_TO, 'Distributors', '', 'foreignKey' => array('distributor'=>'distId')),
            'dealernos' => array(self::HAS_ONE, 'DealerNo', 'id','foreignKey' => array('dealerId'=>'id')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'dealer_id' => 'Dealer Id',
			'dealerCode' => 'Dealer Code',
			'dealerName' => 'Dealer Name',
			'phoneOffice' => 'Phone Office',
			'contactPerson' => 'Contact Person',
			'phoneResidence' => 'Phone Residence',
            'email' => 'E-mail',
			'creditDays' => 'Credit Days',
			'creditAmount' => 'Credit Amount',
			'address1' => 'Address1',
			'address2' => 'Address2',
			'address3' => 'Address3',
			'townId' => 'Town',
			'latitude' => 'Latitude',
			'longitude' => 'Longitude',
			'distributor' => 'Distributor',
			'addedOn' => 'Added On',
			'lastUpdatedOn' => 'Last Updated On',
			'disabled' => 'Disabled',
			'disabled_by' => 'Disabled By',
			'disabled_on' => 'Disabled On',
            'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

        $sort = new CSort();
        $sort->attributes = array(
            'status asc',
        );

        $criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('dealer_id',$this->dealer_id);
		$criteria->compare('dealerCode',$this->dealerCode,true);
		$criteria->compare('dealerName',$this->dealerName,true);
		$criteria->compare('phoneOffice',$this->phoneOffice,true);
		$criteria->compare('contactPerson',$this->contactPerson,true);
		$criteria->compare('email',$this->email,true);
        $criteria->compare('phoneResidence',$this->phoneResidence,true);
		$criteria->compare('creditDays',$this->creditDays);
		$criteria->compare('creditAmount',$this->creditAmount);
		$criteria->compare('address1',$this->address1,true);
		$criteria->compare('address2',$this->address2,true);
		$criteria->compare('address3',$this->address3,true);
		$criteria->compare('townId',$this->townId);
		$criteria->compare('latitude',$this->latitude);
		$criteria->compare('longitude',$this->longitude);
		$criteria->compare('distributor',$this->distributor,true);
		$criteria->compare('addedOn',$this->addedOn,true);
		$criteria->compare('lastUpdatedOn',$this->lastUpdatedOn,true);
		$criteria->compare('disabled',$this->disabled);
		$criteria->compare('disabled_by',$this->disabled_by);
		$criteria->compare('disabled_on',$this->disabled_on,true);
        $criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort'=>$sort
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DealersEdit the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * Returns record status of particular record
     * @param $type
     * @param null $code
     * @return bool
     */
    public static function itemAlias($type,$code=NULL) {
        $_items = array(
            'ItemStatus' => array(
                self::STATUS_NOACTIVE => 'Not Confirmed',
                self::STATUS_ACTIVE => 'Confirmed',
                self::STATUS_BANNED => 'Rejected',
            ),
            'Level' => array(
                '1' =>'Segment',
                '2' =>'Category',
                '3' =>'Web Category'
            ),
        );
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }
}
