<?php

/**
 * This is the model class for table "dealer_no".
 *
 * The followings are the available columns in table 'dealer_no':
 * @property integer $dealerId
 * @property integer $dealerNo
 *
 * The followings are the available model relations:
 * @property Dealers $dealer
 */
class DealerNo extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'dealer_no';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('dealerId, dealerNo', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('dealerId, dealerNo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'dealer' => array(self::BELONGS_TO, 'Dealers', 'dealerId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'dealerId' => 'Dealer',
			'dealerNo' => 'Dealer No',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('dealerId',$this->dealerId);
		$criteria->compare('dealerNo',$this->dealerNo);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DealerNo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * Returns the Dealer No for given dealer id
     * @param $id
     * @return string
     */
    public static function getDealerNo($id)
    {
        $dealerNo = CHtml::listData(DealerNo::model()->findAll(array('condition'=>'dealerId=:dealerid','params'=> array(':dealerid'=>$id))),'dealerId','dealerNo');

        return sizeof($dealerNo[$id]) ? $dealerNo[$id] : '';
    }
}
