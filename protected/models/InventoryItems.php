<?php

/**
 * This is the model class for table "inventory_items".
 *
 * The followings are the available columns in table 'inventory_items':
 * @property integer $id
 * @property integer $inventory_id
 * @property integer $products_id
 * @property integer $quantity
 * @property string $unit_price
 * @property double $discount
 * @property string $total_price
 * @property integer $createdby
 * @property string $createdtime
 * @property integer $updatedby
 * @property string $updatedtime
 */
class InventoryItems extends CActiveRecord
{
    public $add_minus = '';
    public $tot_qty= '';
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'inventory_items';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
//  		   array('products_id, quantity', 'required'),
//			array('inventory_id, products_id, quantity, createdby, updatedby', 'required'),
			array('inventory_id, products_id, createdby, updatedby', 'numerical', 'integerOnly'=>true),
			array('unit_price, total_price', 'length', 'max'=>11),
                        array('discount', 'numerical'),
                        array('quantity', 'length',  'max'=>50),
			array('createdtime, updatedtime', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, inventory_id, products_id, quantity, unit_price,discount, total_price, createdby, createdtime, updatedby, updatedtime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */

    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'products' => array(self::BELONGS_TO, 'Products', 'products_id'),

        );
    }
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'inventory_id' => 'Inventory',
			'products_id' => 'Product Name',
			'quantity' => 'Quantity',
			'unit_price' => 'Unit Price',
                        'discount' => 'Discount',
			'total_price' => 'Total Value',
			'createdby' => 'Createdby',
			'createdtime' => 'Createdtime',
			'updatedby' => 'Updatedby',
			'updatedtime' => 'Updatedtime',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('inventory_id',$this->inventory_id);
		$criteria->compare('products_id',$this->products_id);
		$criteria->compare('quantity',$this->quantity);
		$criteria->compare('unit_price',$this->unit_price,true);
                $criteria->compare('discount',$this->discount);
		$criteria->compare('total_price',$this->total_price,true);
		$criteria->compare('createdby',$this->createdby);
		$criteria->compare('createdtime',$this->createdtime,true);
		$criteria->compare('updatedby',$this->updatedby);
		$criteria->compare('updatedtime',$this->updatedtime,true);

                 $criteria->order = " id DESC ";

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return InventoryItems the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
