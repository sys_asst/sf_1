<?php

/**
 * This is the model class for table "ms_unit".
 *
 * The followings are the available columns in table 'ms_unit':
 * @property integer $id
 * @property integer $cat_id
 * @property string $code
 * @property string $name
 * @property string $abbreviation`
 * @property integer $status
 * @property string $addedon
 * @property integer $addedby
 * @property string $updatedon
 *
 * The followings are the available model relations:
 * @property Users $addedby0
 * @property MsUnitCategory $cat
 */
class Unit extends CActiveRecord
{
    const STATUS_NOACTIVE=0;
    const STATUS_ACTIVE=1;
    const STATUS_BANNED=-1;

    //TODO: Delete for next version (backward compatibility)
    const STATUS_BANED=-1;

    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ms_unit';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cat_id, code, name, abbreviation', 'required'),
			array('cat_id, status, addedby', 'numerical', 'integerOnly'=>true),
			array('code', 'length', 'max'=>4),
			array('name', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, cat_id, code, name, abbreviation, status, addedon, addedby, updatedon', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			//'addeduser' => array(self::MANY_MANY, 'User', 'addedby'),
            'addeduser' => array(self::BELONGS_TO, 'User', 'addedby'),
			'cat' => array(self::BELONGS_TO, 'UnitCategory', 'cat_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'cat_id' => 'Unit Category',
			'code' => 'Code',
			'name' => 'Name',
			'status' => 'Status',
            'abbreviation' => 'Abbreviation',
			'addedon' => 'Added on',
			'addedby' => 'Added by',
			'updatedon' => 'Updated on',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
                (strlen($this->status) < 1) ? $this->status = Unit::STATUS_ACTIVE : '';

		$criteria->compare('id',$this->id);
		$criteria->compare('cat_id',$this->cat_id);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('status',$this->status);
        $criteria->compare('abbreviation',$this->abbreviation);
		$criteria->compare('addedon',$this->addedon,true);
		$criteria->compare('addedby',$this->addedby);
		$criteria->compare('updatedon',$this->updatedon,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Unit the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * Returns record status of particular record
     * @param $type
     * @param null $code
     * @return bool
     */
    public static function itemAlias($type,$code=NULL) {
        $_items = array(
            'ItemStatus' => array(
                self::STATUS_NOACTIVE => 'Not active',
                self::STATUS_ACTIVE => 'Active',
                self::STATUS_BANNED => 'Banned',
            ),
            'Level' => array(
                '1' =>'Segment',
                '2' =>'Category',
                '3' =>'Web Category'
            ),
        );
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

    /**
     * Get users list
     * @return mixed
     */
    public function getUserOptions()
    {
        $usersArray = CHtml::listData($this->addeduser, 'id', 'username');
        return $usersArray;
    }

    /**
     * Get Unit Category Options
     * @return mixed
     */
    public function getUnitCategoryOptions()
    {
        $usersArray = CHtml::listData(UnitCategory::model()->findAll(array('condition'=>'status = 1 ','order'=>'name')), 'id', 'name');;
        return $usersArray;
    }

    /**
     * Get Category Text
     * @return string
     */
    public function getCategoryText() {
        $levelOptions = $this->getUnitCategoryOptions();
        return isset($levelOptions[$this->cat_id]) ?
            $levelOptions[$this->cat_id] : "unknown ({$this->cat_id})";
    }

    /**
     * Get all units
     * @return mixed
     */
    public static function getUnits() {
        $unitsArray = Unit::model()->findAll(array('condition'=>'status = 1 ','order'=>'name'));
        return $unitsArray;
    }
}
