<?php

/**
 * This is the model class for table "products".
 *
 * The followings are the available columns in table 'products':
 * @property integer $id
 * @property integer $uniqueId
 * @property string $description
 * @property string $unit
 * @property double $price
 * @property double $discount1
 * @property integer $main_cat_fk_id
 * @property integer $sub_prod_cat_fk_id
 * @property integer $category_id
 * @property integer $label1Value
 * @property string $label1Text
 * @property string $label2Value
 * @property integer $status
 * @property string $lastUpdatedOn
 * @property integer $mapped
 * @property string $createdtime
 * @property integer $updatedby
 * @property integer $createdby
 *
 * The followings are the available model relations:
 * @property ProductsLabels[] $productsLabels
 */

class Products extends CActiveRecord
{
    const STATUS_NOACTIVE=0;
    const STATUS_ACTIVE=1;
    const STATUS_BANNED=-1;

    //TODO: Delete for next version (backward compatibility)
    const STATUS_BANED=-1;

    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'products';
	}

	/**
	 * @return array validation rules for model attributes.
	 */

    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('uniqueId, unit, main_cat_fk_id, sub_prod_cat_fk_id, label1Value, label1Text, label2Value, lastUpdatedOn', 'required'),
            array('uniqueId, main_cat_fk_id, sub_prod_cat_fk_id, category_id, label1Value, status, mapped, updatedby, createdby', 'numerical', 'integerOnly'=>true),
            array('price, discount1', 'numerical'),
            array('unit, label1Text', 'length', 'max'=>20),
            array('label2Value', 'length', 'max'=>50),
            array('description,name, createdtime', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, uniqueId, description,name, unit, price, discount1, main_cat_fk_id, sub_prod_cat_fk_id, category_id, label1Value, label1Text, label2Value, status, lastUpdatedOn, mapped, createdtime, updatedby, createdby', 'safe', 'on'=>'search'),
        );
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            //'main_category' => array(self::HAS_MANY, 'Categories', 'main_cat_fk_id'),
            //'sub_category' => array(self::MANY_MANY, 'Categories', 'id'),//tbl_project_user_assignment(project_id, user_id)
            //'web_category' => array(self::MANY_MANY, 'Categories', 'id'),
            'productslabels' => array(self::MANY_MANY, 'LeavsLabels', 'leaf_level_id_fk')
		);
	}

	

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'uniqueId' => 'Product Id',
            'description' => 'Description',
            'name' => 'Name',
            'unit' => 'Unit',
            'price' => 'Price',
            'discount1' => 'Discount1',
            'main_cat_fk_id' => 'Main Category',
            'sub_prod_cat_fk_id' => 'Sub Category',
            'category_id' => 'Web Category',
            'label1Value' => 'Label1 Value',
            'label1Text' => 'Label1 Text',
            'label2Value' => 'Label2 Value',
            'status' => 'Status',
            'lastUpdatedOn' => 'Last Updated On',
            'createdtime' => 'Createdtime',
            'updatedby' => 'Updatedby',
            'createdby' => 'Createdby',
        );
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
                (strlen($this->status) < 1) ? $this->status = Products::STATUS_ACTIVE : '';

		$criteria->compare('id',$this->id);
		$criteria->compare('uniqueId',$this->uniqueId);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('unit',$this->unit,true);
		$criteria->compare('price',$this->price);
		$criteria->compare('discount1',$this->discount1);
		$criteria->compare('main_cat_fk_id',$this->main_cat_fk_id);
		$criteria->compare('sub_prod_cat_fk_id',$this->sub_prod_cat_fk_id);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('label1Value',$this->label1Value);
		$criteria->compare('label1Text',$this->label1Text,true);
		$criteria->compare('label2Value',$this->label2Value,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('lastUpdatedOn',$this->lastUpdatedOn,true);
                $criteria->compare('mapped',$this->mapped);
                $criteria->compare('createdtime',$this->createdtime,true);
                $criteria->compare('updatedby',$this->updatedby);
                $criteria->compare('createdby',$this->createdby);


        return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Products the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * Returns the Main Category
     * */
    public function getCategory($id, $get_pk = 0)
    {
        //$categoryArray = CHtml::listData($this->main_category, 'id', 'category');
        if($get_pk)
        {
            $category = CHtml::listData(Categories::model()->findAll('id=:catid',array(':catid'=>$id)), 'id','category');
        } else {
            $category = CHtml::listData(Categories::model()->findAll('parent_cat_id=:parentid',array(':parentid'=>$id)), 'id','category');
        }

        return $category;
    }

    public static function itemAlias($type,$code=NULL) {
        $_items = array(
            'ItemStatus' => array(
                self::STATUS_NOACTIVE => 'Inactive',
                self::STATUS_ACTIVE => 'Active',
//                self::STATUS_BANNED => 'Banned',
            ),
            'AdminStatus' => array(
                '0' =>'No',
                '1' =>'Yes',
            ),
        );
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

    /**
     * Get products
     * @return array
     */
    public static function getProducts() {
        $labelsOptions = CHtml::listData(Products::model()->findAll(array('condition'=>'status = 1','order'=>'description')), 'id', 'label');
        return $labelsOptions;
    }

    /**
     * Get product's name against given product id
     * @param $id
     * @return string
     */
    public function getProductText($id) {
        $levelOptions = Products::getProducts();
        return isset($levelOptions[$id]) ?
            $levelOptions[$id] : "unknown level ({$id})";
    }

    /**
     * Get status text to display in views
     * @return string
     */
    public function getStatusText() {
        $levelOptions = Products::itemAlias('ItemStatus');
        return isset($levelOptions[$this->status]) ?
            $levelOptions[$this->status] : "unknown level ({$this->status})";
    }

    /**
     * Get Products leaf values
     * @return CActiveRecord[]
     */
    public function getProductsLeafValues()
    {
        $productsLabels = ProductsLabels::model()->findAll(array('condition'=>'product_id=:product_id','params'=>array(':product_id'=>$this->id)));
        return $productsLabels;
    }

    public static function getProductLabelValues($id)
    {
        $values = '';
        $connection = Yii::app()->db;//L.`label`, ' ',
        $sql = "SELECT CONCAT(' ', L.`value`, ' ', IFNULL((SELECT IF(U.`abbreviation` = 'Default', '', U.`abbreviation`) FROM `ms_unit` AS U WHERE U.`id` = LB.`unit_id`), 'n/a')) AS `label` FROM(
        SELECT LL.`label`, PL.`value`, PL.`label_id`
        FROM (`products_labels` AS PL INNER JOIN `leaf_level_attribute_labels` AS LL ON LL.`id` = PL.`label_id` AND PL.`product_id` = '{$id}')) L
        INNER JOIN `leaves_labels` AS LB ON LB.`label_id_fk` = L.`label_id` AND LB.`leaf_level_id_fk` = (SELECT P.`category_id` FROM `products` AS P WHERE P.id = '{$id}') GROUP BY L.`label_id`";

        // echo $sql;
        $command = $connection->createCommand($sql);
        $row = $command->queryAll();
        $values = '';
        if(sizeof($row))
        {
            foreach($row as $data)
            {
                $values .= (strlen($values)) ? ', '.$data['label'] : $data['label'] ;
            }
        }

        return strlen($values) ? $values : 'n/a';
    }
    
    
    
     // get product name on 06 Nov 2015
    
    
     public static function getProductName($id) {
        $data = array();
        $connection = Yii::app()->db;
        $sql = "SELECT C.`category` FROM `categories` AS C inner JOIN products  as P on P.category_id = C.id
                WHERE P.id = '{$id}' limit 1";

        $command = $connection->createCommand($sql);
        $data = $command->queryAll();
       
       $result = "";
       if(!empty($data[0]['category'])){
          $result =  $data[0]['category']." ".self::getProductLabelValues($id);
       }
       
       return $result;
    }
    
    
    
       
    public static function getStock($product_id) {
           
    $result =  Yii::app()->db->createCommand()
    ->select('ROUND(SUM(quantity),2) as stockQty')
    ->from('inventory INS')
    ->join('inventory_items INSItems', 'INS.id = INSItems.inventory_id')
    ->where('products_id='.$product_id.' and INS.distId ="'.Yii::app()->user->name.'"')
    ->group('INSItems.products_id')           
    ->queryRow();
     
     if(empty($result['stockQty'])){
        $stockQty = 0; 
     }else{
      $stockQty = $result['stockQty']; 
     }
       
   return $stockQty;

    }  
    
    public static function getInvocedQty($orders_id, $products_id) {
           
   $result =  Yii::app()->db->createCommand()
    ->select('ROUND(SUM(quantity),2) as qty')
    ->from('invoice Ivs')
    ->join('invoice_items IvsItems', 'Ivs.id = IvsItems.invoice_id')
    ->where('orders_id ='.$orders_id.' and IvsItems.products_id='.$products_id, array(':orders_id'=>$orders_id,':products_id'=>$products_id, ':distId'=>Yii::app()->user->name))
    ->group('IvsItems.products_id')           
    ->queryRow();
     
     if(empty($result['qty'])){
        $qty = 0; 
     }else{
      $qty = $result['qty']; 
     }
       
   return $qty;

    }  
    
}
