<?php

/**
 * This is the model class for table "inventory".
 *
 * The followings are the available columns in table 'inventory':
 * @property integer $id
 * @property integer $inventory_type_id
 * @property string $ref_no
 * @property string $remarks
 * @property integer $status_id
 * @property string $transaction_date
 * @property integer $createdby
 * @property string $createdtime
 * @property integer $updatedby
 * @property string $updatedtime
 * @property integer $distId
 *
 * The followings are the available model relations:
 * @property InventoryType $inventoryType
 */
class Inventory extends CActiveRecord {

    public $products_id = '';
    public $uniqueId = '';
    public $sub_cat = '';
    public $web_category = '';
    public $price = '';
    public $total_price = '';
    public $quantity = '';
    public $av_quantity = '';
    public $add_minus = '';
    public $product_name = '';
    public $discount = '';

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'inventory';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('ref_no, transaction_date, quantity', 'required'),
            array('products_id', 'required', 'message' => 'Please enter correct Web Category/Product'),
//          array('inventory_type_id, ref_no, remarks, status_id, transaction_date, createdby, createdtime, updatedby, updatedtime', 'required'),
            array('inventory_type_id, status_id,distId, createdby, updatedby,quantity', 'numerical', 'integerOnly' => true, 'message' => 'Quantity must be a number'),
            array('quantity', 'validateQty'),
            array('createdtime, updatedtime', 'safe'),
            array('invoice_no, ref_no', 'length', 'max' => 50),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, inventory_type_id, invoice_no, distId, ref_no, remarks, status_id, transaction_date, createdby, createdtime, updatedby, updatedtime', 'safe', 'on' => 'search'),
        );
    }

    public static function totQty($products_id) {

        $qty = 0;

        if ($products_id > 0) {

            $result = Yii::app()->db->createCommand()
                    ->select('SUM(quantity) as qty')
                    ->from('inventory_items ii')
                    ->join('inventory i', 'i.id = ii.inventory_id')
                    ->where('ii.products_id=' . $products_id . ' and i.distId = ' . "'" . Yii::app()->user->name . "'")
                    ->queryRow();


            if (isset($result['qty'])) {
                $qty = $result['qty'];
            }
        }
        $invocedQty = self::getInvocedQty($products_id);

        return $qty - $invocedQty;
    }

    public static function getInvocedQty($products_id) {

        $result = Yii::app()->db->createCommand()
                ->select('SUM(quantity) as qty')
                ->from('invoice Ivs')
                ->join('invoice_items IvsItems', 'Ivs.id = IvsItems.invoice_id')
                ->where('Ivs.status_id = 1 and IvsItems.products_id=' . $products_id . ' and Ivs.distributor_id=' . "'" . Yii::app()->user->name . "'")
                ->group('IvsItems.products_id')
                ->queryRow();

        if (empty($result['qty'])) {
            $qty = 0;
        } else {
            $qty = $result['qty'];
        }

        return $qty;
    }

    public function validateQty($attribute, $params) {


        $this->quantity = abs($this->quantity);
        if ($this->quantity < 1) {
            $this->addError($attribute, 'Sorry, Minimum Quantity is one');
        } else {
            $totQty = self::totQty($this->products_id);

            $qty = $this->quantity;
            if (isset($totQty) && $this->add_minus == 1) {
                $qty = $totQty - $this->quantity;
            }

            if (!isset($this->products_id)) {
                $this->addError($attribute, ' Please enter correct Web Category/Product');
            } else if ($qty < 0) {
//            $this->addError($attribute, 'Sorry, Maximum issue quantity is ' . $totQty);
                $this->addError($attribute, 'There is not enough stock');
            }
        }
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'inventoryType' => array(self::BELONGS_TO, 'InventoryType', 'inventory_type_id'),
            'distributors' => array(self::BELONGS_TO, 'Distributors', 'distId'),
            'createdUser' => array(self::BELONGS_TO, 'Users', 'createdby'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'inventory_type_id' => 'Inventory Type',
            'ref_no' => 'Document No',
            'invoice_no' => 'Inventory No',
            'remarks' => 'Remarks',
            'status_id' => 'Status',
            'transaction_date' => 'Document Date',
            'createdby' => 'Created By',
            'createdtime' => 'Transaction Date',
            'updatedby' => 'Updated By',
            'updatedtime' => 'Updated Time',
            'web_category' => 'Web Category',
            'products_id' => 'Product',
            'uniqueId' => 'Product ID',
            'sub_cat' => 'Sub Category',
            'price' => 'Unit Price',
            'total_price' => 'Total Value',
            'quantity' => 'Quantity',
            'av_quantity' => 'Available Quantity',
            'distId' => 'Distributor',
            'add_minus' => '+ / -',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $distId = -1;
        if (Yii::app()->tk->getRole(Yii::app()->user->getId()) == 'Distributor') {
            $distId = Yii::app()->user->name;
        }

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('inventory_type_id', $this->inventory_type_id);
        $criteria->compare('ref_no', $this->ref_no, true);
        $criteria->compare('remarks', $this->remarks, true);
        $criteria->compare('status_id', $this->status_id);
        $criteria->compare('distId', $distId);
        $criteria->compare('transaction_date', $this->transaction_date, true);
        $criteria->compare('createdby', $this->createdby);
        $criteria->compare('createdtime', $this->createdtime, true);
        $criteria->compare('updatedby', $this->updatedby);
        $criteria->compare('updatedtime', $this->updatedtime, true);
        $criteria->compare('distId', $this->distId);
        $criteria->compare('invoice_no', $this->invoice_no, true);
//        $criteria->group = 'ref_no';

        $criteria->order = " invoice_no DESC ";


        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function view() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('inventory_type_id', $this->inventory_type_id);
        $criteria->compare('ref_no', $this->ref_no);
        $criteria->compare('remarks', $this->remarks, true);
        $criteria->compare('status_id', $this->status_id);
        $criteria->compare('transaction_date', $this->transaction_date, true);
        $criteria->compare('createdby', $this->createdby);
        $criteria->compare('createdtime', $this->createdtime, true);
        $criteria->compare('updatedby', $this->updatedby);
        $criteria->compare('updatedtime', $this->updatedtime, true);
        $criteria->compare('distId', $this->distId);
        $criteria->compare('invoice_no', $this->invoice_no, true);

        $criteria->select = 't.*';

        $criteria->join = 'inner JOIN inventory_items ii ON t.id = ii.inventory_id';
//
        $criteria->group = 't.id';

        $criteria->order = " id DESC ";


        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Inventory the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
