<?php

/**
 * This is the model class for table "products".
 *
 * The followings are the available columns in table 'products':
 * @property integer $id
 * @property integer $uniqueId
 * @property string $description
 * @property string $unit
 * @property double $price
 * @property double $discount1
 * @property integer $main_cat_fk_id
 * @property integer $sub_prod_cat_fk_id
 * @property integer $category_id
 * @property integer $label1Value
 * @property string $label1Text
 * @property string $label2Value
 * @property integer $status
 * @property string $lastUpdatedOn
 * @property integer $mapped
 * @property string $createdtime
 * @property integer $updatedby
 * @property integer $createdby
 *
 * The followings are the available model relations:
 * @property ProductsLabels[] $productsLabels
 */
class Products extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Products the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'products';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('uniqueId, unit, main_cat_fk_id, sub_prod_cat_fk_id, label1Value, label1Text, label2Value, lastUpdatedOn', 'required'),
			array('uniqueId, main_cat_fk_id, sub_prod_cat_fk_id, category_id, label1Value, status, mapped, updatedby, createdby', 'numerical', 'integerOnly'=>true),
			array('price, discount1', 'numerical'),
			array('unit, label1Text', 'length', 'max'=>20),
			array('label2Value', 'length', 'max'=>50),
			array('description, createdtime', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, uniqueId, description, unit, price, discount1, main_cat_fk_id, sub_prod_cat_fk_id, category_id, label1Value, label1Text, label2Value, status, lastUpdatedOn, mapped, createdtime, updatedby, createdby', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'productsLabels' => array(self::HAS_MANY, 'ProductsLabels', 'product_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'id' => 'ID',
            'uniqueId' => 'Product Id',
            'description' => 'Description',
            'unit' => 'Unit',
            'price' => 'Price',
            'discount1' => 'Discount1',
            'main_cat_fk_id' => 'Main Category',
            'sub_prod_cat_fk_id' => 'Sub Category',
            'category_id' => 'Web Category',
            'label1Value' => 'Label1 Value',
            'label1Text' => 'Label1 Text',
            'label2Value' => 'Label2 Value',
            'status' => 'Status',
            'lastUpdatedOn' => 'Last Updated On',
			'mapped' => 'Mapped',
			'createdtime' => 'Createdtime',
			'updatedby' => 'Updatedby',
			'createdby' => 'Createdby',
        );
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('uniqueId',$this->uniqueId);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('unit',$this->unit,true);
		$criteria->compare('price',$this->price);
		$criteria->compare('discount1',$this->discount1);
		$criteria->compare('main_cat_fk_id',$this->main_cat_fk_id);
		$criteria->compare('sub_prod_cat_fk_id',$this->sub_prod_cat_fk_id);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('label1Value',$this->label1Value);
		$criteria->compare('label1Text',$this->label1Text,true);
		$criteria->compare('label2Value',$this->label2Value,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('lastUpdatedOn',$this->lastUpdatedOn,true);
		$criteria->compare('mapped',$this->mapped);
		$criteria->compare('createdtime',$this->createdtime,true);
		$criteria->compare('updatedby',$this->updatedby);
		$criteria->compare('createdby',$this->createdby);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
