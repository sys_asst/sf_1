<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 6/11/15
 * Time: 11:07 AM
 */

class PriceUpdate extends CFormModel
{
    public $prdsrc;

    const STATUS_NOACTIVE=0;
    const STATUS_ACTIVE=1;
    const STATUS_BANNED=-1;
    const SUCCESS = 'SUCCESS';
    const FAIL = 'FAIL';

    public static $products_sql = "products.zip";
    public static $products_src = "products_src.zip";
//    public static $admin_mail = array('Margaret'=>'cism@anton.lk');
    public static $admin_mail = array('Margaret'=>'shanaka@saksglobal.com ');

    //TODO: Delete for next version (backward compatibility)
    const STATUS_BANED=-1;

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            // name, email, subject and body are required
            array('prdsrc', 'file', 'types'=>'txt','allowEmpty' => false),
            //array('prdsrc', 'safe'),
        );
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels()
    {
        return array(
            'prdsrc'=>'Product Price Source',
        );
    }

    public static function itemAlias($type,$code=NULL) {
        $_items = array(
            'ItemStatus' => array(
                self::STATUS_NOACTIVE => 'Not active',
                self::STATUS_ACTIVE => 'Active',
                self::STATUS_BANNED => 'Banned',
            ),
            'Type' => array(
                '1' =>'View Order',
                '2' =>'Dealer Comments',
                '3' =>'Dealers',
                '4' =>'Itinerary Achievement',
            ),
        );
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

    public static function updateProducts()
    {
        Yii::app()->appLog->isConsole = true;
        Yii::app()->appLog->username = 'STAT_DAEMON';
        Yii::app()->appLog->logType = 2;
        $default_prd_main_cat = 2;
        $default_prd_sub_cat = 5;
        $default_prd_cat = 182;


        Yii::app()->appLog->writeLog('Process started');

        ini_set('memory_limit', '2048M');

        $prd_src = SYS_TMP_PATH.Yii::app()->params->prdsrc;
        $prd_src_zip = SYS_TMP_PATH.Yii::app()->params->prdbackup;
//        $class = "style = 'padding-left:20px;padding-right:20px; border-color: #d0d5d9'";
        $update_status_in = "";
        if(file_exists($prd_src))
        {
            $data = file($prd_src);
            $curdb  = explode('=', Yii::app()->db->connectionString);

            $mapped = 0;


            $db_name = $curdb[2];

            //$data = array_shift($file);
           unset($data[0]);
            //Tk::a($data);
            $rows = $inserted_rows = $updated_rows = 0;
            if(sizeof($data))
            {
                $insert = "INSERT INTO `products`(`id`, `uniqueId`, `description`, `unit`, `price`, `discount1`, `main_cat_fk_id`, `sub_prod_cat_fk_id`,
                          `category_id`, `label1Value`, `label1Text`, `label2Value`, `status`, `mapped`, `lastUpdatedOn`,`createdtime`)  VALUES";
                $insert_value = '';
                // Backup products table
//                self::backups_tables('products');
                $update_tbl = "";

                $tbl = "<table style='border-collapse:collapse;border-color:#d0d5d9;width: 50%' border=1 ><tr><th style = 'padding: 0 20px 0 20px;'>Unique ID</th><th style = 'padding: 0 20px 0 20px;'>Status</th></tr>";
                $update_fl_tbl = "";
                $in_un_ids = array();

                $conn = Yii::app()->db;
                $transaction = $conn->beginTransaction();


                try {

                foreach($data as $data_row)
                {

                    $d_row = explode(';', $data_row);

                  $sql = "SELECT * FROM `products` AS P WHERE P.`uniqueId` = '{$d_row[0]}'";
                    $product = Tk::sql($sql);

                    if(sizeof($product))
                    {

                        $sql = "UPDATE `products` SET `products`.`discount1` = ROUND((({$d_row[7]}-{$d_row[5]})/{$d_row[7]})*100), `products`.`price` = '{$d_row[7]}' ,`products`.`lastUpdatedOn` = NOW()
                                     WHERE `products`.`uniqueId` = '{$d_row[0]}'; ";


                        $rows  = Tk::execute($sql);

                        if($rows == 0){
                            $update_status = self::FAIL;
                        }else{
                            $update_status = self::SUCCESS;
                        }

                        $updated_rows += $rows;

                        Yii::app()->appLog->writeLog('The executed sql was: '.$sql.' Status: '.$update_status);

                        $update_tbl .= "<tr><td style = 'padding: 0 20px 0 20px;'>".$d_row[0]."</td><td style = 'padding: 0 20px 0 20px;'>".$update_status."</td></tr>";



                    } else {
                        $in_un_ids[] = $d_row[0];

                        $temp = "(NULL,'{$d_row[0]}','{$d_row[3]}','{$d_row[2]}','{$d_row[7]}',
                        ROUND((({$d_row[7]}-{$d_row[5]})/{$d_row[7]})*100),'{$default_prd_main_cat}','{$default_prd_sub_cat}','{$default_prd_cat}',
                        'AUTOMATICALLY INSERTED NEW PRODUCT','AUTOMATICALLY INSERTED NEW PRODUCT - text','AUTOMATICALLY INSERTED NEW PRODUCT - 2','".Products::STATUS_ACTIVE."','{$mapped}',NOW(),NOW())";
                        $insert_value .= (strlen($insert_value)) ? ','.$temp : $temp;
                    }
                }



                if(strlen($insert_value))
                {


                        $rows = Tk::insert($insert.$insert_value);

                        if($rows == 0){
                            $inserted_rows = 0;
                            $update_status_in = self::FAIL;
                        }else{
                            $inserted_rows  = count($in_un_ids);
                            $update_status_in = self::SUCCESS;
                        }



                        Yii::app()->appLog->writeLog('The executed sql was: '.$insert.$insert_value.' Status: '.$update_status_in);


                }

                Yii::app()->appLog->writeLog('Total rows: '.count($data));
                Yii::app()->appLog->writeLog('Updated rows: '.$updated_rows);
                Yii::app()->appLog->writeLog('Inserted rows: '.$inserted_rows);


                // Mail notification message
                $messages = "Dear Admin,<br />\nThe Price Updater triggered and following is the summary of the process.<br/>";


                if(!empty($in_un_ids)){
                    $messages .= "<br/><span style= 'font-size: 12px;font-style: italic'> Inserted Products,<br/>".$tbl;

                   foreach ($in_un_ids as $in_un_id){

                       $messages .= "<tr><td style = 'padding: 0 20px 0 20px;'>".$in_un_id."</td><td style = 'padding: 0 20px 0 20px;'>".$update_status_in."</td></tr>";

                   }
                    $messages .= "</table></span>";
                }

                if($update_tbl!=""){
                    $messages .= "<br/><span style= 'font-size: 12px;font-style: italic'>Updated Products,<br/>".$tbl.$update_tbl."</table></span>";
                }



                // Send email
                self::sendMail($messages);
                Yii::app()->appLog->writeLog('Sent notification mail to '.implode(' => ', self::$admin_mail));
                //unlink($prd_src);
                Yii::app()->appLog->writeLog('Cleaned source file '.$prd_src);

                    $transaction->commit();

                } catch (Exception $e) {

                    Yii::app()->appLog->writeLog(' Error: '.$e->getMessage());

                    $transaction->rollBack();
                }


            } else {
                Yii::app()->appLog->writeLog('No data found', WARNING);
            }


        } else {
            Yii::app()->appLog->writeLog('No data source file found', CRITICAL);
        }


        Yii::app()->appLog->writeLog('Process over');
    }



    public static function backups_tables($tables = '*')
    {
        $curdb  = explode('=', Yii::app()->db->connectionString);
        $db_name = $curdb[2];
        $user = Yii::app()->db->username;
        $pass = Yii::app()->db->password;
        $host = 'localhost';
        $path = SYS_TMP_PATH.Yii::app()->params->prdbackup;
        $time = date('Y-m-d H:i:s');

        $return = "-- Run Backup at {$time}\n\nUSE `{$db_name}`;\nSET FOREIGN_KEY_CHECKS=0;\n";
        $link = mysql_connect($host,$user,$pass);
        mysql_select_db($db_name,$link);
        //get all of the tables
        if($tables == '*')
        {
            $tables = array();
            //$result = mysql_query('SHOW TABLES');
            $result = Tk::sql('SHOW TABLES');
            foreach($result as $res)
            {
                $tables[] = $res['Tables_in_antonsf_test'];
            }
        }
        else
        {
            $tables = is_array($tables) ? $tables : explode(',',$tables);
        }

        //cycle through
        foreach($tables as $table)
        {
            $result = mysql_query('SELECT * FROM `'.$table.'`');
            $num_fields = mysql_num_fields($result);
            $return.= 'DROP TABLE IF EXISTS `'.$table.'`;';
            $row2 = mysql_fetch_row(mysql_query('SHOW CREATE TABLE '.$table));
            $return.= "\n\n".$row2[1].";\n\n";
            for ($i = 0; $i < $num_fields; $i++)
            {
                while($row = mysql_fetch_row($result))
                {
                    $return.= 'INSERT INTO `'.$table.'` VALUES(';
                    for($j=0; $j<$num_fields; $j++)
                    {
                        $row[$j] = addslashes($row[$j]);
                        $row[$j] = ereg_replace("\n","\\n",$row[$j]);
                        if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
                        if ($j<($num_fields-1)) { $return.= ','; }
                    }
                    $return.= ");\n";
                }
            }
            $return.="\n\n";
        }
        $return .= '-- Backup end time '.date('Y-m-d H:i:s');
        //save file
        $sql_file = 'db-backup-'.time().'-'.(md5(implode(',',$tables))).'.sql';
        $handle = fopen($path.$sql_file,'w+');

        fwrite($handle,$return);
        fclose($handle);

        // Zip the sql file
        $zip = new ZipArchive();
        $filename = $path.date('Ymd_His_').self::$products_sql;
        $zip->open($filename, ZipArchive::CREATE);

        $zip->addFromString("produsct.sql", file_get_contents($path.$sql_file));

        $zip->close();

        unlink($path.$sql_file);
    }

    public function sendMail($message, $attachments = array())
    {
        $emailSentStatus = false;

        $subject = 'Price Update ';
        $templatePath = Yii::getPathOfAlias('application.views.emailTemplates.'). DIRECTORY_SEPARATOR .'emailNotificationTemplate.php';

        if(file_exists($templatePath))
        {
            $mail_template = file_get_contents($templatePath);
            $mail_template = str_replace('[COPY_RIGHTS]', Yii::app()->params['copyRight'], $mail_template);
            $mail_template = str_replace('[LOGO]', CHtml::image(Yii::app()->getBaseUrl(true).'/images/logo_small.png'), $mail_template);
        }

        $mail_template = str_replace('[APP_NAME]', Yii::app()->name, $mail_template);
        if(strlen($message))
        {
            $to_send = '';
            $email = self::$admin_mail;//array('anura'=>'anura@saksglobal.com');
                //mail('anurasn@gmail.com','test from php','Test body by app');
            $mail_body = $message;
            $body = str_replace('[CONTENT]', $mail_body, $mail_template);

            $emailSentStatus = Yii::app()->tk->sendEmail(
                array(
                    'name'=>Yii::app()->params['smtp']['defaultFromName'],
                    'email'=>Yii::app()->params['smtp']['defaultFromEmail']
                ),
                $email,
                $subject.' - '.Yii::app()->name,
                $body,
                $attachments
            );
            $to_send = implode('', $email);//dca3e02d6c9e1178f6ad02e8b8ff54f0
            if ($emailSentStatus) {

                Yii::app()->appLog->writeLog('Successfully send Email notification to '.$to_send, WARNING);
            } else {

                Yii::app()->appLog->writeLog('Failure to send Email notification for '.$to_send, WARNING);
            }

        }
        return $emailSentStatus;
    }

    /**
     * File Shorter
     * @param string $dir
     * @return array|bool
     */
    public static function fileShorter($dir)
    {
        $ignored = array('.', '..', '.svn', '.htaccess','.DS_Store');

        $files = array();
        foreach (scandir($dir) as $file) {
            if (in_array($file, $ignored)) continue;
            $files[$file] = filemtime($dir . '/' . $file);
        }

        arsort($files);
        $files = array_keys($files);

        return ($files) ? $files : false;
    }
}