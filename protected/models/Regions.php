<?php

/**
 * This is the model class for table "ms_regions".
 *
 * The followings are the available columns in table 'ms_regions':
 * @property integer $id
 * @property string $code
 * @property string $name
 * @property integer $status
 * @property integer $rcm_id
 * @property integer $added_by
 * @property string $added_on
 * @property integer $updated_by
 * @property integer $updated_on
 *
 * The followings are the available model relations:
 * @property DRegions[] $dRegions
 * @property MsArea[] $msAreas
 * @property Users $updatedBy
 * @property Salesreps $rcm
 * @property Users $addedBy
 */
class Regions extends CActiveRecord
{
    const STATUS_NOACTIVE=0;
    const STATUS_ACTIVE=1;
    const STATUS_BANNED=-1;

    //TODO: Delete for next version (backward compatibility)
    const STATUS_BANED=-1;

    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ms_regions';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, rcm_id, code', 'required'),
			array('status, rcm_id, added_by, updated_by,', 'numerical', 'integerOnly'=>true),
			array('code', 'length', 'max'=>10),
			array('name', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, code, name, status, rcm_id, added_by, added_on, updated_by, updated_on', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'dRegions' => array(self::HAS_MANY, 'DRegions', 'region_id'),
			'msAreas' => array(self::HAS_MANY, 'MsArea', 'reagon_id'),
			'updatedBy' => array(self::BELONGS_TO, 'Users', 'updated_by'),
			'rcm' => array(self::BELONGS_TO, 'Salesreps', 'rcm_id'),
			'addedBy' => array(self::BELONGS_TO, 'Users', 'added_by'),
            'salesreps' => array(self::HAS_MANY, 'Salesreps', 'region_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'code' => 'Code',
			'name' => 'Name',
			'status' => 'Status',
			'rcm_id' => 'Regional Customer Manager',
			'added_by' => 'Added By',
			'added_on' => 'Added On',
			'updated_by' => 'Updated By',
			'updated_on' => 'Updated On',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
                (strlen($this->status) < 1) ? $this->status = Regions::STATUS_ACTIVE : '';

		$criteria->compare('id',$this->id);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('rcm_id',$this->rcm_id);
		$criteria->compare('added_by',$this->added_by);
		$criteria->compare('added_on',$this->added_on,true);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('updated_on',$this->updated_on);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Regions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * Returns record status of particular record
     * @param $type
     * @param null $code
     * @return bool
     */
    public static function itemAlias($type,$code=NULL) {
        $_items = array(
            'ItemStatus' => array(
                self::STATUS_NOACTIVE => 'Not active',
                self::STATUS_ACTIVE => 'Active',
                self::STATUS_BANNED => 'Banned',
            ),
            'UserDef' => array(
                '0' =>'Non - User Defined',
                '1' =>'User Defined'
            )
        );
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

    public function getAreas($id)
    {
        $values = '';
        $connection = Yii::app()->db;
        $sql = "SELECT A.`name` FROM `d_regions` AS DR INNER JOIN `ms_area` AS A ON A.`id` = DR.`area_id` AND DR.`region_id` = '{$id}'";

        $command = $connection->createCommand($sql);
        $row = $command->queryAll();
        $values = '';
        if(sizeof($row))
        {
            foreach($row as $data)
            {
                $values .= (strlen($values)) ? ', '.$data['name'] : $data['name'] ;
            }
        }

        return strlen($values) ? $values : 'n/a';
    }
}
