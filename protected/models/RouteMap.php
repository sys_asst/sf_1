<?php

/**
 * This is the model class for table "ms_route_map".
 *
 * The followings are the available columns in table 'ms_route_map':
 * @property integer $id
 * @property integer $route_id
 * @property string $code
 * @property string $name
 * @property integer $rep_id
 * @property string $target
 * @property string $month
 * @property integer $assigned_by
 * @property string $added_date
 * @property integer $status
 * @property string $updated_on
 *
 * The followings are the available model relations:
 * @property DRouteMap[] $dRouteMaps
 * @property Salesreps $rep
 * @property Users $assignedBy
 * @property Salesreps $repMsRoute $route
 */
class RouteMap extends CActiveRecord
{
    const STATUS_NOACTIVE=0;
    const STATUS_ACTIVE=1;
    const STATUS_BANNED=-1;

    //TODO: Delete for next version (backward compatibility)
    const STATUS_BANED=-1;

    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ms_route_map';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name,rep_id,code', 'required'),
			array('route_id, rep_id, assigned_by, status', 'numerical', 'integerOnly'=>true),
			array('code', 'length', 'max'=>10),
            array('target', 'length', 'max'=>15),
			array('name', 'length', 'max'=>255),
            array('month, updated_on', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, route_id, code, name, rep_id, target, month, assigned_by, added_date, status, month,updated_on', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'dRouteMaps' => array(self::HAS_MANY, 'DRouteMap', 'map_id'),
			'assignedBy' => array(self::BELONGS_TO, 'Users', 'assigned_by'),
			'rep' => array(self::BELONGS_TO, 'Salesreps', 'rep_id'),
            'route' => array(self::BELONGS_TO, 'Route', 'route_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
            'route_id' => 'Route',
            'code' => 'Code',
            'name' => 'Name',
            'rep_id' => 'Rep',
            'target' => 'Target',
            'month' => 'Month',
            'assigned_by' => 'Assigned By',
            'added_date' => 'Added Date',
            'status' => 'Status',
            'updated_on' => 'Updated On',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
        (strlen($this->status) < 1) ? $this->status = RouteMap::STATUS_ACTIVE : '';
        if(Yii::app()->user->profile->src == 2)
        {
            //$criteria->condition= 't.assigned_by = "'.Yii::app()->user->profile->user_id.'"';
            if(Yii::app()->user->getState('src')->designation_id == 1)
            {
                $criteria->condition= 't.assigned_by = "'.Yii::app()->user->profile->user_id.'"';
            }
        }

        $criteria->compare('id',$this->id);
        $criteria->compare('route_id',$this->route_id);
        $criteria->compare('code',$this->code,true);
        $criteria->compare('name',$this->name,true);
        $criteria->compare('rep_id',$this->rep_id);
        $criteria->compare('target',$this->target,true);
        $criteria->compare('month',$this->month,true);
        $criteria->compare('assigned_by',$this->assigned_by);
        $criteria->compare('added_date',$this->added_date,true);
        $criteria->compare('status',$this->status);
        $criteria->compare('updated_on',$this->updated_on,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return RouteMap the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * Returns record status of particular record
     * @param $type
     * @param null $code
     * @return bool
     */
    public static function itemAlias($type,$code=NULL) {
        $_items = array(
            'ItemStatus' => array(
                self::STATUS_NOACTIVE => 'Not active',
                self::STATUS_ACTIVE => 'Active',
                self::STATUS_BANNED => 'Banned',
            ),
            'Level' => array(
                '1' =>'Segment',
                '2' =>'Category',
                '3' =>'Web Category'
            ),
            'Day' => array(
                '7' =>'Working Day',
                '0' =>'Sunday',
                '8' =>'Holiday',
                '9' =>'Conference',
                '10' =>'Leave',
                '11' =>'Travelling'

            )
        );
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

    public function getRoutes($id)
    {
        $values = '';
        $connection = Yii::app()->db;
        $sql = "SELECT A.`name` FROM `d_route_map` AS DR INNER JOIN `ms_route` AS A ON A.`id` = DR.`route_id` AND DR.`map_id` = '{$id}'";

        $command = $connection->createCommand($sql);
        $row = $command->queryAll();
        $values = '';
        if(sizeof($row))
        {
            foreach($row as $data)
            {
                $values .= (strlen($values)) ? ', '.$data['name'] : $data['name'] ;
            }
        }

        return strlen($values) ? $values : 'n/a';
    }

    /**
     * Get Schedule for the given Route Map
     * @param int $route_map_id
     * @return array
     */
    public static function getSchedule($route_map_id)
    {
        $sql = "SELECT DATE_FORMAT(RM.`added_date`, '%d-%m-%Y') AS `date`, DATE_FORMAT(RM.`added_date`, '%d') AS `index`,
             IFNULL(R.`name`, 'n/a') AS `route`, R.`id`, RM.`day` FROM `d_route_map` AS RM
            LEFT JOIN `ms_route` AS R ON R.`id` = RM.`route_id` WHERE RM.`map_id` = '{$route_map_id}' ORDER BY RM.`added_date`";
        $records = Tk::sql($sql);

        $desc = '';
        $data = array();

        if(sizeof($records))
        {
            foreach($records as $rec)
            {
                $data[$rec['index']]['date'] = $rec['date'];
                $data[$rec['index']]['day'] = $rec['day'];
                $data[$rec['index']]['routeids'][] = $rec['id'];
                (isset($data[$rec['index']]['route']) && strlen($data[$rec['index']]['route'])) ? $data[$rec['index']]['route'] .= '<br />'.$rec['route'] : $data[$rec['index']]['route'] = $rec['route'];
            }
        }

        return $data;
    }

    /**
     * List route for particular date and map
     * @param int $map_id
     * @param date $added_date
     * @return array
     */
    public static function getRoutesList($map_id, $added_date)
    {
        $sql = "SELECT RM.`route_id` FROM `d_route_map` AS RM WHERE RM.`map_id` = '{$map_id}' AND RM.`added_date` = '{$added_date}'";
        $records = Tk::sql($sql);
        $temp = array();

        if(sizeof($records))
        {
            foreach($records as $rec)
            {
                $temp[] = $rec['route_id'];
            }
        }

        return $temp;
    }

    /**
     * Get no of dealers for the map
     * @param int $map_id
     * @return int
     */
    public static function getScheduledDealers($map_id)
    {
        $sql = "SELECT COUNT(0) AS `visits` FROM `dealers` AS D
            INNER JOIN `towns` AS T ON T.`id` = D.`townId` INNER JOIN `d_route` AS DR ON DR.`gndiv_id` = T.`ds_divid` AND DR.`route_id` IN(
            SELECT DM.`route_id` FROM `d_route_map` AS DM WHERE DM.`map_id` = '{$map_id}')";
        $records = Tk::sql($sql);

        if(sizeof($records))
        {
            return $records[0]['visits'];
        } else {
            return 0;
        }
    }

    /**
     * Get scheduled data
     * @param date $date
     * @param int $rcm_id
     * @return mixed
     */
    public static function getScheduleProgress($date, $rcm_id = -1)
    {

        $sql = "SELECT REPDATA.*, RM.`name` AS `rmap_name`, RM.`id` AS `rmap_id`, RM.`month`, 0 AS `visits`
                  FROM (
                    SELECT RCM.*, REP.`srName` AS `rep_name`, REP.`id` AS `rep_id`, REP.`srEmpNo` AS `rep_code` FROM (
                    SELECT R.`id` AS `region_id`, R.`name` AS `regionname`, R.`rcm_id`, S.`srName` AS `rcm_name`, S.`srEmpNo` AS `rcm_code`, S.`srTel` AS `rcm_tel`
                    FROM `ms_regions` AS R INNER JOIN `salesreps` AS S ON S.`id` = R.`rcm_id` ".((strlen($rcm_id) && $rcm_id > 0) ? " AND R.`rcm_id` = '{$rcm_id}'" : "" )." ORDER BY R.`name`) AS RCM
                         INNER JOIN `salesreps` AS REP ON REP.`region_id` = RCM.`region_id` ORDER BY RCM.`regionname`, REP.`srName`) AS REPDATA
                         INNER JOIN `ms_route_map` AS RM ON RM.`rep_id` = REPDATA.`rep_id` AND DATE_FORMAT(RM.`month`, '%Y-%m') = DATE_FORMAT('{$date}', '%Y-%m') ORDER BY REPDATA.`rcm_name`";
        $records = Tk::sql($sql);

        return $records;
    }
}
