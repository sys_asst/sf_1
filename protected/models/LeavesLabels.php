<?php

/**
 * This is the model class for table "leaves_labels".
 *
 * The followings are the available columns in table 'leaves_labels':
 * @property integer $id
 * @property integer $leaf_level_id_fk
 * @property integer $label_id_fk
 * @property integer $type
 */
class LeavesLabels extends CActiveRecord
{
    const STATUS_NOACTIVE=0;
    const STATUS_ACTIVE=1;
    const STATUS_BANNED=-1;

    //TODO: Delete for next version (backward compatibility)
    const STATUS_BANED=-1;
    public $updateType;

    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'leaves_labels';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('leaf_level_id_fk, label_id_fk, unit_id', 'required'),
			array('leaf_level_id_fk, label_id_fk, type, unit_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, leaf_level_id_fk, label_id_fk, type, unit_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'leaf_level_id_fk' => 'Web category',
			'label_id_fk' => 'Label Name',
			'type' => 'Label no',
            'unit_id' => 'Unit'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('leaf_level_id_fk',$this->leaf_level_id_fk);
		$criteria->compare('label_id_fk',$this->label_id_fk);
		$criteria->compare('type',$this->type);
        $criteria->compare('unit_id',$this->unit_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort'=>array(
                'defaultOrder'=>'leaf_level_id_fk ASC',
            )
        ));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LeavesLabels the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * Get ststus and Types
     * @param $type
     * @param null $code
     * @return bool
     */
    public static function itemAlias($type,$code=NULL) {
        $_items = array(
            'ItemStatus' => array(
                self::STATUS_NOACTIVE => 'Not active',
                self::STATUS_ACTIVE => 'Active',
                self::STATUS_BANNED => 'Banned',
            ),
            'Type' => array(
                '1' =>'Label 1',
                '2' =>'Label 2'
            ),
        );
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

    /**
     * Get Level text
     * @return string
     */
    public function getTypeText() {
        $levelOptions = LeavesLabels::itemAlias('Type');
        return isset($levelOptions[$this->type]) ?
            $levelOptions[$this->type] : "not assigned";
    }

    /**
     * Get status text
     * @return string
     */
    public function getStatusText() {
        $levelOptions = LeavesLabels::itemAlias('ItemStatus');
        return isset($levelOptions[$this->status]) ?
            $levelOptions[$this->status] : "unknown level ({$this->status})";
    }

    /**
     * Get label text
     * @return string
     */
    public function getLabelText() {
        $levelOptions = LeafLevelAttributeLabels::getLabels();
        return isset($levelOptions[$this->label_id_fk]) ?
            $levelOptions[$this->label_id_fk] : "unknown level ({$this->label_id_fk})";
    }

    /**
     * Get Units
     * @return mixed
     */
    public static function getUnits() {
        $unitOptions = CHtml::listData(Unit::getUnits(),'id','name');
        return $unitOptions;
    }

    /**
     * Get Unit Text
     * @return string
     */
    public function getUnitText() {
        $levelOptions = LeavesLabels::getUnits();
        return isset($levelOptions[$this->unit_id]) ?
            $levelOptions[$this->unit_id] : "not assigned";
    }
}
