<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Randika
 * Date: 6/10/14
 * Time: 2:18 PM
 * To change this template use File | Settings | File Templates.
 */

class OrderForm extends CFormModel
{
    public $fromdate;
    public $todate;
    public $rpttype;
    public $distributor;
    public $rcm;

    const STATUS_NOACTIVE=0;
    const STATUS_ACTIVE=1;
    const STATUS_BANNED=-1;

    //TODO: Delete for next version (backward compatibility)
    const STATUS_BANED=-1;

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            // name, email, subject and body are required
            array('fromdate, rpttype', 'required'),
            array('rpttype, distributor', 'numerical'),
            array('todate', 'safe'),
        );
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels()
    {
        return array(
            'fromdate'=>'From Date',
            'todate'=>'To Date',
            'rpttype'=>'Report Type',
            'distributor'=>'Distributor',
            'rcm'=>'Regional Customer Manager',
        );
    }

    public static function itemAlias($type,$code=NULL) {
        $_items = array(
            'ItemStatus' => array(
                self::STATUS_NOACTIVE => 'Not active',
                self::STATUS_ACTIVE => 'Active',
                self::STATUS_BANNED => 'Banned',
            ),
            'Type' => array(
                '1' =>'View Order',
                '2' =>'Dealer Comments',
                '3' =>'Dealers',
                '4' =>'Itinerary Achievement',
            ),
        );
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }
}