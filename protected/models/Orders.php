<?php

/**
 * This is the model class for table "orders".
 *
 * The followings are the available columns in table 'orders':
 * @property integer $id
 * @property string $deliveryDate
 * @property string $orderPlacedTime
 * @property string $orderType
 * @property string $dealerId
 * @property double $orderTotalValue
 * @property string $paymentType
 * @property string $srCode
 * @property double $latitude
 * @property double $longitude
 * @property string $disabled_on
 * @property integer $disabled
 * @property integer $disabled_by
 */
class Orders extends CActiveRecord {

    public $distributor;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'orders';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('deliveryDate, orderPlacedTime, orderType, dealerId, orderTotalValue, paymentType', 'required'),
            array('disabled, disabled_by', 'numerical', 'integerOnly' => true),
            array('orderTotalValue, latitude, longitude,distributor_id', 'numerical'),
            array('orderType,distributor', 'length', 'max' => 10),
            array('dealerId, srCode', 'length', 'max' => 50),
            array('paymentType', 'length', 'max' => 25),
            array('disabled_on,orderComment,orderInitTime, distributor_id', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, deliveryDate, orderPlacedTime, orderType, dealerId, orderTotalValue, orderInitTime, paymentType, srCode, latitude, longitude, disabled_on, disabled, disabled_by, distributor, distributor_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'dealers' => array(self::BELONGS_TO, 'Dealers', 'dealerId'),
            'salesrefs' => array(self::BELONGS_TO, 'Salesreps', 'srCode'),
            'distribut' => array(self::MANY_MANY, 'Distributors', 'dealers(id,distributor)'),
            'dist' => array(self::BELONGS_TO, 'Distributors', 'distributor_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'deliveryDate' => 'Delivery Date',
            'orderPlacedTime' => 'Order Placed Time',
            'orderType' => 'Order Type',
            'dealerId' => 'Dealer',
            'orderTotalValue' => 'Order Total',
            'paymentType' => 'Payment Type',
            'srCode' => 'Sr Code',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'disabled_on' => 'Disabled On',
            'disabled' => 'Disabled',
            'disabled_by' => 'Disabled By',
            'distributor' => 'Distributor',
            'orderInitTime' => 'Order Initiated Time',
            'orderComment' => 'Order Comments',
            'distributor_id' => 'Distributor'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->with = array(
            'dealers',
                //'statiRichiestes.idStatiPerLabel'
        );
        /* $criteria->alias = array(
          self::tableName()=>'o',
          'dealers'=>'d',
          //'statiRichiestes.idStatiPerLabel'=>'s'
          ); */
        //$criteria->together = true;
        $criteria->select = array(
            "orders.*",
        );

        /* $criteria->condition = array(
          "dealers.distributor = ':username'",
          "distributor.id = orders.dealerId",
          //"sr.idstati = s.dealerId"
          );
          $criteria->params = array(
          ':username'=>70591,//Yii::app()->user->getState('username')
          ); */
        //$criteria->group =  "orders.id";


        if (Yii::app()->getModule('user')->isAdmin() == false) {
            //$criteria->compare('dealers.distributor',Yii::app()->user->name);
            $dist = User::getDistributor();
            $dist = str_replace(',', '","', $dist);

            //$criteria->compare('dealers.distributor',Yii::app()->user->name);
            $criteria->condition = " dealers.distributor IN(\"{$dist}\")";
        }
        if (Yii::app()->getModule('user')->isAdmin() && $this->distributor != '') {
            $criteria->compare('dealers.distributor', $this->distributor);
        }
        $criteria->compare('orders.id', $this->id);
        $criteria->compare('deliveryDate', $this->deliveryDate, true);
        $criteria->compare('orderPlacedTime', $this->orderPlacedTime, true);
        $criteria->compare('orderType', $this->orderType, true);
        $criteria->compare('dealerId', $this->dealerId, true);
        //$criteria->compare('dealerId','SELECT id FROM `dealers` WHERE `dealers`.`distributor`="70591"',true);
        $criteria->compare('orderTotalValue', $this->orderTotalValue);
        $criteria->compare('paymentType', $this->paymentType, true);
        $criteria->compare('t.srCode', $this->srCode, true);
        $criteria->compare('orderInitTime', $this->orderInitTime);
        //$criteria->compare('latitude',$this->latitude);
        //$criteria->compare('longitude',$this->longitude);
        //$criteria->compare('disabled_on',$this->disabled_on,true);
        //$criteria->compare('disabled',$this->disabled);
        //$criteria->compare('disabled_by',$this->disabled_by);
        $criteria->compare('distributor_id', $this->distributor_id);
        $criteria->order = 't.id desc';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
        /* $dbC = Yii::app()->db->createCommand();
          //uncomment next line if you want to acces data in Object manner: $row->nome_stati
          //$dbC->setFetchMode(PDO::FETCH_OBJ);//fetch each row AS Object

          $dbC->select(array('o.*'))
          ->from(array('dealers d', 'orders o'))
          ->where(array(
          'and',
          'd.distributor = :distributor',
          'o.dealerId = d.id',

          ))
          ->group('o.id');
          $dbC->params[':distributor'] = 70591;//Yii::app()->user->getState('username');

          return $dbC; */
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Orders the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function owner() {
        $this->getDbCriteria()->mergeWith(array(
            'join' => ', dealers d',
            'condition' => "d.id = dealerId AND d.distributor='" . Yii::app()->user->name . "'",
        ));
        return $this;
    }

    public static function getOrderItems($id) {
        $data = array();
        $connection = Yii::app()->db;
        $sql = "SELECT IFNULL((SELECT C.`category` FROM `categories` AS C WHERE C.id = P.`category_id`),P.`description`) AS `name`, P.`id`,
            P.`price`, OI.`quantity`,  OI.`discount1`,
                   OI.`discount2`, ROUND(OI.`lineTotal`,2) AS `lineTotal`, (((P.`price`/100)*(100-OI.`discount1`))*OI.`quantity`) AS `linetot`
            FROM `orderitems` AS OI
                 INNER JOIN `products` AS P ON P.`id` = OI.`productId`
            WHERE OI.`orderId` = '{$id}'";


        $command = $connection->createCommand($sql);
        return $data = $command->queryAll();
    }
    
    
    public static function getStock($id) {
       
        
//      return Yii::app()->db->createCommand()
//    ->select('SUM(quantity) as stockQty')
//    ->from('inventory INS')
//    ->join('inventory_items INSItems', 'INS.id = INSItems.inventory_id')
////    ->where('products_id=:products_id and INS.distId =:distId', array(':products_id'=>$id,':distId'=>Yii::app()->user->name))
//    ->group('INSItems.products_id')           
//    ->queryRow();
//       
////   return $result;
       
       
        
        
        $data = array();
        $connection = Yii::app()->db;
        Yii::app()->user->name;
        $sql = " select sum(quantity)AS `stockQty`
            FROM `inventory_items` as INSItems
            INNER JOIN inventory AS INS ON INS.id = INSItems.inventory_id

            WHERE products_id = '{$id}' and INS.distId =  group by products_id ";


        $command = $connection->createCommand($sql);
        return $data = $command->queryAll();
    }
    
    

    public static function getInvoiceItems($id) {
        $data = array();
        $connection = Yii::app()->db;
              
            $sql = "SELECT 
            P.description,
            P.`id`,
            P.`price`,
            P.`name`,
            OI.`discount1`,
            P.unit,
            P.uniqueId,
            sum(insItems.quantity) AS invocedQty,
            OI.`discount2`,
            ROUND(OI.`lineTotal`,2) AS `lineTotal`,
            OI.quantity AS orderQty,
            OI.unitPrice
            FROM `orderitems` AS OI
            INNER JOIN `products` AS P ON P.`id` = OI.`productId`
            INNER JOIN `orders` AS O ON O.`id` = OI.`orderId`
            LEFT JOIN `invoice` AS ins ON ins.`orders_id` = O.`id` AND ins.status_id =1
            LEFT JOIN `invoice_items` AS insItems ON insItems.`products_id` = P.`id` and ins.`id` = insItems.`invoice_id`
            WHERE OI.`orderId` = {$id} 

            GROUP BY P.id";
            
//          WHERE OI.`orderId` = {$id} and ins.distributor_id = '".Yii::app()->user->name."'
        
//        $sql = "SELECT IFNULL((SELECT C.`category` FROM `categories` AS C WHERE C.id = P.`category_id`),P.`description`) AS `name`,"
//                . "P.description, P.`id`, P.`price`, OI.`discount1`,P.unit,P.uniqueId,insItems.quantity as invocedQty, OI.`discount2`,"
//                . " ROUND(OI.`lineTotal`,2) AS `lineTotal`,intItems.quantity as intQty, (intItems.quantity - insItems.quantity )as stockQty,"
//                . " (OI.`quantity` - insItems.quantity )as quantity, sum(intItems.quantity) as totStockQty, "
//                . "OI.quantity as orderQty "
//                . "FROM `orderitems` AS OI "
//                . "RIGHT JOIN `products` AS P ON P.`id` = OI.`productId` "
//                . "LEFT JOIN `inventory_items` AS intItems ON intItems.`products_id` = P.`id` "
//                . "LEFT JOIN `invoice_items` AS insItems ON insItems.`products_id` = P.`id` "
//                . "WHERE OI.`orderId` = {$id} group BY P.id, OI.id ";
                
//                echo $sql;

        $command = $connection->createCommand($sql);
        return $data = $command->queryAll();
    }

//    public static function getInvoiceItems2($id) {
//        $data = array();
//        $connection = Yii::app()->db;
//        $sql = "SELECT IFNULL((SELECT C.`category` FROM `categories` AS C WHERE C.id = P.`category_id`),P.`description`) AS `name`,P.description, P.`id`,
//                P.`price`, OI.`quantity`,  OI.`discount1`,P.unit,P.uniqueId,
//                OI.`discount2`, ROUND(OI.`lineTotal`,2) AS `lineTotal`, intItems.quantity as stockQty, sum(intItems.quantity) as totStockQty
//                    FROM `orderitems` AS OI
//                    RIGHT JOIN `products` AS P ON P.`id` = OI.`productId`
//                    LEFT JOIN `inventory_items` AS intItems ON intItems.`products_id` = P.`id`
//
//            WHERE OI.`orderId` = {$id} group BY P.id, OI.id"; 
//
//        $command = $connection->createCommand($sql);
//        return $data = $command->queryAll();
//
//    }

    /**
     * Get no of orders taken by sales rep
     *
     * @param string $sr_code
     * @param date $from_date
     * @param date $to_date
     * @return int
     */
    public static function getOrderBySR($sr_code, $from_date, $to_date) {
        $sql = "SELECT COUNT(0) AS `ords` FROM `orders` AS O WHERE O.`srCode` = '{$sr_code}' AND O.`orderPlacedTime` BETWEEN '{$from_date} 00:00:00' AND '{$to_date} 23:59:59'";

        $records = Tk::sql($sql);

        if (sizeof($records)) {
            return $records[0]['ords'];
        } else {
            return 0;
        }
    }

}
