<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 6/11/15
 * Time: 11:07 AM
 */

class PriceUpdate extends CFormModel
{
    public $prdsrc;

    const STATUS_NOACTIVE=0;
    const STATUS_ACTIVE=1;
    const STATUS_BANNED=-1;

    public static $products_sql = "products.zip";
    public static $products_src = "products_src.zip";
    public static $admin_mail = array('Margaret'=>'cism@anton.lk');

    //TODO: Delete for next version (backward compatibility)
    const STATUS_BANED=-1;

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            // name, email, subject and body are required
            array('prdsrc', 'file', 'types'=>'txt','allowEmpty' => false),
            //array('prdsrc', 'safe'),
        );
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels()
    {
        return array(
            'prdsrc'=>'Product Price Source',
        );
    }

    public static function itemAlias($type,$code=NULL) {
        $_items = array(
            'ItemStatus' => array(
                self::STATUS_NOACTIVE => 'Not active',
                self::STATUS_ACTIVE => 'Active',
                self::STATUS_BANNED => 'Banned',
            ),
            'Type' => array(
                '1' =>'View Order',
                '2' =>'Dealer Comments',
                '3' =>'Dealers',
                '4' =>'Itinerary Achievement',
            ),
        );
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

    public static function updateProducts()
    {
        Yii::app()->appLog->isConsole = true;
        Yii::app()->appLog->username = 'STAT_DAEMON';
        Yii::app()->appLog->logType = 2;
        $default_prd_main_cat = 2;
        $default_prd_sub_cat = 5;
        $default_prd_cat = 182;


        Yii::app()->appLog->writeLog('Process started');

        ini_set('memory_limit', '2048M');

        $prd_src = SYS_TMP_PATH.Yii::app()->params->prdsrc;
        $prd_src_zip = SYS_TMP_PATH.Yii::app()->params->prdbackup;

        if(file_exists($prd_src))
        {
            $data = file($prd_src);
            $curdb  = explode('=', Yii::app()->db->connectionString);
            $db_name = $curdb[2];

            //$data = array_shift($file);
            unset($data[0]);
            //Tk::a($data);
            $rows = $inserted_rows = $updated_rows = 0;
            if(sizeof($data))
            {
                $insert = "INSERT INTO `products`(`id`, `uniqueId`, `description`, `unit`, `price`, `discount1`, `main_cat_fk_id`, `sub_prod_cat_fk_id`,
                          `category_id`, `label1Value`, `label1Text`, `label2Value`, `status`, `lastUpdatedOn`)  VALUES";
                $insert_value = '';
                // Backup products table
                self::backups_tables('products');

                foreach($data as $data_row)
                {
                    $d_row = explode(';', $data_row);

                    $sql = "SELECT * FROM `products` AS P WHERE P.`uniqueId` = '{$d_row[0]}'";
                    $product = Tk::sql($sql);
                    if(sizeof($product))
                    {
                        $sql = "UPDATE `products` SET `products`.`discount1` = ROUND((({$d_row[7]}-{$d_row[5]})/{$d_row[7]})*100), `products`.`price` = '{$d_row[7]}'
                                     WHERE `products`.`uniqueId` = '{$d_row[0]}'; ";
                        //$rows += Tk::execute($sql);
                        $updated_rows += $rows;
                        Yii::app()->appLog->writeLog($rows.' no of rows affected. The executed sql was: '.$sql);
                    } else {
                        $temp = "(NULL,'{$d_row[0]}','{$d_row[3]}','{$d_row[2]}','{$d_row[7]}',
                        ROUND((({$d_row[7]}-{$d_row[5]})/{$d_row[7]})*100),'{$default_prd_main_cat}','{$default_prd_sub_cat}','{$default_prd_cat}',
                        'AUTOMATICALLY INSERTED NEW PRODUCT','AUTOMATICALLY INSERTED NEW PRODUCT - text','AUTOMATICALLY INSERTED NEW PRODUCT - 2','".Products::STATUS_ACTIVE."',NOW())";
                        $insert_value .= (strlen($insert_value)) ? ','.$temp : $temp;
                    }
                }
                if(strlen($insert_value))
                {
                    //$rows = Tk::insert($insert.$insert_value);
                    $inserted_rows += $rows;
                    Yii::app()->appLog->writeLog($rows.' no of rows affected. The executed sql was: '.$insert.$insert_value);
                }

                // Backup Product Sourc file
                $zip = new ZipArchive();
                $filename = $prd_src_zip.date('Ymd_His_').self::$products_src;

                $attachements[] = $filename;
                $attachements[] = $prd_src_zip.date('Ymd_His_').self::$products_sql;

                $zip->open($filename, ZipArchive::CREATE);
                $zip->addFromString("products_src.txt", file_get_contents($prd_src));

                $zip->close();

                // Mail notification message
                $messages = "Dear Admin,<br />\nThe Price Updater triggered and following is the summary of the process.\n\nUpdated Products:{$updated_rows} <br />\nNew Products:{$inserted_rows} <br />\n
                    Product source and backup of the products table before update was attached.<br /><br />\n\nSales Force Automatic Mailer";

                // Send email
                self::sendMail($messages, $attachements);
                Yii::app()->appLog->writeLog('Sent notification mail to '.implode(' => ', self::$admin_mail));
                //unlink($prd_src);
                Yii::app()->appLog->writeLog('Cleaned source file '.$prd_src);
            } else {
                Yii::app()->appLog->writeLog('No data found', WARNING);
            }
        } else {
            Yii::app()->appLog->writeLog('No data source file found', CRITICAL);
        }


        Yii::app()->appLog->writeLog('Process over');
    }



    public static function backups_tables($tables = '*')
    {
        $curdb  = explode('=', Yii::app()->db->connectionString);
        $db_name = $curdb[2];
        $user = Yii::app()->db->username;
        $pass = Yii::app()->db->password;
        $host = 'localhost';
        $path = SYS_TMP_PATH.Yii::app()->params->prdbackup;
        $time = date('Y-m-d H:i:s');

        $return = "-- Run Backup at {$time}\n\nUSE `{$db_name}`;\nSET FOREIGN_KEY_CHECKS=0;\n";
        $link = mysql_connect($host,$user,$pass);
        mysql_select_db($db_name,$link);
        //get all of the tables
        if($tables == '*')
        {
            $tables = array();
            //$result = mysql_query('SHOW TABLES');
            $result = Tk::sql('SHOW TABLES');
            foreach($result as $res)
            {
                $tables[] = $res['Tables_in_antonsf_test'];
            }
        }
        else
        {
            $tables = is_array($tables) ? $tables : explode(',',$tables);
        }

        //cycle through
        foreach($tables as $table)
        {
            $result = mysql_query('SELECT * FROM `'.$table.'`');
            $num_fields = mysql_num_fields($result);
            $return.= 'DROP TABLE IF EXISTS `'.$table.'`;';
            $row2 = mysql_fetch_row(mysql_query('SHOW CREATE TABLE '.$table));
            $return.= "\n\n".$row2[1].";\n\n";
            for ($i = 0; $i < $num_fields; $i++)
            {
                while($row = mysql_fetch_row($result))
                {
                    $return.= 'INSERT INTO `'.$table.'` VALUES(';
                    for($j=0; $j<$num_fields; $j++)
                    {
                        $row[$j] = addslashes($row[$j]);
                        $row[$j] = ereg_replace("\n","\\n",$row[$j]);
                        if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
                        if ($j<($num_fields-1)) { $return.= ','; }
                    }
                    $return.= ");\n";
                }
            }
            $return.="\n\n";
        }
        $return .= '-- Backup end time '.date('Y-m-d H:i:s');
        //save file
        $sql_file = 'db-backup-'.time().'-'.(md5(implode(',',$tables))).'.sql';
        $handle = fopen($path.$sql_file,'w+');

        fwrite($handle,$return);
        fclose($handle);

        // Zip the sql file
        $zip = new ZipArchive();
        $filename = $path.date('Ymd_His_').self::$products_sql;
        $zip->open($filename, ZipArchive::CREATE);

        $zip->addFromString("produsct.sql", file_get_contents($path.$sql_file));

        $zip->close();

        unlink($path.$sql_file);
    }

    public function sendMail($message, $attachments = array())
    {
        $emailSentStatus = false;

        $subject = 'Price Update ';
        $templatePath = Yii::getPathOfAlias('application.views.emailTemplates.'). DIRECTORY_SEPARATOR .'emailNotificationTemplate.php';

        if(file_exists($templatePath))
        {
            $mail_template = file_get_contents($templatePath);
            $mail_template = str_replace('[COPY_RIGHTS]', Yii::app()->params['copyRight'], $mail_template);
            $mail_template = str_replace('[LOGO]', CHtml::image(Yii::app()->getBaseUrl(true).'/images/logo_small.png'), $mail_template);
        }

        $mail_template = str_replace('[APP_NAME]', Yii::app()->name, $mail_template);
        if(strlen($message))
        {
            $to_send = '';
            $email = self::$admin_mail;//array('anura'=>'anura@saksglobal.com');
                //mail('anurasn@gmail.com','test from php','Test body by app');
            $mail_body = $message;
            $body = str_replace('[CONTENT]', $mail_body, $mail_template);

            $emailSentStatus = Yii::app()->tk->sendEmail(
                array(
                    'name'=>Yii::app()->params['smtp']['defaultFromName'],
                    'email'=>Yii::app()->params['smtp']['defaultFromEmail']
                ),
                $email,
                $subject.' - '.Yii::app()->name,
                $body,
                $attachments
            );
            $to_send = implode('', $email);//dca3e02d6c9e1178f6ad02e8b8ff54f0
            if ($emailSentStatus) {

                Yii::app()->appLog->writeLog('Successfully send Email notification to '.$to_send, WARNING);
            } else {

                Yii::app()->appLog->writeLog('Failure to send Email notification for '.$to_send, WARNING);
            }

        }
        return $emailSentStatus;
    }

    /**
     * File Shorter
     * @param string $dir
     * @return array|bool
     */
    public static function fileShorter($dir)
    {
        $ignored = array('.', '..', '.svn', '.htaccess','.DS_Store');

        $files = array();
        foreach (scandir($dir) as $file) {
            if (in_array($file, $ignored)) continue;
            $files[$file] = filemtime($dir . '/' . $file);
        }

        arsort($files);
        $files = array_keys($files);

        return ($files) ? $files : false;
    }
}