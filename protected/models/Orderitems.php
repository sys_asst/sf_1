<?php

/**
 * This is the model class for table "orderitems".
 *
 * The followings are the available columns in table 'orderitems':
 * @property integer $id
 * @property integer $productId
 * @property double $discount1
 * @property double $discount2
 * @property double $quantity
 * @property double $unitPrice
 * @property double $lineTotal
 * @property integer $orderId
 */
class Orderitems extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'orderitems';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('productId, quantity,  lineTotal, orderId', 'required'),
			array('productId, orderId', 'numerical', 'integerOnly'=>true),
			array('discount1, discount2, quantity, unitPrice, lineTotal', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, productId, discount1, discount2, quantity, unitPrice, lineTotal, orderId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'productId' => 'Product',
			'discount1' => 'Discount1',
			'discount2' => 'Discount2',
			'quantity' => 'Quantity',
			'unitPrice' => 'Unit Price',
			'lineTotal' => 'Line Total',
			'orderId' => 'Order',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('productId',$this->productId);
		$criteria->compare('discount1',$this->discount1);
		$criteria->compare('discount2',$this->discount2);
		$criteria->compare('quantity',$this->quantity);
		$criteria->compare('unitPrice',$this->unitPrice);
		$criteria->compare('lineTotal',$this->lineTotal);
		$criteria->compare('orderId',$this->orderId);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Orderitems the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
