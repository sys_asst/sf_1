<?php

/**
 * This is the model class for table "invoice".
 *
 * The followings are the available columns in table 'invoice':
 * @property integer $id
 * @property integer $distributor_id
 * @property integer $orders_id
 * @property string $invoice_no
 * @property integer $status_id
 * @property integer $credit_days
 * @property string $due_date
 * @property string $createdtime
 * @property integer $createdby
 * @property string $updatedtime
 * @property integer $updatedby
 */
class Invoice extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'invoice';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('distributor_id, orders_id, status_id, credit_days, createdby, updatedby', 'numerical', 'integerOnly'=>true),
			array('invoice_no', 'length', 'max'=>50),
			array('due_date, createdtime, updatedtime', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, distributor_id, orders_id, invoice_no, status_id, credit_days, due_date, createdtime, createdby, updatedtime, updatedby', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'distributor_id' => 'Distributor',
			'orders_id' => 'Orders',
			'invoice_no' => 'Invoice No',
			'status_id' => 'Status',
			'credit_days' => 'Credit Days',
			'due_date' => 'Due Date',
			'createdtime' => 'Createdtime',
			'createdby' => 'Createdby',
			'updatedtime' => 'Updatedtime',
			'updatedby' => 'Updatedby',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('distributor_id',$this->distributor_id);
		$criteria->compare('orders_id',$this->orders_id);
		$criteria->compare('invoice_no',$this->invoice_no,true);
		$criteria->compare('status_id',$this->status_id);
		$criteria->compare('credit_days',$this->credit_days);
		$criteria->compare('due_date',$this->due_date,true);
		$criteria->compare('createdtime',$this->createdtime,true);
		$criteria->compare('createdby',$this->createdby);
		$criteria->compare('updatedtime',$this->updatedtime,true);
		$criteria->compare('updatedby',$this->updatedby);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Invoice the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
