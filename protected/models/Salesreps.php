<?php

/**
 * This is the model class for table "salesreps".
 *
 * The followings are the available columns in table 'salesreps':
 * @property integer $id
 * @property string $srEmpNo
 * @property string $srName
 * @property string $srDesignation
 * @property string $srJoinedDate
 * @property string $srAddress
 * @property string $srTel
 * @property string $srEmail
 * @property string $srResignDate
 * @property string $passCode
 * @property string $distributor
 * @property integer $status
 * @property integer $loginCreated
 * @property string $addedOn
 * @property string $lastUpdatedOn
 * @property integer $user_id
 * @property integer $disabled
 * @property string $disabled_date
 * @property integer $disabled_by
 * @property integer $login_created_by
 * @property string $login_created_on
 *
 * The followings are the available model relations:
 * @property Checkedin[] $checkedins
 * @property MsRegions[] $msRegions
 * @property MsRouteMap[] $msRouteMaps
 * @property Orders[] $orders
 * @property MsDesignations $designation
 * @property Users $loginCreatedBy
 * @property Users $user
 * @property MsArea $area
 * @property MsRegions $region
 */
class Salesreps extends CActiveRecord
{
    const STATUS_NOACTIVE=0;
    const STATUS_ACTIVE=1;
    const STATUS_BANNED=-1;

    //TODO: Delete for next version (backward compatibility)
    const STATUS_BANED=-1;

    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'salesreps';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('srEmpNo, srName', 'required'),
			array('region_id, area_id, designation_id, status, loginCreated, user_id, disabled, disabled_by, login_created_by', 'numerical', 'integerOnly'=>true),
			array('srEmpNo, distributor', 'length', 'max'=>50),
			array('srName', 'length', 'max'=>255),
			array('srDesignation, srEmail', 'length', 'max'=>100),
			array('srTel', 'length', 'max'=>25),
			array('passCode', 'length', 'max'=>4),
			array('srJoinedDate, srAddress, srResignDate, addedOn, disabled_date, login_created_on', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, region_id, area_id, srEmpNo, srName, srDesignation, designation_id, srJoinedDate, srAddress, srTel, srEmail, srResignDate, passCode, distributor, status, loginCreated, addedOn, lastUpdatedOn, user_id, disabled, disabled_date, disabled_by, login_created_by, login_created_on', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'checkedins' => array(self::HAS_MANY, 'Checkedin', 'srCode'),
            'msRegions' => array(self::HAS_MANY, 'Regions', 'rcm_id'),
            'msRouteMaps' => array(self::HAS_MANY, 'RouteMap', 'rep_id'),
            'orders' => array(self::HAS_MANY, 'Orders', 'srCode'),
            'designation' => array(self::BELONGS_TO, 'Designations', 'designation_id'),
            'loginCreatedBy' => array(self::BELONGS_TO, 'Users', 'login_created_by'),
            'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
            'area' => array(self::BELONGS_TO, 'Area', 'area_id'),
            'region' => array(self::BELONGS_TO, 'Regions', 'region_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'srEmpNo' => 'Emp No',
			'srName' => 'Name',
			'srDesignation' => 'Designation',
			'srJoinedDate' => 'Joined Date',
			'srAddress' => 'Address',
			'srTel' => 'Tel',
			'srEmail' => 'Email',
			'srResignDate' => 'Resign Date',
			'passCode' => 'Pass Code',
			'distributor' => 'Distributor',
			'status' => 'Status',
			'loginCreated' => 'Login Created',
			'addedOn' => 'Added On',
			'lastUpdatedOn' => 'Last Updated On',
			'user_id' => 'User',
			'disabled' => 'Disabled',
			'disabled_date' => 'Disabled Date',
			'disabled_by' => 'Disabled By',
			'login_created_by' => 'Login Created By',
			'login_created_on' => 'Login Created On',
            'region_id' => 'Region',
            'area_id' => 'Area',
            'designation_id' => 'Designation',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
        (strlen($this->status) < 1) ? $this->status = Salesreps::STATUS_ACTIVE : '';
        
//Tk::a($criteria);
        $criteria->condition= 't.status > '.self::STATUS_BANED;
        $criteria->compare('id',$this->id);
        $criteria->compare('region_id',$this->region_id);
        $criteria->compare('area_id',$this->area_id);
        $criteria->compare('srEmpNo',$this->srEmpNo,true);
        $criteria->compare('srName',$this->srName,true);
        $criteria->compare('srDesignation',$this->srDesignation,true);
        $criteria->compare('designation_id',$this->designation_id);
        $criteria->compare('srJoinedDate',$this->srJoinedDate,true);
        $criteria->compare('srAddress',$this->srAddress,true);
        $criteria->compare('srTel',$this->srTel,true);
        $criteria->compare('srEmail',$this->srEmail,true);
        $criteria->compare('srResignDate',$this->srResignDate,true);
        $criteria->compare('passCode',$this->passCode,true);
        $criteria->compare('distributor',$this->distributor,true);
        $criteria->compare('status',$this->status);
        $criteria->compare('loginCreated',$this->loginCreated);
        $criteria->compare('addedOn',$this->addedOn,true);
        $criteria->compare('lastUpdatedOn',$this->lastUpdatedOn,true);
        $criteria->compare('user_id',$this->user_id);
        $criteria->compare('disabled',$this->disabled);
        $criteria->compare('disabled_date',$this->disabled_date,true);
        $criteria->compare('disabled_by',$this->disabled_by);
        $criteria->compare('login_created_by',$this->login_created_by);
        $criteria->compare('login_created_on',$this->login_created_on,true);

        if(Yii::app()->user->profile->src == 2)
        {
            $criteria->condition= 't.area_id IN(SELECT IFNULL(DR.`area_id`,0) FROM `ms_regions` AS R INNER JOIN `d_regions` AS DR ON DR.`region_id` = R.`id` AND R.`rcm_id` 
                IN(SELECT S.`id` FROM `salesreps` AS S WHERE S.`srEmail` IN ("'.Yii::app()->user->src->srEmail.'")))';
        }

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Salesreps the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public static function getSalesRefs() {
        $categoryArray = CHtml::listData(Salesreps::model()->findAll(array('condition'=>'disabled = 0 ','order'=>'srName')), 'srEmpNo', 'srName');
        return $categoryArray;
    }

    /**
     * Returns record status of particular record
     * @param $type
     * @param null $code
     * @return bool
     */
    public static function itemAlias($type,$code=NULL) {
        $_items = array(
            'ItemStatus' => array(
                self::STATUS_NOACTIVE => 'Not active',
                self::STATUS_ACTIVE => 'Active',
                self::STATUS_BANNED => 'Banned',
            ),
            'Level' => array(
                '1' =>'Segment',
                '2' =>'Category',
                '3' =>'Web Category'
            ),
        );
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }
}
