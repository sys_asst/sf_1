<?php

/**
 * This is the model class for table "distributors".
 *
 * The followings are the available columns in table 'distributors':
 * @property integer $id
 * @property string $distId
 * @property string $distName
 * @property string $distEmail
 * @property string $distAddress1
 * @property string $distAddress2
 * @property string $distAddress3
 * @property string $distType
 * @property string $distPhone
 * @property integer $status
 * @property integer $loginCreated
 * @property string $addedOn
 * @property string $lastUpdatedOn
 * @property integer $disabled
 * @property integer $disabled_by
 * @property integer $disabled_date
 * @property integer $user_id
 * @property integer $login_created_by
 * @property string $login_created_on
 *
 * The followings are the available model relations:
 * @property Users $loginCreatedBy
 * @property Areas $area
 * @property MsArea[] $msAreas
 * @property Users $user
 */
class Distributors extends CActiveRecord
{
    
    const STATUS_NOACTIVE=0;
    const STATUS_ACTIVE=1;
    const STATUS_BANNED=-1;

    //TODO: Delete for next version (backward compatibility)
    const STATUS_BANED=-1;
	
/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'distributors';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('distId, distName', 'required'),
			array('area_id, status, loginCreated, disabled, disabled_by, disabled_date, user_id, login_created_by', 'numerical', 'integerOnly'=>true),
			array('distId', 'length', 'max'=>25),
			array('distName, distEmail', 'length', 'max'=>100),
			array('distType, distPhone', 'length', 'max'=>50),
			array('distAddress1, distAddress2, distAddress3, addedOn, login_created_on', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, area_id, distId, distName, distEmail, distAddress1, distAddress2, distAddress3, distType, distPhone, status, loginCreated, addedOn, lastUpdatedOn, disabled, disabled_by, disabled_date, user_id, login_created_by, login_created_on', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            //'dealers' => array(self::HAS_MANY, 'Dealers', 'distId'),
            //'area' => array(self::BELONGS_TO, 'Area', 'area_id'),
            'dealers' => array(self::HAS_MANY, 'Dealers', 'distId'),
            'loginCreatedBy' => array(self::BELONGS_TO, 'Users', 'login_created_by'),
            'area' => array(self::BELONGS_TO, 'Area', 'area_id'),
            'msAreas' => array(self::HAS_MANY, 'Area', 'distributor_id'),
            'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'distId' => 'Distributor Id',
			'distName' => 'Dist Name',
			'distEmail' => 'Dist Email',
			'distAddress1' => 'Dist Address1',
			'distAddress2' => 'Dist Address2',
			'distAddress3' => 'Dist Address3',
			'distType' => 'Dist Type',
			'distPhone' => 'Dist Phone',
			'status' => 'Status',
			'loginCreated' => 'Login Created',
			'addedOn' => 'Added On',
			'lastUpdatedOn' => 'Last Updated On',
			'disabled' => 'Disabled',
			'disabled_by' => 'Disabled By',
			'disabled_date' => 'Disabled Date',
			'user_id' => 'User',
			'login_created_by' => 'Login Created By',
			'login_created_on' => 'Login Created On',
                        'aria_id' => 'Area',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
                (strlen($this->status) < 1) ? $this->status = Distributors::STATUS_ACTIVE : '';
		

		$criteria->compare('id',$this->id);
		$criteria->compare('distId',$this->distId,true);
		$criteria->compare('distName',$this->distName,true);
		$criteria->compare('distEmail',$this->distEmail,true);
		$criteria->compare('distAddress1',$this->distAddress1,true);
		$criteria->compare('distAddress2',$this->distAddress2,true);
		$criteria->compare('distAddress3',$this->distAddress3,true);
		$criteria->compare('distType',$this->distType,true);
		$criteria->compare('distPhone',$this->distPhone,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('loginCreated',$this->loginCreated);
		$criteria->compare('addedOn',$this->addedOn,true);
		$criteria->compare('lastUpdatedOn',$this->lastUpdatedOn,true);
		$criteria->compare('disabled',$this->disabled);
		$criteria->compare('disabled_by',$this->disabled_by);
		$criteria->compare('disabled_date',$this->disabled_date);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('login_created_by',$this->login_created_by);
		$criteria->compare('login_created_on',$this->login_created_on,true);
                $criteria->compare('area_id',$this->login_created_on,true);
                
                if(Yii::app()->user->profile->src == 3)
        	{
                    //$criteria->compare('status',Records::STATUS_SUBMITED);
                    $criteria->condition= 't.distId = "'.Yii::app()->user->name.'"';
                }

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Distributors the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * Return distributor name from id or distId
     * @param $id
     * @param int $bydistId
     * @return string
     */
    public static function getDistributor($id, $bydistId = 0)
    {
        $connection = Yii::app()->db;
        $sql = "SELECT `distName` FROM `distributors` WHERE ".($bydistId ? "`distId` = '{$id}'" : "`id` = '{$id}'")." AND `disabled` = '0'";
        $command = $connection->createCommand($sql);
        $row = $command->queryAll();

        $sqlData = new CArrayDataProvider(array($row));
        $leaflevels = $sqlData->getData();
        //print_r($leaflevels);exit;
        return sizeof($leaflevels[0]) ? $leaflevels[0][0]['distName'] : 'n/a';
    }
   
   /**
     * Returns record status of particular record
     * @param $type
     * @param null $code
     * @return bool
     */
    public static function itemAlias($type,$code=NULL) {
        $_items = array(
            'ItemStatus' => array(
                self::STATUS_NOACTIVE => 'Not active',
                self::STATUS_ACTIVE => 'Active',
                self::STATUS_BANNED => 'Banned',
            ),
            'Type' => array(
                'distType' =>'DISTRIBU',
            ),
        );
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

    public static function getDistId($id)
    {
        $distId = 0;
        $sql = "SELECT D.`distId` FROM `distributors` AS D WHERE D.`id` = '{$id}'";
        $records = Tk::sql($sql);

        if(sizeof($distId))
        {
            $distId = $records[0]['distId'];
        }

        return $distId;
    }
}
