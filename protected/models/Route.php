<?php

/**
 * This is the model class for table "ms_route".
 *
 * The followings are the available columns in table 'ms_route':
 * @property integer $id
 * @property integer $area_id
 * @property string $code
 * @property string $name
 * @property integer $status
 * @property integer $added_by
 * @property string $added_on
 * @property integer $updated_by
 * @property string $updated_on
 *
 * The followings are the available model relations:
 * @property DRoute[] $dRoutes
 * @property Area $area
 * @property Users $addedBy
 * @property Users $updatedBy
 * @property RouteMap[] $msRouteMaps
 */
class Route extends CActiveRecord
{
    const STATUS_NOACTIVE=0;
    const STATUS_ACTIVE=1;
    const STATUS_BANNED=-1;

    //TODO: Delete for next version (backward compatibility)
    const STATUS_BANED=-1;

    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ms_route';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, area_id', 'required'),
			array('area_id, status, added_by, updated_by', 'numerical', 'integerOnly'=>true),
			array('code', 'length', 'max'=>10),
			array('name', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, area_id, code, name, status, added_by, added_on, updated_by, updated_on', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'dRoutes' => array(self::HAS_MANY, 'DRoute', 'route_id'),
			'area' => array(self::BELONGS_TO, 'Area', 'area_id'),
			'addedBy' => array(self::BELONGS_TO, 'Users', 'added_by'),
			'updatedBy' => array(self::BELONGS_TO, 'Users', 'updated_by'),
            'msRouteMaps' => array(self::HAS_MANY, 'RouteMap', 'route_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'area_id' => 'Area',
			'code' => 'Code',
			'name' => 'Name',
			'status' => 'Status',
			'added_by' => 'Added By',
			'added_on' => 'Added On',
			'updated_by' => 'Updated By',
			'updated_on' => 'Updated On',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
                (strlen($this->status) < 1) ? $this->status = Route::STATUS_ACTIVE : '';

		$criteria->compare('id',$this->id);
		$criteria->compare('area_id',$this->area_id);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('t.name',$this->name,true);
		$criteria->compare('t.status',$this->status);
		$criteria->compare('added_by',$this->added_by);
		$criteria->compare('added_on',$this->added_on,true);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('updated_on',$this->updated_on,true);

                $criteria->join= 'JOIN ms_area a ON (t.area_id=a.id)';

                $criteria->order = 'a.name, t.name';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Route the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * Returns record status of particular record
     * @param $type
     * @param null $code
     * @return bool
     */
    public static function itemAlias($type,$code=NULL) {
        $_items = array(
            'ItemStatus' => array(
                self::STATUS_ACTIVE => 'Active',
                self::STATUS_NOACTIVE => 'Not active',
                self::STATUS_BANNED => 'Banned',
            ),
            'UserDef' => array(
                '0' =>'Non - User Defined',
                '1' =>'User Defined'
            ),
            'weekdays'=>Route::getWeekdays()
        );
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

    public function getGNDivisions($id)
    {
        $values = '';
        $connection = Yii::app()->db;
        $sql = "SELECT A.`name` FROM `d_route` AS DR INNER JOIN `ms_dsdivisions` AS A ON A.`id` = DR.`gndiv_id` AND DR.`route_id` = '{$id}'";

        $command = $connection->createCommand($sql);
        $row = $command->queryAll();
        $values = '';
        if(sizeof($row))
        {
            foreach($row as $data)
            {
                $values .= (strlen($values)) ? ', '.$data['name'] : $data['name'] ;
            }
        }

        return strlen($values) ? $values : 'n/a';
    }

    public static function getWeekdays()
    {
        $leaflevels = array();
        $connection = Yii::app()->db;
        $sql = 'SELECT DAYNAME(O.`orderPlacedTime`) AS `day` FROM `orders` AS O GROUP BY WEEKDAY(O.`orderPlacedTime`)';

        $command = $connection->createCommand($sql);
        $row = $command->queryAll();

        if(sizeof($row))
        {
            foreach($row as $data)
            {
                $leaflevels[] = $data['day'] ;
            }
        }

        return $leaflevels;
    }

    /**
     * Get dealers against given route
     * @param int $route_id
     * @return mixed
     */
    public static function getDealers($route_id)
    {
        $sql = "SELECT D.`dealerName`, D.`dealerCode`, T.`town`
            FROM `dealers` AS D
                 INNER JOIN `towns` AS T ON T.`id` = D.`townId` AND T.`ds_divid` IN (SELECT
                 DR.`gndiv_id` FROM `d_route` AS DR WHERE DR.`route_id` IN({$route_id})) AND D.`disabled` = '".Dealers::STATUS_ACTIVE."'
            ORDER BY T.`town`, D.`dealerName`";

        $records = Tk::sql($sql);

        return $records;
    }
}
