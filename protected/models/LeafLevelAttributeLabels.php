<?php

/**
 * This is the model class for table "leaf_level_attribute_labels".
 *
 * The followings are the available columns in table 'leaf_level_attribute_labels':
 * @property integer $id
 * @property string $label
 * @property integer $unit_cat_id
 * @property integer $status
 * @property string $addedon
 * @property integer $added_by
 *
 * The followings are the available model relations:
 * @property Users $addedBy
 * @property MsUnitCategory $unitCat
 */
class LeafLevelAttributeLabels extends CActiveRecord
{
    const STATUS_NOACTIVE=0;
    const STATUS_ACTIVE=1;
    const STATUS_BANNED=-1;

    //TODO: Delete for next version (backward compatibility)
    const STATUS_BANED=-1;

    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'leaf_level_attribute_labels';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('label', 'required'),
			array('status,unit_cat_id', 'numerical', 'integerOnly'=>true),
			array('label', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, label, unit_cat_id, status, addedon, added_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            //'addedby' => array(self::BELONGS_TO, 'User', 'added_by'),
            'unitCat' => array(self::BELONGS_TO, 'UnitCategory', 'unit_cat_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'label' => 'Label',
			'status' => 'Status',
            'unit_cat_id' => 'Unit Category',
            'addedon' => 'Added on',
            'added_by' => 'Added By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('label',$this->label,true);
        $criteria->compare('unit_cat_id',$this->unit_cat_id);
        $criteria->compare('status',$this->status);
        $criteria->compare('addedon',$this->addedon,true);
        $criteria->compare('added_by',$this->added_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LeafLevelAttributeLabels the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * Get ststus and Types
     * @param $type
     * @param null $code
     * @return bool
     */
    public static function itemAlias($type,$code=NULL) {
        $_items = array(
            'ItemStatus' => array(
                self::STATUS_NOACTIVE => 'Not active',
                self::STATUS_ACTIVE => 'Active',
                self::STATUS_BANNED => 'Banned',
            ),
            'Type' => array(
                '1' =>'Type 1',
                '2' =>'Type 2'
            ),
        );
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

    /**
     * Get all labels
     * @return array
     */
    public static function getLabels() {
        $labelsOptions = CHtml::listData(LeafLevelAttributeLabels::model()->findAll('status = 1 '), 'id', 'label');
        return $labelsOptions;
    }

    /**
     * Get Level text
     * @return string
     */
    public function getLeafLevelText() {
        $levelOptions = LeafLevelAttributeLabels::getLabels();
        return isset($levelOptions[$this->id]) ?
            $levelOptions[$this->id] : "unknown item ({$this->id})";
    }

    /**
     * Get status text
     * @return string
     */
    public function getStatusText() {
        $levelOptions = LeafLevelAttributeLabels::itemAlias('ItemStatus');
        return isset($levelOptions[$this->status]) ?
            $levelOptions[$this->status] : "unknown item ({$this->status})";
    }
}
