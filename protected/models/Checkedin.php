<?php

/**
 * This is the model class for table "checkedin".
 *
 * The followings are the available columns in table 'checkedin':
 * @property integer $id
 * @property string $srcode
 * @property string $checkedInTime
 * @property integer $dealerId
 * @property string $latitude
 * @property string $longitude
 * @property string $addedon
 *
 * The followings are the available model relations:
 * @property Dealers $dealer
 * @property Salesreps $srcode0
 */
class Checkedin extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'checkedin';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('dealerId', 'numerical', 'integerOnly'=>true),
			array('srCode', 'length', 'max'=>5),
			array('latitude, longitude', 'length', 'max'=>20),
			array('checkedInTime, addedon', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, srcode, checkedInTime, dealerId, latitude, longitude, addedon', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'dealer' => array(self::BELONGS_TO, 'Dealers', 'dealerId'),
			'srcode0' => array(self::BELONGS_TO, 'Salesreps', 'srcode'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'srCode' => 'Srcode',
			'checkedInTime' => 'Checked In Time',
			'dealerId' => 'Dealer',
			'latitude' => 'Latitude',
			'longitude' => 'Longitude',
			'addedon' => 'Addedon',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('srcode',$this->srcode,true);
		$criteria->compare('checkedInTime',$this->checkedInTime,true);
		$criteria->compare('dealerId',$this->dealerId);
		$criteria->compare('latitude',$this->latitude,true);
		$criteria->compare('longitude',$this->longitude,true);
		$criteria->compare('addedon',$this->addedon,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Checkedin the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
