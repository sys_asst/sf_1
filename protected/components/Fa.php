<?php
/**
 * Fa class file, work with Fontawesome library to prepare various 
 * HTML contents
 */
class Fa extends CApplicationComponent
{
	/**
	 * Prepare fontawsome icon
	 * @param string $iconName Icon name
	 * @param string $message Any message to be apended to icon
	 * @param string $cssClass Any other class name to be append to i class
	 * @param integer $size Icon size 1,2..6
	 * @param string $spanClass Wrap icon with span class if you want to chant its color
	 * @return string HTML content
	 */	
	public static function getIcon($iconName, $message=null, $cssClass='', $size=1, $spanClass=null)
	{
		$icon  = "<i class='fa fa-{$iconName} fa-{$size}x {$cssClass}'></i>";
		if (null != $spanClass) {
			$icon = "<span class='{$spanClass}'>{$icon}</span>";
		}
		
		if (null != $message) {
			$icon .= " {$message}";
		}
		
		return $icon;
	}
}
?>