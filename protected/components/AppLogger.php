<?php

// Log levels
define('CRITICAL', 0);
define('WARNING', 1);
define('INFO', 2);
define('DEBUG', 3);

/**
 * AppLogger class. Which writes user activities
 */
class AppLogger
{	
	/**
	 * Log level labels for print in log
	 */
	private $logLevelLabel = array (
		CRITICAL => 'CRITICAL',
		WARNING => 'WARNING',
		INFO => 'INFO',
		DEBUG => 'DEBUG'
	);

	/**
	 * Log path
	 */
	private $logPath = './tmp/';
	
	/**
	 * Log Server socket
	 */
	private $logSocket = 'localhost:0';
	
	/**
	 * Log file name
	 */
	private $logName = '';
	
	/**
	 * Logged user name
	 */		
	public $username = '';						
	
	/**
	 * Log type 1 - Activity log
	 */	
	public $logType = 1; 					
	
	/**
	 * Log data
	 */	
	public $logParams = array();	
	
	/**
	 * Current log level
	 */		
	public $logLevel = DEBUG;
	
	/**
	 * Requester IP
	 */	
	public $ip;
	
	/**
	 * Page action
	 */	
	public $pageAction;
	
	/**
	 * Uniqid to track user session
	 */
	public $uniqId;
	
	/**
	 * Model attributes to be logged
	 */
	public $attributes;
	
	public function init()
	{
	}
	
   /**
	* Set log data to an array	
	*/
	private function setLogParams()
	{
		$logParams = $this->logParams[$this->logType];
		$this->logPath = $logParams['logPath'];
		$this->logName = $logParams['logName'];
		$this->logSocket = $logParams['logSocket'];
		$this->logLevel = $logParams['logLevel'];
	}
	
   /**
    * Prepare log message and write accordingly  
	*
	* @param string	$message Log message
	* @param integer $logLevel Log level
	*/
	function writeLog($message, $logLevel=INFO)
	{
		// Initialize log params according log type
		$this->setLogParams();
		
		$msg = '';
		
		switch ($this->logType) {

			// User activity log.
			// Format: Date|Time|UniqId|Username|IP|PageAction|LogLevel|Activity
			case 1:

				$date = date('Y-m-d|H:i:s');	
				$msg = "{$date}|{$this->uniqId}|{$this->username}|{$this->ip}|{$this->pageAction}|{$this->logLevelLabel[$logLevel]}|{$message} \n";
			
				break;

			// Daemon api log.
			// Format: Date|Time|UniqId|Username|IP|PageAction|LogLevel|Activity
			case 2:
			
				$date = date('Y-m-d|H:i:s');	
				$msg = "{$date}|{$this->uniqId}|{$this->username}|{$this->ip}|{$this->pageAction}|{$this->logLevelLabel[$logLevel]}|{$message} \n";	
			
				break;
		}

        $logFile = $this->logPath . $this->logName;


		if (!is_file($logFile)) {

            //////////////

//            $fp = fopen($_SERVER['DOCUMENT_ROOT'] . "/myText.txt","wb");
//            $handle = fopen($logFile, "wb");
//            fwrite($handle,$msg);
//            fclose($handle);
//            chmod($logFile, 777);
//            @exec("chmod 777 {$logFile}");



            ///////////////


			$handle = fopen($logFile, "w+");
			fclose($handle);
            chmod($logFile, 777);
            @exec("chmod 777 {$logFile}");
		}

		if ($logLevel <= $this->logLevel) {
			file_put_contents($logFile, $msg, FILE_APPEND);
		}

	}



    /**
     * Prepare log message and write accordingly
     *
     * @param string	$message Log message
     * @param integer $logLevel Log level
     */
    function writeMailFile($message, $logLevel=INFO)
    {


        $logFile = $this->logPath .'mailFile.txt';


        if (!is_file($logFile)) {

            $handle = fopen($logFile, "w+");
            fclose($handle);
            chmod($logFile, 777);
            @exec("chmod 777 {$logFile}");
        }

        if ($logLevel <= $this->logLevel) {
            file_put_contents($logFile, $message, FILE_APPEND);
        }

    }
	
   /**
   	* Submit log data to log server
   	* 
   	* @param string $socket Logserver ip and port.ex:localhost:8089
   	* @param string $data Log data
   	* @param integer $timeout Socket timeout
   	* @return bool true or false
	*/
	public function sendUdpData($socket, $data, $timeOut = 2)
	{
		if ($fp = @stream_socket_client('udp://' . $socket,
			$errno,
			$errstr,
			$timeOut)) {
			
				$res = @fwrite($fp, $data);

				fclose($fp);

				if ($res) {
					return true;
				}
		}
	}
}
?>