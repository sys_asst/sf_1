<?php
/**
 * Tk class file, Which handles various utility functions
 */
class Tk extends CApplicationComponent
{
	
	public function init()
	{

	}
	
	/**
	 * Register main css and javascripts requred by the system.
	 */
	public function registerMainScripts()
	{
		// Register bootstap extension related scripts
		Yii::app()->bootstrap->register();
		
		// Unregister some original scripts comes with bootstrap extension
		// Since I want to overide these scripts with theme specific
		Yii::app()->clientScript->scriptMap = array(
			'bootstrap.css' => false,
			'bootstrap-responsive.css' => false,
			'jquery-ui.css' => false,		
			'bootstrap-responsive.min.css' => false,
			'bootstrap.min.css' => false,
			'yii.css' => false
		);

		// Registering theme sepecific scripts
		/*Yii::app()->getClientScript()->registerCssFile(Yii::app()->theme->baseUrl . '/css/t-bootstrap.min.css');
		Yii::app()->getClientScript()->registerCssFile(Yii::app()->theme->baseUrl . '/css/t-bootstrap-responsive.min.css');
		Yii::app()->getClientScript()->registerCssFile(Yii::app()->theme->baseUrl . '/css/t-yii.css');
		Yii::app()->getClientScript()->registerCssFile(Yii::app()->theme->baseUrl . '/font-awesome-4.3.0/css/font-awesome.css');
		Yii::app()->getClientScript()->registerCssFile(Yii::app()->theme->baseUrl . '/css/social-buttons.css');
		Yii::app()->getClientScript()->registerCssFile(Yii::app()->theme->baseUrl . '/css/jquery-ui-redmond/jquery-ui-1.10.3.custom.min.css');
		Yii::app()->getClientScript()->registerCssFile(Yii::app()->theme->baseUrl . '/css/styles.css');*/
	}
	
	/**
	 * Register admin login page related css and js scripts.
	 */
	public function registerLoginScripts()
	{
		Yii::app()->getClientScript()->registerCssFile(Yii::app()->theme->baseUrl . '/css/login.css');
	}
	
	/**
	 * Register popup dialogbox related scripts
	 */
	public function registerDialogScripts()
	{
		Yii::app()->getClientScript()->registerCssFile(Yii::app()->theme->baseUrl . '/css/dialog.css');
	}
	
	/**
	 * Retrieve image path.
	 */
	public function getImagePath()
	{
		return Yii::app()->theme->baseUrl . '/img/';
	}
	
	/**
	 * Retrieve URL GET params
	 * @param stirng $param Get parameter name
	 * @param mixed default Default value to be return when parameter is not set
	 * @param mixed Get parameter value	 
	 */
	public static function get($param, $default=null)
	{
		return Yii::app()->request->getParam($param, $default);
	}
	
	/**
	 * Retrieve URL POST params
	 * @param stirng $param Post parameter name
	 * @param mixed default Default value to be return when parameter is not set
	 * @param mixed Post parameter value	 
	 */
	public static function post($param, $default=null)
	{
		return Yii::app()->request->getPost($param, $default);
	}
	
	/**
	 * Print flash message when performing ajax requests.
	 */	
	public static function setAjaxFlash($type, $message, $return = false)
	{
		$flash = "<div id='ajax_flash' class='alert in fade alert-{$type}'>
			<a class='close' data-dismiss='alert'>×</a>
				{$message}
			</div>";
		
		$flash .= "<script>jQuery('html, body').animate({scrollTop:0}, 'slow');</script>";
		$flash .= "<script>jQuery('#ajax_flash').animate({opacity: 1.0}, 60000).fadeOut('slow');</script>";
			  
		if ($return) {
			return $flash;
		}
		
		echo $flash;
	}
	
   /**
    * System sends mails via this function. Used PHP mailer wrapper class
    * @param array $from From email address. Ex:array('name'=>'Jhon', 'email'=>'jhon@gmail.com')
	* @param array $to Receipients addresses. Ex:array('Jhon'=>'jhon@gmail.com',tom@yahoo.com)
	* @param string $subject Subject of the email
	* @param string $body Email body
	* @param array $attachments Paths of attachements.Not implemented
	* @return boolean $sendStatus true if email sent otherwise false
	*/
	public function sendEmail($from, $to, $subject, $body, $attachments = array())
	{
		$sendStatus = true;
		
		try {
			$mail = new PHPMailer(true);
            //$mail->IsSendmail();
			$mail->IsHTML(true);
			
			foreach ($to as $name => $email) {
				if (is_string($name)) {
					$mail->AddAddress($email, $name);
				} else {
					$mail->AddAddress($email);
				}
			}

            if(isset($attachments) && sizeof($attachments))
            {
                foreach($attachments as $attachment)
                {
                    $mail->AddAttachment($attachment);
                }
            }
            //$mail->AddEmbeddedImage("rocks.png", "my-attach", "rocks.png");
			//$mail->Host = Yii::app()->params['smtp']['host'];
			//$mail->Username = Yii::app()->params['smtp']['username'];
			//$mail->Password = Yii::app()->params['smtp']['password'];
            $mail->AddBCC('noreply@saksglobal.com', 'Saks Global');
			//$mail->IsSMTP();
            //$mail->Port = 80;
			//$mail->CharSet = "UTF-8";
			//$mail->SMTPAuth = true;
            $mail->SetFrom($from['email'], $from['name']);
			//$mail->AddCustomHeader("sender", Yii::app()->params['smtp']['senderEmail']);
			$mail->Subject = $subject;
			$mail->Body = $body;
			$mail->Send();
			$mail->ClearAllRecipients();
			Yii::app()->appLog->writeLog("Email sent. To:" . json_encode($to));
		} catch (Exception $e) {
			$sendStatus = false;
			Yii::app()->appLog->writeLog("Email sent failed. Error:{$e->getMessage()}");
		}
		
		return $sendStatus;
	}

    /**
     * To query the sql use this function
     *
     * @param string $sql
     * @return mixed
     */
    public static function sql($sql)
    {
        $result = array();

        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();

        return $result;
    }

    /**
     * To print the array or Object used this method
     * @param array $array array or object to
     */
    public static function a($array)
    {
        if(!is_null($array))
        {
            echo '<pre>';
            print_r($array);
            echo '</pre>';
        } else {
            echo $array;
        }
    }

    public static function execute($sql)
    {
        $rowCount = 0;
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $rowCount = $command->execute();

        return $rowCount;
    }

    public static function insert($sql)
    {
        $id = 0;
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $command->execute();

        $id = Yii::app()->db->getLastInsertId();

        return $id;
    }

    /**
     * Shorten longer text
     * @param string  $text
     * @param int $length
     * @return string
     */
    public static function short($text, $length = 20)
    {
        if(strlen($text) > $length)
            $text = substr($text, 0, $length).'...';
        return $text;
    }

    /**
     * Get user's Role
     * @param int $user_id
     * @return string
     */
    public static function getRole($user_id)
    {
        $role_name = '';
        $role = Rights::getAssignedRoles($user_id);
        if(sizeof($role))
        {
            foreach($role as $role_data)
            {
                $role_name .= (strlen($role_name)) ? ', '.$role_data->name : $role_data->name;
            }
        }
        return (strlen($role_name)) ? $role_name : 'n/a';
    }
}