<?php
// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.


return CMap::mergeArray(
    array (
        'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
        'name'=>'Anton Sales Force Product Updater',

        // preloading 'log' component
        'preload'=>array('log'),

        // autoloading model and component classes
        'import'=>array(
            'application.models.*',
            'application.components.*',
            'application.vendor.PhpMailer.*',
        ),

        'params'=>array(
            'isConsole'=>true,
        ),

    ),local_config()
);

// return an array of custom local configuration settings
function local_config()
{
    if (file_exists(dirname(__FILE__).'/config.php'))
    {
        return require_once(dirname(__FILE__).'/config.php');
    }

    return array();
};
