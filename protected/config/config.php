<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 6/11/15
 * Time: 11:55 AM
 */
define('LOG_BASE_PATH', './tmp/');
define('SYS_TMP_PATH', './tmp/');
return array(
    'components'=>array(
        // This connection initially connected with master database and
        // dynamically change according to database
        'db'=>array(
//            'connectionString' => 'mysql:host=localhost;dbname=antonl5_salesforce',
//            'emulatePrepare' => true,
//            'username' => 'antonl5_salesf',
//            'password' => 'l?el?.bxNa5X',
            'connectionString' => 'mysql:host=localhost;dbname=anton_14_1',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => 'password',
		'charset' => 'utf8',
		'tablePrefix' => '',
	),

        'tk' => array (
            'class' => 'Tk',
        ),

        'fa' => array (
            'class' => 'Fa',
        ),

        'appLog' => array(
            'class' => 'AppLogger',
            'logType' => 1,
            'logParams' => array(
                1 => array(
                    'logPath' => LOG_BASE_PATH . 'log/',
                    'logName' => date('Ymd') . '-activity.log',
                    'logLevel' => 3, // Take necessary value from apploger class
                    'logSocket' => '',
                ),
                2 => array(
                    'logPath' => LOG_BASE_PATH . 'log/',
                    'logName' => date('Ymd') . '-api.log',
                    'logLevel' => 3, // Take necessary value from apploger class
                    'logSocket' => '',
                ),
            ),
        ),
        'log'=>array(
            'class'=>'CLogRouter',
            'routes'=>array(
                array(
                    'class'=>'CWebLogRoute',
                    'levels'=>'trace,info,error,warning',
                    'filter' => array(
                        'class' => 'CLogFilter',
                        'prefixSession' => true,
                        'prefixUser' => false,
                        'logUser' => false,
                        'logVars' => array(),
                    ),
                ),
            ),
        ),
    ),

    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params'=>array(
        //'copyRight' => Yii::t('messages', 'Copyright &copy; {date} by {company_by}. All Rights Reserved.', array('{date}'=>date('Y'),'{company_by}'=>'Saks Global')),
        'copyRight' => Yii::t('messages', "Copyright &copy; {date} by <a href=\"{website}\" target=\"{target}\"> {company_by}</a><br/>
		All Rights Reserved.<br/>",array('{date}'=>date('Y'),'{company_by}'=>'Saks Global', '{website}'=>'http://www.saksglobal.com', '{target}'=>'_blank')),

        // Site By company Name
        //'companyBy'=> Yii::t('messages', 'Nile Point Consulting LLC.'),

        // Default page size
        'pageSize' => 10,

        // SMTP connection settings
        'smtp' => array (
            // Mailjet account
            'host' => 'in-v3.mailjet.com',
//            'host' => 'smtp.gmail.com',
//            'username' => '018313ca8be2300dfdc3f1bb06e584da',
//            'password' => '0e34521041be605d4866ad8275efb931',
            'username' => '18f8109274f445a8e161766b371e9883',
//            'password' => '0e34521041be605d4866ad8275efb931',
            'password' => '71b743565c524961f8d985e4a96bb8a5',
            'senderEmail' => 'shanaka@saksglobal.com',
            'defaultFromName' => 'SAKS Global',
            'defaultFromEmail' => 'shanaka@saksglobal.com'
        ),

        // Languages for localization
        'lang' => array(
            'en' => array(
                'identifier' => 'en',
                'flagName' => 'United-Kingdom.png',
                'name' => 'English',
            ),
            'fr' => array(
                'identifier' => 'fr',
                'flagName' => 'France.png',
                'name' => 'Français',
            ),
        ),

        // Default language
        'defLang' => 'fr',

        'prdsrc'=>'productupdates/PARTVAT.txt',
//        'prdsrc'=>'productupdates/products.txt',
        'prdbackup'=>'db_backups/',

        // API access username and password
        'restApi'=>array(
            'username' => 'apiuser',
            'password' => 'api!#1*'
        ),
    ),
);