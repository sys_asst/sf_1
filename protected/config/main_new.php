<?php
Yii::setPathOfAlias('bootstrap', dirname(__FILE__).'/../extensions/bootstrap');
Yii::setPathOfAlias('editable', dirname(__FILE__).'/../extensions/x-editable');

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return CMap::mergeArray(
    array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Anton Sales Force',

	// preloading 'log' component
	'preload'=>array('log','bootstrap'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.modules.user.models.*',
        'application.modules.user.components.*',
        'application.modules.rights.*',
        'application.modules.rights.components.*',
        'application.extensions.dynamictabularform.*',
        'application.extensions.xreturnable.*',
        'application.vendor.phpexcel.PHPExcel',
        'ext.wform.*',
        'ext.wrest.*',
        'editable.*',
        //'ext.editable.*',
        'application.extensions.csv.*',
        'application.extensions.TabularInputManager',
	),
	'modules'=>array(
		'user'=>array(
            # encrypting method (php hash function)
            'hash' => 'md5', 
            # send activation email
            'sendActivationMail' => true, 
            # allow access for non-activated users
            'loginNotActiv' => false, 
            # activate user on registration (only sendActivationMail = false)
            'activeAfterRegister' => false, 
            # automatically login from registration
            'autoLogin' => true, 
            # registration path
            'registrationUrl' => array('/user/registration'), 
            # recovery password path
            'recoveryUrl' => array('/user/recovery'), 
            # login form path
            'loginUrl' => array('/user/login'), 
            # page after login
            //'returnUrl' => array('/user/profile'),
            'returnUrl' => array('/'),
            # page after logout
            'returnLogoutUrl' => array('/user/login'),
        ),

        'rights'=>array( 
        	'superuserName'=>'Admin', // Name of the role with super user privileges. 
	        'authenticatedName'=>'Authenticated', // Name of the authenticated user role. 
	        'userIdColumn'=>'id', // Name of the user id column in the database. 
	        'userNameColumn'=>'username', // Name of the user name column in the database. 
	        'enableBizRule'=>true, // Whether to enable authorization item business rules. 
	        'enableBizRuleData'=>false, // Whether to enable data for business rules. 
	        'displayDescription'=>true, // Whether to use item description instead of name. 
	        'flashSuccessKey'=>'RightsSuccess', // Key to use for setting success flash messages. 
	        'flashErrorKey'=>'RightsError', // Key to use for setting error flash messages. 
	        'install'=>false, // Whether to install rights. 
	        'baseUrl'=>'/rights', // Base URL for Rights. Change if module is nested. 
	        'layout'=>'rights.views.layouts.main', // Layout to use for displaying Rights. 
	        'appLayout'=>'application.views.layouts.main', // Application layout. 
	        'cssFile'=>'rights.css', // Style sheet file to use for Rights. 
	        'install'=>false, // Whether to enable installer. 
	        'debug'=>false, // Whether to enable debug mode. 
        ),
        'd_export'=>array(
            'class'=>'application.modules.d_export.DExportModule',
            'accessPermissionUsers'=>array('@'),
            'userModel'=>'User',
            'install'=>false,
            'adminUserId'=>1,
            'layout'=>'//layouts/column2'
        ),
		// uncomment the following to enable the Gii tool

        'gii'=>array(
            'class'=>'system.gii.GiiModule',
            'password'=>'1234',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters'=>array('127.0.0.1','::1','192.168.1.*'),
            'generatorPaths'=>array(
                'bootstrap.gii',
            ),
		),
		
		'rights'=>array( 
			'install'=>false, // Enables the installer.
		),
        'reportico' => array(),
	),
    'controllerMap'=>array(
        'csv'=>array(
            'class'=>'application.extensions.csv.CsvController',

        )
     ),
	// application components
	'components'=>array(
		'user'=>array(
			'class'=>'RWebUser',
            'allowAutoLogin'=>true,
            'loginUrl' => array('/user/login'),
		),
        'bootstrap'=>array(
            'class'=>'application.extensions.bootstrap.components.Bootstrap',
        ),
        //X-editable config
        'editable' => array(
            'class'     => 'editable.EditableConfig',
            'form'      => 'bootstrap',        //form style: 'bootstrap', 'jqueryui', 'plain'
            'mode'      => 'popup',            //mode: 'popup' or 'inline'
            'defaults'  => array(              //default settings for all editable elements
                'emptytext' => 'Click to edit'
            )
        ),
        // uncomment the following to enable URLs in path-format

        'urlManager'=>array(
            'urlFormat'=>'path',
            'rules'=>array(
                'post/<id:\d+>/<title:.*?>'=>'post/view',
                'posts/<tag:.*?>'=>'post/index',
                // REST patterns
                array('api/list', 'pattern'=>'api/<model:\w+>', 'verb'=>'GET'),
                array('api/view', 'pattern'=>'api/<model:\w+>/<id:\d+>', 'verb'=>'GET'),
                array('api/update', 'pattern'=>'api/<model:\w+>/<id:\d+>', 'verb'=>'PUT'),  // Update
                array('api/delete', 'pattern'=>'api/<model:\w+>/<id:\d+>', 'verb'=>'DELETE'),
                array('api/create', 'pattern'=>'api/<model:\w+>', 'verb'=>'POST'), // Create
                '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
            ),
        ),
        'authManager'=>array(
            'class'=>'RDbAuthManager', // Provides support authorization item sorting. ......
        ),
		'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),
		// uncomment the following to use a MySQL database
		
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=antonsf_test',//antonl5_salesforce
			'emulatePrepare' => true,
			'username' => 'root',//antonl5_salesf
			'password' => 'asn2014',//'l?el?.bxNa5X',
			'charset' => 'utf8',
			'tablePrefix' => '',
		),
        'tk' => array (
            'class' => 'Tk',
        ),
        'fa' => array (
            'class' => 'Fa',
        ),
        'theme'=>'bootstrap_spacelab',
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
                /*array(
                    'class'=>'CFileLogRoute',
                    'levels'=>'trace',
                    'categories'=>'system.db.*',
                    'logFile'=>'sql.log',
                ),*/
			),
		),
        //'theme'=>'bootstrap_spacelab',
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'anura@saksglobal.com',
		'defaultUserEmail'=>'default@saks.com',
        'allowednooflabels'=>2,
        'dealsercodeprefix'=>'C',
        'currencysymbol'=>'Rs ',
	),
),local_config()
);

// return an array of custom local configuration settings
function local_config()
{
    if (file_exists(dirname(__FILE__).'/config.php'))
    {
        return require_once(dirname(__FILE__).'/config.php');
    }

    return array();
};
