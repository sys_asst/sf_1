<?php
$this->breadcrumbs=array(
	(UserModule::t('Users'))=>array('admin'),
	$model->email=>array('view','id'=>$model->id),
	(UserModule::t('Update')),
);
//Tk::a($model);exit;
$this->menu=array(
    array('label'=>UserModule::t('Create User'), 'url'=>array('create')),
    array('label'=>UserModule::t('View User'), 'url'=>array('view','id'=>$model->id)),
    array('label'=>UserModule::t('Manage Users'), 'url'=>array('admin')),
    array('label'=>UserModule::t('Manage Profile Field'), 'url'=>array('profileField/admin')),
    array('label'=>UserModule::t('List User'), 'url'=>array('/user')),
);

$refid = Yii::app()->getRequest()->getQuery('refid');
$src = Yii::app()->getRequest()->getQuery('src');
$name = '';
$values = array(
    'name' => '',
    'password' => '',
    'email'=>'',
    'username'=>'',
    'src'=>1,
    'companyname'=>'',
    'accno'=>''
);

if($src == 'sref')
{
    $src_module =  Salesreps::model()->findbyPk($refid);
    //$name = $src_module->srName;
    //$username = strtolower($src_module->srEmpNo);
    $password = $src_module->srEmpNo;//Yii::app()->controller->module->encrypting($src_module->srEmpNo);
    //$email = strlen(trim($src_module->srEmail)) ? trim($src_module->srEmail) : $src_module->srEmpNo.'_'.Yii::app()->params['defaultUserEmail'];
    $src = 2;
    $companyName = $src_module->designation->name;
    $accno = $src_module->srEmpNo;
} elseif($src == 'dist') {
    $src_module =  Distributors::model()->findbyPk($refid);
    //$name = $src_module->distName;
    //$username = strtolower($src_module->distId);
    $password = $src_module->distId;//Yii::app()->controller->module->encrypting($src_module->distId);
    //$email = strlen(trim($src_module->distEmail)) ? trim($src_module->distEmail) : $src_module->distId.'_'.Yii::app()->params['defaultUserEmail'];
    $src = 3;
    $companyName = $src_module->distName;
    $accno = $src_module->distId;
} else {
    $src = $profile->src;
    $password = $model->username;
    $companyName = '';//$model->designation->name;
}
//echo $src;
if($src == 2)
{
    $sql = "SELECT D.`name`,S.`srName` FROM `salesreps` AS S INNER JOIN `ms_designations` AS D ON D.`id` = S.`designation_id` AND S.`user_id` = '".$model->id."'";
    $rec = Tk::sql($sql);
    //Tk::a($rec);exit;
    $companyName = (sizeof($rec)) ? $rec[0]['name'] : '';
    $name = (sizeof($rec)) ? $rec[0]['srName'] : '';
}
if($src == 3)
{
    $sql = "SELECT D.`distName`, D.`distId` FROM `distributors` AS D WHERE D.`distid` = '".$model->username."'";
    $rec = Tk::sql($sql);
    //Tk::a($rec);exit;
    $companyName = (sizeof($rec)) ? $rec[0]['distName'] : '';
    $accno = (sizeof($rec)) ? $rec[0]['distId'].'' : '';
}

if(strlen(trim($src)))
{
    $values= array(
        'name' => $name,
        'password' => $password,
        //'email'=> $email,
        //'username'=>$username,
        'src'=>$src,
        'companyname'=> $companyName
    );
    //$accno = $model->username;
}
$profile->src = $src;
//Tk::a($values);
?>

<h1><?php echo  UserModule::t('Update User')." ".$model->email; ?></h1>

<?php
	echo $this->renderPartial('_form', array('model'=>$model,'profile'=>$profile, 'values'=>$values));
?>