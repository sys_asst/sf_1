<?php
$this->breadcrumbs=array(
	UserModule::t('Users')=>array('admin'),
	UserModule::t('Create'),
);

/*$this->menu=array(
    array('label'=>UserModule::t('Manage Users'), 'url'=>array('admin')),
    array('label'=>UserModule::t('Manage Profile Field'), 'url'=>array('profileField/admin')),
    array('label'=>UserModule::t('List User'), 'url'=>array('/user')),
);*/
Yii::app()->basePath;//
//$this->renderPartial(Yii::app()->basePath.'/views/layouts/_actions', array('model'=>$model));

$refid = Yii::app()->getRequest()->getQuery('refid');
$src = Yii::app()->getRequest()->getQuery('src');
$values = array(
    'name' => '',
    'password' => '',
    'email'=>'',
    'username'=>'',
    'src'=>1,
    'companyname'=>'',
    'accno'=>''
);

if($src == 'sref')
{
    $src_module =  Salesreps::model()->findbyPk($refid);
    $name = $src_module->srName;
    $username = strtolower($src_module->srEmpNo);
    $password = $src_module->srEmpNo;//Yii::app()->controller->module->encrypting($src_module->srEmpNo);
    $email = strlen(trim($src_module->srEmail)) ? trim($src_module->srEmail) : $src_module->srEmpNo.'_'.Yii::app()->params['defaultUserEmail'];
    $src = 2;
    $companyName = $src_module->srDesignation;
    $accno = $src_module->srEmpNo;
} elseif($src == 'dist') {
    $src_module =  Distributors::model()->findbyPk($refid);
    $name = $src_module->distName;
    $username = strtolower($src_module->distId);
    $password = $src_module->distId;//Yii::app()->controller->module->encrypting($src_module->distId);
    $email = strlen(trim($src_module->distEmail)) ? trim($src_module->distEmail) : $src_module->distId.'_'.Yii::app()->params['defaultUserEmail'];
    $src = 3;
    $companyName = $src_module->distName;
    $accno = $src_module->distId;
}

if(strlen(trim($src)))
{
    $values= array(
        'name' => $name,
        'password' => $password,
        'email'=> $email,
        'username'=>$username,
        'src'=>$src,
        'companyname'=> $companyName
    );
}
$profile->src = $src;

?>
<h1><?php echo UserModule::t("Create User"); ?></h1>

<?php
	echo $this->renderPartial('_form', array('model'=>$model,'profile'=>$profile, 'values'=>$values));
?>
