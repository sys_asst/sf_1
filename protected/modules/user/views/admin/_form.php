<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'user-form',
	'enableAjaxValidation'=>true,
    'enableClientValidation'=>true,
	'htmlOptions' => array('enctype'=>'multipart/form-data'),
));

/**
 * Get values for user form from sales refs or distributors model
 */
$action = Yii::app()->controller->getAction()->getId();

$fname = $lname = $password = $email = $username = '';

if(isset($values) && sizeof($values))
{
    @$name = $values['name'];
    @$name = explode(' ', $name);
    @$username = $values['username'];//strtolower($name[0]);
    @$fname = $name[0];
    unset($name[0]);
    $lname = implode(' ',$name);
    @$password = $values['password'];
    @$email = $values['email'];
    @$companyName = $values['companyname'];
}
?>

	<p class="note"><?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>

	<?php echo $form->errorSummary(array($model,$profile)); ?>
    <div class="row-fluid">
        <div class="span6">
            <fieldset><legend>Account Details</legend>
                <?php
                if($values['src']==1)
                {
                    ?>
                     <?php echo $form->textFieldRow($model,'username',($action == 'create') ? array('class'=>'input-block-level','size'=>20,'maxlength'=>20, 'value'=>$username):  array('class'=>'input-block-level','size'=>20,'maxlength'=>20)); ?>
                <?php
                }
                ?>
            <?php
            if($values['src']==3)
            {
                echo $form->hiddenField($model,'username',array('value'=>$username));
                ?>
                <?php //echo $form->textFieldRow($model,'accountNo',($action == 'create') ? array('size'=>20,'maxlength'=>20, 'value'=>$password, 'readonly'=>true):  array('size'=>20,'maxlength'=>20, 'value'=>$password, 'readonly'=>true)); ?>
                <?php echo $form->textFieldRow($model,'accountNo',($action == 'create') ? array('size'=>20,'maxlength'=>20, 'value'=>$password, 'readonly'=>true):  array('size'=>20,'maxlength'=>20, 'value'=>$password, 'readonly'=>true)); ?>
                <?php echo $form->textFieldRow($model,'companyName',($action == 'create') ? array('class'=>'input-block-level','size'=>20,'maxlength'=>20, 'value'=>$companyName, 'readonly'=>true):  array('class'=>'input-block-level','size'=>20,'maxlength'=>20, 'value'=>$companyName, 'readonly'=>true)); ?>

            <?php
            }elseif($values['src']==2)
            {       echo $form->hiddenField($model,'username',array('value'=>$username));
                ?>
                    <?php echo $form->textFieldRow($model,'empNo',($action == 'create') ? array('class'=>'input-block-level','size'=>20,'maxlength'=>20, 'value'=>$password, 'readonly'=>true):  array('class'=>'input-block-level','size'=>20,'maxlength'=>20, 'value'=>$password,'readonly'=>true)); ?>
                    <?php echo $form->textFieldRow($model,'empName',($action == 'create') ? array('class'=>'input-block-level','size'=>50,'maxlength'=>50, 'value'=>$fname.' '.$lname, 'readonly'=>true):  array('class'=>'input-block-level','size'=>50,'maxlength'=>50, 'value'=>$fname.' '.$lname, 'readonly'=>true)); ?>
                    <?php echo $form->textFieldRow($model,'designation',($action == 'create') ? array('class'=>'input-block-level','size'=>20,'maxlength'=>20, 'value'=>$companyName, 'readonly'=>true):  array('class'=>'input-block-level','size'=>20,'maxlength'=>20, 'value'=>$companyName, 'readonly'=>true)); ?>

            <?php
            }
            ?>
                <?php
                if(($values['src'] > 1))
                {
                    ?>
                    <?php
                        $htmlOptions = array(
                            'options'=>array(1=>array('disabled'=>'disabled')),
                        );
                        echo $form->dropDownListRow($model,'superuser',User::itemAlias('AdminStatus'),$htmlOptions); ?>
                <?php
                } else {
                    ?>
                        <?php
                        echo $form->dropDownListRow($model,'superuser',User::itemAlias('AdminStatus')); ?>
                <?php
                }
                ?>
                <?php echo $form->textFieldRow($model,'email',($action == 'create') ? array('size'=>60,'maxlength'=>128,'value'=>$email,'class'=>'input-block-level',) : array('size'=>60,'maxlength'=>128, 'class'=>'input-block-level',)); ?>
                <?php echo $form->passwordFieldRow($model,'password',($action == 'create') ? array('size'=>60,'maxlength'=>128, 'value'=>$password, 'class'=>'input-block-level') : array('size'=>60,'maxlength'=>128,'class'=>'input-block-level',)); ?>
                <?php echo $form->passwordFieldRow($model,'confPassword',array('class'=>'input-block-level','maxlength'=>128)); ?>
            </fieldset>
        </div>
        <div class="span6">
            <fieldset><legend>Profile Details</legend>
                <?php
                $profileFields=$profile->getFields();
                if ($profileFields) {
                    foreach($profileFields as $field) {
                        if(!$field->range)
                        {
                            ?>
                            <div class="row">
                                <?php //echo $form->labelEx($profile,$field->varname); ?>
                                <?php
                                if ($widgetEdit = $field->widgetEdit($profile)) {
                                    echo $widgetEdit;
                                } elseif ($field->range) {
                                    echo $form->dropDownListRow($profile,$field->varname,Profile::range($field->range));
                                } elseif ($field->field_type=="TEXT") {
                                    echo CHtml::activeTextArea($profile,$field->varname,array('rows'=>6, 'cols'=>50));
                                } else {
                                    (($field->varname == 'firstname') ? $selected_value = $fname : '');
                                    (($field->varname == 'lastname') ? $selected_value = $lname : '');
                                    echo $form->textFieldRow($profile,$field->varname,($action == 'create') ? array('class'=>'input-block-level','size'=>60,'value'=>$selected_value,'maxlength'=>(($field->field_size)?$field->field_size:255)) : array('class'=>'input-block-level','size'=>60,'maxlength'=>(($field->field_size)?$field->field_size:255)));
                                }
                                ?>
                                <?php //echo $form->error($profile,$field->varname); ?>
                            </div>
                        <?php
                        }
                    }
                }
                ?>
                <?php
                $profileFields=$profile->getFields();
                if ($profileFields) {
                    foreach($profileFields as $field) {
                        if($field->range)
                        {
                            ?>
                            <div class="row">
                                <?php //echo $form->labelEx($profile,$field->varname); ?>
                                <?php
                                if ($widgetEdit = $field->widgetEdit($profile)) {
                                    echo $widgetEdit;
                                } elseif ($field->range) {
                                    echo $form->dropDownListRow($profile,$field->varname,Profile::range($field->range));
                                } elseif ($field->field_type=="TEXT") {
                                    echo CHtml::activeTextArea($profile,$field->varname,array('rows'=>6, 'cols'=>50));
                                } else {
                                    (($field->varname == 'firstname') ? $selected_value = $fname : '');
                                    (($field->varname == 'lastname') ? $selected_value = $lname : '');
                                    echo $form->textFieldRow($profile,$field->varname,($action == 'create') ? array('size'=>60,'value'=>$selected_value,'maxlength'=>(($field->field_size)?$field->field_size:255)) : array('size'=>60,'maxlength'=>(($field->field_size)?$field->field_size:255)));
                                }
                                ?>
                                <?php //echo $form->error($profile,$field->varname); ?>
                            </div>
                        <?php
                        }
                    }
                }
                ?>
                <?php echo $form->dropDownListRow($model,'status',User::itemAlias('UserStatus')); ?>

            </fieldset>
        </div>
    </div>

    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>$model->isNewRecord ? Yii::t('messages', 'Create') : Yii::t('messages', 'Save'),
        )); ?>

        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'label'=>Yii::t('messages', 'Cancel'),
            'type'=>'info',
            'url'=>Yii::app()->createUrl('user/admin')
        )); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
