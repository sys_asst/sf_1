<?php
$this->breadcrumbs=array(
	UserModule::t("Users"),
);
if(UserModule::isAdmin()) {
	$this->layout='//layouts/column2';
	$this->menu=array(
	    array('label'=>UserModule::t('Manage Users'), 'url'=>array('/user/admin')),
	    array('label'=>UserModule::t('Manage Profile Field'), 'url'=>array('profileField/admin')),
	);
}
?>

<h1><?php echo UserModule::t("List User"); ?></h1>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
	'dataProvider'=>$dataProvider,
    'columns'=>array(
        array(
            'name' => 'username',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->username),array("user/view","id"=>$data->id))',
        ),
        array(
            'header' => 'Name',
            'type'=>'raw',
            'value' => 'User::getFrofileName($data->id)',
        ),
        array(
            'header' => 'User Type',
            'type'=>'raw',
            'value' => 'User::getFrofileName($data->id, 1)',
        ),
        'create_at',
        'lastvisit_at',
    ),
)); ?>
