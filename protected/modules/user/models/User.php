<?php

class User extends CActiveRecord
{
	const STATUS_NOACTIVE=0;
	const STATUS_ACTIVE=1;
	const STATUS_BANNED=-1;
	
	//TODO: Delete for next version (backward compatibility)
	const STATUS_BANED=-1;

    public $companyName;
    public $accountNo;
    public $designation;
    public $empNo;
    public $department;
    public $confPassword;
    public $userType;

	
	/**
	 * The followings are the available columns in table 'users':
	 * @var integer $id
	 * @var string $username
	 * @var string $password
	 * @var string $email
	 * @var string $activkey
	 * @var integer $createtime
	 * @var integer $lastvisit
	 * @var integer $superuser
	 * @var integer $status
     * @var timestamp $create_at
     * @var timestamp $lastvisit_at
	 */

	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return Yii::app()->getModule('user')->tableUsers;
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.CConsoleApplication
		return ((get_class(Yii::app())=='CConsoleApplication' || (get_class(Yii::app())!='CConsoleApplication' && Yii::app()->getModule('user')->isAdmin()))?array(
			array('username', 'length', 'max'=>20, 'min' => 3,'message' => UserModule::t("Incorrect username (length between 3 and 20 characters).")),
			array('password', 'length', 'max'=>128, 'min' => 4,'message' => UserModule::t("Incorrect password (minimal length 4 symbols).")),
			array('confPassword', 'length', 'max'=>128, 'min' => 4,'message' => UserModule::t("Incorrect password (minimal length 4 symbols).")),
			array('email', 'email'),
			array('username', 'unique', 'message' => UserModule::t("This user's name already exists.")),
			array('email', 'unique', 'message' => UserModule::t("This user's email address already exists.")),
			array('username', 'match', 'pattern' => '/^[A-Za-z0-9_]+$/u','message' => UserModule::t("Incorrect symbols (A-z0-9).")),
			array('status', 'in', 'range'=>array(self::STATUS_NOACTIVE,self::STATUS_ACTIVE,self::STATUS_BANNED)),
			array('superuser', 'in', 'range'=>array(0,1)),
            array('create_at', 'default', 'value' => date('Y-m-d H:i:s'), 'setOnEmpty' => true, 'on' => 'insert'),
            array('lastvisit_at', 'default', 'value' => '0000-00-00 00:00:00', 'setOnEmpty' => true, 'on' => 'insert'),
			array('username, email, superuser, status, password, confPassword', 'required'),
			array('superuser, status', 'numerical', 'integerOnly'=>true),
			array('id, username, password, email, activkey, create_at, lastvisit_at, superuser, status,userType', 'safe', 'on'=>'search'),
		):((Yii::app()->user->id==$this->id)?array(
			array('username, email', 'required'),
            array('confPassword', 'compare', 'compareAttribute'=>'password', 'on'=>'create,update', 'operator'=>'==' ,'message'=>'Please enter the same password twice'),
            array('username, password, confPassword', 'required', 'on'=>'create'),
			array('username', 'length', 'max'=>20, 'min' => 3,'message' => UserModule::t("Incorrect username (length between 3 and 20 characters).")),
			array('email', 'email'),
			array('username', 'unique', 'message' => UserModule::t("This user's name already exists.")),
			array('username', 'match', 'pattern' => '/^[A-Za-z0-9_]+$/u','message' => UserModule::t("Incorrect symbols (A-z0-9).")),
			array('email', 'unique', 'message' => UserModule::t("This user's email address already exists.")),
            array('confPassword', 'compare', 'compareAttribute'=>'password', 'on'=>'create,update'),
		):array()));
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
        $relations = Yii::app()->getModule('user')->relations;
        if (!isset($relations['profile']))
            $relations['profile'] = array(self::HAS_ONE, 'Profile', 'user_id');
        return $relations;
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => UserModule::t("Id"),
			'username'=>UserModule::t("Username"),
			'password'=>UserModule::t("Password"),
			'verifyPassword'=>UserModule::t("Retype Password"),
			'email'=>UserModule::t("Login E-mail"),
			'verifyCode'=>UserModule::t("Verification Code"),
			'activkey' => UserModule::t("activation key"),
			'createtime' => UserModule::t("Registration date"),
			'create_at' => UserModule::t("Registration date"),
            'companyName' => UserModule::t("Company Name"),
            'accountNo' => UserModule::t("Company Account No"),
            'designation' => UserModule::t("Designation"),
            'empNo' => UserModule::t("Emp No"),
            'empName' => UserModule::t("Emp Name"),
            'department' => UserModule::t("Department"),
			'lastvisit_at' => UserModule::t("Last visit"),
			'superuser' => UserModule::t("Superuser"),
			'status' => UserModule::t("Status"),
                        'userType' => UserModule::t("User Type"),
		);
	}
	
	public function scopes()
    {
        return array(
            'active'=>array(
                'condition'=>'status='.self::STATUS_ACTIVE,
            ),
            'notactive'=>array(
                'condition'=>'status='.self::STATUS_NOACTIVE,
            ),
            'banned'=>array(
                'condition'=>'status='.self::STATUS_BANNED,
            ),
            'superuser'=>array(
                'condition'=>'superuser=1',
            ),
            'notsafe'=>array(
            	'select' => 'id, username, password, email, activkey, create_at, lastvisit_at, superuser, status',
            ),
        );
    }
	
	public function defaultScope()
    {
        return CMap::mergeArray(Yii::app()->getModule('user')->defaultScope,array(
            'alias'=>'user',
            'select' => 'user.id, user.username, user.email, user.create_at, user.lastvisit_at, user.superuser, user.status',
        ));
    }
	
	public static function itemAlias($type,$code=NULL) {
		$_items = array(
			'UserStatus' => array(
				self::STATUS_NOACTIVE => UserModule::t('Not active'),
				self::STATUS_ACTIVE => UserModule::t('Active'),
				self::STATUS_BANNED => UserModule::t('Banned'),
			),
			'AdminStatus' => array(
				'0' => UserModule::t('No'),
				'1' => UserModule::t('Yes'),
			),
                        'Type' => array(
                             '1' => UserModule::t('System User'),
                             '2' => UserModule::t('Sales Team'),
                             '3' => UserModule::t('Distributor'),
                       ),
		);
		if (isset($code))
			return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
		else
			return isset($_items[$type]) ? $_items[$type] : false;
	}
	
/**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;
        $criteria->with = array( 'profile' );
        (strlen($this->status) < 1) ? $this->status = User::STATUS_ACTIVE : '';
        
        $criteria->compare('id',$this->id);
        $criteria->compare('username',$this->username,true);
        $criteria->compare('password',$this->password);
        $criteria->compare('email',$this->email,true);
        $criteria->compare('activkey',$this->activkey);
        $criteria->compare('create_at',$this->create_at);
        $criteria->compare('lastvisit_at',$this->lastvisit_at);
        $criteria->compare('superuser',$this->superuser);
        $criteria->compare('status',$this->status);
        $criteria->compare( 'profile.src', $this->userType, true );

        return new CActiveDataProvider(get_class($this), array(
            'criteria'=>$criteria,
        	'pagination'=>array(
				'pageSize'=>Yii::app()->getModule('user')->user_page_size,
			),
        ));
    }

    public function getCreatetime() {
        return strtotime($this->create_at);
    }

    public function setCreatetime($value) {
        $this->create_at=date('Y-m-d H:i:s',$value);
    }

    public function getLastvisit() {
        return strtotime($this->lastvisit_at);
    }

    public function setLastvisit($value) {
        $this->lastvisit_at=date('Y-m-d H:i:s',$value);
    }

    public static function getDistributor()
    {
        $distributor = '';

        $data = Yii::app()->db->createCommand()
            ->select('U.*, P.src')
            ->from('users U')
            ->join('profiles P', 'U.id=P.user_id')
            ->where('status=1 AND username = "'.Yii::app()->user->name.'"')
            ->queryRow();

        if($data['src'] == 1)
        {
            $distributor = '';//Yii::app()->user->name;
            $sql = "SELECT DI.`distId` FROM `distributors` AS DI WHERE DI.`status` = '1'";
            $records = Tk::sql($sql);
            $data = '';

            if(sizeof($records))
            {
                foreach($records as $rec)
                {
                    //$data .= (strlen($data)) ? ','.$rec['distId'] : $rec['distId'];
                    $data[] = $rec['distId'];
                }

                $distributor = implode(',', $data);;
            }
        } elseif($data['src'] == 2 )
        {
            if(Yii::app()->user->getState('src')->designation_id == 1 || Yii::app()->user->getState('src')->designation_id == 2)
            {
                $distributors = array();
                if(Yii::app()->user->getState('src')->designation_id == 1)
                {
                    $sql = "SELECT D.`distId` FROM `distributors` AS D WHERE D.`area_id` IN(
                    SELECT DR.`area_id` FROM `ms_regions` AS R INNER JOIN `d_regions` AS DR ON DR.`region_id` = R.`id` WHERE R.`rcm_id` = '".Yii::app()->user->getState('src')->id."')";
                    $records = Tk::sql($sql);
                }
                if(Yii::app()->user->getState('src')->designation_id == 2)
                {
                    $sql = "SELECT D.`distId` FROM `distributors` AS D";
                    $records = Tk::sql($sql);
                }
                //Tk::a($records);exit;
                if(sizeof($records))
                {
                    foreach($records as $rec)
                    {
                        $distributors[] = $rec['distId'];
                    }
                }
                if(sizeof($distributors))
                {
                    $distributor = implode(',', $distributors);
                } else {
                    $distributor = 0;
                }
                //UPDATE `salesreps` SET `salesreps`.`designation_id` = '2' WHERE `salesreps`.`srDesignation` = 'Sales Manager'
                //UPDATE `salesreps` SET `salesreps`.`designation_id` = '1' WHERE `salesreps`.`srDesignation` = 'Regional Customer Manager'
                //UPDATE `salesreps` SET `salesreps`.`designation_id` = '3' WHERE `salesreps`.`srDesignation` = 'Sales Representative'
                //UPDATE `salesreps` SET `salesreps`.`designation_id` = '3' WHERE `salesreps`.`srDesignation` = 'Sales Representative'
            } else {
                $data = Yii::app()->db->createCommand()
                    ->select('S.distributor')
                    ->from('salesreps S')
                    ->where('status=1 AND srEmpNo = "'.Yii::app()->user->name.'"')
                    ->queryRow();
                $distributor = $data['distributor'];
            }

        } elseif($data['src'] == 3)
        {
            $distributor = Yii::app()->user->name;
        }

        return $distributor;
    }

    /**
     * Get User's profile
     * @param $userid
     * @return string
     */
    public static function getFrofileName($userid, $user_type = 0)
    {
        $profile= array();
        //$user = User::model()->notsafe()->findbyPk($userid);
        $profile = Profile::model()->findByPk($userid);
        if($user_type == 0)
        {
            return $name = sizeof($profile) ? $profile->firstname.', '.$profile->lastname : 'n/a';
        } elseif($user_type == 1) {
            if($profile->src == 1)
            {
                $profile = 'System User';
            } elseif($profile->src == 2)
            {
                $profile = 'Sales Team';
            } elseif($profile->src == 3)
            {
                $profile = 'Distributor';
            }

            return $profile ;
        }
    }

    public static function userCreated($username)
    {
        $data = Yii::app()->db->createCommand()
            ->select('U.*, P.src')
            ->from('users U')
            ->join('profiles P', 'U.id=P.user_id')
            ->where('status=1 AND username = "'.$username.'"')
            ->queryRow();

        $created = 0;

        if(sizeof($data))
        {
            $created = 1;
        }
        return $created;
    }

    public static function getReps()
    {
        $data = Yii::app()->db->createCommand()
            ->select('U.*, P.src')
            ->from('users U')
            ->join('profiles P', 'U.id=P.user_id')
            ->where('status=1 AND username = "'.Yii::app()->user->name.'"')
            ->queryRow();
        //Tk::a($data);
        if($data['src'] == 1)
        {
            $records = array(Yii::app()->user->id => Yii::app()->user->name);
        } else if($data['src'] == 2)
        {
            if(Yii::app()->user->getState('src')->designation_id == 1 || Yii::app()->user->getState('src')->designation_id == 2)
            {

                if(Yii::app()->user->getState('src')->designation_id == 1)
                {
                    $sql = "SELECT * FROM `salesreps` AS S WHERE S.`region_id` = '".Yii::app()->user->getState('src')->region_id."' AND S.`designation_id` = '3'  ORDER BY S.`srName`";
                }
                if(Yii::app()->user->getState('src')->designation_id == 2)
                {
                    $sql = "SELECT * FROM `salesreps` AS S WHERE S.`designation_id` = '3' ORDER BY S.`srName`";
                }
            } else {
                $sql = "SELECT * FROM `salesreps` AS S WHERE S.`id` = '".Yii::app()->user->id."' AND S.`designation_id` = '3' ORDER BY S.`srName`";
            }
            $records = Tk::sql($sql);
        } else if($data['src'] == 3){
            $sql = "SELECT * FROM `salesreps` AS S WHERE S.`distributor` = '".Yii::app()->user->name."'  ORDER BY S.`srName`";
            $records = Tk::sql($sql);
        }

        return $records;
    }

    /**
     * Get sales rep id or code
     * @param int $code
     * @return string
     */
    public static function getRepIds($code = 1)
    {
        $reps = self::getReps();
        $sreps = '';

        if(sizeof($reps))
        {
            foreach($reps as $rep)
            {
                if($code == 1)
                {
                    $sreps .= ($sreps) ? ','.$rep['srEmpNo'] : $rep['srEmpNo'];
                } else {
                    $sreps .= ($sreps) ? ','.$rep['id'] : $rep['id'];
                }
            }
        }

        return $sreps;
    }
}