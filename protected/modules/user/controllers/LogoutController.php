<?php

class LogoutController extends RController
{
	public $defaultAction = 'logout';
	
	/**
	 * Logout the current user and redirect to returnLogoutUrl.
	 */
	public function actionLogout()
	{
//		Yii::app()->user->logout(true);
        Yii::app()->session->destroy();
		$this->redirect(Yii::app()->controller->module->returnLogoutUrl);
	}

}