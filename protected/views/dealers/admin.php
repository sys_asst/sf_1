<?php
/* @var $this DealersController */
/* @var $model Dealers */

$this->breadcrumbs=array(
	'Dealers'=>array('index'),
	'Manage',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#dealers-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Dealers</h1>

<!-- <p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p> -->

<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
));

?>
</div><!-- search-form -->

<?php /*$this->widget('application.extensions.csv.csvWidget', array(
    'table' => '.items',
    'csvFile' => ''));*/
    //$this->widget('application.extensions.jui.EJqueryUiInclude', array('theme'=>'humanity'));
?>
<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
	'id'=>'dealers-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
        array(
            'name'=>'id',
            'header'=>'Dealer No',
            'value'=>'DealerNo::getDealerNo($data->id)',
        ),
		'dealerCode',
		'dealerName',
        array(
            'name'=>'distributor',
            'value'=>'isset($data->distributor0["distName"]) ? $data->distributor0["distName"] : "n/a"',
            'filter' => CHtml::listData(Distributors::model()->findAll(), 'distId','distName'),
        ),
		'phoneOffice',
		'phoneResidence',
        'contactPerson',
        'email',
		'creditDays',
        'creditAmount',
        'address1',
        'address2',
        'address3',
array(
            'name'=>'townId',
            'value'=>'$data->town->town',
            'filter' => CHtml::listData(Towns::model()->findAll(array('order'=>'Town')), 'id','town'),
        ),
array(
            'name'=>'disabled',
            'value'=>'Dealers::itemAlias("ItemStatus",$data->disabled)',
            'filter' => Dealers::itemAlias("ItemStatus"),
        ),
		/*
		'townId',
		'latitude',
		'longitude',
		'distributor',
		'addedOn',
		'lastUpdatedOn',
		'disabled',
		'disabled_by',
		'disabled_on',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'template'=>'{view}{update}'
		),
	),
));
//print_r($model);
?>
