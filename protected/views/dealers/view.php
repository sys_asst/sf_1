<?php
/* @var $this DealersController */
/* @var $model Dealers */

$this->breadcrumbs=array(
	'Dealers'=>array('index'),
	$model->dealerName,
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));

?>

<h1>View Dealers <?php echo $model->dealerName; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
        array(
            'label'=>'Dealer NO',
            'value'=>DealerNo::getDealerNo($model->id),
        ),
		'dealerCode',
		'dealerName',
		'phoneOffice',
		'phoneResidence',
        'email',
        'contactPerson',
		'creditDays',
		'creditAmount',
		'address1',
		'address2',
		'address3',
        array(
            'name'=>'townId',
            'value'=>$model->town->town,
        ),
		'latitude',
		'longitude',
        array(
            //'label'=>'dealer NO',
            'name'=>'distributor',
            'value'=>isset($model->distributor0) ? $model->distributor0->distName : 'n/a',
        ),
        //'srcode',
        array(
            'name'=>'srcode',
            'value'=>isset($model->srep) ? $model->srep->srName : 'n/a'
        ),
		'addedOn',
		'lastUpdatedOn',
        array(
            'name'=>'disabled',
            'value'=>CHtml::encode(Dealers::itemAlias('ItemStatus', $model->disabled))
        ),
		'disabled_by',
		'disabled_on',
	),
)); ?>
