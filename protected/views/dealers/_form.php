<?php
/* @var $this DealersController */
/* @var $model Dealers */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'=>'dealers-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation'=>false,
        'type'=>'horizontal',
    )); ?>
	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

    <div class="row-fluid">
        <div class="span5">
            <?php echo $form->textFieldRow($model,'dealerCode',array('class'=>'input-block-level','maxlength'=>10,'size'=>4, 'readonly'=>false)); ?>
            <?php echo $form->textFieldRow($model,'dealerName',array('class'=>'input-block-level','maxlength'=>100,'size'=>60, 'readonly'=>false)); ?>
            <?php echo $form->textFieldRow($model,'email',array('class'=>'input-block-level','size'=>60,'maxlength'=>100)); ?>
            <?php echo $form->textFieldRow($model,'phoneOffice',array('class'=>'input-block-level','maxlength'=>25,'size'=>25, 'readonly'=>false)); ?>
            <?php echo $form->textFieldRow($model,'phoneResidence',array('class'=>'input-block-level','maxlength'=>25,'size'=>25, 'readonly'=>false)); ?>
            <?php echo $form->textFieldRow($model,'contactPerson',array('class'=>'input-block-level','size'=>60,'maxlength'=>255)); ?>
            <?php echo $form->textFieldRow($model,'latitude',array('class'=>'input-block-level','maxlength'=>25,'size'=>25, 'readonly'=>false)); ?>
            <?php echo $form->textFieldRow($model,'longitude',array('class'=>'input-block-level','maxlength'=>25,'size'=>25, 'readonly'=>false)); ?>
            <?php echo $form->dropDownListRow($model, 'srcode', CHtml::listData(Salesreps::model()->findAll('status = 1', array('order'=>'srName asc')), 'srEmpNo','srName'), array('multiple'=>false, 'class'=>'input-block-level','prompt'=>'Select Sales Rep')); ?>
            <?php echo $form->dropDownListRow($model,'disabled',Dealers::itemAlias('ItemStatus'),array('multiple'=>false, 'class'=>'input-block-level','prompt'=>'Select Status')); ?>
        </div>
        <div class="span5">
            <?php echo $form->textFieldRow($model,'creditDays',array('class'=>'input-block-level','size'=>60,'maxlength'=>3)); ?>
            <?php echo $form->textFieldRow($model,'creditAmount',array('class'=>'input-block-level','size'=>20,'maxlength'=>20)); ?>
            <?php echo $form->textAreaRow($model,'address1',array('class'=>'input-block-level','rows'=>5)); ?>
            <?php echo $form->textAreaRow($model,'address2',array('class'=>'input-block-level','rows'=>5)); ?>
            <?php echo $form->dropDownListRow($model, 'townId', CHtml::listData(Towns::model()->findAll('disabled = 0'), 'id','town'), array('multiple'=>false, 'class'=>'input-block-level','prompt'=>'Select Town')); ?>
            <?php echo $form->dropDownListRow($model, 'distributor', CHtml::listData(Distributors::model()->findAll('status = 1', array('order'=>'distName asc')), 'distId','distName'), array('multiple'=>false, 'class'=>'input-block-level')); ?>
            
        </div>
    </div>
    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>$model->isNewRecord ? Yii::t('messages', 'Create') : Yii::t('messages', 'Save'),
        )); ?>

        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'label'=>Yii::t('messages', 'Cancel'),
            'type'=>'info',
            'url'=>Yii::app()->createUrl('dealers/admin')
        )); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->