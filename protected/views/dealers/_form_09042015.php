<?php
/* @var $this DealersController */
/* @var $model Dealers */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'dealers-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'dealerCode'); ?>
		<?php echo $form->textField($model,'dealerCode',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'dealerCode'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dealerName'); ?>
		<?php echo $form->textField($model,'dealerName',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'dealerName'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'phoneOffice'); ?>
		<?php echo $form->textField($model,'phoneOffice',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'phoneOffice'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'phoneResidence'); ?>
		<?php echo $form->textField($model,'phoneResidence'); ?>
		<?php echo $form->error($model,'phoneResidence'); ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($model,'email'); ?>
        <?php echo $form->textField($model,'email'); ?>
        <?php echo $form->error($model,'email'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'contactPerson'); ?>
        <?php echo $form->textField($model,'contactPerson'); ?>
        <?php echo $form->error($model,'contactPerson'); ?>
    </div>

	<div class="row">
		<?php echo $form->labelEx($model,'creditDays'); ?>
		<?php echo $form->textField($model,'creditDays'); ?>
		<?php echo $form->error($model,'creditDays'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'creditAmount'); ?>
		<?php echo $form->textField($model,'creditAmount'); ?>
		<?php echo $form->error($model,'creditAmount'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'address1'); ?>
		<?php echo $form->textArea($model,'address1',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'address1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'address2'); ?>
		<?php echo $form->textArea($model,'address2',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'address2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'address3'); ?>
		<?php echo $form->textArea($model,'address3',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'address3'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'townId'); ?>
		<?php echo $form->textArea($model,'townId',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'townId'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'latitude'); ?>
		<?php echo $form->textField($model,'latitude'); ?>
		<?php echo $form->error($model,'latitude'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'longitude'); ?>
		<?php echo $form->textField($model,'longitude'); ?>
		<?php echo $form->error($model,'longitude'); ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($model,'distributor'); ?>
        <?php
        $arroptions = CHtml::listData(Distributors::model()->findAll('status = 1'), 'distId','distName');
        //print_r($arroptions);
        echo $form->dropDownList($model,'distributor',$arroptions,array('empty'=>'Select Distributor')); ?>
        <?php echo $form->error($model,'distributor'); ?>
    </div>

	<!-- <div class="row">
		<?php //echo $form->labelEx($model,'distributor'); ?>
		<?php //echo $form->textField($model,'distributor',array('size'=>50,'maxlength'=>50)); ?>
		<?php //echo $form->error($model,'distributor'); ?>
	</div>-->
    <?php /*
	<div class="row">
		<?php echo $form->labelEx($model,'addedOn'); ?>
		<?php echo $form->textField($model,'addedOn'); ?>
		<?php echo $form->error($model,'addedOn'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'lastUpdatedOn'); ?>
		<?php echo $form->textField($model,'lastUpdatedOn'); ?>
		<?php echo $form->error($model,'lastUpdatedOn'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'disabled'); ?>
		<?php echo $form->textField($model,'disabled'); ?>
		<?php echo $form->error($model,'disabled'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'disabled_by'); ?>
		<?php echo $form->textField($model,'disabled_by'); ?>
		<?php echo $form->error($model,'disabled_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'disabled_on'); ?>
		<?php echo $form->textField($model,'disabled_on'); ?>
		<?php echo $form->error($model,'disabled_on'); ?>
	</div>
*/ ?>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->