<?php
/* @var $this DealersController */
/* @var $model Dealers */

$this->breadcrumbs=array(
	'Dealers'=>array('index'),
	$model->dealerName=>array('view','id'=>$model->id),
	'Update',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>Update Dealers <?php echo $model->dealerName; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>