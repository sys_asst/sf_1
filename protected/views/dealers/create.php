<?php
/* @var $this DealersController */
/* @var $model Dealers */

$this->breadcrumbs=array(
	'Dealers'=>array('index'),
	'Create',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>Create Dealers</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>