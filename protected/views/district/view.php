<?php
/* @var $this DistrictController */
/* @var $model District */

$this->breadcrumbs=array(
	'Districts'=>array('index'),
	$model->description,
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>View District <?php echo $model->description; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
        array(
            'name'=>'province_id',
            'value'=>CHtml::encode($model->province->description)
        ),
		'code',
		'description',
        array(
            'name'=>'added_by',
            'value'=>CHtml::encode($model->addedBy->username)
        ),
		'added_on',
        array(
            'name'=>'status',
            'value'=>CHtml::encode(Country::itemAlias('ItemStatus', $model->status))
        ),
		'deleted_on',
	),
)); ?>
