<?php
/* @var $this DistrictController */
/* @var $model District */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'district-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
    'type'=>'horizontal',
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

    <div class="row-fluid">
        <div class="span6">
            <?php $dataArray = CHtml::listData(Provinces::model()->findAll(), 'id','description');
            echo $form->dropDownListRow($model,'province_id',$dataArray); ?>
            <?php echo $form->textFieldRow($model,'code',array('size'=>10,'maxlength'=>10)); ?>

        </div>
        <div class="span6">
            <?php echo $form->textFieldRow($model,'description',array('size'=>10,'maxlength'=>255)); ?>
            <?php echo $form->dropDownListRow($model,'status',Country::itemAlias('ItemStatus')); ?>
        </div>
    </div>



    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>$model->isNewRecord ? Yii::t('messages', 'Create') : Yii::t('messages', 'Save'),
        )); ?>

        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'label'=>Yii::t('messages', 'Cancel'),
            'type'=>'info',
            'url'=>Yii::app()->createUrl('district/admin')
        )); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->