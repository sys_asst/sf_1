<?php
/* @var $this TownsController */
/* @var $model Towns */

$this->breadcrumbs=array(
	'Towns'=>array('index'),
	'Manage',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#towns-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Towns</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
	'id'=>'towns-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
        array(
            'name'=>'province_id',
            'value'=>'$data->province0->description',
            'filter' => CHtml::listData(Provinces::model()->findAll(), 'id','description'),
        ),
        array(
            'name'=>'district_id',
            'value'=>'$data->district0->description',
            'filter' => $model->province_id ? CHtml::listData(District::model()->findAllByAttributes(
                    array(),
                    "province_id = :province_id",
                    array(':province_id'=>$model->province_id)),
                'id','description') : CHtml::listData(District::model()->findAll(array('order'=>'description')), 'id','description'),
        ),
		//'dsDivision',
                array(
                    'name'=>'ds_divid',
                    'value'=>'$data->dsDiv->name',
                    'filter' =>  $model->district_id ? CHtml::listData(Dsdivisions::model()->findAllByAttributes(
                    array(),
                    "district_id = :district_id",
                    array(':district_id'=>$model->district_id)),
                'id','name') : CHtml::listData(Dsdivisions::model()->findAll(array('order'=>'name')), 'id','name'),
                ),
		'town',
		'addedOn',
        array(
            'name'=>'status',
            'value'=>'Towns::itemAlias("ItemStatus",$data->status)',
            'filter' => Towns::itemAlias("ItemStatus"),
        ),
		/*
		'lastUpdatedOn',
		'disabled',
		'disabled_by',
		'disabled_on',
		'province_id',
		'district_id',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'template' => '{view}{update}',
		),
	),
)); ?>
