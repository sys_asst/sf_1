<?php
/* @var $this TownsController */
/* @var $model Towns */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'towns-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
    'type'=>'horizontal',
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

    <div class="row-fluid">
        <div class="span6">
            <?php
            $dataArray = CHtml::listData(Provinces::model()->findAll('status=:dis',array(':dis'=>1)), 'id','description');
            echo $form->dropDownListRow($model,'province_id',$dataArray); ?>
            <?php
            $dataArray = CHtml::listData(Dsdivisions::model()->findAll('status=:dis',array(':dis'=>1)), 'id','name');
            echo $form->dropDownListRow($model,'ds_divid',$dataArray); ?>
            <?php //echo $form->textFieldRow($model,'dsDivision',array('size'=>10,'maxlength'=>255)); ?>
            <?php echo $form->dropDownListRow($model,'status',Towns::itemAlias('ItemStatus')); ?>
        </div>
        <div class="span6">
            <?php $dataArray = CHtml::listData(District::model()->findAll(array('condition'=>'status=:dis','order'=>'description','params'=>array(':dis'=>1))), 'id','description');
            echo $form->dropDownListRow($model,'district_id',$dataArray, array(
            //'empty'=>'Select Category',
                //'prompt'=>'Select Main Category',
                'ajax'=>array(
                'type'=>'POST', // request type
                'url'=>CController::createUrl('getdsdivisions'),
                'update'=>'#Towns_ds_divid', // selector to update
                'data'=>array('district_id'=>'js:this.value'),
            )
            ));?>
            <?php echo $form->textFieldRow($model,'town',array('size'=>10,'maxlength'=>255)); ?>
        </div>
    </div>

    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>$model->isNewRecord ? Yii::t('messages', 'Create') : Yii::t('messages', 'Save'),
        )); ?>

        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'label'=>Yii::t('messages', 'Cancel'),
            'type'=>'info',
            'url'=>Yii::app()->createUrl('towns/admin')
        )); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->