<?php
/* @var $this TownsController */
/* @var $model Towns */

$this->breadcrumbs=array(
	'Towns'=>array('index'),
	'Create',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>Create Towns</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>