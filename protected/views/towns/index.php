<?php
/* @var $this TownsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Towns',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>Towns</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
