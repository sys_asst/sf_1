<?php
/* @var $this TownsController */
/* @var $model Towns */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'province'); ?>
		<?php echo $form->textArea($model,'province',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'district'); ?>
		<?php echo $form->textArea($model,'district',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dsDivision'); ?>
		<?php echo $form->textArea($model,'dsDivision',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'town'); ?>
		<?php echo $form->textArea($model,'town',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'addedOn'); ?>
		<?php echo $form->textField($model,'addedOn'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'lastUpdatedOn'); ?>
		<?php echo $form->textField($model,'lastUpdatedOn'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'disabled'); ?>
		<?php echo $form->textField($model,'disabled'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'disabled_by'); ?>
		<?php echo $form->textField($model,'disabled_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'disabled_on'); ?>
		<?php echo $form->textField($model,'disabled_on'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'province_id'); ?>
		<?php echo $form->textField($model,'province_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'district_id'); ?>
		<?php echo $form->textField($model,'district_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->