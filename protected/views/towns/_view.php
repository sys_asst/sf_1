<?php
/* @var $this TownsController */
/* @var $data Towns */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('province')); ?>:</b>
	<?php echo CHtml::encode($data->province); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('district')); ?>:</b>
	<?php echo CHtml::encode($data->district); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dsDivision')); ?>:</b>
	<?php echo CHtml::encode($data->dsDivision); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('town')); ?>:</b>
	<?php echo CHtml::encode($data->town); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('addedOn')); ?>:</b>
	<?php echo CHtml::encode($data->addedOn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lastUpdatedOn')); ?>:</b>
	<?php echo CHtml::encode($data->lastUpdatedOn); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('disabled')); ?>:</b>
	<?php echo CHtml::encode($data->disabled); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('disabled_by')); ?>:</b>
	<?php echo CHtml::encode($data->disabled_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('disabled_on')); ?>:</b>
	<?php echo CHtml::encode($data->disabled_on); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('province_id')); ?>:</b>
	<?php echo CHtml::encode($data->province_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('district_id')); ?>:</b>
	<?php echo CHtml::encode($data->district_id); ?>
	<br />

	*/ ?>

</div>