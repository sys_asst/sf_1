<?php
/* @var $this TownsController */
/* @var $model Towns */

$this->breadcrumbs=array(
	'Towns'=>array('index'),
	$model->town,
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>View Towns <?php echo $model->town; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'province',
		'district',
		//'dsDivision',
        array(
            'name'=>'ds_divid',
            'value'=>CHtml::encode($model->dsDiv->name)
        ),
		'town',
		'addedOn',
		'lastUpdatedOn',
        array(
            'name'=>'disabled',
            'value'=>$model->disabled ? 'yes' : 'no',
        ),
        array(
            'name'=>'status',
            'value'=>CHtml::encode(Country::itemAlias('ItemStatus', $model->status))
        ),
		'disabled_by',
		'disabled_on',
        array(
            'name'=>'province_id',
            'value'=>CHtml::encode($model->province0->description)
        ),
        array(
            'name'=>'district_id',
            'value'=>CHtml::encode($model->district0->description)
        ),
	),
)); ?>
