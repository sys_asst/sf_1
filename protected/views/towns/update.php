<?php
/* @var $this TownsController */
/* @var $model Towns */

$this->breadcrumbs=array(
	'Towns'=>array('index'),
	$model->town=>array('view','id'=>$model->id),
	'Update',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>Update Towns <?php echo $model->town; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>