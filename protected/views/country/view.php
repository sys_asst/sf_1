<?php
/* @var $this CountryController */
/* @var $model Country */

$this->breadcrumbs=array(
	'Countries'=>array('index'),
	$model->description,
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>View Country <?php echo $model->description; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'code',
		'description',
        array(
            'name'=>'added_by',
            'value'=>CHtml::encode($model->addedBy->username)
        ),
		'added_on',
        array(
            'name'=>'deleted',
            'value'=>CHtml::encode(Country::itemAlias('ItemStatus', $model->deleted))
        ),
		'deleted_on',
	),
)); ?>
