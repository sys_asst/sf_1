<?php
/* @var $this ProvincesController */
/* @var $model Provinces */

$this->breadcrumbs=array(
	'Provinces'=>array('index'),
	$model->description,
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>View Provinces <?php echo $model->description; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
        array(
            'name'=>'country_id',
            'value'=>CHtml::encode($model->country->description)
        ),
		'code',
		'description',
        array(
            'name'=>'added_by',
            'value'=>CHtml::encode($model->addedBy->username)
        ),
		'added_on',
        array(
            'name'=>'status',
            'value'=>CHtml::encode(Provinces::itemAlias('ItemStatus', $model->status))
        ),
		'deleted_on',
	),
)); ?>
