<?php
/* @var $this ProvincesController */
/* @var $model Provinces */

$this->breadcrumbs=array(
	'Provinces'=>array('index'),
	'Create',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>Create Provinces</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>