<?php
/* @var $this InvoiceController */
/* @var $model Invoice */

$this->breadcrumbs = array(
    'Invoices' => array('index'),
    $model->id,
);

$this->menu = array(
//	array('label'=>'List Invoice', 'url'=>array('index')),
//	array('label'=>'Create Invoice', 'url'=>array('create')),
//	array('label'=>'Update Invoice', 'url'=>array('update', 'id'=>$model->id)),
//	array('label'=>'Delete Invoice', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
    array('label' => 'Manage Invoice', 'url' => array('admin')),
);
?>

<h1>View Invoice #<?php echo $model->id; ?></h1>

<?php
$this->widget('bootstrap.widgets.TbDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
        array(
            'name' => 'distributor_id',
            'value' => CHtml::encode(isset($model->distributors->distName) ? $model->distributors->distName : 'n/a'),
        ),
        'orders_id',
        'invoice_no',
        'createdtime',
    ),
));
?>

<h4>Manage Invoice Items</h4>

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'type' => 'striped bordered condensed',
    'id' => 'invoice-items-grid',
    'htmlOptions' => array('style' => 'padding-top:0px'),
    'dataProvider' => $model1->search(),
    'columns' => array(
        array(
            'name' => 'products_id',
            'value' => 'isset($data->products->description) ? $data->products->description : "n/a"',
        ),
        'quantity',
        'pending_quantity',
        'createdtime',
//		'createdby',
    /*
      'createdtime',
      'updatedby',
      'updatedtime',
     */
    ),
));
?>

