<?php
/* @var $this InvoiceController */
/* @var $model Invoice */

$this->breadcrumbs=array(
	'Invoices'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Invoice', 'url'=>array('/orders/admin')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#invoice-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Invoices</h1>
<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
	'id'=>'invoice-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
//		'id',
               'invoice_no',
//             'orders_id',
             array(
            'filter'=>FALSE,
            'name'=>'orders_id',
            'value'=>'isset($data->orders_id) ? OrderNos::getOrderNo($data->orders_id) : ""',
            'htmlOptions'=>array('width'=>'200'),
        ),
             array(
            'name'=>'distributor_id',
            'value'=>'isset(Distributors::model()->findByAttributes(array("distId"=>$data->distributor_id))->distName) ? Distributors::model()->findByAttributes(array("distId"=>$data->distributor_id))->distName : "n/a"',
            'filter' => CHtml::listData(Distributors::model()->findAll(), 'distId','distName'),
            'htmlOptions'=>array('width'=>'200'),
        ),
//		'distributor_id',
		
//		'status_id',
		'createdtime',
//		'createdby',
		/*
		'updatedtime',
		'updatedby',
		*/
            
             array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
//            'htmlOptions'=>'width:20',
//            'htmlOptions'=>array('style'=>'width: 100px; text-align:center'),
            'template' => '{view}',
            'buttons' => array(
                'view' => array( //the name {reply} must be same
                    'label' => 'View', // text label of the button user/admin/create&refid=68&src=sref
                    'url' => 'CHtml::normalizeUrl(array("/orders/viewinvoice?id=".$data->id))', //Your URL According to your wish
                    //'imageUrl' => Yii::app()->baseUrl . '/images/user_add.png', // image URL of the button. If not set or false, a text link is used, The image must be 16X16 pixels
                ),
            ),

        ),
//        array(
//            'class'=>'bootstrap.widgets.TbButtonColumn',
//            'template' => '{view}'
//        )
	),
)); ?>
