<?php
/* @var $this DealercommentreasonsController */
/* @var $model Dealercommentreasons */

$this->breadcrumbs=array(
	'Comment Reasons'=>array('index'),
	$model->reason=>array('view','id'=>$model->id),
	'Update',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>Update Comment Reasons <?php echo $model->reason; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>