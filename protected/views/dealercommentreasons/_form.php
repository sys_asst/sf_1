<?php
/* @var $this DealercommentreasonsController */
/* @var $model Dealercommentreasons */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'dealercommentreasons-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
    'type'=>'horizontal',
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

    <div class="row-fluid">
		<div class="span8">
            <?php echo $form->textAreaRow($model,'reason',array('class'=>'input-block-level', 'rows'=>6)); ?>
		</div>
	</div>

	<!--<div class="row">
		<?php //echo $form->labelEx($model,'lastUpdatedOn'); ?>
		<?php //echo $form->textField($model,'lastUpdatedOn'); ?>
		<?php //echo $form->error($model,'lastUpdatedOn'); ?>
	</div>-->

    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>$model->isNewRecord ? Yii::t('messages', 'Create') : Yii::t('messages', 'Save'),
        )); ?>

        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'label'=>Yii::t('messages', 'Cancel'),
            'type'=>'info',
            'url'=>Yii::app()->createUrl('dealercommentreasons/admin')
        )); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->