<?php
/* @var $this DealercommentreasonsController */
/* @var $model Dealercommentreasons */

$this->breadcrumbs=array(
	'Comment Reasons'=>array('index'),
	'Create',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>Create Comment Reasons</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>