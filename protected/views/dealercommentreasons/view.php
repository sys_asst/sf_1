<?php
/* @var $this DealercommentreasonsController */
/* @var $model Dealercommentreasons */

$this->breadcrumbs=array(
	'Dealercommentreasons'=>array('index'),
	$model->reason,
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>View Comment Reasons <?php echo $model->reason; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'reason',
		'lastUpdatedOn',
	),
)); ?>
