<?php
/* @var $this DealercommentreasonsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Comment Reasons',
);

$this->menu=array(
	array('label'=>'Create Comment Reasons', 'url'=>array('create')),
	array('label'=>'Manage Comment Reasons', 'url'=>array('admin')),
);
?>

<h1>Comment Reasons</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
