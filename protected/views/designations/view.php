<?php
/* @var $this DesignationsController */
/* @var $model Designations */

$this->breadcrumbs=array(
	'Designations'=>array('index'),
	$model->name,
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>View Designations <?php echo $model->name; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'code',
		'name',
        array(
            'name'=>'status',
            'value'=>CHtml::encode(Designations::itemAlias('ItemStatus', $model->status))
        ),
		'added_on',
        array(
            'name'=>'added_by',
            'value'=>CHtml::encode($model->addedBy->username)
        ),
	),
)); ?>
