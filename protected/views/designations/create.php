<?php
/* @var $this DesignationsController */
/* @var $model Designations */

$this->breadcrumbs=array(
	'Designations'=>array('index'),
	'Create',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>Create Designations</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>