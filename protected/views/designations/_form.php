<?php
/* @var $this DesignationsController */
/* @var $model Designations */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'designations-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
    'type'=>'horizontal',
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
    <div class="row-fluid">
        <div class="span5">
            <?php echo $form->textFieldRow($model,'code',array('class'=>'','maxlength'=>5,'size'=>4, 'readonly'=>false)); ?>
            <?php echo $form->dropDownListRow($model, 'status', Designations::itemAlias('ItemStatus'), array('multiple'=>false, 'class'=>'')); ?>
        </div>
        <div class="span5">
            <?php echo $form->textFieldRow($model,'name',array('class'=>'input-block-level','size'=>60,'maxlength'=>255)); ?>
        </div>
    </div>

    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>$model->isNewRecord ? Yii::t('messages', 'Create') : Yii::t('messages', 'Save'),
        )); ?>

        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'label'=>Yii::t('messages', 'Cancel'),
            'type'=>'info',
            'url'=>Yii::app()->createUrl('designation/admin')
        )); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->