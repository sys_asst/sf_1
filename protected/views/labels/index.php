<?php
/* @var $this LabelsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Labels',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>Labels</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
