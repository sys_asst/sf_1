<?php
/* @var $this LabelsController */
/* @var $model Labels */

$this->breadcrumbs=array(
	'Labels'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>Update Labels <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>