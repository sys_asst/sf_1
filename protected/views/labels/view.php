<?php
/* @var $this LabelsController */
/* @var $model Labels */

$this->breadcrumbs=array(
	'Labels'=>array('index'),
	$model->id,
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>View Labels #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'label',
		'status',
	),
)); ?>
