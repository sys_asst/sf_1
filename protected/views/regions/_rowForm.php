<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Randika
 * Date: 7/1/14
 * Time: 9:35 AM
 * To change this template use File | Settings | File Templates.
 */

$row_id = "sladetail-" . $key ?>

<div class='row-fluid' id="<?php echo $row_id ?>">
    <?php
    echo $form->hiddenField($model, "[$key]region_id");
    echo $form->updateTypeField($model, $key, "region_id", array('key' => $key));
    ?>
    <table border="0">
        <tr>
            <td>
                <div class="span3">
                    <?php
                    echo $form->labelEx($model, "[$key]area_id");
                    $leaflevelattrbutes = CHtml::listData(Area::model()->findAll('status > 0 ORDER BY name'), 'id','name');
                    echo $form->dropDownList($model, "[$key]area_id",$leaflevelattrbutes,array('prompt'=>'Select Area'));
                    echo $form->error($model, "[$key]area_id");
                    ?>
                </div>
            </td>

            <td>

                <div class="span2">

                    <?php echo $form->deleteRowButton($row_id, $key); ?>
                </div>
            </td>
        </tr>
    </table>
</div>