<?php
/* @var $this RegionsController */
/* @var $model Regions */
/* @var $form CActiveForm */
?>

<div class="form">

<?php //$form=$this->beginWidget('CActiveForm', array(
	//'id'=>'regions-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	//'enableAjaxValidation'=>false,
//)); ?>
    <?php $form=$this->beginWidget('DynamicTabularForm', array(
        'defaultRowView'=>'_rowForm',
        'type'=>'horizontal',
    )); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row-fluid">
        <div class="span6">
            <?php echo $form->textFieldRow($model,'code',array('size'=>10,'maxlength'=>10)); ?>
            <?php
            if($model->isNewRecord)
            {
                $leaflevelattrbutes = CHtml::listData(Salesreps::model()->findAll(array('condition'=>'status=1 AND srDesignation="Regional Customer Manager" AND id NOT IN (SELECT R.`rcm_id` FROM `ms_regions` AS R GROUP BY R.`rcm_id`)','order'=>'srName')), 'id','srName');
            } else {
                $leaflevelattrbutes = CHtml::listData(Salesreps::model()->findAll(array('condition'=>'status=1 AND srDesignation="Regional Customer Manager"','order'=>'srName')), 'id','srName');
            }

            echo $form->dropDownListRow($model,'rcm_id',$leaflevelattrbutes,array('prompt'=>'Select Regional Customer Manager')); ?>
        </div>
		<?php //echo $form->labelEx($model,'code'); ?>

		<?php //echo $form->error($model,'code'); ?>
        <div class="span6">
            <?php echo $form->textFieldRow($model,'name',array('size'=>60,'maxlength'=>255)); ?>
            <?php echo $form->dropDownListRow($model,'status',Regions::itemAlias('ItemStatus')); ?>
        </div>
	</div>

    <fieldset class="label_data" style="display: <?php echo ($model->scenario == 'update')? 'block' : 'block';?>">
        <legend>Area Details</legend>
        <?php
        //echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save');
        $max_leafs = 25;
        echo $form->rowForm($details, null, array(), $max_leafs);
        ?>
    </fieldset>

    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>$model->isNewRecord ? Yii::t('messages', 'Create') : Yii::t('messages', 'Save'),
        )); ?>

        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'label'=>Yii::t('messages', 'Cancel'),
            'type'=>'info',
            'url'=>Yii::app()->createUrl('regions/admin')
        )); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->