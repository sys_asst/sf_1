<?php
/* @var $this RegionsController */
/* @var $model Regions */

$this->breadcrumbs=array(
	'Regions'=>array('index'),
	'Manage',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#regions-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Regions</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
	'id'=>'regions-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
		'code',
		'name',
        array(
            'name'=>'status',
            'value'=>'Regions::itemAlias("ItemStatus",$data->status)',
            'filter' => Regions::itemAlias("ItemStatus"),
        ),
        array(
            'name'=>'rcm_id',
            'value'=>'$data->rcm->srName',
            'filter' => CHtml::listData(Salesreps::model()->findAll(array('condition'=>'status=1 AND srDesignation="Regional Customer Manager"','order'=>'srName')),'id','srName'),
        ),
        array(
            'name'=>'added_by',
            'value'=>'$data->addedBy->username',
            'filter' => CHtml::listData(User::model()->findAll(array('condition'=>'status=1','order'=>'username')),'id','username'),
        ),
		/*
		'added_on',
		'updated_by',
		'updated_on',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'template'=>'{view}{update}'
		),
	),
)); ?>
