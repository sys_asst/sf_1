<?php
/* @var $this RegionsController */
/* @var $model Regions */

$this->breadcrumbs=array(
	'Regions'=>array('index'),
	$model->name,
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>View Regions <?php echo $model->name; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'code',
		'name',
        array(
            'label'=>'Areas',
            'value'=>CHtml::encode($model->getAreas($model->id))
        ),
        array(
            'name'=>'status',
            'value'=>CHtml::encode(Regions::itemAlias('ItemStatus', $model->status))
        ),
        array(
            'name'=>'rcm_id',
            'value'=>CHtml::encode($model->rcm->srName)
        ),
        array(
            'name'=>'added_by',
            'value'=>CHtml::encode($model->addedBy->username)
        ),
		'added_on',
        /*array(
            'name'=>'updated_by',
            'value'=>CHtml::encode($model->updatedBy->username)
        ),*/
		'updated_on',
	),
)); ?>
