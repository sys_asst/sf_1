<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Randika
 * Date: 7/8/14
 * Time: 9:40 AM
 * To change this template use File | Settings | File Templates.
 */
    //Tk::a($details);echo 'wwwww';
    $connection = Yii::app()->db;
        $sql = "SELECT DATEDIFF(DATE_ADD(DATE_FORMAT('{$month}', '%Y-%m-01'), INTERVAL 1 MONTH), DATE_FORMAT('{$month}', '%Y-%m-01')) AS `dates`,
            DATE_FORMAT('{$month}', '%Y-%m-') AS `month`";

        $command = $connection->createCommand($sql);
        $row = $command->queryRow();
        $dates = $row['dates'];
        $month = $row['month'];
        $class = '';
        $leaflevelattrbutes = CHtml::listData(Route::model()->findAll('status > 0 AND area_id IN(SELECT SR.area_id FROM salesreps AS SR WHERE SR.id = "'.$rep_id.'") ORDER BY name'), 'id','name');

        $sql = "";
        //echo '<div id="close">Close</div>';
        echo '<table border="0" class="items table table-striped table-bordered table-condensed" style="width: 80%"><thead>';
        echo '<tr class="'.$class.'"><th>Date</th><th>Route</th><th style="width:100px;">No of Dealers</th><th></th></thead></tr>';
        for($i =1; $i <= $dates; $i++)
        {
            $class = $class == 'odd' ? 'even' : 'odd';
            echo '<tr class="'.'">';
            ?>
                <td><?php
                    if(isset($form))
                    {
                        echo $form->textFieldRow($details[$i-1],"[$i]added_date",array('maxlength'=>10,'readonly'=>true,'class'=>'span2'));
                    } else {
                        echo @CHtml::textField('DRouteMap['.($i-1).'][added_date]',(strlen($data[($i-1)]['added_date'])) ? $data[($i-1)]['added_date'] : date('Y-m-d', strtotime(date($month.$i))),array('maxlength'=>10,'readonly'=>true,'class'=>'span2'));
                    }
                    ?></td>
                <td><?php
                    //echo $form->labelEx($model, "[$key]route_id");
                    if(isset($form))
                    {
                        echo $form->dropDownListRow($details[$i-1], "[$i]route_id",$leaflevelattrbutes,array('multiple'=>true,'size'=>5,'prompt'=>'Select Route',
                            'ajax'=>array(
                                'type'=>'POST',
                                'url'=>CController::createUrl('GetDealers'),
                                //'update'=>'$(this).find(\'~ span\').text(\'hi\')',
                                'success'=>"js:function(data){ $('#my_{$i}').text(data); }",
                                //'update'=>"js:alert('hi')",
                                'data'=>array('rout_id'=>'js:$(this).val().join(",")','class'=>'span3')
                            )));
                        echo $form->error($details[$i-1], "[$i]route_id");
                    } else {
                        echo @CHtml::dropDownList('DRouteMap['.($i-1).'][route_id]', (strlen($data[($i-1)]['route_id']) ? $data[($i-1)]['route_id'] : $details[($i-1)]),$leaflevelattrbutes,array('multiple'=>true,'size'=>5,'prompt'=>'Select Route',
                            'ajax'=>array(
                                'type'=>'POST',
                                'url'=>CController::createUrl('GetDealers'),
                                //'update'=>'$(this).find(\'~ span\').text(\'hi\')',
                                'success'=>"js:function(data){ $('#my_{($i-1)}').text(data); }",
                                //'update'=>"js:alert('hi')",
                                'data'=>array('rout_id'=>'js:$(this).val().join(",")')
                            )));
                    }
                    //echo CHtml::error('DRouteMap','DRouteMap_'.$i.'_route_id');
                    //print_r($details[$i-1]);
                    //Tk::a($details[$i-1]['route_id']);
                    ?>
                </td>
                <td><span id="my_<?php echo $i?>">
                        <?php
                            if(isset($details[$i-1]['route_id']) && sizeof($details[$i-1]['route_id']))
                            {
                                $dealers = isset($details[$i-1]['route_id']) && sizeof($details[$i-1]['route_id']) ? implode(',',$details[$i-1]['route_id']) : '';
                                $records = strlen($dealers) ? Route::getDealers($dealers) : null;
                                $no_dealers = sizeof($records);
                                echo $no_dealers ? $no_dealers : '';
                            }
                        ?>
                    </span></td>
            <td>
                <?php
                if(isset($form))
                {
                    echo $form->dropDownListRow($details[$i-1], "[$i]day",RouteMap::itemAlias('Day'),array('multiple'=>false));
                    echo $form->error($details[$i-1], "[($i-1)]day");
                } else {
                    echo @CHtml::dropDownList('DRouteMap['.($i-1).'][day]', (strlen($details[($i-1)]['day']) ? $details[($i-1)]['day'] : $details[($i-1)]),RouteMap::itemAlias('Day'),array('multiple'=>false));
                }
                ?>
            </td>
            <?php
            echo '</tr>';
        }
        echo '</table>';
/*Yii::app()->clientScript->registerScript('$("button").click(function(){
    $("p").toggle();
})');*/
?>