<?php
/* @var $this RouteMapController */
/* @var $model RouteMap */

$this->breadcrumbs=array(
	'Route Maps'=>array('index'),
	'Create',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>Create Route Map</h1>

<?php $this->renderPartial('_form', array('model'=>$model,'details'=>$details, 'entered'=>$entered)); ?>