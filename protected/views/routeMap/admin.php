<?php
/* @var $this RouteMapController */
/* @var $model RouteMap */

$this->breadcrumbs=array(
	'Route Maps'=>array('index'),
	'Manage',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#route-map-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");

$records = array();
if(Yii::app()->getModule('user')->isAdmin() == false)
{
    $sql = 'SELECT IFNULL(DR.`area_id`,0) AS `area_id` FROM `ms_regions` AS R INNER JOIN `d_regions` AS DR ON DR.`region_id` = R.`id` AND R.`rcm_id`
                IN(SELECT S.`id` FROM `salesreps` AS S WHERE S.`srEmail` IN("'.Yii::app()->user->getState('src')->srEmail.'"))';
    $records = Tk::sql($sql);
}
$areas = '';
//Tk::a($records);
if(sizeof($records) && Yii::app()->user->profile->src == 2)
{
    foreach($records as $rec)
    {
        $areas .= (strlen($areas)) ? ','.$rec['area_id'] : $rec['area_id'];
    }
}
?>

<h1>Manage Route Maps</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
	'id'=>'route-map-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
		'code',
		'name',
        array(
            'name'=>'rep_id',
            'value'=>'$data->rep->srName',
            'filter' => CHtml::listData(Salesreps::model()->findAll(array('condition'=>'status=:dis '.(strlen($areas) ? " AND area_id IN({$areas})" : ''),'order'=>'srName','params'=>array(':dis'=>1))), 'id','srName'),
        ),
        array(
            'name'=>'assigned_by',
            'value'=>'$data->assignedBy->email',
            'filter' => CHtml::listData(User::model()->findAll(), 'id','username'),
        ),
		'added_date',
        array(
            'name'=>'status',
            'value'=>'RouteMap::itemAlias("ItemStatus",$data->status)',
            'filter' => RouteMap::itemAlias("ItemStatus"),
        ),
		/*
		'status',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'template'=>'{view}{update}'
		),
	),
)); ?>
