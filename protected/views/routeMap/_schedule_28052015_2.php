<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Randika
 * Date: 7/8/14
 * Time: 9:40 AM
 * To change this template use File | Settings | File Templates.
 */
    //Tk::a($data);echo 'wwwww';
    $connection = Yii::app()->db;
        $sql = "SELECT DATEDIFF(DATE_ADD(DATE_FORMAT('{$month}', '%Y-%m-01'), INTERVAL 1 MONTH), DATE_FORMAT('{$month}', '%Y-%m-01')) AS `dates`,
            DATE_FORMAT('{$month}', '%Y-%m-') AS `month`";

        $command = $connection->createCommand($sql);
        $row = $command->queryRow();
        $dates = $row['dates'];
        $month = $row['month'];
        $class = '';
        $leaflevelattrbutes = CHtml::listData(Route::model()->findAll('status > 0 AND area_id IN(SELECT SR.area_id FROM salesreps AS SR WHERE SR.id = "'.$rep_id.'") ORDER BY name'), 'id','name');

        $sql = "";
        //echo '<div id="close">Close</div>';
        echo '<table border="0" class="items table table-striped table-bordered table-condensed" style="width: 80%"><thead>';
        echo '<tr class="'.$class.'"><th>Date</th><th>Route</th><th class="span2">No of Dealers</th><th>Type Of Day</th></thead></tr>';
        for($i =1; $i <= $dates; $i++)
        {
            $class = $class == 'odd' ? 'even' : 'odd';
            echo '<tr class="'.'">';
            ?>
                <td><?php
                    if(isset($form))
                    {
                        //echo $form->textFieldRow($details[$i-1],"[$i]added_date",array('maxlength'=>10,'readonly'=>true,'class'=>'span2'));
                        echo @CHtml::textField('DRouteMap['.($i-1).'][added_date]', $details[($i-1)]['added_date'],array('readonly'=>true,'htmlOptions'=>'width:50px;'));
                    } else {
                        echo @CHtml::textField('DRouteMap['.($i-1).'][added_date]',(strlen($data[($i-1)]['added_date'])) ? $data[($i-1)]['added_date'] : date('Y-m-d', strtotime(date($month.$i))),array('maxlength'=>10,'readonly'=>true,'htmlOptions'=>'width:50px;'));
                    }
                    //echo '<span>'.(isset($details[($i-0)]['added_date']) ? date('l', strtotime($details[($i-0)]['added_date'])) : (isset($data[($i-0)]['added_date'])) ? date('l', strtotime($data[($i-0)]['added_date'])) : date('l', strtotime(date($month.$i)))).'</span>';
                    $day = '';
                    if(isset($details[($i-1)]['added_date']))
                    {
                        $day = date('l', strtotime($details[($i-1)]['added_date']));
                    } elseif(isset($data[($i-1)]['added_date'])) {
                        $day = date('l', strtotime($data[($i-1)]['added_date']));
                    } else {
                        $day = date('l', strtotime(date($month.$i)));
                    }
                    echo '<span>'.$day.'</span>';
                    //isset($data[($i-0)]['added_date']) ? Tk::a($data[($i-0)]['added_date']) : '';
                    ?></td>
                <td><?php
                    //echo $form->labelEx($model, "[$key]route_id");
                    if(isset($form))
                    {
                        //echo $form->dropDownListRow($details[$i-1], "[$i]route_id",$leaflevelattrbutes,array('multiple'=>true,'size'=>5,'prompt'=>'Select Route',
                        echo @CHtml::dropDownList('DRouteMap['.($i-1).'][route_id][]', $details[($i-1)]['route_id'],$leaflevelattrbutes,array('class'=>'span5','multiple'=>true,'size'=>5,'prompt'=>'Select Route',
                            'ajax'=>array(
                                'type'=>'POST',
                                'url'=>CController::createUrl('GetDealers'),
                                'success'=>"js:function(data){ $('#my_".($i-1)."').text(data); }",
                                'data'=>array('rout_id'=>'js:$(this).val().join(",")','class'=>'span3')
                            )));
                        //Tk::a($details[($i-1)]['route_id']);
                        //echo $form->error($details[$i-1], "[$i]route_id");
                    } else {
                        echo @CHtml::dropDownList('DRouteMap['.($i).'][route_id]', (isset($data[($i)]['route_id']) ? $data[($i)]['route_id'] : ''),$leaflevelattrbutes,array('class'=>'span5','multiple'=>true,'size'=>5,'prompt'=>'Select Route',
                            'ajax'=>array(
                                'type'=>'POST',
                                'url'=>CController::createUrl('GetDealers'),
                                'success'=>"js:function(data){ $('#my_{$i}').text(data); }",
                                'data'=>array('rout_id'=>'js:$(this).val().join(",")')
                            )));
                    }

                    ?>
                </td>
                <td><span id="my_<?php echo $i?>">
                        <?php
                            if(isset($details[($i-1)]['route_id']))
                        {
                            $r_data = $details[$i-1]['route_id'];
                        } elseif(isset($data[($i)]['route_id'])) {
                            $r_data = $data[($i)]['route_id'];
                        } else {
                            $r_data = null;
                        }
                            if(isset($r_data) && sizeof($r_data))
                            {
                                $dealers = implode(',',$r_data);
                                $records = strlen($dealers) ? Route::getDealers($dealers) : null;
                                $no_dealers = sizeof($records);
                                echo $no_dealers ? $no_dealers : '';
                            }
                        ?>
                    </span></td>
            <td>
                <?php
                $day = '';
                if(isset($details[($i-1)]['day']))
                {
                    $day = $details[($i-1)]['day'];
                } elseif(isset($data[($i-1)]['day'])) {
                    $day = $data[($i-1)]['day'];
                } else {
                    $day = date('w', strtotime(date($month.$i)));
                }
                //echo $day;
                if(isset($form))
                {
                    //echo $form->dropDownListRow($details[$i-1], "[$i]day",RouteMap::itemAlias('Day'),array('multiple'=>false));
                    echo @CHtml::dropDownList('DRouteMap['.($i-1).'][day]', $day, RouteMap::itemAlias('Day'),array('class'=>'span2','multiple'=>false));
                    //echo $form->error($details[$i-1], "[($i-1)]day");
                } else {
                    echo @CHtml::dropDownList('DRouteMap['.($i-1).'][day]', $day, RouteMap::itemAlias('Day'),array('class'=>'span2','multiple'=>false));
                }
                ?>
            </td>
            <?php
            echo '</tr>';
        }
        echo '</table>';
/*Yii::app()->clientScript->registerScript('$("button").click(function(){
    $("p").toggle();
})');*/
?>