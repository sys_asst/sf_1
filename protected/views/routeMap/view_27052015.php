<?php
/* @var $this RouteMapController */
/* @var $model RouteMap */

$this->breadcrumbs=array(
	'Route Maps'=>array('index'),
	$model->name,
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
$data = RouteMap::getSchedule($model->id);

$desc = '';

if(sizeof($data))
{
    $desc = '<table class="items table table-striped table-bordered table-condensed"><thead><tr><th style="text-align: center;width: 30px;">Date</th><th style="text-align: center">Route</th><th style="text-align: center">No of Dealers</th></tr></thead><tbody>';
    foreach($data as $rec)
    {
        $dealers = isset($rec['routeids']) && sizeof($rec['routeids']) ? implode(',',$rec['routeids']) : '';
        $records = strlen($dealers) ? Route::getDealers($dealers) : null;
        $no_dealers = sizeof($records);
        $desc .= '<tr><td>'.$rec['date'].'</td><td>'.$rec['route'].'</td><td>'.(($no_dealers) ? $no_dealers : '').'</td></tr>';
    }
    $desc .= '</tbody></table>';
} else {
    $desc = 'no routes added';
}

?>

<h1>View Route Map for <?php echo $model->name; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'code',
		'name',
        array(
            'name'=>'rep_id',
            'value'=>CHtml::encode($model->rep->srName)
        ),
        array(
            'label'=>'Routes',
            'type' => 'raw',
            //'value'=>CHtml::encode($model->getRoutes($model->id))
            'value'=>$desc
        ),
        array(
            'name'=>'assigned_by',
            'value'=>CHtml::encode($model->assignedBy->username)
        ),
		'added_date',
        array(
            'name'=>'status',
            'value'=>CHtml::encode(RouteMap::itemAlias('ItemStatus', $model->status))
        ),
        'updated_on'
	),
)); ?>
