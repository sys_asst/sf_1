<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Randika
 * Date: 7/2/14
 * Time: 10:45 AM
 * To change this template use File | Settings | File Templates.
 */

$row_id = "sladetail-" . $key ?>

<div class='row-fluid' id="<?php echo $row_id ?>">
    <?php
    echo $form->hiddenField($model, "[$key]map_id");
    echo $form->updateTypeField($model, $key, "map_id", array('key' => $key));
    ?>
    <table border="0">
        <tr>
            <td>
                <div class="span3">
                    <?php echo $form->labelEx($model,"[$key]added_date"); ?>
                    <?php echo $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'=>$model,
                            'attribute'=>'['.$key.']added_date',
                            //'language' => 'ja',
                            // 'i18nScriptFile' => 'jquery.ui.datepicker-ja.js', (#2)
                            'htmlOptions' => array(
                                'id' => 'added_date_'.$key.'_search',
                                'size' => '10',
                            ),
                            'options' => array(
                                'dateFormat' => 'yy-mm-dd',
                                'showButtonPanel' => true,
                                'showOn' => 'focus'
                            )
                        ),
                        true); ?>
                    <?php echo $form->error($model,"[$key]added_date"); ?>
                </div>
            </td>
            <td>
                <div class="span3">
                    <?php
                    echo $form->labelEx($model, "[$key]route_id");
                    $leaflevelattrbutes = CHtml::listData(Route::model()->findAll('status > 0 ORDER BY name'), 'id','name');
                    echo $form->dropDownList($model, "[$key]route_id",$leaflevelattrbutes,array('prompt'=>'Select Route'));
                    echo $form->error($model, "[$key]route_id");
                    ?>
                </div>
            </td>

            <td>

                <div class="span2">

                    <?php echo $form->deleteRowButton($row_id, $key); ?>
                </div>
            </td>
        </tr>
    </table>
</div>