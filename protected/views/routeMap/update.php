<?php
/* @var $this RouteMapController */
/* @var $model RouteMap */

$this->breadcrumbs=array(
	'Route Maps'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>Update Route Map <?php echo $model->name; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model,'details'=>$details, 'entered'=>$entered)); ?>