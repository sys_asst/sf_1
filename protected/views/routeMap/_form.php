<?php
/* @var $this RouteMapController */
/* @var $model RouteMap */
/* @var $form CActiveForm */

$baseScriptUrl=Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets'));
//Yii::app()->clientScript->registerCssFile($baseScriptUrl.'/gridview/styles.css');
?>

<div class="form">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'=>'route-map-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation'=>false,
        'type'=>'horizontal',
    )); ?>
    <?php //$form=$this->beginWidget('DynamicTabularForm', array(
        //'defaultRowView'=>'_rowForm',
    //)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

    <div class="row-fluid">
        <div class="span6">
            <?php echo $form->textFieldRow($model,'code',array('class'=>'input-block-level', 'size'=>10,'maxlength'=>10)); ?>
            <?php echo $form->textFieldRow($model,'name',array('class'=>'input-block-level', 'size'=>60,'maxlength'=>255)); ?>
            <?php echo $form->dropDownListRow($model,'status',RouteMap::itemAlias('ItemStatus'),array('class'=>'input-block-level')); ?>
        </div>
        <div class="span6">
            <?php
            $records = array();
            if(Yii::app()->getModule('user')->isAdmin() == false)
            {
                $sql = 'SELECT IFNULL(DR.`area_id`,0) AS `area_id` FROM `ms_regions` AS R INNER JOIN `d_regions` AS DR ON DR.`region_id` = R.`id` AND R.`rcm_id`
                IN(SELECT S.`id` FROM `salesreps` AS S WHERE S.`srEmail` IN("'.Yii::app()->user->getState('src')->srEmail.'"))';
                $records = Tk::sql($sql);
            }

            $areas = '';
            //Tk::a($records);exit;
            if(sizeof($records) && Yii::app()->user->profile->src == 2)
            {
                foreach($records as $rec)
                {
                    $areas .= (strlen($areas)) ? ','.$rec['area_id'] : $rec['area_id'];
                }
            }
            echo $form->dropDownListRow($model,'rep_id',CHtml::listData(Salesreps::model()->findAll(array('condition'=>'status=:dis '.(strlen($areas) ? " AND area_id IN({$areas})" : ''),'order'=>'srName','params'=>array(':dis'=>1))), 'id','srName'),array('prompt'=>'Select Sales Rep','class'=>'input-block-level')); ?>
            <div class="control-group ">
                <label class="control-label " for="month">
                    <?php echo Yii::t('messages', 'Month');//$attributeLabels['publishDate']?>
                </label>
                <div class="controls">
                <?php echo $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,
                        'attribute'=>'month',
                        'htmlOptions' => array(
                            'id' => 'assigned_month',
                            'size' => '10',
                            'maxlength'=>10
                        ),
                        'options' => array(
                            'dateFormat' => 'yy-mm-dd',
                            'showButtonPanel' => true,
                            'showOn' => 'focus'
                        ),
                    ),
                    true); ?>
                <?php echo $form->error($model,'month'); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
    <?php //echo CHtml::ajaxButton ("Get Schedule",
        //CController::createUrl('routeMap/CreateShedule/'),
        //array('update' => '#schedule_data','data'=>array('month'=>'js:$("#assigned_month").val()','id'=>$model->id)));
    ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'ajaxButton',
        'type'=>'info',
        'label'=>'Get Schedule',
        'url'=>CController::createUrl('routeMap/CreateShedule/'),
        'ajaxOptions'=>array(
                'type'=>'POST',
                'url'=>CController::createUrl('routeMap/CreateShedule/'),
                'update'=>'#schedule_data',
                'data'=>array('month'=>'js:$("#assigned_month").val()','id'=>$model->id, 'rep_id'=>'js:$("#RouteMap_rep_id").val()')
        ),
    )); ?>
    </div>

    <fieldset class="label_data" style="display: <?php echo ($model->scenario == 'update')? 'block' : 'block';?>; width:800px;">
        <legend>Route Details</legend>
        <div id="schedule_data">
            <?php
                //echo sizeof($details);
                if(sizeof($details) >1)
                {
                    $this->renderPartial('_schedule', array('id'=>$model->id, 'month'=>$model->month, 'form'=>$form, 'rep_id'=>$model->rep_id, 'data'=>$entered,'details'=>$details),false,false);
                }
            ?>
        </div>
        <?php
        //echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save');
        //$max_leafs = sizeof(Route::model()->findAll('status=1'));
        //print_r(Route::model()->findAll('status=1'));
        //echo $form->rowForm($details, null, array(), $max_leafs);
        ?>
    </fieldset>

    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>$model->isNewRecord ? Yii::t('messages', 'Create') : Yii::t('messages', 'Save'),
        )); ?>

        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'label'=>Yii::t('messages', 'Cancel'),
            'type'=>'info',
            'url'=>Yii::app()->createUrl('dealercommentreasons/admin')
        )); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->