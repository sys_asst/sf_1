<?php
/* @var $this RouteMapController */
/* @var $model RouteMap */

$this->breadcrumbs=array(
	'Route Maps'=>array('index'),
	$model->name,
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
$data = RouteMap::getSchedule($model->id);

$desc = '';

if(sizeof($data))
{
    $desc = '<table class="items table table-striped table-bordered table-condensed"><thead><tr><th style="text-align: center;width: 90px;">Date</th>
<th style="text-align: center;width:300px;">Route</th><th style="text-align: center;width:50px;">No of Dealers</th><th style="text-align: center;width:50px;">Type of Day</th></tr></thead><tbody>';;
    foreach($data as $rec)
    {
        $dealers = isset($rec['routeids']) && sizeof($rec['routeids']) ? implode(',',$rec['routeids']) : '';
        $records = strlen($dealers) ? Route::getDealers($dealers) : null;
        $no_dealers = sizeof($records);
        $day_index = date('w', strtotime($rec['date']));
        if($day_index == 6)
        {
            $class = 'label label-danger';
        } else if($day_index == 0)
        {
            $class = 'label label-warning';
        } else {
            $class = 'label label-success';
        }
        $desc .= '<tr><td>'.$rec['date'].', <span class="'.$class.'">'.date('l', strtotime($rec['date'])).'</span></td><td>'.$rec['route'].'</td><td style="text-align:center;">'.(($no_dealers) ? $no_dealers : '').'</td><td>'.RouteMap::itemAlias('Day',$rec['day']).'</td></tr>';
    }
    $desc .= '</tbody></table>';
} else {
    $desc = 'no routes added';
}

?>

<h1>View Route Map for <?php echo $model->name; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'code',
		'name',
        array(
            'name'=>'rep_id',
            'value'=>CHtml::encode($model->rep->srName)
        ),
        array(
            'label'=>'Routes',
            'type' => 'raw',
            //'headerHtmlOptions' => array('style' => 'width: 30px'),
            //'value'=>CHtml::encode($model->getRoutes($model->id))
            'value'=>$desc
        ),
        array(
            'name'=>'assigned_by',
            'value'=>CHtml::encode($model->assignedBy->username)
        ),
		'added_date',
        array(
            'name'=>'status',
            'value'=>CHtml::encode(RouteMap::itemAlias('ItemStatus', $model->status))
        ),
        'updated_on'
	),
)); ?>
