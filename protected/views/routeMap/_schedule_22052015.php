<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Randika
 * Date: 7/8/14
 * Time: 9:40 AM
 * To change this template use File | Settings | File Templates.
 */
    //print_r($details);
    $connection = Yii::app()->db;
        $sql = "SELECT DATEDIFF(DATE_ADD(DATE_FORMAT('{$month}', '%Y-%m-01'), INTERVAL 1 MONTH), DATE_FORMAT('{$month}', '%Y-%m-01')) AS `dates`,
            DATE_FORMAT('{$month}', '%Y-%m-') AS `month`";

        $command = $connection->createCommand($sql);
        $row = $command->queryRow();
        $dates = $row['dates'];
        $month = $row['month'];
        $class = '';
        //$leaflevelattrbutes = CHtml::listData(Route::model()->findAll('status > 0 ORDER BY name'), 'id','name');
$leaflevelattrbutes = CHtml::listData(Route::model()->findAll('status > 0 AND area_id IN(SELECT SR.area_id FROM salesreps AS SR WHERE SR.id = "'.$rep_id.'") ORDER BY name'), 'id','name');

        //echo '<div id="close">Close</div>';
        echo '<table border="0" class="items table table-striped table-bordered table-condensed"><thead>';
        echo '<tr class="'.$class.'"><th>Date</th><th>Route</th></thead></tr>';
        for($i =1; $i <= $dates; $i++)
        {
            $class = $class == 'odd' ? 'even' : 'odd';
            echo '<tr class="'.'">';
            ?>
                <td><?php
                    if(isset($form))
                    {
                        echo $form->textFieldRow($details[$i-1],"[$i]added_date",array('size'=>10,'maxlength'=>10,'readonly'=>true));
                    } else {
                        echo @CHtml::textField('DRouteMap['.$i.'][added_date]',(strlen($data[$i]['added_date'])) ? $data[$i]['added_date'] : date('Y-m-d', strtotime(date($month.$i))),array('size'=>10,'maxlength'=>10,'readonly'=>true));
                    }
                    ?></td>
                <td><?php
                    //echo $form->labelEx($model, "[$key]route_id");
                    if(isset($form))
                    {
                        echo $form->dropDownListRow($details[$i-1], "[$i]route_id",$leaflevelattrbutes,array('prompt'=>'Select Route'));
                        echo $form->error($details[$i-1], "[$i]route_id");
                    } else {
                        echo @CHtml::dropDownList('DRouteMap['.$i.'][route_id]', (strlen($data[$i]['route_id']) ? $data[$i]['route_id'] : $details[$i]),$leaflevelattrbutes,array('prompt'=>'Select Route'));
                    }
                    //echo CHtml::error('DRouteMap','DRouteMap_'.$i.'_route_id');
                    //print_r($details[$i-1]);
                    ?>
                </td>

            <?php
            echo '</tr>';
        }
        echo '</table>';
/*Yii::app()->clientScript->registerScript('$("button").click(function(){
    $("p").toggle();
})');*/
?>