<?php
/* @var $this RouteMapController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Route Maps',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>Route Maps</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
