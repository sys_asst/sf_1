<?php
/* @var $this OrdersController */
/* @var $model Orders */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'orders-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'deliveryDate'); ?>
		<?php echo $form->textField($model,'deliveryDate'); ?>
		<?php echo $form->error($model,'deliveryDate'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'orderPlacedTime'); ?>
		<?php echo $form->textField($model,'orderPlacedTime'); ?>
		<?php echo $form->error($model,'orderPlacedTime'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'orderType'); ?>
		<?php echo $form->textField($model,'orderType',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'orderType'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dealerId'); ?>
		<?php echo $form->textField($model,'dealerId',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'dealerId'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'orderTotalValue'); ?>
		<?php echo $form->textField($model,'orderTotalValue'); ?>
		<?php echo $form->error($model,'orderTotalValue'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'paymentType'); ?>
		<?php echo $form->textField($model,'paymentType',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'paymentType'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'srCode'); ?>
		<?php echo $form->textField($model,'srCode',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'srCode'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'latitude'); ?>
		<?php echo $form->textField($model,'latitude'); ?>
		<?php echo $form->error($model,'latitude'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'longitude'); ?>
		<?php echo $form->textField($model,'longitude'); ?>
		<?php echo $form->error($model,'longitude'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'disabled_on'); ?>
		<?php echo $form->textField($model,'disabled_on'); ?>
		<?php echo $form->error($model,'disabled_on'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'disabled'); ?>
		<?php echo $form->textField($model,'disabled'); ?>
		<?php echo $form->error($model,'disabled'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'disabled_by'); ?>
		<?php echo $form->textField($model,'disabled_by'); ?>
		<?php echo $form->error($model,'disabled_by'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->