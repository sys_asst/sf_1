<?php
/* @var $this OrdersController */
/* @var $model Orders */
?>

<div class="form">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'invoice-form',
        'enableAjaxValidation' => false,
    ));
//    $date = date('Y-m-d');
    $date = date('Y-m-d H:i');
    ?>

    <input type="hidden" name="order_no" value="<?php echo $model->id; ?>">

    <table  class="detail-view table table-striped table-condensed tblpadding" style="width: 100%;">
        <tbody>
            <tr class="even">
                <th width="15%"> <p style="font-size:18px;font-weight: normal">Dealer details  </p></th>

        <td width="35%" >
            <?php
            echo "<b>No:</b> " . $model->dealers->dealerCode . "<br/>";
            echo "<b>Name:</b> " . $model->dealers->dealerName . "</br>";
            echo "<b>Address:</b> " . $model->dealers->address2 . ", " . $model->dealers->address3;
            ?></td> 
        <th width="20%"><p style="font-size:18px;font-weight: normal"> Sales Rep Details  </p></th>
        <td width="30%">
            <?php
            echo "<b>No:</b> ";
            echo isset($model->salesrefs->srEmpNo) ? $model->salesrefs->srEmpNo : "n/a";
            echo "<br/>";

            echo "<b>Name:</b> ";
            echo isset($model->salesrefs->srName) ? $model->salesrefs->srName : "n/a";
            echo "<br/>";

            echo "<b>Order type:</b> ";
            echo isset($model->orderType) ? $model->orderType : "n/a";
            echo "<br/>";
            ?>
        </td>
        </tr>

        <tr class="odd" >
            <th width="15%"> <p style="font-size:18px;font-weight: normal">Payment details  </p></th>

        <td width="35%" >
            <?php
            echo "<b>Payment terms:</b> " . $model->paymentType . "<br/>";
            echo "<b>No of Credit Dates:</b>";
            ?>
            	<?php // echo $form->labelEx($invoice,'credit_days'); ?>
		<?php // echo $form->textField($invoice,'credit_days'); ?>
		<?php // echo $form->error($invoice,'credit_days'); ?>
            <input type="hidden" name="paymentType" value="<?php echo $model->paymentType; ?>" >
            <input type="text" style="width:60px" name="Invoice[credit_days]" id="invoice_creditd" value="0"  >
            <?php echo $form->error($invoice,'credit_days'); ?>
            
            <br/>
            <b>Order Total: </b>
            <?php echo Yii::app()->numberFormatter->formatCurrency($model->orderTotalValue, Yii::app()->params['currencysymbol']); ?>

        </td> 
        <th width="20%">&nbsp;</th>
        <td width="30%">
            <?php
            echo "<b>Delivery date:</b> ";
            echo isset($model->deliveryDate) ? $model->deliveryDate : "n/a";
            echo "<br/>";

            echo "<b>Order comments:</b> ";
            echo $model->orderComment != "" ? $model->orderComment : "n/a";

            ?>
        </td>
        </tr>

        <tr class="even" >
            <th width="20%"><p style="font-size:18px;font-weight: normal">Distributor name</p></th>

        <td width="30%" >
            <?php
            echo isset($model->dist->distName) ? $model->dist->distName : "n/a";
            echo "<br/>";
            ?>
        </td> 
        <th width="20%">&nbsp;</th>
        <td width="30%">&nbsp;</td>
        </tr>

        </tbody>
    </table>


    <?php
    $classTbl = "style='text-align:right;height:30px; vertical-align:middle'";
    ?>

    <fieldset class="label_data" style="clear: both; width: 100%;">
        <legend>Invoice details</legend>


        <table  class="table table-striped table-bordered table-condensed table ">
            <input type="hidden" name="Invoice[date]" value="<?php echo $date; ?>" >

            <?php
            $count = 0;
            if (sizeof($row)) {
                $class = 'odd';
                ?>
                <thead><tr class="<?php $class; ?>">
                        <th>Line no</th>
                        <th style=\"text-align:right\">Product Code</th>
                        <th>Product description</th>
                        <th>Unit</th>
                        <th>Available Stock</th>
                        <th>Order Quantity</th>
                        <th>Invoice Quantity</th>
                        <th>Price</th>
                        <th>Discount</th>
                        <th style="text-align: right">Line Total</th>
                    </tr></thead><tbody>
                    <?php
                    $orderTotal = 0;
                    $class = 'odd';
                    $invoice_count = count($row);

                    $totQty = 0;
          
                    foreach ($row as $key => $data) {


                        $qty = 0;
                        $class == 'odd' ? 'even' : 'odd';
                        $stockQty = Products::getStock($data['id']);

                        $insQty = isset($data['invocedQty']) ? $data['invocedQty'] : 0;

                        $stockQty = $stockQty - $insQty;

                        $orderQty = isset($data['orderQty']) ? $data['orderQty'] : 0;
                        $unitPrice = isset($data['unitPrice']) ? $data['unitPrice'] : 0;
                        $discount1 = isset($data['discount1']) ? $data['discount1'] : 0;
                        $quantity = $orderQty - $insQty;

                        if ($stockQty < $quantity) {
                            $qty = $stockQty;
                        } else {
                            $qty = $quantity;
                        }

                        if ($qty < 0) {
                            $qty = 0;
                        }

                        $totQty += $qty;
                        $invoiceTotal = $unitPrice * $qty * (100 - $discount1) / 100;

                        echo "<input type='hidden' name= 'inv[{$key}][id]' value='{$data['id']}' >";
                        echo "<input type='hidden' name= 'inv[{$key}][unit_price]' value = {$unitPrice} >";

                        if ($qty > 0) {

                            $count++;

                            echo '<tr class="' . $class . '">';
                            echo "<td>{$count}</td>";
                            echo "<td style='vertical-align:middle'>{$data['uniqueId']}</td>";
                            echo " <td style='height:30px; vertical-align:middle'>{$data['name']}</td>";
                            echo "<td {$classTbl}>{$data['unit']}</td>
                                      <td {$classTbl}>" . round($stockQty, 2) . "</td>   
                                      <td {$classTbl}>{$orderQty}</td>
                                      <td {$classTbl}> <input type = \"text\" value=" . round($qty, 2) . "  id='invoice_qty_{$key}' name=\"inv[{$key}][invoice_qty]\" style= \"width:60px\"   onKeyup =\"js:priceValidate(this,{$qty});\"  onChange =\"js:price(this,{$key},{$unitPrice},{$invoice_count},{$qty}, {$discount1})\"></td>
                                      <td {$classTbl}>" . Yii::app()->numberFormatter->formatCurrency($unitPrice, '') . "</td>
                                      <td {$classTbl}>{$data['discount1']}</td>
                                      <td {$classTbl} id='row_{$key}' >" . Yii::app()->numberFormatter->formatCurrency($invoiceTotal, '') . "</td>";
                            echo '</tr>';

                            $orderTotal += $invoiceTotal;
                        } else {
                            echo "<input type = 'hidden' value='0'  id='invoice_qty_{$key}' name=\"inv[{$key}][invoice_qty]\" >";
                        }
                    }

                    if ($count == 0) {
                        echo '<tr class="' . $class . '"><th colspan="10"><p style="padding:10px;">No order items to invoice</p></th></tr>';
                    } else {
                        echo '<tr class="' . $class . '"><th>Total</th><th colspan="8"></th><th style="text-align: right" id="total_invoice_value">' . Yii::app()->numberFormatter->formatCurrency($orderTotal, '') . '</th></tr>';
                    }
                } else {
                    echo '<tr><td><p style="padding:10px;">No order items to invoice</p></td></tr>';
                }
                ?>
            </tbody>
        </table>



    </fieldset>
    <?php ?>
    <div class="form-actions" style="text-align: center">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'primary',
            'type' => 'info',
            'label' => 'Back to orders',
            'url' => Yii::app()->createUrl('orders/admin'),
            'htmlOptions' => array('name' => 'orders'),
                )
        );

        echo "&nbsp;&nbsp;";

        if ($count != 0) {
            $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType' => 'submit',
                'type' => 'primary',
                'label' => 'Create Invoice',
                'htmlOptions' => array('name' => 'invoice'),
                    )
            );
        }
        ?>

    </div>
    <?php ?>



    <fieldset class="label_data" style="clear: both; width: 100%;">
        <legend>Pending Items </legend>
        <table  class="table table-striped table-bordered table-condensed table ">

            <?php
            if (sizeof($row)) {
                $class = 'odd';
                ?>
                <thead><tr class="<?php echo $class; ?>">
                        <th>Line No</th>
                        <th style=\"text-align:right\">Product Code</th>
                        <th>Product description</th>
                        <th>Unit</th>
                        <th>Available Stock</th>
                        <th>Order Quantity</th>
                        <th>Invoiced Quantity</th>
                        <th>Pending Quantity</th>
                        <th>Price</th>
                        <th>Discount</th>
                        <th style="text-align: right">Line total</th>         
                    </tr></thead><tbody>
                    <?php
                    $orderTotal = 0;
                    $class = 'odd';
                    $count = 0;
                    foreach ($row as $key => $data) {
                        $qty = 0;
                        $count++;
///////////////////////////////
                        $stockQty = Products::getStock($data['id']);


                        $insQty = isset($data['invocedQty']) ? $data['invocedQty'] : 0;

                        $stockQty = $stockQty - $insQty;


                        $quantity = isset($data['quantity']) ? $data['quantity'] : 0;
                        $orderQty = isset($data['orderQty']) ? $data['orderQty'] : 0;
//                        $lineTotal = isset($data['lineTotal']) ? $data['lineTotal'] : 0;
                        $unitPrice = isset($data['unitPrice']) ? $data['unitPrice'] : 0;
                        $discount1 = isset($data['discount1']) ? $data['discount1'] : 0;

                        $quantity = $orderQty - $insQty;
                        if ($stockQty < $quantity) {
                            $qty = $quantity - $stockQty;
                        } else {
                            $qty = 0;
                        }

                        $pendingTotal = ($unitPrice * $qty) * (100 - $discount1) / 100;


//////////////////////////

                        $class = $class == 'odd' ? 'even' : 'odd';

                        echo '<tr class="' . $class . '">';
                        //  echo "<input type='hidden' name= 'pen[{$key}][pending_qty]' value='{$qty}' >";
                        echo " <td style='vertical-align:middle'>{$count}</td>";
                        echo " <td style='vertical-align:middle'>{$data['uniqueId']}</td>
                                <td style='height:30px; vertical-align:middle'>{$data['name']} </td>
                                <td {$classTbl}>{$data['unit']}</td>
                                <td {$classTbl}>" . round($stockQty,2)."</td>
                                <td {$classTbl}>" . round($orderQty, 2) . "</td>
                                <td {$classTbl}>" . round($insQty, 2) . "</td>
                                <td {$classTbl}><input type=\"text\" value={$qty}  id='pending_qty_{$key}' name=\"pen[{$key}][pending_qty]'\" style= \"width:60px\" readonly=\"true\" ></td>    
                                <td {$classTbl}>" . Yii::app()->numberFormatter->formatCurrency($unitPrice, '') . "</td>
                                <td {$classTbl}>" . $data['discount1'] . "</td>
                                <td {$classTbl} id='pending_price_{$key}' >" . Yii::app()->numberFormatter->formatCurrency($pendingTotal, '') . "</td>";
                        echo '</tr>';

                        $orderTotal += $pendingTotal;
//                        }
                    }

                    $class = $class == 'odd' ? 'even' : 'odd';
                    echo '<tr class="' . $class . '"><th>Total</th><th colspan="9"></th><th style="text-align: right" id="total_pending_value">' . Yii::app()->numberFormatter->formatCurrency($orderTotal, '') . '</th></tr>';
                } else {
                    echo '<tr><td>no order items added</td></tr>';
                }
                ?>
            </tbody>
        </table>
    </fieldset>



    <?php $this->endWidget(); ?>

</div>


<script type="text/javascript">

//     function dueDate(dates) {
//           due_date = $("#invoice_due_date_h" ).val();
//           
//            var dateFormat = $(this).text()
//        var dateFormat = $.datepicker.formatDate('MM dd, yy', new Date(dateFormat));
//
//           var d = new Date('2011-01-01');
//         d.setDate(d.getDate() + dates);
//
//       month = '' + d.getMonth(),
//        day = '' + d.getDate(),
//        year = d.getFullYear();
//
//    if (month.length < 2) month = '0' + month;
//    if (day.length < 2) day = '0' + day;
//
//    date = [year, month, day].join('-');
//           $('#invoice_due_date').html(date);
//     }

    function priceValidate($this, stockQty) {
//         console.log($this.id);
//         $("#"+$this.id).val(1234);
        var valid = /^\d{0,4}?$/.test($this.value),
                val = $this.value;

        if (!valid) {
            console.log("Invalid input!");
            $("#" + $this.id).val(val.substring(0, val.length - 1));
        } else if (stockQty < $this.value) {
            $("#" + $this.id).val(stockQty);
        }
//    $this.value(1234);
//     $('#row_' + key).html(totalPrice.toFixed(2));
    }


    function price($this, key, price, count, invoice_qty, discount1) {

//        console.log(price);
        totalPrice = (price * $this.value) * (100 - discount1) / 100;
        pending_qty = invoice_qty - $this.value;

        if (pending_qty < 0) {
            pending_qty = 0;
        }
        pending_value = (price * pending_qty) * (100 - discount1) / 100;



        $('#row_' + key).html(totalPrice.toFixed(2));


        total_invoice = 0;
        for (i = 0; i < count; i++) {

            if (!isNaN(parseFloat($('#row_' + i).html()))) {
                total_invoice += parseFloat($('#row_' + i).html());
            }
        }
        $('#total_invoice_value').html(total_invoice.toFixed(2));

        ///pending..............................

        $('#pending_qty_' + key).val(pending_qty);

        $('#pending_price_' + key).html(pending_value.toFixed(2));

        total_pending = 0;
        for (i = 0; i < count; i++) {

            val = parseFloat($('#pending_price_' + i).html());
            if (!isNaN(val)) {
                total_pending += val;
            }

        }

        $('#total_pending_value').html(total_pending.toFixed(2));

    }

</script>
