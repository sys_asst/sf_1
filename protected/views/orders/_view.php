<?php
/* @var $this OrdersController */
/* @var $data Orders */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('deliveryDate')); ?>:</b>
	<?php echo CHtml::encode($data->deliveryDate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('orderPlacedTime')); ?>:</b>
	<?php echo CHtml::encode($data->orderPlacedTime); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('orderType')); ?>:</b>
	<?php echo CHtml::encode($data->orderType); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dealerId')); ?>:</b>
	<?php echo CHtml::encode($data->dealerId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('orderTotalValue')); ?>:</b>
	<?php echo CHtml::encode(Yii::app()->numberFormatter->formatCurrency($data->orderTotalValue,'')); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('paymentType')); ?>:</b>
	<?php echo CHtml::encode($data->paymentType); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('srCode')); ?>:</b>
	<?php echo CHtml::encode($data->srCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('latitude')); ?>:</b>
	<?php echo CHtml::encode($data->latitude); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('longitude')); ?>:</b>
	<?php echo CHtml::encode($data->longitude); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('disabled_on')); ?>:</b>
	<?php echo CHtml::encode($data->disabled_on); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('disabled')); ?>:</b>
	<?php echo CHtml::encode($data->disabled); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('disabled_by')); ?>:</b>
	<?php echo CHtml::encode($data->disabled_by); ?>
	<br />

	*/ ?>

</div>