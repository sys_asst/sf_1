<?php
/* @var $this OrdersController */
/* @var $model Orders */

$this->breadcrumbs=array(
	'Orders'=>array('index'),
	'Manage',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#orders-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");

?>

<h1>Manage Orders</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php 
//print_r(Salesreps::model()->findAll(array('condition'=>'status=:active', 'order'=>'srName', 'params'=>array(':active'=>1))));exit;

echo '<style>
/**
 * Hide first and last buttons by default.
 */
ul.yiiPager .first,
ul.yiiPager .last
{
    display:inline;
}
</style>';

$dist = User::getDistributor();
$reps = User::getReps();
//Tk::a($dist);exit;
$temp = array();
$distributor = '';
Yii::app()->getModule('user')->isAdmin() == false ? $temp = User::getDistributor() : null;
(!is_array($temp)) ? $temp = explode(',', $temp) : '';
if(sizeof($temp) > 1)
{
    $distributor = "'".implode("','", $temp)."'";
} elseif(sizeof($temp) == 1) {
    $distributor = "'{$temp[0]}'";
} else {
    $distributor = "''";
}

$dealers = Dealers::getDealersByDist($distributor);
?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
	'id'=>'orders-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
    'afterAjaxUpdate' => 'reinstallDatePicker',
	'columns'=>array(
        array(
            'name'=>'id',
            'header'=>'Order No',
            'value'=>'OrderNos::getOrderNo($data->id)',
//            'value'=>'$data->id',
            'htmlOptions' => array('style' => 'width: 50px;'),
        ),
        array(
            'name'=>'distributor_id',
            'header'=>'Distributor',
            //'value'=>'Distributors::getDistributor($data->dealers->distributor,1)',
            'value'=>'$data->dist->distName',
            'filter' =>CHtml::listData(Distributors::model()->findAll(array('condition'=>'status=:active'.(Yii::app()->getModule('user')->isAdmin() == false ? " AND distId IN (".$distributor.")" : ''), 'order'=>'distName', 'params'=>array(':active'=>1))),'id','distName'),
            'htmlOptions' => array('style' => 'width: 180px;'),//Distributor, Rep, Order Date & Time, Order Type, Dealer, Order Total, Delivery date
        ),
               array(
            'name'=>'srCode',
            'header'=>'Rep',
            'value'=>'$data->salesrefs->srName',
            //'filter' =>CHtml::listData($reps,'srEmpNo','srName'),
            'filter' => $model->distributor_id ? CHtml::listData(Salesreps::model()->findAllByAttributes(
                    array(),
                    "distributor = :distributor",
                    array(':distributor'=>Distributors::getDistId($model->distributor_id))),
                    'srEmpNo','srName') : CHtml::listData($reps,'srEmpNo','srName'),
        ),
      
        array(
            'name' => 'orderPlacedTime',
            'htmlOptions' => array('style' => 'width: 120px;'),
            'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,
                    'attribute'=>'orderPlacedTime',
                    //'language' => 'ja',
                    // 'i18nScriptFile' => 'jquery.ui.datepicker-ja.js', (#2)
                    'htmlOptions' => array(
                        'id' => 'orderPlacedTime_search',
                        'size' => '10',
                    ),
                    'options' => array(
                        'dateFormat' => 'yy-mm-dd',
                        'showButtonPanel' => true,
                        'showOn' => 'focus'
                    )
                ),
                true), // (#4)
        ),
		array(
            'name'=>'orderType',
            'headerHtmlOptions' => array('style' => 'width: 80px'),
            'value'=>'$data->orderType'
        ),
        array(
            'name'=>'dealerId',
            'value'=>'$data->dealers->dealerName',
            //'filter' =>CHtml::listData(Dealers::model()->findAll(array('condition'=>'disabled=:disabled'.(Yii::app()->getModule('user')->isAdmin() == false ? " AND distributor = '".Yii::app()->user->name."'" : ''), 'order'=>'dealerName', 'params'=>array(':disabled'=>0))),'id','dealerName'),
            'filter' =>$model->srCode ? CHtml::listData(Dealers::model()->findAllByAttributes(
                    array(),
                    "distributor = :distributor",
                    array(':distributor'=>Dealers::getDealersByRef($model->srCode))),
                'id','dealerName') : CHtml::listData($dealers, 'id','dealerName'),
            'htmlOptions' => array('style' => 'width: 70px;'),
        ),
        array(
            'name'=>'orderTotalValue',
            'value'=>'Yii::app()->numberFormatter->formatCurrency($data->orderTotalValue,"")',
            'htmlOptions' => array('style' => 'width: 80px;text-align:right;'),
        ),
        array(
            'name' => 'deliveryDate',
            'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,
                    'attribute'=>'deliveryDate',
                    //'language' => 'ja',
                    // 'i18nScriptFile' => 'jquery.ui.datepicker-ja.js', (#2)
                    'htmlOptions' => array(
                        'id' => 'deliveryDate_search',
                        'size' => '10',
                    ),
                    'options' => array(
                        'dateFormat' => 'yy-mm-dd',
                        'showButtonPanel' => true,
                        'showOn' => 'focus'
                    )
                ),
                true), // (#4)
        ),
        array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
//            'htmlOptions'=>'width:20',
            'htmlOptions'=>array('style'=>'width: 100px; text-align:center'),
            'template' => '{view} | {invoice}',
            'buttons' => array(
                'view' => array( //the name {reply} must be same
                    'label' => 'Add to System', // text label of the button user/admin/create&refid=68&src=sref
                    'url' => 'CHtml::normalizeUrl(array("orders/vieworder/id/".$data->id))', //Your URL According to your wish
                    //'imageUrl' => Yii::app()->baseUrl . '/images/user_add.png', // image URL of the button. If not set or false, a text link is used, The image must be 16X16 pixels
                ),

                'invoice' => array( //the name {reply} must be same
                    'label' => 'Invoice', // text label of the button user/admin/create&refid=68&src=sref
                    'url' => 'CHtml::normalizeUrl(array("orders/invoice/id/".$data->id))', //Your URL According to your wish
                    //'imageUrl' => Yii::app()->baseUrl . '/images/user_add.png', // image URL of the button. If not set or false, a text link is used, The image must be 16X16 pixels
                ),
            ),
        ),
		/*
		'paymentType',
		'srCode',
		'latitude',
		'longitude',
		'disabled_on',
		'disabled',
		'disabled_by',
		*/
		/*array(
			'class'=>'CButtonColumn',
            //'template' => '{view}'
		),*/
	),
));

Yii::app()->clientScript->registerScript('re-install-date-picker', "
function reinstallDatePicker(id, data) {
        //use the same parameters that you had set in your widget else the datepicker will be refreshed by default
    $('#orderPlacedTime_search').datepicker(jQuery.extend({showMonthAfterYear:false},jQuery.datepicker.regional['en'],{'dateFormat':'yy-mm-dd','showButtonPanel':true}));
    $('#deliveryDate_search').datepicker(jQuery.extend({showMonthAfterYear:false},jQuery.datepicker.regional['en'],{'dateFormat':'yy-mm-dd','showButtonPanel':true}));
}
");
?>
