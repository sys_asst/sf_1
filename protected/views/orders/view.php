<?php
/* @var $this OrdersController */
/* @var $model Orders */

$this->breadcrumbs=array(
	'Orders'=>array('index'),
	$model->id,
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>View Orders #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'deliveryDate',
		'orderPlacedTime',
		'orderType',
		'dealerId',
		'orderTotalValue',
		'paymentType',
		'srCode',
		//'latitude',
		//'longitude',
		//'disabled_on',
		//'disabled',
		//'disabled_by',
	),
)); ?>
