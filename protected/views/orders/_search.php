<?php
/* @var $this OrdersController */
/* @var $model Orders */
/* @var $form CActiveForm */
?>
<?php
$this->widget('application.modules.d_export.components.exportWidget.ExportWidget',
    array(
        'searchFormId'=>'order_search',
        'model'=>$model
    )
);
?>
<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
    'id'=>'order_search'
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'deliveryDate'); ?>
		<?php echo $form->textField($model,'deliveryDate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'orderPlacedTime'); ?>
		<?php echo $form->textField($model,'orderPlacedTime'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'orderType'); ?>
		<?php echo $form->textField($model,'orderType',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dealerId'); ?>
		<?php echo $form->textField($model,'dealerId',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'orderTotalValue'); ?>
		<?php echo $form->textField($model,'orderTotalValue'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'paymentType'); ?>
		<?php echo $form->textField($model,'paymentType',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'srCode'); ?>
		<?php echo $form->textField($model,'srCode',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'latitude'); ?>
		<?php echo $form->textField($model,'latitude'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'longitude'); ?>
		<?php echo $form->textField($model,'longitude'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'disabled_on'); ?>
		<?php echo $form->textField($model,'disabled_on'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'disabled'); ?>
		<?php echo $form->textField($model,'disabled'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'disabled_by'); ?>
		<?php echo $form->textField($model,'disabled_by'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->