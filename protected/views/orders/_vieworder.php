<?php
/* @var $this OrdersController */
/* @var $model Orders */
?>

<?php
$this->widget('bootstrap.widgets.TbDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
        'deliveryDate',
        'orderPlacedTime',
        'orderType',
        'orderComment',
        array(
            'name' => 'dealerId',
            'value' => CHtml::encode($model->dealers->dealerName),
        ),
        array(
            'label' => 'Dealer Address',
            'type' => 'raw',
            'value' => CHtml::encode($model->dealers->address1 . ', ' . $model->dealers->address2 . ', ' . $model->dealers->address3 . ''),
        ),
        array(
            'name' => 'orderTotalValue',
            'value' => Yii::app()->numberFormatter->formatCurrency($model->orderTotalValue, Yii::app()->params['currencysymbol']),
        ),
        'paymentType',
        'srCode',
        array(
            'name' => 'srCode',
            'label' => 'Sales Rep Name',
            'value' => CHtml::encode($model->salesrefs->srName),
        ),
        //'latitude',
        //'longitude',
        //'disabled_on',
        //'disabled',
        //'disabled_by',isset($data->addedby)?CHtml::encode($data->addedby0->username):"unknown";
    ),
));

/*
$connection = Yii::app()->db;
$sql = "SELECT CONCAT(P.`description`,' ',P.`label1Value`,' ',P.`label1Text`,' ',IF(P.`label2Value` = 0,'',P.`label2Value`)) AS `name`,
P.`price`, OI.`quantity`,  OI.`discount1`,
       OI.`discount2`, ROUND(OI.`lineTotal`,2) AS `lineTotal`, (((P.`price`/100)*(100-OI.`discount1`))*OI.`quantity`) AS `linetot`
FROM `orderitems` AS OI
     INNER JOIN `products` AS P ON P.`id` = OI.`productId`
WHERE OI.`orderId` = '{$model->id}'";
$command = $connection->createCommand($sql);
$row = $command->queryAll();*/
$row = Orders::getOrderItems($model->id);
//print_r($row);
echo '<div class="clear"></div>';
?>
<h3>Order details </h3>

<?php

echo '<table border="0" class="detail-view table table-striped table-condensed">';

if (sizeof($row)) {
    $class = 'odd';
    echo '<tr class="' . $class . '">
    <th style="text-align:left">Product</th>
    <th>Unit Price</th>
    <th>Discount 1</th>
    <th>Quantity</th>
    <th style="text-align: right">Line Total</th>
  </tr>';

    $orderTotal = 0;
    $class = 'odd';
    foreach ($row as $data) {
        $class = $class == 'odd' ? 'even' : 'odd';
        echo '<tr class="' . $class . '">';
        echo " <td style=\"width:30em\">{$data['name']} " . Products::getProductLabelValues($data['id']) . "</td>
       <td style=\"text-align:right\">" . Yii::app()->numberFormatter->formatCurrency($data['price'], '') . "</td>
       <td style=\"text-align:right\">{$data['discount1']}</td>
       <td style=\"text-align:right\">{$data['quantity']}</td>
       <td style=\"text-align: right\">" . Yii::app()->numberFormatter->formatCurrency($data['lineTotal'], '') . "</td>";
        echo '</tr>';
        $orderTotal += $data['lineTotal'];
    }
    $class = $class == 'odd' ? 'even' : 'odd';
    echo '<tr class="' . $class . '"><th style="text-align:left">Total</th><th colspan="3"></th><th style="text-align: right">' . Yii::app()->numberFormatter->formatCurrency($orderTotal, '') . '</th></tr>';
} else {
    echo '<tr class="' . $class . '"><td style="text-align:left">no order items added</td></tr>';
}


?>
        </table>
