<?php
/* @var $this OrdersController */
/* @var $model Orders */
?>

<div class="form">
    <?php
    $date = date('Y-m-d H:i');
    $classTbl = "style='text-align:right;height:30px; vertical-align:middle'";

    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'invoice-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <input type="hidden" name="order_no" value="<?php echo $model->invoice_no; ?>">

    <table  class="detail-view table table-striped table-condensed tblpadding" style="width: 100%;">
        <tbody>
            <tr class="even">
                <th width="20%"> <p style="font-size:18px;font-weight: normal">Dealer details  </p></th>

        <td width="30%" >
            <?php
            echo "<b>No:</b> " . $model->orders->dealers->dealerCode . "<br/>";
            echo "<b>Name:</b> " . $model->orders->dealers->dealerName . "</br>";
            echo "<b>Address:</b> " . $model->orders->dealers->address2 . ", " . $model->orders->dealers->address3;
            ?></td> 
        <th width="20%"><p style="font-size:18px;font-weight: normal"> Sales Rep Details  </p></th>
        <td width="30%">
            <?php
            echo "<b>No:</b> ";
            echo isset($model->orders->salesrefs->srEmpNo) ? $model->orders->salesrefs->srEmpNo : "n/a";
            echo "<br/>";

            echo "<b>Name:</b> ";
            echo isset($model->orders->salesrefs->srName) ? $model->orders->salesrefs->srName : "n/a";
            echo "<br/>";

            echo "<b>Order type:</b> ";
            echo isset($model->orders->orderType) ? $model->orders->orderType : "n/a";
//            echo "<br/>";
            ?>
        </td>
        </tr>

        <tr class="odd" >
            <th width="20%"> <p style="font-size:18px;font-weight: normal">Payment details  </p></th>

        <td width="30%" >
            <?php
            echo "<b>Payment terms:</b> " . $model->orders->paymentType . "<br/>";
            echo "<b>No of Credit Dates:</b> ";
            echo isset($model->credit_days) ? $model->credit_days : "n/a";
//            echo "<br/>"
            ?>

        </td> 
        <th width="20%">&nbsp;</th>
        <td width="30%">
            <?php
            echo "<b>Delivery date:</b> ";
            echo isset($model->orders->deliveryDate) ? $model->orders->deliveryDate : "n/a";
            echo "<br/>";

            echo "<b>Order comments:</b> ";
            echo $model->orders->orderComment != "" ? $model->orders->orderComment : "n/a";
//            echo "<br/>";
            ?>
        </td>
        </tr>

        <tr class="even" >
            <th width="20%"><p style="font-size:18px;font-weight: normal">Distributor name</p></th>

        <td width="30%" >
            <?php
            echo isset($model->orders->dist->distName) ? $model->orders->dist->distName : "n/a";
//            echo "<br/>";
            ?>
        </td> 
        <th width="20%"><b>Due Date:</b><br/><b>Transaction Date:</b></th>
        <td width="30%"> 
            <?php
            echo $model->createdtime != "" ? $model->due_date : "n/a";
            echo "<br/>";
            echo $model->createdtime != "" ? date('Y-m-d H:i', strtotime($model->createdtime)) : "n/a";
            ?></td>
        </tr>

        </tbody>
    </table>

    <table  class="table table-striped table-bordered table-condensed table ">
        <input type="hidden" name="Invoice[date]" value="<?php echo $date; ?>" >

        <?php
        if (sizeof($row)) {
            $class = 'odd';
            ?>
            <thead><tr class="<?php $class; ?>">
                    <th>Line no</th>
                    <th style=\"text-align:right\">Product Code</th>
                    <th>Product</th>
                    <th>Unit</th>
                    <th>Invoiced Qty</th>
                    <th>Price</th>
                    <th>Discount</th>
                    <th style="text-align: right">Invoice Total</th>     
                </tr></thead><tbody>
                <?php
                $orderTotal = 0;
                $class = 'odd';
                $invoice_count = count($row);
                $count = 0;
                foreach ($row as $key => $data) {

                    if ($data->quantity > 0) {
                        $count++;
                        $class == 'odd' ? 'even' : 'odd';

                        $invoiceTotal = ($data->unit_price * $data->quantity) * (100 - $data->products->discount1) / 100;

                        echo '<tr class="' . $class . '">';
                        echo "<td style='vertical-align:middle'>" . $count . "</td>";
                        echo "<td style='vertical-align:middle'>" . $data->products->uniqueId . "</td>";
                        echo "<td style='height:30px; vertical-align:middle'>" . $data->products->name . "</td>";
                        echo "<td {$classTbl}>{$data->products->unit}</td>
                                <td {$classTbl}>{$data->quantity}</td>
                                <td {$classTbl}>" . Yii::app()->numberFormatter->formatCurrency($data->unit_price, '') . "</td>
                                <td {$classTbl}>{$data->products->discount1}</td>
                                <td {$classTbl} id='row_{$key}' >" . Yii::app()->numberFormatter->formatCurrency($invoiceTotal, '') . "</td>";
                        echo '</tr>';

                        $orderTotal += $invoiceTotal;
                    }
                }

                echo '<tr class="' . $class . '"><th>Total</th><th colspan="6"></th><th style="text-align: right" id="total_invoice_value">' . Yii::app()->numberFormatter->formatCurrency($orderTotal, '') . '</th></tr>';
            } else {
                echo '<tr><td>no order items added</td></tr>';
            }
            ?>
        </tbody>
    </table>

    <div class="form-actions" style="text-align: center">
        <?php
//        $this->widget('bootstrap.widgets.TbButton', array(
//            'buttonType' => 'submit',
//            'type' => 'danger',
//            'label' => 'Cancel Invoice',
//            'htmlOptions' => array('name' => 'cancel'),)
//        );
//        echo "&nbsp;&nbsp;";
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type' => 'primary',
            'label' => 'Print Invoice',
            'htmlOptions' => array('name' => 'invoice')
        ));
        ?>

    </div>

    <?php $this->endWidget(); ?>

</div>

