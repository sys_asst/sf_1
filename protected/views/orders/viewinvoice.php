<?php
/* @var $this OrdersController */
/* @var $model Orders */

$this->breadcrumbs=array(
	'Orders'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>View Invoice #<?php echo $model->invoice_no; ?></h1>
<div id="orders-grid" class="grid-view">
<?php $this->renderPartial('_viewinvoice', array('model'=>$model, 'row'=>$inventoryItems )); ?>
</div>