<?php
/* @var $this OrdersController */
/* @var $model Orders */

$this->breadcrumbs=array(
	'Orders'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>View Sales Order #<?php echo OrderNos::getOrderNo($model->id); ?></h1>
<div id="orders-grid" class="grid-view">
<?php $this->renderPartial('_invoice', array('model'=>$model, 'row'=>$row )); ?>
</div>