<?php
/* @var $this UnitCategoryController */
/* @var $model UnitCategory */

$this->breadcrumbs=array(
	'Unit Categories'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>Update UnitCategory <?php echo $model->name; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>