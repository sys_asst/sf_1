<?php
/* @var $this UnitCategoryController */
/* @var $model UnitCategory */

$this->breadcrumbs=array(
	'Unit Categories'=>array('index'),
	'Manage',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#unit-category-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Unit Categories</h1>

<!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
	'id'=>'unit-category-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'code',
		'name',
        array(
            'name'=>'status',
            'value'=>'UnitCategory::itemAlias("ItemStatus",$data->status)',
            'filter' => UnitCategory::itemAlias("ItemStatus"),
        ),
		'addedon',
        array(
            'name'=>'addedby',
            'value'=>'$data->addedby0->username',
            'filter' => CHtml::listData(User::model()->findAll(), 'id','username'),
        ),
		/*
		'updatedon',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'template'=>'{view}{update}'
		),
	),
)); ?>
