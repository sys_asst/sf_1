<?php
/* @var $this UnitCategoryController */
/* @var $model UnitCategory */

$this->breadcrumbs=array(
	'Unit Categories'=>array('index'),
	'Create',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>Create UnitCategory</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>