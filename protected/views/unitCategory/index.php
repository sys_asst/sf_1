<?php
/* @var $this UnitCategoryController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Unit Categories',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>Unit Categories</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
