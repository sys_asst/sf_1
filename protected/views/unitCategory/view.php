<?php
/* @var $this UnitCategoryController */
/* @var $model UnitCategory */

$this->breadcrumbs=array(
	'Unit Categories'=>array('index'),
	$model->name,
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>View UnitCategory <?php echo $model->name; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'code',
		'name',
        array(
            'name'=>'status',
            'value'=>CHtml::encode(UnitCategory::itemAlias('ItemStatus', $model->status))
        ),
		'addedon',
        array(
            'name'=>'addedby',
            'value'=>CHtml::encode(isset($model->addedby0->username) ? $model->addedby0->username : 'n/a')
        ),
		'updatedon',
	),
)); ?>
