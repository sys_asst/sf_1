<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 6/11/15
 * Time: 11:17 AM
 */

$this->breadcrumbs=array(
    'Products'=>array('admin'),
    'Price Update',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

    <h1>Product Price Update</h1>

<?php $this->renderPartial('_priceupdate', array('model'=>$model)); ?>