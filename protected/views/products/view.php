<?php
/* @var $this ProductsController */
/* @var $model Products */

$this->breadcrumbs=array(
	'Products'=>array('index'),
	$model->description,
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));

?>

<h1>View Products <?php echo $model->description; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'uniqueId',
		'description',
		'unit',
		'price',
		'discount1',
        array(
            'name'=>'main_cat_fk_id',
            'value'=>Categories::getCategoryText($model->main_cat_fk_id)
        ),
        array(
            'name'=>'sub_prod_cat_fk_id',
            'value'=>CHtml::encode(Categories::getCategoryText($model->sub_prod_cat_fk_id))
        ),
        array(
            'name'=>'category_id',
            'value'=>Categories::getCategoryText($model->category_id)
        ),
        array(
            'label'=>'Label Value',
            'value'=>Products::getProductLabelValues($model->id),
        ),
        array(
            'name'=>'status',
            'value'=>CHtml::encode($model->getStatusText())
        ),
		'lastUpdatedOn',
	),
)); ?>
