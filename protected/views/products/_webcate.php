<?php
/**
 * Created by JetBrains PhpStorm.
 * User: anura
 * Date: 5/15/14
 * Time: 1:16 PM
 * To change this template use File | Settings | File Templates.
 */
/*$form=$this->beginWidget('CActiveForm', array(
    'id'=>'products-form',
    'enableAjaxValidation'=>false
))*/
if(sizeof($leaflevels))
{
    $sladetails = array(new ProductsLabels);
    foreach($leaflevels as $leaf)
    {
        if(sizeof($leaf))
        {
            foreach($leaf as $k=>$leafs){
                echo CHtml::hiddenField('ProductsLabels['.$k.'][label_id]', "{$leafs['label_id_fk']}");
                echo CHtml::hiddenField('ProductsLabels['.$k.'][unit_name]', "{$leafs['unit']}");
            ?>
                <div class="row">
                    <?php // echo CHtml::label($leafs['label'],'ProductsLabels_'.$k.'_value'); ?>
                    <?php echo CHtml::textField('ProductsLabels['.$k.'][value]', (sizeof($values) && strlen($values[$k]['value'])) ? $values[$k]['value'] : ''); echo ' '.$leafs['unit']?>
                    <?php echo CHtml::error($model,'label'.$leafs['type'].'Value'); ?>
                </div>
            <?php
            }
        } else{
            echo '<div class="row">No added Label Values. Add Leaf Values for ';
            echo CHtml::link('this Category',Yii::app()->createUrl('/categories/update', array('id'=>$catid), '&'));
            echo '</div>';
        }
    }
} else {
    echo '<div class="row">No added Label Values. Add Leaf Values for ';
    echo CHtml::link('this Category',Yii::app()->createUrl('/categories/update', array('id'=>$catid), '&'));
    echo '</div>';
}
?>