<?php
/* @var $this ProductsController */
/* @var $data Products */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('uniqueId')); ?>:</b>
	<?php echo CHtml::encode($data->uniqueId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('unit')); ?>:</b>
	<?php echo CHtml::encode($data->unit); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('price')); ?>:</b>
	<?php echo CHtml::encode($data->price); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('discount1')); ?>:</b>
	<?php echo CHtml::encode($data->discount1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('main_cat_fk_id')); ?>:</b>
	<?php echo CHtml::encode($data->getProductText($data->main_cat_fk_id)); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('sub_prod_cat_fk_id')); ?>:</b>
	<?php echo CHtml::encode($data->sub_prod_cat_fk_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('category_id')); ?>:</b>
	<?php echo CHtml::encode($data->category_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('label1Value')); ?>:</b>
	<?php echo CHtml::encode($data->label1Value); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('label1Text')); ?>:</b>
	<?php echo CHtml::encode($data->label1Text); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('label2Value')); ?>:</b>
	<?php echo CHtml::encode($data->label2Value); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lastUpdatedOn')); ?>:</b>
	<?php echo CHtml::encode($data->lastUpdatedOn); ?>
	<br />

	*/ ?>

</div>