<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 6/11/15
 * Time: 11:18 AM
 */

?>
<div class="form">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
    'id'=>'price-update',
    'enableAjaxValidation'=>true,
    'type'=>'horizontal',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
    ),
));


    ?>

<p class="note">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>



    <div class="row-fluid">
        <?php echo $form->fileFieldRow($model, 'prdsrc', array('size' => 48));
        $form->error($model, 'prdsrc', array('clientValidation' => 'js:customValidateFile(messages)'), false, true);

        $infoFieldFile = (end($form->attributes));
        ?>
    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=> Yii::t('messages', 'Upload'),
        )); ?>

        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'label'=>Yii::t('messages', 'Cancel'),
            'type'=>'info',
            'url'=>Yii::app()->createUrl('products/admin')
        )); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
    <script>
        function customValidateFile(messages){
            var nameC= '#<?php echo $infoFieldFile['inputID']; ?>';
            //or var nameC= '#<?php //echo $infoFieldFileID; ?>';

            var control = $(nameC).get(0);

            //simulates the required validator and allowEmpty setting of the file validator
            if (control.files.length==0) {
                messages.push("File is required");
                return false;
            }

            file = control.files[0];

            //simulates the types setting of the file validator
            if (file.name.substr((file.name.lastIndexOf('.') +1)) !='txt') {
                messages.push("This is not a text file");
                return false;
            }

            //simulates the maxSize setting of the file validator
            if (file.size>1000000) {
                messages.push("File cannot be too large (size cannot exceed 1.000.000 bytes.)");
                return false;
            }

            //simulates the minSize setting of the file validator
            if (file.size<100) {
                messages.push("File cannot be too small (size cannot be smaller 100 bytes.)");
                return false;
            }

            //simulates the format type (extra checking) see also http://en.wikipedia.org/wiki/Internet_media_type

            if (file.type!="text/plain") {
                messages.push("This is not a valid file");
                return false;
            }

        }

    </script>