<?php
/* @var $this SalesrepsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Sales Team',
);

$this->menu=array(
	//array('label'=>'Create Salesreps', 'url'=>array('create')),
	array('label'=>'Manage Sales Team', 'url'=>array('admin')),
);
?>

<h1>Sales Team</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
