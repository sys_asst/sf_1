<?php
/* @var $this ProductsController */
/* @var $model Products */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
        'id'=>'products-form',
        'enableAjaxValidation'=>false,
        'type'=>'horizontal',
    ));
    ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
    <?php
    //print_r($model->getCategory($model->category_id));
    ?>
    <div class="row-fluid">
        <div class="span6">
            <fieldset>
                <legend>Product Category Details</legend>
            <?php //echo $form->textFieldRow($model,'distId',array('class'=>'input-block-level','maxlength'=>5,'size'=>4, 'readonly'=>true)); ?>
            <?php //echo $form->textFieldRow($model,'distName',array('class'=>'input-block-level','size'=>60,'maxlength'=>255)); ?>
            <?php //echo $form->textFieldRow($model,'distEmail',array('class'=>'input-block-level','size'=>60,'maxlength'=>100)); ?>
            <?php //echo $form->textFieldRow($model,'distPhone',array('class'=>'input-block-level','size'=>60,'maxlength'=>100)); ?>
            <?php echo $form->dropDownListRow($model, 'main_cat_fk_id', Categories::getCategoriesByLevel(1), array(
                //'empty'=>'Select Category',
                'prompt'=>'Select Main Category',
                'ajax'=>array(
                    'type'=>'POST', // request type
                    'url'=>CController::createUrl('getcategories'),
                    'update'=>'#Products_sub_prod_cat_fk_id', // selector to update
                    'data'=>array('parentid'=>'js:this.value'),
                )
            )); ?>
                <?php $subcategory = $model->getCategory($model->main_cat_fk_id);
                echo $form->dropDownListRow($model, "sub_prod_cat_fk_id", $subcategory, array(
                    'ajax'=>array(
                        'type'=>'POST', // request type
                        'url'=>CController::createUrl('getcategories'),
                        'update'=>'#Products_category_id', // selector to update
                        'data'=>array('parentid'=>'js:this.value'),
                    )
                ));
                ?>
                <?php
                $webcategory = CHtml::listData(Categories::model()->findAll('id=:catid',array(':catid'=>$model->category_id)), 'id','category');//CHtml::listData(Categories::model()->findByPk($model->category_id),'id','category');$model->getCategory($model->category_id,1);//

                echo $form->dropDownListRow($model, "category_id", $webcategory,array(
                    'ajax'=>array(
                        'type'=>'POST',
                        'url'=>CController::createUrl('getleaflevels'),
                        'update'=>'#web_cat',
                        'data'=>array('catid'=>'js:this.value','prodid'=>$model->id)
                    ))); ?>
                <?php echo $form->dropDownListRow($model,'status',Products::itemAlias('ItemStatus')); ?>

            </fieldset>
            <fieldset>
                <legend>Labels Value</legend>
                <div id="web_cat">
                    <?php
                    $leafValues = $model->getProductsLeafValues();

                    $leaflevels = Categories::getLeafValues($model->category_id);// $sqlData->getData();

                    $this->renderPartial('_webcate', array('model'=>$model,'leaflevels'=>$leaflevels,'values'=>$leafValues, 'catid'=>$model->category_id));
                    ?>
                </div>
            </fieldset>
        </div>
        <div class="span6">
            <fieldset>
                <legend>Product Details</legend>
                <?php //echo $form->textFieldRow($model,'uniqueId', array('readonly'=>($model->scenario == 'update')? true : false)); ?>
                <?php //echo $form->textAreaRow($model,'description',array('rows'=>6, 'cols'=>50,'readonly'=>($model->scenario == 'update')? true : false)); ?>
                <?php //echo $form->textAreaRow($model,'unit',array('size'=>20,'maxlength'=>20,'readonly'=>($model->scenario == 'update')? true : false)); ?>
                <?php //echo $form->textAreaRow($model,'price',array('readonly'=>($model->scenario == 'update')? true : false)); ?>
                <?php //echo $form->textAreaRow($model,'discount1',array('readonly'=>($model->scenario == 'update')? true : false)); ?>
                <?php echo $form->textFieldRow($model,'uniqueId', array('readonly'=>($model->scenario == 'update') ? true : false)); ?>
                <?php echo $form->textAreaRow($model,'description',array('rows'=>1, 'cols'=>50,'readonly'=>($model->scenario == 'update') ? true : false)); ?>
                <?php echo $form->textFieldRow($model,'unit',array('size'=>20,'maxlength'=>20,'readonly'=>($model->scenario == 'update') ? true : false)); ?>
                <?php echo $form->textFieldRow($model,'price',array('maxlength'=>20,'readonly'=>($model->scenario == 'update') ? true : false)); ?>
                <?php echo $form->textFieldRow($model,'discount1',array('maxlength'=>20,'readonly'=>($model->scenario == 'update') ? true : false)); ?>
            </fieldset>
        </div>
    </div>
    
    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>$model->isNewRecord ? Yii::t('messages', 'Create') : Yii::t('messages', 'Save'),
        )); ?>

        <?php

        $this->widget('bootstrap.widgets.TbButton', array(
            'label'=>Yii::t('messages', 'Cancel'),
            'type'=>'info',
            'url'=>Yii::app()->createUrl('products/admin')
        ));

        ?>
    </div>


<?php $this->endWidget(); ?>

</div><!-- form -->