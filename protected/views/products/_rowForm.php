<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Randika
 * Date: 5/23/14
 * Time: 2:18 PM
 * To change this template use File | Settings | File Templates.
 */


 $row_id = "sladetail-" . $key ?>

<div class='row-fluid' id="<?php echo $row_id ?>">
    <?php
    echo $form->hiddenField($model, "[$key]product_id");
    echo $form->updateTypeField($model, $key, "product_id", array('key' => $key));
    ?>
    <div class="span3">
        <?php
        echo $form->labelEx($model, "[$key]label_id");
        $leaflevelattrbutes = CHtml::listData(LeafLevelAttributeLabels::model()->findAll('status > 0 ORDER BY label'), 'id','label');
        echo $form->dropDownList($model, "[$key]label_id",$leaflevelattrbutes);
        echo $form->error($model, "[$key]label_id");
        ?>

    </div>

    <div class="span3">
        <?php
        echo $form->labelEx($model, "[$key]value");
        echo $form->textField($model, "[$key]value");
        echo $form->error($model, "[$key]value");
        ?>
    </div>

    <div class="span3">
        <?php
        /*echo $form->labelEx($model, "[$key]schedule");

        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model' => $model,
            'attribute' => "[$key]schedule",
            'options' => array(
                'showAnim' => 'fold',
            ),
            'htmlOptions' => array(
                'style' => 'height:20px;'
            ),
        ));
        echo $form->error($model, "[$key]schedule");*/
        ?>


    </div>
    <div class="span2">

        <?php echo $form->deleteRowButton($row_id, $key); ?>
    </div>
</div>