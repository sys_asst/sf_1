<?php
/* @var $this ProductsController */
/* @var $model Products */

$this->breadcrumbs=array(
	'Products'=>array('admin'),
	'Manage',
);

//$this->renderPartial('../layouts/_actions', array('model'=>$model));

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#products-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Products</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
	'id'=>'products-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
		'uniqueId',
        array(
            'name'=>'main_cat_fk_id',
            'value'=>'Categories::getCategoryText($data->main_cat_fk_id)',
            'filter'=>Categories::getCategoriesByLevel(1),
        ),
        array(
            'name'=>'sub_prod_cat_fk_id',
            'value'=>'Categories::getCategoryText($data->sub_prod_cat_fk_id)',
            //'filter'=>Categories::getCategoriesByLevel(2),
            'filter' => $model->main_cat_fk_id ? CHtml::listData(Categories::model()->findAllByAttributes(
                    array(),
                    "parent_cat_id = :parent_cat_id",
                    array(':parent_cat_id'=>$model->main_cat_fk_id)),
                'id','category') : Categories::getCategoriesByLevel(2),
        ),
        array(
            'name'=>'category_id',
            'value'=>'Categories::getCategoryText($data->category_id)',
            //'filter'=>Categories::getCategoriesByLevel(3),
            'filter' => $model->sub_prod_cat_fk_id ? CHtml::listData(Categories::model()->findAllByAttributes(
                    array(),
                    "parent_cat_id = :parent_cat_id",
                    array(':parent_cat_id'=>$model->sub_prod_cat_fk_id)),
                'id','category') : Categories::getCategoriesByLevel(3),
        ),
        'unit',
		'description',
        array(
            'name'=>'status',
            'value'=>'Products::itemAlias("ItemStatus",$data->status)',
            'headerHtmlOptions' => array('style' => 'width: 80px'),
            'filter' => Products::itemAlias("ItemStatus"),
        ),
        array(
            'name'=>'mapped',
            'value'=>'Products::itemAlias("AdminStatus",$data->mapped)',
            'headerHtmlOptions' => array('style' => 'width: 80px'),
            'filter' => Products::itemAlias("AdminStatus"),
        ),
		//'price',
		//'discount1',

		/*
		'main_cat_fk_id',
		'sub_prod_cat_fk_id',
		'category_id',
		'label1Value',
		'label1Text',
		'label2Value',
		'status',
		'lastUpdatedOn',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'template' => '{view}{update}',
		),
	),
)); ?>
