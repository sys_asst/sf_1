<?php
/* @var $this SalesrepsController */
/* @var $model Salesreps */

$this->breadcrumbs=array(
    'Inventory'=>array('create'),
    'Manage',
);

$this->menu=array(
//    array('label'=>'List InventoryItems', 'url'=>array('index')),
    array('label'=>'Add Inventory', 'url'=>array('create')),
);

//$this->breadcrumbs=array(
//    'Salesreps'=>array('index'),
//    'Manage',
//);

//$this->renderPartial('../layouts/_actions', array('model'=>$model));

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#salesreps-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Inventory</h1>

<p>
    You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
    or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
    <?php $this->renderPartial('_search',array(
        'model'=>$model,
    )); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
    'id'=>'inventory-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
//        'id',
        'invoice_no',   
        array(
            'name'=>'inventory_type_id',
            'value'=>'isset($data->inventoryType->name) ? $data->inventoryType->name : "n/a"',
            'filter' => CHtml::listData(InventoryType::model()->findAll(), 'id','name'),
        ),
        'ref_no',
//   
//        array(
//            'name'=>'distId',
//            'value'=>'isset(Distributors::model()->findByAttributes(array("distId"=>$data->distId))->distName) ? Distributors::model()->findByAttributes(array("distId"=>$data->distId))->distName : "n/a"',
//            'filter' => CHtml::listData(Distributors::model()->findAll(), 'id','distName'),
//            'htmlOptions'=>array('width'=>'200'),
//        ),

        'transaction_date',
         'createdtime',
         'remarks',

        array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
//            'htmlOptions'=>'width:20',
//            'htmlOptions'=>array('style'=>'width: 100px; text-align:center'),
            'template' => '{view}',
            'buttons' => array(
                'view' => array( //the name {reply} must be same
                    'label' => 'View', // text label of the button user/admin/create&refid=68&src=sref
                    'url' => 'CHtml::normalizeUrl(array("inventory/view?id=".$data->id))', //Your URL According to your wish
                    //'imageUrl' => Yii::app()->baseUrl . '/images/user_add.png', // image URL of the button. If not set or false, a text link is used, The image must be 16X16 pixels
                ),
            ),

        ),
//
//        array(
//            'class'=>'bootstrap.widgets.TbButtonColumn',
//            'template' => '{view}'
//        )
    ),
)); ?>

<div class="form-actions" style="text-align: center">
    <?php echo CHtml::button('Create PSV', array('submit' => array('inventory/create',),'class'=>'btn btn-primary')); ?>
</div>



