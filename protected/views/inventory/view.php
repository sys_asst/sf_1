<?php
/* @var $this InventoryItemsController */
/* @var $model InventoryItems */

$this->breadcrumbs = array(
    'Inventory Items' => array('admin'),
    $model->id,
);
?>

<h1>View Inventory #<?php echo $model->invoice_no; ?></h1>

<?php
$this->widget('bootstrap.widgets.TbDetailView', array(
//$this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
//		'id',
        array(
            'name' => 'inventory_type_id',
            'value' => isset($model->inventoryType->name) ? $model->inventoryType->name : "n/a",
        ),
        'invoice_no',
        'createdtime',
        'remarks',
        'ref_no',
        'transaction_date',
//        array(
//            'name' => 'transaction_date',
//            'value' => isset($model->transaction_date) ? date('Y-m-d', strtotime($model->transaction_date)) : 'n/a',
//        ),
//        array(
//            'name' => 'distId',
//            'value' => isset($model->distributors->distName) ? $model->distributors->distName : 'n/a',
//        ),
//        'createdtime',
    ),
));

if (!empty($inventoryItems)) {
    ?>
    <h4>Product Details</h4>
    <table class="items table table-striped table-bordered table-condensed">
        <thead>
            <tr>
                <th id="yw1_c0">Product ID</th>
                <th id="yw1_c1">Product</th>
                <th id="yw1_c2">Unit</th>
                <th id="yw1_c3" style="text-align:right">Quantity</th>
                <th id="yw1_c4" style="text-align:right">Unit Price</th>
                <th id="yw1_c5" style="text-align:right">Discount</th>
                <th id="yw1_c6" style="text-align:right">Total Value</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $totalPrice = 0;
            foreach ($inventoryItems1 as $data) {
                $totalPrice += $data->total_price;
                echo '<tr class="odd">
                    <td>' . $data->products->uniqueId . '</td>
                    <td>' . $data->products->name . '</td>
                    <td>' . $data->products->unit . '</td>
                    <td style="text-align:right">' . $data->quantity . '</td>
                    <td style="text-align:right">' . number_format($data->unit_price,2) . '</td>
                    <td style="text-align:right">' . $data->discount . ' </td>
                    <td style="text-align:right">' . number_format($data->total_price,2) . '</td>
                  </tr>';
            }
            ?>
            <tr class="even">
                <td colspan="6" style="text-align:right"><b>Grand Total</b></td><td style="text-align:right"><?php echo number_format($totalPrice, 2); ?></td></tr>

        </tbody>
    </table>

<?php } ?>

<div class="form-actions" style="text-align: center">
    <?php echo CHtml::button('Manage Inventory', array('submit' => array('inventory/admin',), 'class' => 'btn btn-primary')); ?>
</div>

