<?php
/* @var $this InventoryController */
/* @var $model Inventory */

$this->breadcrumbs = array(
    'Inventories' => array('index'),
    $model->id,
);

$this->menu = array(
    array('label' => 'List Inventory', 'url' => array('index')),
    array('label' => 'Create Inventory', 'url' => array('create')),
    array('label' => 'Update Inventory', 'url' => array('update', 'id' => $model->id)),
    array('label' => 'Delete Inventory', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?')),
    array('label' => 'Manage Inventory', 'url' => array('admin')),
);
?>

<h1>View Inventory #<?php echo Yii::app()->getRequest()->getParam('id'); ?></h1>

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'type' => 'striped bordered condensed',
    'dataProvider' => $model->view(),
    'htmlOptions' => array('style' => 'padding-top:0px'),
//    'filter'=>$model,
    'columns' => array(
        'id',
        'invoice_no',
        'remarks',
        'transaction_date',
        'createdtime',
    ),
));

if (isset($inventoryItems)) {

    echo "<h4>Product Details</h4>";

    $this->widget('bootstrap.widgets.TbGridView', array(
        'type' => 'striped bordered condensed',
        'dataProvider' => $inventoryItems->search(),
        'htmlOptions' => array('style' => 'padding-top:0px'),
//    'filter'=>$model,
        'columns' => array(
            'id',
            array(
                'name' => 'products_id',
                'value' => '$data->products->name',
            ),
            'quantity',
            'unit_price',
            'total_price',
        ),
    ));
}
?>

<div class="form-actions" style="text-align: center">
<?php echo CHtml::button('Manage Inventory', array('submit' => array('inventory/admin',), 'class' => 'btn btn-primary')); ?>
</div>
