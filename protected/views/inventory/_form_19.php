<?php
/* @var $this InventoryController */
/* @var $model Inventory */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'=>'inventory-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation'=>false,
        'type'=>'horizontal',
    )); ?>

<?php //$form=$this->beginWidget('CActiveForm', array(
//	'id'=>'inventory-form',
//	// Please note: When you enable ajax validation, make sure the corresponding
//	// controller action is handling ajax validation correctly.
//	// There is a call to performAjaxValidation() commented in generated controller code.
//	// See class documentation of CActiveForm for details on this.
//	'enableAjaxValidation'=>false,
//)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>


    <div class="row-fluid">
        <div class="span5">

            <?php echo $form->labelEx($model,'ref_no'); ?>
            <?php echo $form->textField($model,'ref_no',array('size'=>50,'maxlength'=>50)); ?>
            <?php echo $form->error($model,'ref_no'); ?>



<!--            <div class="control-group ">-->
<!--                <label class="control-label " for="tomdate">-->
<!--                    --><?php //echo Yii::t('messages', 'To Date');//$attributeLabels['publishDate']?>
<!---->
<!--                </label>-->
<!--                <div class="controls">-->
<!--                    --><?php
//                    $calLang = '';//Yii::app()->toolKit->getComponenetSpecificLangIdentifier('juiDateTimePicker');
//                    echo $this->widget('zii.widgets.jui.CJuiDatePicker', array(
//                            'model'=>$model,
//                            'attribute'=>'todate',
//                            //'language' => 'ja',
//                            // 'i18nScriptFile' => 'jquery.ui.datepicker-ja.js', (#2)
//                            'htmlOptions' => array(
//                                'id' => 'todate_search',
//                                'size' => '10',
//                            ),
//                            'options' => array(
//                                'dateFormat' => 'yy-mm-dd',
//                                'showButtonPanel' => true,
//                                'showOn' => 'focus'
//                            )
//                        ),
//                        true);
//                    ?>
<!--                    --><?php //echo $form->error($model, 'transaction_date'); ?>
<!--                </div>-->
<!--            </div>-->





<!--            --><?php //echo $form->labelEx($model,'transaction_date'); ?>
<!--            --><?php //echo $form->textField($model,'transaction_date'); ?>
<!--            --><?php //echo $form->error($model,'transaction_date'); ?>


            <?php echo $form->labelEx($model,'remarks'); ?>
            <?php echo $form->textArea($model,'remarks',array('rows'=>6, 'cols'=>50)); ?>
            <?php echo $form->error($model,'remarks'); ?>

            </div>

<!---->
<!--        <div class="span5">-->
<!---->
<!--      -->
<!--        </div>-->
        <div class="span6">

        <div class="control-group ">
            <label class="control-label " for="transaction_date">
                <?php echo Yii::t('messages', 'Transaction Date');//$attributeLabels['publishDate']?>

            </label>
            <div class="controls">
                <?php
                $calLang = '';//Yii::app()->toolKit->getComponenetSpecificLangIdentifier('juiDateTimePicker');
                echo $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,
                        'attribute'=>'transaction_date',
                        //'language' => 'ja',
                        // 'i18nScriptFile' => 'jquery.ui.datepicker-ja.js', (#2)
                        'htmlOptions' => array(
                            'id' => 'transaction_date_search',
                            'size' => '10',
                        ),
                        'options' => array(
                            'dateFormat' => 'yy-mm-dd',
                            'showButtonPanel' => true,
                            'showOn' => 'focus'
                        )
                    ),
                    true);
                ?>
                <?php echo $form->error($model, 'transaction_date'); ?>
            </div>
        </div>

    </div>
        </div>






    <?php $this->widget('bootstrap.widgets.TbGridView', array(
        'type'=>'striped bordered condensed',
        'id'=>'products-grid',
        'dataProvider'=>$product->search(),
        'filter'=>$product,
        'columns'=>array(
            //'id',
//            'uniqueId',
//            array(
//                'name'=>'main_cat_fk_id',
//                'value'=>'Categories::getCategoryText($data->main_cat_fk_id)',
//                'filter'=>Categories::getCategoriesByLevel(1),
//            ),
//            array(
//                'name'=>'sub_prod_cat_fk_id',
//                'value'=>'Categories::getCategoryText($data->sub_prod_cat_fk_id)',
//                //'filter'=>Categories::getCategoriesByLevel(2),
//                'filter' => $product->main_cat_fk_id ? CHtml::listData(Categories::model()->findAllByAttributes(
//                        array(),
//                        "parent_cat_id = :parent_cat_id",
//                        array(':parent_cat_id'=>$product->main_cat_fk_id)),
//                    'id','category') : Categories::getCategoriesByLevel(2),
//            ),
            array(
                'name'=>'category_id',
                'value'=>'Categories::getCategoryText($data->category_id)',
                //'filter'=>Categories::getCategoriesByLevel(3),
                'filter' => $product->sub_prod_cat_fk_id ? CHtml::listData(Categories::model()->findAllByAttributes(
                        array(),
                        "parent_cat_id = :parent_cat_id",
                        array(':parent_cat_id'=>$product->sub_prod_cat_fk_id)),
                    'id','category') : Categories::getCategoriesByLevel(3),
                'htmlOptions'       => array('style' => 'width:20%'),

            ),
            array(
                'name'=>'description',
                'value'=>'$data->description',
                //'filter'=>Categories::getCategoriesByLevel(3),
//                'filter' => $product->description ,
                'htmlOptions'       => array('style' => 'width:20%'),

            ),array(
                'name'=>'id',
                'value'=>'$data->description',
                //'filter'=>Categories::getCategoriesByLevel(3),
//                'filter' => $product->description ,
//            'textfeild'=>true,
                'htmlOptions'       => array('<input type="text" maxlength="100" name="Distributors[distEmail]">'),

            ),
//            'description',

            array(
                'class'=>'bootstrap.widgets.TbButtonColumn',
                'template' => '{update}',
            ),
        ),
    )); ?>


	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->