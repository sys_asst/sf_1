<?php
/* @var $this InventoryItemsController */
/* @var $model InventoryItems */

$this->breadcrumbs = array(
    'Inventory Items' => array('admin'),
    $model->id,
);

//$this->menu = array(
//    array('label' => 'List InventoryItems', 'url' => array('index')),
//    array('label' => 'Create InventoryItems', 'url' => array('create')),
//    array('label' => 'Update InventoryItems', 'url' => array('update', 'id' => $model->id)),
//    array('label' => 'Delete InventoryItems', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?')),
//    array('label' => 'Manage InventoryItems', 'url' => array('admin')),
//);
?>

<h1>View Inventory #<?php echo $model->invoice_no; ?></h1>

<?php
$this->widget('bootstrap.widgets.TbDetailView', array(
//$this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
//		'id',
        array(
            'name' => 'inventory_type_id',
            'value' => isset($model->inventoryType->name) ? $model->inventoryType->name : "n/a",
        ),
        'invoice_no',
        'createdtime',
        'remarks',
        'ref_no',
        'transaction_date',
//        array(
//            'name' => 'transaction_date',
//            'value' => isset($model->transaction_date) ? date('Y-m-d', strtotime($model->transaction_date)) : 'n/a',
//        ),
//        array(
//            'name' => 'distId',
//            'value' => isset($model->distributors->distName) ? $model->distributors->distName : 'n/a',
//        ),
//        'createdtime',
    ),
));

Yii::import('zii.widgets.grid.CGridColumn');

class TotalColumn extends CGridColumn {

    private $_total = 0;

    public function renderDataCellContent($row, $inventoryItems) { // $row number is ignored
//        $this->_total .= "- ".$row." -";
        $this->_total += $inventoryItems->total_price;
        if(count($inventoryItems) == $row){
       return   $this->_total;
        }

    }

}

if (isset($inventoryItems)) {

    echo "<h4>Product Details</h4>";

    $this->widget('bootstrap.widgets.TbGridView', array(
        'type' => 'striped bordered condensed',
        'dataProvider' => $inventoryItems->search(),
        'htmlOptions' => array('style' => 'padding-top:0px; text-align:right'),
//    'filter'=>$model,
        'columns' => array(
//            'id',
            array(
//                 'htmlOptions' => array('style' => ' text-align:right'),
                'header' => 'Product Code',
                'value' => '$data->products->uniqueId',
            ),
            array(
                'name' => 'products_id',
                'value' => '$data->products->name',
            ),
            array(
                'header' => 'Unit',
                'value' => '$data->products->unit',
            ),
            array(
                'name' => 'quantity',
                'value' => '$data->quantity',
                'headerHtmlOptions' => array('style' => 'text-align:right'),
                'htmlOptions' => array('style' => 'text-align:right'),
            ),
            array(
                'name' => 'unit_price',
                'value' => '$data->unit_price',
                'headerHtmlOptions' => array('style' => 'text-align:right'),
                'htmlOptions' => array('style' => 'text-align:right'),
            ),
            array(
                'name' => 'discount',
                'value' => '$data->discount',
                'headerHtmlOptions' => array('style' => 'text-align:right'),
                'htmlOptions' => array('style' => 'text-align:right'),
            ),
            array(
                'name' => 'total_price',
                'value' => '$data->total_price',
                'headerHtmlOptions' => array('style' => 'text-align:right'),
                'htmlOptions' => array('style' => 'text-align:right'),
            ),

            array(
//                'header' => 'Grand Total',
                'class' => 'TotalColumn',
                'headerHtmlOptions' => array('style' => 'visibility:hidden'),
                'htmlOptions' => array('style' => 'visibility:hidden'),
            )
        ),

    ));
    
}
?>
<table id="yw0" class="detail-view table table-striped table-condensed">
<tr class="odd">
    <td colspan="6" ><b>Grand Total</b></td><td style="text-align:right">1000</td></tr>
</table>


<div class="form-actions" style="text-align: center">
    <?php echo CHtml::button('Manage Inventory', array('submit' => array('inventory/admin',), 'class' => 'btn btn-primary')); ?>
</div>

