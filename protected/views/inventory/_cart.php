<?php
//$ajaxUpdate = true;
//
//if(isset(Yii::app()->session['cart_count'])){
//    $itemsCount = Yii::app()->session['cart_count'];
//}
//
//if($itemsCount < 2){
//    $ajaxUpdate = false;
//}
//echo $itemsCount;

$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'inventory-items-grid',
    'dataProvider' => $model->search(),
    'ajaxUpdate' => false,
    'columns' => array(
        array(
            'header' => 'Prodcut ID',
            'value' => 'isset($data->products->uniqueId) ? $data->products->uniqueId : "n/a"',
        ),
        array(
            'name' => 'products_id',
            'value' => 'isset($data->products->name) ? $data->products->name : "n/a"',
        ),
        array(
            'header' => 'Unit',
            'value' => 'isset($data->products->unit) ? $data->products->unit : "n/a"',
        ),
        array(
            'name' => 'quantity',
            'value' => '$data->quantity',
            'headerHtmlOptions' => array('style' => 'text-align:center'),
            'htmlOptions' => array('style' => 'text-align:center'),
        ),
        array(
            'name' => 'unit_price',
            'value' => '$data->unit_price',
            'headerHtmlOptions' => array('style' => 'text-align:right'),
            'htmlOptions' => array('style' => 'text-align:right'),
        ),
        array(
            'name' => 'discount',
            'value' => '$data->discount',
            'headerHtmlOptions' => array('style' => 'text-align:right'),
            'htmlOptions' => array('style' => 'text-align:right'),
        ),
        array(
            'name' => 'total_price',
            'value' => '$data->total_price',
            'headerHtmlOptions' => array('style' => 'text-align:right'),
            'htmlOptions' => array('style' => 'text-align:right'),
        ),
//		'products_id',
//		'quantity',
//		'unit_price',
//		'discount',
//		'total_price',
//        array(
//            'class'=>'bootstrap.widgets.TbButtonColumn',
////            'htmlOptions'=>'width:20',
//            'htmlOptions'=>array('style'=>'width: 100px; text-align:center'),
//            'template' => '{delete}',
//            'buttons' => array(
//                'delete' => array( //the name {reply} must be same
//                    'label' => 'Delete', // text label of the button user/admin/create&refid=68&src=sref
//                    'url' => 'CHtml::normalizeUrl(array("inventoryItems/delCart?id=".$data->id))', //Your URL According to your wish
//                    //'imageUrl' => Yii::app()->baseUrl . '/images/user_add.png', // image URL of the button. If not set or false, a text link is used, The image must be 16X16 pixels
//                ),
//            ),
//
//        ),
    ),
));
?>
<!--<div class="form-actions">
    <?php
//    $this->widget('bootstrap.widgets.TbButton', array(
//        'buttonType' => 'submit',
//        'type' => 'info',
//        'label' => 'Close',
//        'htmlOptions' => array('name' => 'close'),
//    ));
//    ?>
</div>-->


