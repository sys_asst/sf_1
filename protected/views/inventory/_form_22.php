<?php
/* @var $this InventoryController */
/* @var $model Inventory */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'inventory-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
        'type' => 'horizontal',
    )); ?>



    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>


    <div class="row-fluid">




        <div class="span5">

            <?php echo $form->textFieldRow($model, 'ref_no', array('readonly'=>$readonly,'style'=>'width:220px', 'class' => 'input-block-level', 'size' => 20, 'maxlength' => 10)); ?>

            <div class="control-group ">
                <?php echo $form->labelEx($model, 'web_category',array('class'=>'control-label')); ?>
                <div class="controls">
                    <?php
                    $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                        'name' => 'web_category',
//                'value'=>'test:ttt',
                        // additional javascript options for the autocomplete plugin
                        'options' => array(
                            'minLength' => '1',
                        ),
//                'source'=>"js:function(request, response) {
//        $.getJSON('".$this->createUrl('inventory/getProducts')."', {
//            term: $('#Inventory_ref_no').val(),
//        }, response);}",

                        'source' => $this->createUrl("inventory/getProducts"),
                        'htmlOptions' => array(
                            'style' => 'height:20px;',
                        ),
                    ));
                    ?>
                </div>
            </div>

            <div class="control-group ">
                <?php echo $form->labelEx($model, 'products_id',array('class'=>'control-label')); ?>
                <div class="controls">
                    <?php
                    $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                        'name' => 'product_name',
                        // additional javascript options for the autocomplete plugin
                        'options' => array(
                            'minLength' => '1',
                        ),
                        'source'=>"js:function(request, response) {
        $.getJSON('".$this->createUrl('inventory/getProductsName')."', {
            term: $('#product_name').val(),web_cat:$('#web_category').val()
        }, response);}",

                        'options'=>array(
//                    'delay'=>300,
                            'minLength'=>'1',
                            'showAnim'=>'fold',
                            'select'=>"js:function(event, ui) {
////            $('#product_name').val(ui.name);

            $('#div_uniqueId').show();
            $('#Inventory_uniqueId').val(ui.item.uniqueId);

            $('#div_price').show();
            $('#Inventory_price').val(ui.item.price);

            $('#Inventory_product_id').val(ui.item.id);

            $('#div_qty').show();

            $('#div_av_qty').show();
            $('#Inventory_av_qty').val(ui.item.av_qty);

        }"
                        ),
                        'htmlOptions' => array(
                            'style' => 'height:20px;',
                        ),
                    ));
                    ?>
                </div>
            </div>



            <div id ='div_uniqueId' style="display: none">
                <?php echo $form->textFieldRow($model, 'uniqueId', array('readonly'=>true,'name'=>'InventoryItems[uniqueId]', 'id'=>'Inventory_uniqueId','style'=>'width:220px', 'class' => 'input-block-level')); ?>
            </div>


            <div id ='div_price' style="display: none">
                <?php echo $form->textFieldRow($model, 'price', array('readonly'=>true,'name'=>'InventoryItems[price]', 'id'=>'Inventory_price','style'=>'width:220px', 'class' => 'input-block-level')); ?>
            </div>


            <div id ='div_av_qty' style="display: none">
                <?php echo $form->textFieldRow($model, 'av_quantity', array('readonly'=>true,'name'=>'InventoryItems[av_quantity]', 'id'=>'Inventory_av_qty','style'=>'width:220px', 'class' => 'input-block-level')); ?>

            </div>


            <div id ='div_qty' style="display: none">
                <?php echo $form->textFieldRow($model, 'quantity', array('name'=>'InventoryItems[quantity]', 'id'=>'Inventory_qty','style'=>'width:220px', 'class' => 'input-block-level')); ?>

            </div>


            <input type="hidden" name="InventoryItems[products_id]" id="Inventory_product_id">


            <!--            <div class="row">-->
            <!--                --><?php //echo $form->labelEx($model,'products_id'); ?>
            <!--                --><?php //echo $form->textField($model,'products_id'); ?>
            <!--                --><?php //echo $form->error($model,'products_id'); ?>
            <!--            </div>-->

        </div>


        <div class="span6">



            <div class="control-group ">
                <label class="control-label " for="transaction_date">
                    <?php echo Yii::t('messages', 'Document Date');//$attributeLabels['publishDate']?>

                </label>

                <div class="controls">
                    <?php
                    $calLang = '';//Yii::app()->toolKit->getComponenetSpecificLangIdentifier('juiDateTimePicker');
                    echo $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model' => $model,
                            'attribute' => 'transaction_date',
                            //'language' => 'ja',
                            // 'i18nScriptFile' => 'jquery.ui.datepicker-ja.js', (#2)
                            'htmlOptions' => array(
                                'id' => 'transaction_date_search',
                                'size' => '10',
                                'readonly'=>$readonly,
                            ),
                            'options' => array(
                                'dateFormat' => 'yy-mm-dd',
                                'showButtonPanel' => true,
                                'showOn' => 'focus'
                            )
                        ),
                        true);
                    ?>
                    <?php echo $form->error($model, 'transaction_date'); ?>
                </div>
            </div>

            <?php echo $form->textFieldRow($model, 'createdtime', array('value'=> date('Y-m-d H:i:s'), 'readonly'=>true, 'style'=>'width:220px', 'class' => 'input-block-level', 'size' => 20, 'maxlength' => 10)); ?>


            <?php echo $form->textAreaRow($model, 'remarks', array('readonly'=>$readonly, 'style'=>'width:220px', 'class' => 'input-block-level', 'rows' => 3, 'cols' => 50)); ?>

<!--            --><?php //echo $form->labelEx($model, 'remarks'); ?>
<!--            --><?php //echo $form->textArea($model, 'remarks', array('rows' => 3, 'cols' => 50)); ?>
<!--            --><?php //echo $form->error($model, 'remarks'); ?>


        </div>


        <fieldset class="label_data">
            <legend>Product Details</legend>
            <?php echo $form->textAreaRow($model, 'remarks', array('readonly'=>$readonly, 'style'=>'width:220px', 'class' => 'input-block-level', 'rows' => 3, 'cols' => 50)); ?>

        </fieldset>



    </div>

        <div class="form-actions">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType' => 'submit',
                'type' => 'primary',
                'label' => $model->isNewRecord ? Yii::t('messages', 'Add') : Yii::t('messages', 'Update'),
            )); ?>


        </div>





<!--    <div class="row buttons">-->
<!--        --><?php //echo CHtml::submitButton($model->isNewRecord ? 'Add' : 'Update'); ?>
<!--    </div>-->



    <?php

    if($itemsCount > 0)
    $this->renderPartial('_cart', array('model'=>$inventoryItems)); ?>


    <?php $this->endWidget(); ?>

</div><!-- form -->