<?php
/* @var $this InventoryController */
/* @var $model Inventory */

$this->breadcrumbs=array(
	'Inventories'=>array('admin'),
	'Create',
);



$this->menu=array(
    array('label'=>'Create Inventory', 'url'=>array('create')),
    array('label'=>'Manage Inventory', 'url'=>array('admin')),
);

//$this->menu=array(
//	array('label'=>'List Inventory', 'url'=>array('index')),
//	array('label'=>'Manage Inventory', 'url'=>array('admin')),
//);
?>

<h1>Inventory >> Physical Stock Verification</h1>

<?php //$this->renderPartial('_form', array('model'=>$model)); ?>
<?php $this->renderPartial('_form', array('readonly' => $readonly,'model'=>$model, 'itemsCount'=>$itemsCount, 'inventoryItems'=>$inventoryItems)); ?>