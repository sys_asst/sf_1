<?php
/* @var $this InventoryController */
/* @var $model Inventory */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'inventory-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
        'type' => 'horizontal',
    ));
    ?>



    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>


    <!--<div class="row-fluid">-->

    <div class="span5">

        <?php echo $form->textFieldRow($model, 'ref_no', array('readonly' => $readonly, 'style' => 'width:220px', 'class' => 'input-block-level', 'size' => 20, 'maxlength' => 10)); ?>


        <div class="control-group ">
            <label class="control-label " for="transaction_date">
                <?php echo $form->labelEx($model, 'transaction_date'); ?>
            </label>

            <div class="controls">
                <?php
                $calLang = ''; //Yii::app()->toolKit->getComponenetSpecificLangIdentifier('juiDateTimePicker');
                echo $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $model,
                    'attribute' => 'transaction_date',
                    //'language' => 'ja',
                    // 'i18nScriptFile' => 'jquery.ui.datepicker-ja.js', (#2)
                    'htmlOptions' => array(
                        'id' => 'transaction_date_search',
                        'size' => '10',
                        'readonly' => $readonly,
                        'value' => $model->transaction_date
                    ),
                    'options' => array(
                        'dateFormat' => 'yy-mm-dd',
                        'showButtonPanel' => false,
                        'showOn' => 'focus',
                        'maxDate' => date('Y-m-d H:i:s'),
                        'today' => true,
                    )
                        ), true);
                ?>
                <?php echo $form->error($model, 'transaction_date'); ?>
            </div>
        </div>

    </div>

    <div class="span6">

      <?php echo $form->textFieldRow($model, 'createdtime', array('value' => date('Y-m-d H:i:s'), 'readonly' => true, 'style' => 'width:220px', 'class' => 'input-block-level', 'size' => 20, 'maxlength' => 10)); ?>
      
      <?php echo $form->textAreaRow($model, 'remarks', array('readonly' => $readonly, 'style' => 'width:220px', 'class' => 'input-block-level', 'rows' => 2, 'cols' => 50)); ?>
    </div>



    <fieldset class="label_data" style="clear: both; width: 100%;">
        <legend>Product Details</legend>
        <table  class="table table-striped table-bordered table-condensed table ">

            <?php
            $class = 'odd';
            ?>
            <thead><tr class="<?php echo $class; ?>">
                    <th><?php echo $form->labelEx($model, 'sub_cat'); ?> </th>
                    <th><?php echo $form->labelEx($model, 'web_category') ?></th>
                    <th><?php echo $form->labelEx($model, 'uniqueId'); ?></th>
                    <th><?php echo $form->labelEx($model, 'products_id'); ?></th>
                    <th> <?php echo $form->labelEx($model, 'add_minus'); ?></th>
                    <th> <?php echo $form->labelEx($model, 'quantity'); ?></th>
                    <th><?php echo $form->labelEx($model, 'price'); ?></th>
                    <th>Discount</th>
                    <th><?php echo $form->labelEx($model, 'total_price'); ?></th>
                    <th><?php echo $form->labelEx($model, 'av_quantity'); ?></th>

                </tr></thead><tbody>
                 <td><?php echo $form->textField($model, 'sub_cat', array('disabled' => true, 'name' => 'InventoryItems[sub_cat]', 'id' => 'Inventory_sub_cat', 'style' => 'width:150px')); ?></td>
            <td>

                <?php
                $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                    'id' => 'web_category',
                    'name' => 'Inventory[web_category]', 
                    'htmlOptions' => array('style' => 'width:10px'),
                    'value' => $model->web_category,
                    // additional javascript options for the autocomplete plugin
                    'options' => array(
                        'minLength' => '1',
                        'showAnim' => 'fold',                    
                        'select' => "js:function(event, ui) {

                        $('#Inventory_sub_cat').val('');
                        $('#Inventory_uniqueId').val('');
                        $('#Inventory_price').val('');
                        $('#Inventory_product_id').val('');
                        $('#Inventory_product_name').val('');
                        $('#Inventory_av_qty').val('');
                        $('#Inventory_discount').val('');
        }"
                    ),

                    'source' => $this->createUrl("inventory/getProducts"),
                    'htmlOptions' => array(
                        'style' => 'height:20px;width:150px',
                    ),
                ));
                ?>

            </td>
            <td><?php echo $form->textField($model, 'uniqueId', array('disabled' => true, 'name' => 'InventoryItems[uniqueId]', 'id' => 'Inventory_uniqueId', 'style' => 'width:40px')); ?></td>
            <td>

                <?php
                $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                    'name' => 'Inventory[product_name]',
                    'value' => $model->product_name,
                    // additional javascript options for the autocomplete plugin
                    'options' => array(
                        'minLength' => '1',
                    ),
                    'source' => "js:function(request, response) {
        $.getJSON('" . $this->createUrl('inventory/getProductsName') . "', {
            term: $('#Inventory_product_name').val(),web_cat:$('#web_category').val()
        }, response);}",
                    'options' => array(
//                    'delay'=>300,
                        'minLength' => '1',
                        'showAnim' => 'fold',
                        'select' => "js:function(event, ui) {

            $('#Inventory_sub_cat').val(ui.item.sub_cat);
            
            $('#Inventory_uniqueId').val(ui.item.uniqueId);

            $('#Inventory_price').val(ui.item.price);

            $('#Inventory_product_id').val(ui.item.id);

            $('#Inventory_av_qty').val(ui.item.av_qty);
           
            $('#web_category').val(ui.item.cat);
            
            $('#Inventory_discount').val(ui.item.discount);

        }"
                    ),
                    'htmlOptions' => array(
                        'style' => 'height:20px;',
                    ),
                ));
                ?>
                <?php echo $form->error($model, 'products_id'); ?>
                <!--</div>-->   

            </td>
            <td>
                <input type="hidden" name="Inventory[products_id]" id="Inventory_product_id" value="<?php echo $model->products_id; ?>">
                <?php echo $form->dropDownList($model, 'add_minus', array('0' => 'Add', '1' => 'Issue'), array('style' => 'width:80px'), array('options' => array($model->add_minus => array('selected' => true)))); ?>
            </td>
            <td><?php echo $form->textField($model, 'quantity', array('name' => 'InventoryItems[quantity]', 'id' => 'Inventory_qty', 'style' => 'width:50px', 'onChange' => 'js:totalPrice(this.value)')); ?></td>
            <td><?php echo $form->textField($model, 'price', array('readonly' => true, 'name' => 'InventoryItems[price]', 'id' => 'Inventory_price', 'style' => 'width:50px;text-align:right')); ?></td>
            <td><?php echo $form->textField($model, 'discount', array('readonly' => true, 'name' => 'InventoryItems[discount]', 'id' => 'Inventory_discount', 'style' => 'width:40px;text-align:right')); ?></td>
            <td><?php echo $form->textField($model, 'total_price', array('disabled' => true, 'value' => 0, 'name' => 'InventoryItems[total_price]', 'id' => 'Inventory_total_price', 'style' => 'width:80px;text-align:right')); ?></td>
            <td><?php echo $form->textField($model, 'av_quantity', array('disabled' => true, 'name' => 'InventoryItems[av_quantity]', 'id' => 'Inventory_av_qty', 'style' => 'width:40px;text-align:right')); ?></td>
            </tbody>
        </table>
    </fieldset>

    <div class="form-actions">
         <?php
        if (isset(Yii::app()->session['inventoryData'])) {

            $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType' => 'submit',
                'type' => 'danger',
                'label' => 'Close',
                'htmlOptions' => array('name' => 'close'),
            ));
            echo "&nbsp;&nbsp;";
        }
        
     //...........................................
        if($itemsCount > 0){
            $add_lbl = 'Add another';
        }else{
           $add_lbl = 'Add';  
        }
        
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type' => 'primary',
            'label' => $add_lbl
        ));
        ?>

    </div>
    <?php
    if ($itemsCount > 0)
        $this->renderPartial('_cart', array('model' => $inventoryItems, 'itemsCount' => $itemsCount));
    ?>

    <?php $this->endWidget(); ?>

</div>

<script type="text/javascript">


    function totalPrice(qty) {
        discount = $('#Inventory_discount').val();
        unit_price = $('#Inventory_price').val();
        total_price = (unit_price * qty) * (100 - discount)/100;
        $('#Inventory_total_price').val(total_price.toFixed(2));
    }

    $(document).ready(function () {
        product_id = $('#Inventory_product_id').val();
        if (product_id) {
            $.ajax({
                url: 'getProductsName',
                type: "GET",
                data: {
                    id: product_id
                },
                dataType: "json",
                contentType: "text/html",
                success: function (ui)
                {
                    if (ui[0]) {
                        $('#Inventory_product_name').val(ui[0].value);
                        $('#Inventory_sub_cat').val(ui[0].sub_cat);
                        $('#Inventory_uniqueId').val(ui[0].uniqueId);
                        $('#Inventory_price').val(ui[0].price);
                        $('#Inventory_product_id').val(ui[0].id);
                        $('#Inventory_av_qty').val(ui[0].av_qty);
                        $('#web_category').val(ui[0].cat);
                        $('#Inventory_discount').val(ui[0].discount);
                    }
                }
            });
        }

    });

</script>