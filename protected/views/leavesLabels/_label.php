<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Randika
 * Date: 5/19/14
 * Time: 11:44 AM
 * To change this template use File | Settings | File Templates.
 */


$label = $id = '';
if(sizeof($leaflevels))
{
    foreach($leaflevels as $leaf)
    {
        if(sizeof($leaf))
        {
            foreach($leaf as $leafs){
                $label .= (strlen($label)) ? ', '.$leafs['label'] : $leafs['label'];
                $id .= (strlen($id)) ? ', '.$leafs['label_id_fk'] : $leafs['label_id_fk'];
            }
        }
    }
} else {
    $label = 'No label assigned for this category';
    $id = '0';
}
?>
    <div class="row">
        Already selected labels: <?php echo $label?>
    </div>
    <div class="row">
        <?php echo CHtml::label('Label ','LeavesLabels[label_id_fk]'); ?>
        <?php
        $leaflevelattrbutes = CHtml::listData(LeafLevelAttributeLabels::model()->findAll("status > 0 AND id NOT IN ({$id})"), 'id','label');
        echo CHtml::dropDownList('LeavesLabels[label_id_fk]','LeavesLabels[label_id_fk]', $leaflevelattrbutes); ?>
        <?php echo CHtml::error($model,'label_id_fk'); ?>
    </div>