<?php
/* @var $this LeavesLabelsController */
/* @var $model LeavesLabels */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'leaf_level_id_fk'); ?>
		<?php echo $form->textField($model,'leaf_level_id_fk'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'label_id_fk'); ?>
		<?php echo $form->textField($model,'label_id_fk'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'type'); ?>
		<?php echo $form->textField($model,'type'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->