<?php
/* @var $this LeavesLabelsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Leaves Labels',
);

$this->menu=array(
	array('label'=>'Create LeavesLabels', 'url'=>array('create')),
	array('label'=>'Manage LeavesLabels', 'url'=>array('admin')),
);
?>

<h1>Leaves Labels</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
