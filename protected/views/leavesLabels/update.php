<?php
/* @var $this LeavesLabelsController */
/* @var $model LeavesLabels */

$this->breadcrumbs=array(
	'Leaves Labels'=>array('admin'),
	Categories::getCategoryText($model->leaf_level_id_fk)=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	//array('label'=>'List LeavesLabels', 'url'=>array('index')),
	array('label'=>'Create Web Category', 'url'=>array('create')),
	array('label'=>'View Web Category', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Web Category', 'url'=>array('admin')),
);
?>

<h1>Update Web Category <?php echo Categories::getCategoryText($model->leaf_level_id_fk); ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>