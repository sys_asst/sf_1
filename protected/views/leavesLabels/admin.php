<?php
/* @var $this LeavesLabelsController */
/* @var $model LeavesLabels */

$this->breadcrumbs=array(
	'Leaves Labels'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Web Category', 'url'=>array('index')),
	array('label'=>'Create Web Category', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#leaves-labels-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Assign Labels to Web Category</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'leaves-labels-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
        array(
            'name'=>'leaf_level_id_fk',
            'value'=>'Categories::getCategoryText($data->leaf_level_id_fk)',
            'filter' => Categories::getCategories(),
        ),
        array(
            'name'=>'label_id_fk',
            'value'=>'$data->getLabelText()',
            'filter' => LeafLevelAttributeLabels::getLabels(),
        ),
        array(
            'name'=>'unit_id',
            'value'=>'$data->getUnitText()',
            'filter' => LeavesLabels::getUnits(),
        ),
        array(
            'name'=>'type',
            'value'=>'LeavesLabels::itemAlias("Type",$data->type)',
            'filter' => LeavesLabels::itemAlias("Type"),
        ),
		array(
			'class'=>'CButtonColumn',
            'template' => '{view}'
		),
	),
)); ?>
