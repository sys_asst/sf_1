<?php
/* @var $this LeavesLabelsController */
/* @var $model LeavesLabels */

$this->breadcrumbs=array(
	'Leaves Labels'=>array('index'),
    CHtml::encode(Categories::getCategoryText($model->leaf_level_id_fk)),
);

$this->menu=array(
	//array('label'=>'List LeavesLabels', 'url'=>array('index')),
	array('label'=>'Create Leaves Labels', 'url'=>array('create')),
	array('label'=>'Update Leaves Labels', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Leaves Labels', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Leaves Labels', 'url'=>array('admin')),
);
?>

<h1>View Leaves Labels <?php echo CHtml::encode(Categories::getCategoryText($model->leaf_level_id_fk)); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
        array(
            'name'=>'leaf_level_id_fk',
            'value'=>CHtml::encode(Categories::getCategoryText($model->leaf_level_id_fk))
        ),
        array(
            'name'=>'label_id_fk',
            'value'=>CHtml::encode($model->getLabelText())
        ),
        array(
            'name'=>'unit_id',
            'value'=>CHtml::encode($model->getUnitText())
        ),
        array(
            'name'=>'type',
            'value'=>CHtml::encode($model->getTypeText())
        )
	),
)); ?>
