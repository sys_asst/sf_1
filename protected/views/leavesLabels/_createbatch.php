<?php
/* @var $this LeavesLabelsController */
/* @var $model LeavesLabels */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'leaves-labels-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($models); ?>

    <table>
        <tr><th>Name</th><th>Price</th><th>Count</th><th>Description</th></tr>
        <?php foreach($models as $i=>$item): ?>
            <tr>
                <td><?php echo CHtml::activeTextField($item,"[$i]name"); ?></td>
                <td><?php echo CHtml::activeTextField($item,"[$i]price"); ?></td>
                <td><?php //echo CHtml::activeTextField($item,"[$i]count"); ?></td>
                <td><?php echo CHtml::activeTextArea($item,"[$i]description"); ?></td>
            </tr>
        <?php endforeach; ?>
    </table>

	<div class="row">
		<?php echo $form->labelEx($model,'leaf_level_id_fk'); ?>
		<?php
        $webcategory = CHtml::listData(Categories::model()->findAll('disabled=0 AND category_level=3'), 'id','category');
        echo $form->DropDownList($model,'leaf_level_id_fk',$webcategory,array(
            'ajax'=>array(
                'type'=>'POST',
                'url'=>CController::createUrl('getLabels'),
                'update'=>'#label_details',
                'data'=>array('catid'=>'js:this.value', 'levelid'=>$model->id)
            ))); ?>
		<?php echo $form->error($model,'leaf_level_id_fk'); ?>
	</div>

    <div id="label_details">
        <div class="row">
            <?php echo $form->labelEx($model,'label_id_fk'); ?>
            <?php
            $leaflevelattrbutes = CHtml::listData(LeafLevelAttributeLabels::model()->findAll('status > 0 '), 'id','label');
            echo $form->DropDownList($model,'label_id_fk',$leaflevelattrbutes); ?>
            <?php echo $form->error($model,'label_id_fk'); ?>
        </div>
    </div>

	<div class="row">
		<?php echo $form->labelEx($model,'type'); ?>
		<?php echo $form->dropDownList($model,'type', LeavesLabels::itemAlias('ItemStatus')); ?>
		<?php echo $form->error($model,'type'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->