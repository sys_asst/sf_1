<?php
/* @var $this LeavesLabelsController */
/* @var $data LeavesLabels */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('leaf_level_id_fk')); ?>:</b>
	<?php echo CHtml::encode(Categories::getCategoryText($data->leaf_level_id_fk)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('label_id_fk')); ?>:</b>
	<?php echo CHtml::encode($data->getLabelText()); ?>
	<br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('unit_id')); ?>:</b>
    <?php echo CHtml::encode($data->getUnitText()); ?>
    <br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('type')); ?>:</b>
	<?php echo CHtml::encode($data->getTypeText()); ?>
	<br />


</div>