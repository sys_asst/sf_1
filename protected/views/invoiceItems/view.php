<?php
/* @var $this InvoiceItemsController */
/* @var $model InvoiceItems */

$this->breadcrumbs=array(
	'Invoice Items'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List InvoiceItems', 'url'=>array('index')),
	array('label'=>'Create InvoiceItems', 'url'=>array('create')),
	array('label'=>'Update InvoiceItems', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete InvoiceItems', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage InvoiceItems', 'url'=>array('admin')),
);
?>

<h1>View InvoiceItems #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'invoice_id',
		'orders_id',
		'status_id',
		'quantity',
		'createdby',
		'createdtime',
		'updatedby',
		'updatedtime',
	),
)); ?>
