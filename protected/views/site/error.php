<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle=Yii::app()->name . ' - Error';
$this->breadcrumbs=array(
	'Error',
);
$err = "";
if($code != 403){
  $err = 'Error '.$code;
}
?>

<h2><?php echo $err; ?></h2>

<div class="error">
<?php echo CHtml::encode($message); ?>
</div>