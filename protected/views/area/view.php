<?php
/* @var $this AreaController */
/* @var $model Area */

$this->breadcrumbs=array(
	'Areas'=>array('index'),
	$model->name,
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>View Area <?php echo $model->name; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
        array(
            'name'=>'reagon_id',
            'value'=>CHtml::encode($model->reagon->name)
        ),
		'code',
		'name',
        array(
            'name'=>'distributor_id',
            'value'=>CHtml::encode($model->distributor->distName)
        ),
        array(
            'label'=>'Districts',
            'value'=>CHtml::encode($model->getDistricts($model->id))
        ),
        array(
            'name'=>'status',
            'value'=>CHtml::encode(Area::itemAlias('ItemStatus', $model->status))
        ),
        array(
            'name'=>'added_by',
            'value'=>CHtml::encode($model->addedBy->username)
        ),
        'addedon',
        /*array(
            'name'=>'updated_by',
            'value'=>CHtml::encode($model->updatedBy->username)
        ),*/
		'updatedon',
	),
)); ?>
