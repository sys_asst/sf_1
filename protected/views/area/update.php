<?php
/* @var $this AreaController */
/* @var $model Area */

$this->breadcrumbs=array(
	'Areas'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>Update Area <?php echo $model->name; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model,'details'=>$details)); ?>