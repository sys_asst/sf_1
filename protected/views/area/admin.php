<?php
/* @var $this AreaController */
/* @var $model Area */

$this->breadcrumbs=array(
	'Areas'=>array('index'),
	'Manage',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#area-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Areas</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
	'id'=>'area-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
        array(
            'name'=>'reagon_id',
            'value'=>'$data->reagon->name',
            'filter' => CHtml::listData(Regions::model()->findAll(array('condition'=>'status=1','order'=>'name')),'id','name'),
        ),
		'code',
		'name',
		'addedon',
        array(
            'name'=>'added_by',
            'value'=>'$data->addedBy->username',
            'filter' => CHtml::listData(User::model()->findAll(array('condition'=>'status=1','order'=>'username')),'id','username'),
        ),
        array(
            'name'=>'status',
            'value'=>'Area::itemAlias("ItemStatus",$data->status)',
            'filter' => Area::itemAlias("ItemStatus"),
        ),
		/*
		'status',
		'updated_by',
		'updatedon',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'template'=>'{view}{update}'
		),
	),
)); ?>
