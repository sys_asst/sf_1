<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Randika
 * Date: 7/1/14
 * Time: 1:35 PM
 * To change this template use File | Settings | File Templates.
 */

$row_id = "sladetail-" . $key ?>

<div class='row-fluid' id="<?php echo $row_id ?>">
    <?php
    echo $form->hiddenField($model, "[$key]area_id");
    echo $form->updateTypeField($model, $key, "area_id", array('key' => $key));
    ?>
    <table border="0" class="span6">
        <tr>
            <td>
                <div class="span6">
                    <?php //echo $form->dropDownListRow($model, "[$key]district_id",$leaflevelattrbutes); ?>
                    <?php
                    //echo $form->labelEx($model, "[$key]district_id");
                    $leaflevelattrbutes = CHtml::listData(District::model()->findAll('status > 0 ORDER BY description'), 'id','description');
                    echo $form->dropDownListRow($model, "[$key]district_id",$leaflevelattrbutes,array('prompt'=>'Select District'));
                    //echo $form->error($model, "[$key]district_id");
                    ?>
                </div>
            </td>
            <td>
                <div class="span2">
                    <?php echo $form->deleteRowButton($row_id, $key); ?>
                </div>
            </td>
        </tr>
    </table>
</div>