<?php
/* @var $this AreaController */
/* @var $model Area */
/* @var $form CActiveForm */
?>

<div class="form">

<?php //$form=$this->beginWidget('CActiveForm', array(
	//'id'=>'area-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	//'enableAjaxValidation'=>false,
//)); ?>
    <?php $form=$this->beginWidget('DynamicTabularForm', array(
        'defaultRowView'=>'_rowForm',
    )); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

    <div class="row-fluid">
        <div class="span6">
            <?php echo $form->textFieldRow($model,'code',array('class'=>'input-block-level','maxlength'=>10,'size'=>4, 'readonly'=>$model->isNewRecord ? false : true)); ?>
            <?php echo $form->textFieldRow($model,'name',array('class'=>'input-block-level','maxlength'=>255,'size'=>60)); ?>
            <?php echo $form->dropDownListRow($model,'status',Area::itemAlias('ItemStatus')); ?>
        </div>

        <div class="span6">
            <?php echo $form->dropDownListRow($model,'reagon_id',CHtml::listData(Regions::model()->findAll(array('condition'=>'status=1','order'=>'name')),'id','name'),array('prompt'=>'Select Region')); ?>
            <?php echo $form->dropDownListRow($model,'distributor_id',CHtml::listData(Distributors::model()->findAll(array('condition'=>'status=1','order'=>'distName')),'id','distName'),array('prompt'=>'Select Distributor')); ?>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span6">
        <fieldset class="label_data" style="display: <?php echo ($model->scenario == 'update')? 'block' : 'block';?>">
            <legend>Districts Details</legend>
            <?php
            //echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save');
            $max_leafs = 25;
            echo $form->rowForm($details, null, array(), $max_leafs);
            ?>
        </fieldset>
        </div>
    </div>
    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>$model->isNewRecord ? Yii::t('messages', 'Create') : Yii::t('messages', 'Save'),
        )); ?>

        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'label'=>Yii::t('messages', 'Cancel'),
            'type'=>'info',
            'url'=>Yii::app()->createUrl('area/admin')
        )); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->