<div class="form" id="mydialog">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'inventory-items-form',
    'action'=>'create1',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'inventory_id'); ?>
		<?php echo $form->textField($model,'inventory_id'); ?>
		<?php echo $form->error($model,'inventory_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'products_id'); ?>
		<?php echo $form->textField($model,'products_id'); ?>
		<?php echo $form->error($model,'products_id'); ?>
	</div>
        
        <div class="row buttons">
            
            <?php // echo 
//    CHtml::ajaxSubmitButton(
//        // ... old code
//        array(
//            'dataType'=>'json',
//            'success'=>'function(data) {
//                if(data != null && data.status == "success") {
//                    $("#User_institutionid").append(data.data);
//                    $("#institutionDialog").dialog("close");
//                }
//
//            }'),
//        // ... old code
//    ); 
?>
        <?php echo CHtml::ajaxSubmitButton(Yii::t('job','Create Job'),CHtml::normalizeUrl(array('inventoryItems/create1','render'=>false)),
                
                array('success'=>'js: function(data) {
       obj = JSON.parse(data);
                    console.log(obj.status);
                    console.log("teeth");
                 if(obj != null && obj.status == "success") {

                      $("#mydialog").dialog("close");
//                       window.location = "create";
                }

                      
                    }'),array('id'=>'closeJobDialog')); ?>
    </div>


<!--	<div class="row buttons">
		<?php // echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>-->

<?php $this->endWidget(); ?>

</div><!-- form -->

