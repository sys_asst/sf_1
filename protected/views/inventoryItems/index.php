<?php
/* @var $this InventoryItemsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Inventory Items',
);

$this->menu=array(
	array('label'=>'Create InventoryItems', 'url'=>array('create')),
	array('label'=>'Manage InventoryItems', 'url'=>array('admin')),
);
?>

<h1>Inventory Items</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
