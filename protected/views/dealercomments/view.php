<?php
/* @var $this DealercommentsController */
/* @var $model Dealercomments */

$this->breadcrumbs=array(
	'Dealercomments'=>array('index'),
	$model->id,
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>View Dealercomments #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
        array(
            'name'=>'reasonId',
            'value'=>CHtml::encode($model->reason->reason)
        ),
        array(
            'name'=>'dealerId',
            'value'=>CHtml::encode($model->dealer->dealerName)
        ),
		'comment',
		'commentDate',
		'srCode',
		'latitude',
		'longitude',
	),
)); ?>
