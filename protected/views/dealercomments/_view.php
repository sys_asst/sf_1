<?php
/* @var $this DealercommentsController */
/* @var $data Dealercomments */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reasonId')); ?>:</b>
	<?php echo CHtml::encode($data->reasonId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dealerId')); ?>:</b>
	<?php echo CHtml::encode($data->dealerId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('comment')); ?>:</b>
	<?php echo CHtml::encode($data->comment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('commentDate')); ?>:</b>
	<?php echo CHtml::encode($data->commentDate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('srCode')); ?>:</b>
	<?php echo CHtml::encode($data->srCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('latitude')); ?>:</b>
	<?php echo CHtml::encode($data->latitude); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('longitude')); ?>:</b>
	<?php echo CHtml::encode($data->longitude); ?>
	<br />

	*/ ?>

</div>