<?php
/* @var $this DealercommentsController */
/* @var $model Dealercomments */

$this->breadcrumbs=array(
	'Dealercomments'=>array('index'),
	'Manage',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#dealercomments-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Dealercomments</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
	'id'=>'dealercomments-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
        array(
            'name'=>'reasonId',
            'value'=>'$data->getComentReasontext()',//' null($data->reason->reason) ? 0 : $data->reason->reason ',
            'filter' => Dealercommentreasons::getCommentReasons(),
        ),
        array(
            'name'=>'dealerId',
            'value'=>'$data->dealer->dealerName',
            'filter' => Dealers::getDealers(),//CHtml::listData(Dealers::model()->findAll(array('order'=>'dealerName')), 'id','dealerName'),
        ),
		'comment',
		'commentDate',
        array(
            'name'=>'srCode',
            'value'=>'$data->getSalesRefName()',
            'filter' => Salesreps::getSalesRefs(),
        ),
		/*
		'latitude',
		'longitude',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'template' => '{view}{update}',
		),
	),
)); ?>
