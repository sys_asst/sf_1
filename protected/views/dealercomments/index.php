<?php
/* @var $this DealercommentsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Dealercomments',
);

$this->menu=array(
	array('label'=>'Create Dealercomments', 'url'=>array('create')),
	array('label'=>'Manage Dealercomments', 'url'=>array('admin')),
);
?>

<h1>Dealercomments</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
