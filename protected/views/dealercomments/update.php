<?php
/* @var $this DealercommentsController */
/* @var $model Dealercomments */

$this->breadcrumbs=array(
	'Dealercomments'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>Update Dealer Comments <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>