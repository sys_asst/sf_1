<?php
/* @var $this DealercommentsController */
/* @var $model Dealercomments */

$this->breadcrumbs=array(
	'Dealercomments'=>array('index'),
	'Create',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>Create Dealer comments</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>