<?php
/* @var $this SalesrepsController */
/* @var $model Salesreps */

$this->breadcrumbs=array(
	'Salesreps'=>array('index'),
	'Create',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>Create Sales Team Member</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>