<?php
/* @var $this SalesrepsController */
/* @var $model Salesreps */

$this->breadcrumbs=array(
	'Salesreps'=>array('index'),
	'Manage',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#salesreps-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Sales Team</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
	'id'=>'salesreps-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
		'srEmpNo',
		'srName',
		//'srDesignation',
        array(
            'name'=>'designation_id',
            'value'=>'isset($data->designation_id) ? $data->designation->name : "n/a"',
            'filter' => CHtml::listData(Designations::model()->findAll(), 'id','name'),
        ),
		'srAddress',
        array(
            'name'=>'status',
            'value'=>'Salesreps::itemAlias("ItemStatus",$data->status)',
            'filter' => Salesreps::itemAlias("ItemStatus"),
        ),
		/*
		'srTel',
		'srEmail',
		'srResignDate',
		'passCode',
		'distributor',
		'status',
		'loginCreated',
		'addedOn',
		'lastUpdatedOn',
		'user_id',
		'disabled',
		'disabled_date',
		'disabled_by',
		'login_created_by',
		'login_created_on',
		*/
		array(
            'header' => 'New User',
			'class'=>'CButtonColumn',
            'template' => '{newuser}',
            'buttons' => array(
                'newuser' => array( //the name {reply} must be same
                    'label' => Yii::app()->fa->getIcon('user-plus','','',2), // text label of the button user/admin/create&refid=68&src=sref
                    'url' => '$data->loginCreated ? CHtml::normalizeUrl(array("user/admin/update/src/sref/id/".$data->user_id."/refid/".$data->id)) : CHtml::normalizeUrl(array("user/admin/create/src/sref/refid/".$data->id))', //Your URL According to your wish
                    //'imageUrl' => Yii::app()->baseUrl . '/images/user_add.png', // image URL of the button. If not set or false, a text link is used, The image must be 16X16 pixels
                ),
            ),
		),
        array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'template' => '{view}{update}'
        )
	),
)); ?>
