<?php
/* @var $this SalesrepsController */
/* @var $model Salesreps */

$this->breadcrumbs=array(
	'Salesreps'=>array('index'),
	$model->srName=>array('view','id'=>$model->id),
	'Update',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>Update Sales Team <?php echo $model->srName; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>