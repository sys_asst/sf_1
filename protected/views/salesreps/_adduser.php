<?php
/* @var $this SalesrepsController */
/* @var $model Salesreps */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'salesreps-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

<?php
$name = explode(' ', $model->srName);
$username = strtolower($name[0]);
$model->srEmpNo;
$password = Yii::app()->controller->module->encrypting($model->srEmpNo);
$email = strlen(trim($model->srEmail)) ? trim($model->srEmail) : $model->srEmpNo.'_'.Yii::app()->params['defaultUserEmail'];
$status = $superuser = $uid = 0;
$connection=Yii::app()->db;

unset($username[0]);

$post = array(
    'firstname'=>$username,
    'lastname'=>implode(' ',$username)
);

$profile=new Profile;

$profile->attributes=$post;
$profile->user_id=0;

print_r($profile);
$sql="INSERT INTO `users` (`username`, `password`,`email`,`status`,`superuser`) VALUES(:username,:password,:email,:status,:superuser)";
$command=$connection->createCommand($sql);



$command->bindParam(":username",$username,PDO::PARAM_STR);
$command->bindParam(":password",$password,PDO::PARAM_STR);
$command->bindParam(":email",$email,PDO::PARAM_STR);
$command->bindParam(":status",$status,PDO::PARAM_STR);
$command->bindParam(":superuser",$superuser,PDO::PARAM_STR);
$command->execute();

$uid = Yii::app()->db->lastInsertID;//user/admin/update&id=2
//$this->redirect(array('user/admin/update', 'id' => $uid));
if($uid){
    $profile->user_id=$uid;
    $profile->save();
}

echo '<h2>User Added for '.$model->srName.'</h2>';
//echo '</pre>';
?>

<?php $this->endWidget(); ?>

</div><!-- form -->