<?php
/* @var $this SalesrepsController */
/* @var $model Salesreps */

$this->breadcrumbs=array(
	'Sales Team'=>array('index'),
	$model->srName,
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));

//$model = $distributors = '';
//$records = array();
//
//
//if($model->designation_id == 1)
//{
//    echo $sql = 'SELECT A.`name` FROM `d_regions` AS DR INNER JOIN `ms_area` AS A ON A.`id` = DR.`area_id` WHERE DR.`region_id` = "'.$model->region_id.'" ORDER BY A.`name`';
//    $records = Tk::sql($sql);
//
//    if(sizeof($records))
//    {
//        foreach($records as $record)
//        {
//            $areas .= (strlen($areas)) ? ', '.$record['name'] : $record['name'] ;
////            $areas[] =  $record['name'];
//        }
//    }
//
//    $sql = 'SELECT D.`distName` AS `name` FROM `d_regions` AS DR INNER JOIN `distributors` AS D ON D.`area_id` = DR.`area_id` WHERE DR.`region_id` = "'.$model->region_id.'" ORDER BY D.`distName`';
//    $records = Tk::sql($sql);
//
//    if(sizeof($records))
//    {
//        foreach($records as $record)
//        {
//            $distributors .= (strlen($distributors)) ? ', '.$record['name'] : $record['name'] ;
//        }
//    }
//}


?>

<h1>View Sales Member <?php echo $model->srName; ?></h1>

<?php
//echo $areas;
$this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'srEmpNo',
		'srName',
		'srDesignation',
		'srJoinedDate',
		'srAddress',
		'srTel',
        array(
            'name'=>'srEmail',
            'value'=>strlen($model->srEmail) ? $model->srEmail : 'n/a'
        ),
        array(
            'name'=>'designation_id',
            'value'=>CHtml::encode(isset($model->designation->name) ? $model->designation->name : 'n/a')
        ),
		'srResignDate',
		'passCode',
        array(
            'name'=>'region_id',
            'value'=>CHtml::encode(isset($model->region->name) ? $model->region->name : 'n/a')
        ),
        array(
            'name'=>'distributor',
            'type'=>'raw',
//            'value'=>CHtml::encode(Distributors::getDistributor($model->distributor,1)).(strlen($distributors) ? ', '.$distributors : '')
            'value'=>strlen($distributors) ? $distributors : ''
        ),

        array(
            'name'=>'area_id',
            'type'=>'raw',
            'value'=>isset($areas) ? $areas : ''
//            'value'=>CHtml::encode(isset($model->area->name) ? $model->area->name : ' ').$areas

        ),
        array(
            'name'=>'status',
            'value'=>CHtml::encode(Salesreps::itemAlias('ItemStatus', $model->status))
        ),
        array(
            'name'=>'loginCreated',
            'value'=>$model->loginCreated ? 'Yes' : 'No'
        ),
		'addedOn',
		'lastUpdatedOn',
        array(
            'name'=>'user_id',
            'value'=>isset($model->user_id) ? $model->user->username : ''
//            'value'=>CHtml::encode(isset($model->area->name) ? $model->area->name : ' ').$areas

        ),
		/*'disabled',
		'disabled_date',
		'disabled_by',
		'login_created_by',
		'login_created_on',*/
	),
)); ?>
