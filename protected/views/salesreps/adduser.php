<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Randika
 * Date: 5/7/14
 * Time: 1:37 PM
 * To change this template use File | Settings | File Templates.
 */

/* @var $this SalesrepsController */
/* @var $model Salesreps */

$this->breadcrumbs=array(
    'Salesreps'=>array('index'),
    $model->id=>array('view','id'=>$model->id),
    'Update',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

    <h1>Add User for Salesreps <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_adduser', array('model'=>$model)); ?>