<?php
/* @var $this SalesrepsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Salesreps',
);

//$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>Salesreps</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
