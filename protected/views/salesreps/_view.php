<?php
/* @var $this SalesrepsController */
/* @var $data Salesreps */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('srEmpNo')); ?>:</b>
	<?php echo CHtml::encode($data->srEmpNo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('srName')); ?>:</b>
	<?php echo CHtml::encode($data->srName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('srDesignation')); ?>:</b>
	<?php echo CHtml::encode($data->srDesignation); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('srJoinedDate')); ?>:</b>
	<?php echo CHtml::encode($data->srJoinedDate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('srAddress')); ?>:</b>
	<?php echo CHtml::encode($data->srAddress); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('srTel')); ?>:</b>
	<?php echo CHtml::encode($data->srTel); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('srEmail')); ?>:</b>
	<?php echo CHtml::encode($data->srEmail); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('srResignDate')); ?>:</b>
	<?php echo CHtml::encode($data->srResignDate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('passCode')); ?>:</b>
	<?php echo CHtml::encode($data->passCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('distributor')); ?>:</b>
	<?php echo CHtml::encode($data->distributor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('loginCreated')); ?>:</b>
	<?php echo CHtml::encode($data->loginCreated); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('addedOn')); ?>:</b>
	<?php echo CHtml::encode($data->addedOn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lastUpdatedOn')); ?>:</b>
	<?php echo CHtml::encode($data->lastUpdatedOn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('disabled')); ?>:</b>
	<?php echo CHtml::encode($data->disabled); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('disabled_date')); ?>:</b>
	<?php echo CHtml::encode($data->disabled_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('disabled_by')); ?>:</b>
	<?php echo CHtml::encode($data->disabled_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('login_created_by')); ?>:</b>
	<?php echo CHtml::encode($data->login_created_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('login_created_on')); ?>:</b>
	<?php echo CHtml::encode($data->login_created_on); ?>
	<br />

	*/ ?>

</div>