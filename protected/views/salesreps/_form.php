<?php
/* @var $this SalesrepsController */
/* @var $model Salesreps */
/* @var $form CActiveForm */
?>

<div class="form">
    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'salesreps-form',
        'enableAjaxValidation' => false,
        'type' => 'horizontal',
    ));
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php

    $cs = Yii::app()->clientScript->registerScript(
        'salesreps-form',
        '$(document).ready(function() {

        var designation_id = $("#Salesreps_designation_id").val();

 if(designation_id != 3){

$(".control-group ").eq(8).hide();
$(".control-group ").eq(9).hide();
} else{

$(".control-group ").eq(8).show();
$(".control-group ").eq(9).show();
}

if(designation_id == 2){
$(".control-group ").eq(7).hide();
} else {$(".control-group ").eq(7).show();
}
    });'
    );


    echo $form->errorSummary($model); ?>
    <p>
        <?php //echo Yii::app()->createUrl('/salesreps/adduser', array(), 'https');//'salesreps/adduser';
        if($model->isNewRecord == true)
        echo CHtml::link('Add User for ' . $model->srName, Yii::app()->createUrl('/user/admin/create', array('refid' => $model->id, 'src' => 'sref'), '&'));
        ?>
    </p>

    <div class="row-fluid">
        <div class="span6">
            <?php echo $form->textFieldRow($model, 'srEmpNo', array('class' => 'input-block-level', 'maxlength' => 5, 'size' => 4, 'readonly' => $model->isNewRecord ? false : true)); ?>
            <?php echo $form->dropDownListRow($model, 'designation_id', CHtml::listData(Designations::model()->findAll(array('condition' => 'status=1', 'order' => 'name')), 'id', 'name'), array('multiple' => false, 'class' => 'input-block-level', 'prompt' => 'Select Designation', 'onchange' => 'js:if(this.value != 3){$(".control-group ").eq(8).hide();$(".control-group ").eq(9).hide();} else{ $(".control-group ").eq(8).show();$(".control-group ").eq(9).show();} if(this.value == 2){$(".control-group ").eq(7).hide();} else {$(".control-group ").eq(7).show();}'))
            ?>
            <?php echo $form->textFieldRow($model, 'srName', array('class' => 'input-block-level', 'size' => 60, 'maxlength' => 255)); ?>
            <?php echo $form->textFieldRow($model, 'srTel', array('class' => 'input-block-level', 'size' => 60, 'maxlength' => 20)); ?>
            <?php echo $form->textFieldRow($model, 'srEmail', array('class' => 'input-block-level', 'size' => 60, 'maxlength' => 100)); ?>
            <div class="control-group ">
                <label class="control-label " for="fromdate">
                    <?php echo Yii::t('messages', 'Joined Date');//$attributeLabels['publishDate']?>

                </label>

                <div class="controls">
                    <?php
                    $calLang = '';//Yii::app()->toolKit->getComponenetSpecificLangIdentifier('juiDateTimePicker');
                    echo $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model' => $model,
                            'attribute' => 'srJoinedDate',
                            //'language' => 'ja',
                            // 'i18nScriptFile' => 'jquery.ui.datepicker-ja.js', (#2)
                            'htmlOptions' => array(
                                'id' => 'srJoinedDate_search',
                                'size' => '10',
                            ),
                            'options' => array(
                                'dateFormat' => 'yy-mm-dd',
                                'showButtonPanel' => true,
                                'showOn' => 'focus'
                            )
                        ),
                        true);
                    ?>
                    <?php echo $form->error($model, 'srJoinedDate'); ?>
                </div>
            </div>
        </div>
        <div class="span6">
            <?php echo $form->textAreaRow($model, 'srAddress', array('class' => 'input-block-level', 'row' => 5)); ?>
            <?php echo $form->dropDownListRow($model, 'region_id', CHtml::listData(Regions::model()->findAll(array('condition' => 'status=1', 'order' => 'name')), 'id', 'name'), array('multiple' => false, 'class' => 'input-block-level', 'prompt' => 'Select Reigon')); ?>
            <?php echo $form->dropDownListRow($model, 'area_id', CHtml::listData(Area::model()->findAll(array('condition' => 'status=1', 'order' => 'name')), 'id', 'name'), array('multiple' => false, 'class' => 'input-block-level', 'prompt' => 'Select Area')); ?>
            <?php echo $form->dropDownListRow($model, 'distributor', CHtml::listData(Distributors::model()->findAll('status = 1'), 'distId', 'distName'), array('multiple' => false, 'class' => 'input-block-level')); ?>
            <?php echo $form->dropDownListRow($model, 'status', Salesreps::itemAlias('ItemStatus'), array('multiple' => false, 'class' => 'input-block-level')); ?>
            <div class="control-group ">
                <label class="control-label " for="srResignDate">
                    <?php echo Yii::t('messages', 'Resign Date');//$attributeLabels['publishDate']?>

                </label>

                <div class="controls">
                    <?php
                    $calLang = '';//Yii::app()->toolKit->getComponenetSpecificLangIdentifier('juiDateTimePicker');
                    echo $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model' => $model,
                            'attribute' => 'srResignDate',
                            //'language' => 'ja',
                            // 'i18nScriptFile' => 'jquery.ui.datepicker-ja.js', (#2)
                            'htmlOptions' => array(
                                'id' => 'srResignDate_search',
                                'size' => '10',
                            ),
                            'options' => array(
                                'dateFormat' => 'yy-mm-dd',
                                'showButtonPanel' => true,
                                'showOn' => 'focus'
                            )
                        ),
                        true);
                    ?>
                    <?php echo $form->error($model, 'srResignDate'); ?>
                </div>
            </div>
        </div>
    </div>


    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type' => 'primary',
            'label' => $model->isNewRecord ? Yii::t('messages', 'Create') : Yii::t('messages', 'Save'),
        )); ?>

        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'label' => Yii::t('messages', 'Cancel'),
            'type' => 'info',
            'url' => Yii::app()->createUrl('salesreps/admin')
        )); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->