<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Randika
 * Date: 5/21/14
 * Time: 3:40 PM
 * To change this template use File | Settings | File Templates.
 */

 $row_id = "sladetail-" . $key ?>

<div class='row-fluid' id="<?php echo $row_id ?>">
    <?php
    echo $form->hiddenField($model, "[$key]leaf_level_id_fk");
    echo $form->updateTypeField($model, $key, "leaf_level_id_fk", array('key' => $key));
    ?>
    <table border="0">
        <tr>
            <td>
            <div class="span3">
                <?php
                echo $form->labelEx($model, "[$key]label_id_fk");
                $leaflevelattrbutes = CHtml::listData(LeafLevelAttributeLabels::model()->findAll('status > 0 ORDER BY label'), 'id','label');
                echo $form->dropDownList($model, "[$key]label_id_fk",$leaflevelattrbutes,array('prompt'=>'Select Label'));
                echo $form->error($model, "[$key]label_id_fk");
                ?>
            </div>
            </td>
            <td>
                <div class="span3">
                    <?php echo $form->labelEx($model,"[$key]unit_id"); ?>
                    <?php echo $form->dropDownList($model,"[$key]unit_id",LeavesLabels::getUnits(),array('prompt'=>'Select Unit')); ?>
                    <?php echo $form->error($model,"[$key]unit_id"); ?>
                </div>
            </td>
            <!--<td>
            <div class="span3">
                <?php
                //echo $form->labelEx($model, "[$key]type");
                //echo $form->dropDownList($model, "[$key]type",LeavesLabels::itemAlias('Type'));
                //echo $form->error($model, "[$key]type");
                ?>
            </div>
            </td>-->
            <td>

            <div class="span2">

                <?php echo $form->deleteRowButton($row_id, $key); ?>
            </div>
            </td>
        </tr>
    </table>
</div>