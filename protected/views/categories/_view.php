<?php
/* @var $this CategoriesController */
/* @var $data Categories */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('category')); ?>:</b>
	<?php echo CHtml::encode($data->category); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('parent_cat_id')); ?>:</b>
	<?php echo CHtml::encode($data->parent_cat_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('category_level')); ?>:</b>
	<?php echo CHtml::encode($data->category_level); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('addedOn')); ?>:</b>
	<?php echo CHtml::encode($data->addedOn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lastUpdatedOn')); ?>:</b>
	<?php echo CHtml::encode($data->lastUpdatedOn); ?>
	<br />


</div>