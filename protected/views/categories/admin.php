<?php
/* @var $this CategoriesController */
/* @var $model Categories */

$this->breadcrumbs=array(
	'Categories'=>array('index'),
	'Manage',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#categories-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Categories</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
	'id'=>'categories-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
		'category',
        array(
            'name'=>'parent_cat_id',
            'value'=>'$data->getParentCategoryText()',
            'filter' => Categories::getCategories(),
        ),
        array(
            'name'=>'category_level',
            'value'=>'Categories::itemAlias("Level",$data->category_level)',
            'filter' => Categories::itemAlias("Level"),
        ),
        array(
            'name'=>'status',
            'value'=>'Categories::itemAlias("ItemStatus",$data->status)',
            'filter' => Categories::itemAlias("ItemStatus"),
        ),
		'addedOn',
		/*
		'lastUpdatedOn',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
                        'template' => '{view} {update}',
		),
	),
)); ?>
