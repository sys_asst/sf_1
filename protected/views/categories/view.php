<?php
/* @var $this CategoriesController */
/* @var $model Categories */

$this->breadcrumbs=array(
	'Categories'=>array('index'),
	$model->category,
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>View Categories <?php echo $model->category; ?></h1>

<?php
$leafValues = Categories::getLeafValues($model->id);
$leafText = '';

if(sizeof($leafValues))
{
    foreach($leafValues as $values)
    {
        if(sizeof($values))
        {
            foreach($values as $val)
            {
                $leafText .= (strlen($leafText)) ? ', '.$val['label'].' '.$val['unit'] : $val['label'].' '.$val['unit'];
            }
        }
    }
}

$this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'category',
		array(
            'name' => 'parent_cat_id',
            'label'=>Categories::itemAlias('Level', ($model->category_level > 1) ? $model->category_level -1 : $model->category_level),
            'value'=>CHtml::encode($model->getParentCategoryText())
        ),
		array(
            'name'=>'category_level',

            'value'=>CHtml::encode($model->getLevelText())
        ),
        array(
            'label'=>'Leaf Levels',
            //'name'=>'category_level',
            'value'=>$leafText
        ),
        array(
            'name'=>'status',
            'value'=>CHtml::encode($model->getStatusText())
        ),
		'addedOn',
		'lastUpdatedOn',
	),
)); ?>
