<?php
/* @var $this CategoriesController */
/* @var $model Categories */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
'id'=>'categories-form',
// Please note: When you enable ajax validation, make sure the corresponding
// controller action is handling ajax validation correctly.
// There is a call to performAjaxValidation() commented in generated controller code.
// See class documentation of CActiveForm for details on this.
'enableAjaxValidation'=>false,
)); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'category_level'); ?>
        <?php echo $form->dropDownList($model,'category_level',Categories::itemAlias('Level'),array(
            'ajax'=>array(
                'type'=>'POST',
                'url'=>CController::createUrl('getCategoriesByLevel'),
                'update'=>'#Categories_parent_cat_id',
                'data'=>array('levelid'=>'js:this.value') //if(this.value==2){//$(".parentcat > label").text("Segment")}
            ),'onchange'=>'js:if(this.value == 1){$(".parentcat").hide();} else{ $(".parentcat").show();}if(this.value == 3){$(".level2").show();$(".parentcat > label").text("Category")} else{ $(".level2").hide();};if(this.value==2){$(".parentcat > label").text("Segment")}'));?>
        <?php echo $form->error($model,'category_level'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'category'); ?>
        <?php echo $form->textField($model,'category',array('size'=>60,'maxlength'=>80)); ?>
        <?php echo $form->error($model,'category'); ?>
    </div>

    <div class="row level2" style="display: <?php echo ($model->scenario == 'update')? 'block' : 'none' ?>">
        <?php echo CHtml::label('Segment','Level[label_id_fk]'); ?>
        <?php
        $leaflevelattrbutes = CHtml::listData(Categories::model()->findAll("status > 0 AND disabled = 0 AND category_level = 1"), 'id','category');
        $prompt = ($model->scenario == 'create') ? "'empty'=>'Select Segment'" : '';
        echo CHtml::dropDownList('Level[level_2]','Level[level_2]', $leaflevelattrbutes,array(
            $prompt,
            'empty'=>'Select Segment',
            'ajax'=>array(
                'type'=>'POST',
                'url'=>CController::createUrl('GetChildCategory'),
                'update'=>'#Categories_parent_cat_id',
                'data'=>array('parentid'=>'js:this.value')
            ))); ?>
        <?php echo CHtml::error($model,'level_2'); ?>
    </div>

    <div class="row parentcat" style="display: <?php echo ($model->scenario == 'update')? 'block' : 'none' ?>">
        <?php echo $form->labelEx($model,'parent_cat_id'); ?>
        <?php
        $maincategory = CHtml::listData(Categories::model()->findAll('disabled=:dis',array(':dis'=>0)), 'id','category');
        echo $form->DropDownList($model,'parent_cat_id',$maincategory,array('prompt'=>'Select Parent Category'));
        ?>
        <?php echo $form->error($model,'parent_cat_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'status'); ?>
        <?php echo $form->dropDownList($model,'status',Categories::itemAlias('ItemStatus')); ?>
        <?php echo $form->error($model,'status'); ?>
    </div>


    <!-- <div class="row">
		<?php //echo $form->labelEx($model,'addedOn');  GetChildCategory ?>
		<?php //echo $form->textField($model,'addedOn',array('readonly'=>($model->scenario == 'update')? true : false)); ?>
		<?php //echo $form->error($model,'addedOn'); ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'lastUpdatedOn'); ?>
		<?php //echo $form->textField($model,'lastUpdatedOn',array('readonly'=>($model->scenario == 'update')? true : false)); ?>
		<?php //echo $form->error($model,'lastUpdatedOn'); ?>
	</div> -->

    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->