<?php
/* @var $this CategoriesController */
/* @var $model Categories */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'category'); ?>
		<?php echo $form->textField($model,'category',array('size'=>60,'maxlength'=>80)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'parent_cat_id'); ?>
		<?php echo $form->textField($model,'parent_cat_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'category_level'); ?>
		<?php echo $form->textField($model,'category_level'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'status'); ?>
		<?php echo $form->textField($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'addedOn'); ?>
		<?php echo $form->textField($model,'addedOn'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'lastUpdatedOn'); ?>
		<?php echo $form->textField($model,'lastUpdatedOn'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->