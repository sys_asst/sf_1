<?php
/* @var $this CategoriesController */
/* @var $model Categories */

$this->breadcrumbs=array(
	'Categories'=>array('index'),
	$model->category=>array('view','id'=>$model->category),
	'Update',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>Update Categories <?php echo $model->category; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'details'=>$details)); ?>