<?php
/* @var $this DistributorsController */
/* @var $model Distributors */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'distId'); ?>
		<?php echo $form->textField($model,'distId',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'distName'); ?>
		<?php echo $form->textField($model,'distName',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'distEmail'); ?>
		<?php echo $form->textField($model,'distEmail',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'distAddress1'); ?>
		<?php echo $form->textArea($model,'distAddress1',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'distAddress2'); ?>
		<?php echo $form->textArea($model,'distAddress2',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'distAddress3'); ?>
		<?php echo $form->textArea($model,'distAddress3',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'distType'); ?>
		<?php echo $form->textField($model,'distType',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'distPhone'); ?>
		<?php echo $form->textField($model,'distPhone',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'status'); ?>
		<?php echo $form->textField($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'loginCreated'); ?>
		<?php echo $form->textField($model,'loginCreated'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'addedOn'); ?>
		<?php echo $form->textField($model,'addedOn'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'lastUpdatedOn'); ?>
		<?php echo $form->textField($model,'lastUpdatedOn'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'disabled'); ?>
		<?php echo $form->textField($model,'disabled'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'disabled_by'); ?>
		<?php echo $form->textField($model,'disabled_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'disabled_date'); ?>
		<?php echo $form->textField($model,'disabled_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user_id'); ?>
		<?php echo $form->textField($model,'user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'login_created_by'); ?>
		<?php echo $form->textField($model,'login_created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'login_created_on'); ?>
		<?php echo $form->textField($model,'login_created_on'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->