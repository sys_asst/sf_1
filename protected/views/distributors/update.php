<?php
/* @var $this DistributorsController */
/* @var $model Distributors */

$this->breadcrumbs=array(
	'Distributors'=>array('index'),
	$model->distName=>array('view','id'=>$model->id),
	'Update',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));

?>

<h1>Update Distributors <?php echo $model->distName; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>