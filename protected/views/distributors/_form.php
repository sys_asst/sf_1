<?php
/* @var $this DistributorsController */
/* @var $model Distributors */
/* @var $form CActiveForm */
?>
<div>
    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
        'id'=>'distributors-form',
        'enableAjaxValidation'=>false,
        'type'=>'horizontal',
    ));
    ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

    <p>
        <?php //echo Yii::app()->createUrl('/salesreps/adduser', array(), 'https');//'salesreps/adduser';
        echo CHtml::link('Add User for '.$model->distName,Yii::app()->createUrl('/user/admin/create', array('refid'=>$model->id,'src'=>'dist'), '&'));
        ?>
    </p>
    <div class="row-fluid">
        <div class="span6">
            <?php echo $form->textFieldRow($model,'distId',array('class'=>'input-block-level','maxlength'=>5,'size'=>4, 'readonly'=>$model->isNewRecord ? false : true)); ?>
            <?php echo $form->textFieldRow($model,'distName',array('class'=>'input-block-level','size'=>60,'maxlength'=>255)); ?>
            <?php echo $form->textFieldRow($model,'distEmail',array('class'=>'input-block-level','size'=>60,'maxlength'=>100)); ?>
            <?php echo $form->textFieldRow($model,'distPhone',array('class'=>'input-block-level','size'=>60,'maxlength'=>100)); ?>
            <?php echo $form->dropDownListRow($model, 'area_id', CHtml::listData(Area::model()->findAll(array('condition'=>'status=1','order'=>'name')),'id','name'), array('multiple'=>false, 'class'=>'input-block-level','prompt'=>'Select Area')); ?>

        </div>
        <div class="span6">
            <?php echo $form->textAreaRow($model,'distAddress1',array('class'=>'input-block-level','row'=>5)); ?>
            <?php echo $form->textAreaRow($model,'distAddress2',array('class'=>'input-block-level','row'=>5)); ?>
            <?php echo $form->textAreaRow($model,'distAddress3',array('class'=>'input-block-level','row'=>5)); ?>
            <?php echo $form->dropDownListRow($model, 'status', Distributors::itemAlias('ItemStatus'), array('multiple'=>false, 'class'=>'input-block-level')); ?>
        </div>
    </div>


    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>$model->isNewRecord ? Yii::t('messages', 'Create') : Yii::t('messages', 'Save'),
        )); ?>

        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'label'=>Yii::t('messages', 'Cancel'),
            'type'=>'info',
            'url'=>Yii::app()->createUrl('distributors/admin')
        )); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->