<?php
/* @var $this DistributorsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Distributors',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>Distributors</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
