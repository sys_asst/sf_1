<?php
/* @var $this DistributorsController */
/* @var $data Distributors */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('distId')); ?>:</b>
	<?php echo CHtml::encode($data->distId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('distName')); ?>:</b>
	<?php echo CHtml::encode($data->distName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('distEmail')); ?>:</b>
	<?php echo CHtml::encode($data->distEmail); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('distAddress1')); ?>:</b>
	<?php echo CHtml::encode($data->distAddress1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('distAddress2')); ?>:</b>
	<?php echo CHtml::encode($data->distAddress2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('distAddress3')); ?>:</b>
	<?php echo CHtml::encode($data->distAddress3); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('distType')); ?>:</b>
	<?php echo CHtml::encode($data->distType); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('distPhone')); ?>:</b>
	<?php echo CHtml::encode($data->distPhone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('loginCreated')); ?>:</b>
	<?php echo CHtml::encode($data->loginCreated); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('addedOn')); ?>:</b>
	<?php echo CHtml::encode($data->addedOn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lastUpdatedOn')); ?>:</b>
	<?php echo CHtml::encode($data->lastUpdatedOn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('disabled')); ?>:</b>
	<?php echo CHtml::encode($data->disabled); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('disabled_by')); ?>:</b>
	<?php echo CHtml::encode($data->disabled_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('disabled_date')); ?>:</b>
	<?php echo CHtml::encode($data->disabled_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('login_created_by')); ?>:</b>
	<?php echo CHtml::encode($data->login_created_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('login_created_on')); ?>:</b>
	<?php echo CHtml::encode($data->login_created_on); ?>
	<br />

	*/ ?>

</div>