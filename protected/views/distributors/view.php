<?php
/* @var $this DistributorsController */
/* @var $model Distributors */

$this->breadcrumbs=array(
	'Distributors'=>array('index'),
	$model->distName,
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));

?>

<h1>View Distributors <?php echo $model->distName; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'distId',
		'distName',
		'distEmail',
		'distAddress1',
		'distAddress2',
		'distAddress3',
		'distType',
		'distPhone',
        array(
            'name'=>'area_id',
            'label'=>'Area',
            'value'=>CHtml::encode(isset($model->area->name) ? $model->area->name : 'n/a')
        ),
        array(
            'name'=>'status',
            'value'=>CHtml::encode(Distributors::itemAlias('ItemStatus', $model->status))
        ),
        array(
            'name'=>'loginCreated',
            'value'=>CHtml::encode(isset($model->loginCreated) ? 'yes' : 'no')
        ),
		'addedOn',
		'lastUpdatedOn',
        array(
            'name'=>'disabled',
            'value'=>CHtml::encode(isset($model->disabled) == 0 ? 'no' : 'yes')
        ),
		'disabled_by',
		'disabled_date',
        array(
            'name'=>'user_id',
            'value'=>CHtml::encode(isset($model->user->username) ? $model->user->username : 'n/a')
        ),
        array(
            'name'=>'login_created_by',
            'value'=>CHtml::encode(isset($model->loginCreatedBy->username) ? $model->loginCreatedBy->username : 'n/a')
        ),
		'login_created_on',
	),
)); ?>
