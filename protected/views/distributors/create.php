<?php
/* @var $this DistributorsController */
/* @var $model Distributors */

$this->breadcrumbs=array(
	'Distributors'=>array('index'),
	'Create',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>Create Distributors</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>