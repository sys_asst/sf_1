<?php
/* @var $this DealersEditController */
/* @var $model DealersEdit */

$this->breadcrumbs=array(
	'Dealers Update Request'=>array('index'),
	'Manage',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#dealers-edit-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Dealers Update Request</h1>

<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php /*$this->renderPartial('_search',array(
	'model'=>$model,
));*/ ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
	'id'=>'dealers-edit-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
		//'dealer_id',
		'dealerCode',
		'dealerName',
        array(
            'name'=>'distributor',
            'value'=>'$data->distributor0->distName',
            'filter' => CHtml::listData(Distributors::model()->findAll(array('order'=>'distName asc')), 'distId','distName'),
        ),
		'phoneOffice',
		'contactPerson',
        'phoneResidence',
        'email',
        'creditDays',
        'creditAmount',
        'address1',
        'address2',
        'address3',
        array(
            'name'=>'status',
            'value'=>'DealersEdit::itemAlias("ItemStatus",$data->status)',
            'filter' => DealersEdit::itemAlias("ItemStatus"),
        ),
		/*
		'creditDays',
		'creditAmount',
		'address1',
		'address2',
		'address3',
		'townId',
		'latitude',
		'longitude',
		'distributor',
		'addedOn',
		'lastUpdatedOn',
		'disabled',
		'disabled_by',
		'disabled_on',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'template' => '{view}{update}',
		),
	),
)); ?>
