<?php
/* @var $this DealersEditController */
/* @var $model DealersEdit */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dealer_id'); ?>
		<?php echo $form->textField($model,'dealer_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dealerCode'); ?>
		<?php echo $form->textField($model,'dealerCode',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dealerName'); ?>
		<?php echo $form->textField($model,'dealerName',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'phoneOffice'); ?>
		<?php echo $form->textField($model,'phoneOffice',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'contactPerson'); ?>
		<?php echo $form->textField($model,'contactPerson',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'phoneResidence'); ?>
		<?php echo $form->textField($model,'phoneResidence'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'creditDays'); ?>
		<?php echo $form->textField($model,'creditDays'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'creditAmount'); ?>
		<?php echo $form->textField($model,'creditAmount'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'address1'); ?>
		<?php echo $form->textArea($model,'address1',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'address2'); ?>
		<?php echo $form->textArea($model,'address2',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'address3'); ?>
		<?php echo $form->textArea($model,'address3',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'townId'); ?>
		<?php echo $form->textArea($model,'townId',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'latitude'); ?>
		<?php echo $form->textField($model,'latitude'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'longitude'); ?>
		<?php echo $form->textField($model,'longitude'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'distributor'); ?>
		<?php echo $form->textField($model,'distributor',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'addedOn'); ?>
		<?php echo $form->textField($model,'addedOn'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'lastUpdatedOn'); ?>
		<?php echo $form->textField($model,'lastUpdatedOn'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'disabled'); ?>
		<?php echo $form->textField($model,'disabled'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'disabled_by'); ?>
		<?php echo $form->textField($model,'disabled_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'disabled_on'); ?>
		<?php echo $form->textField($model,'disabled_on'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->