<?php
/* @var $this DealersEditController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Dealers Update Request',
);

$this->menu=array(
	array('label'=>'Create Dealers Update Request', 'url'=>array('create')),
	array('label'=>'Manage Dealers Update Request', 'url'=>array('admin')),
);
?>

<h1>Dealers Update Requests</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
