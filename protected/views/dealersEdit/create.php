<?php
/* @var $this DealersEditController */
/* @var $model DealersEdit */

$this->breadcrumbs=array(
	'Dealers Update Request'=>array('index'),
	'Create',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>Create Dealers Update Request</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>