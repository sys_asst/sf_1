<?php
/* @var $this DealersEditController */
/* @var $model DealersEdit */

$this->breadcrumbs=array(
	'Dealers Update Request'=>array('index'),
	$model->dealerName,
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>View Dealers Update Request for <?php echo $model->dealerName; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'dealer_id',
		'dealerCode',
		'dealerName',
		'phoneOffice',
		'contactPerson',
		'phoneResidence',
        'email',
		'creditDays',
		'creditAmount',
		'address1',
		'address2',
		'address3',
        array(
            'name'=>'townId',
            'value'=>$model->town->town,
        ),
		'latitude',
		'longitude',
        array(
            'name'=>'distributor',
            'value'=>$model->distributor0->distName,
        ),
		'addedOn',
		'lastUpdatedOn',
        array(
            'name'=>'status',
            'value'=>CHtml::encode(DealersEdit::itemAlias('ItemStatus', $model->status))
        ),
        array(
            'name'=>'disabled',
            'value'=>$model->disabled ? 'yes' : 'no',
        ),
		'disabled_by',
		'disabled_on',
	),
)); ?>
