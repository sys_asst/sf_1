<?php
/* @var $this DealersEditController */
/* @var $model DealersEdit */

$this->breadcrumbs=array(
	'Dealers Update Request'=>array('index'),
	$model->dealerName=>array('view','id'=>$model->id),
	'Update',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>Update Dealers' Comment Edit <?php echo $model->dealerName; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>