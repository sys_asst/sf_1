<?php
/* @var $this DealersEditController */
/* @var $model DealersEdit */

$this->breadcrumbs=array(
	'Dealers Update Request'=>array('index'),
	$model->dealerName,
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>Compare Dealers Update Request for <?php echo $model->dealerName; ?></h1>
<div>
    <?php //echo Yii::app()->createUrl('/salesreps/adduser', array(), 'https');//'salesreps/adduser';
    echo CHtml::link('Confirm '.$model->dealerName,Yii::app()->createUrl('/dealersEdit/confirm', array('id'=>$model->id), '&'));
    ?>
</div>

<table style="width: 65em" >
    <tr><td>Dealer</td><td>Requested Updates</td></tr>
    <tr><td><?php $this->widget('bootstrap.widgets.TbDetailView', array(
                'data'=>$dealer,
                'attributes'=>array(
                    array(
                        'name'=>'id',
                        'type'=>'raw',
                        'value'=>CHtml::link($dealer->id,Yii::app()->createUrl('/dealers/update', array('id'=>$dealer->id), '&'), array('target'=>'_blank'))
                    ),
                    //'dealer_id',
                    'dealerCode',
                    'dealerName',
                    'phoneOffice',
                    'email',
                    'contactPerson',
                    'phoneResidence',
                    'creditDays',
                    'creditAmount',
                    'address1',
                    'address2',
                    'address3',
                    array(
                        'name'=>'townId',
                        'value'=>$dealer->town->town,
                    ),
                    'latitude',
                    'longitude',
                    array(
                        'name'=>'distributor',
                        'value'=>$dealer->distributor0->distName,
                    ),
                    'addedOn',
                    'lastUpdatedOn',
                    array(
                        'name'=>'disabled',
                        'value'=>$dealer->disabled ? 'yes' : 'no',
                    ),
                    'disabled_by',
                    'disabled_on',
                ),
            )); ?></td><td><?php $this->widget('bootstrap.widgets.TbDetailView', array(
                'data'=>$model,
                'attributes'=>array(
                    //'id',
                    'dealer_id',
                    'dealerCode',
                    'dealerName',
                    'phoneOffice',
                    'email',
                    'contactPerson',
                    'phoneResidence',
                    'creditDays',
                    'creditAmount',
                    'address1',
                    'address2',
                    'address3',
                    array(
                        'name'=>'townId',
                        'value'=>$model->town->town,
                    ),
                    'latitude',
                    'longitude',
                    array(
                        'name'=>'distributor',
                        'value'=>$model->distributor0->distName,
                    ),
                    'addedOn',
                    'lastUpdatedOn',
                    array(
                        'name'=>'disabled',
                        'value'=>$model->disabled ? 'yes' : 'no',
                    ),
                    'disabled_by',
                    'disabled_on',
                ),
            )); ?></td></tr>
</table>

