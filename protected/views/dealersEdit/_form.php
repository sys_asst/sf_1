<?php
/* @var $this DealersEditController */
/* @var $model DealersEdit */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
        'id'=>'dealers-edit-form',
        'enableAjaxValidation'=>false,
        'type'=>'horizontal',
    ));
    ?>
	<p class="note">Fields with <span class="required">*</span> are required.</p>

    <div>
        <?php //echo Yii::app()->createUrl('/salesreps/adduser', array(), 'https');//'salesreps/adduser';
        echo CHtml::link('Compare With Added Dealer '.$model->dealerName,Yii::app()->createUrl('/dealersEdit/compare', array('id'=>$model->id), '&'));
        ?>
    </div>

	<?php echo $form->errorSummary($model); ?>

    <div class="row-fluid">
        <div class="span6">
            <?php echo $form->textFieldRow($model,'dealer_id',array('class'=>'input-block-level','maxlength'=>5,'size'=>4, 'readonly'=>true)); ?>
            <?php echo $form->textFieldRow($model,'dealerCode',array('class'=>'input-block-level','maxlength'=>5,'size'=>4, 'readonly'=>true)); ?>
            <?php echo $form->textFieldRow($model,'dealerName',array('class'=>'input-block-level','size'=>60,'maxlength'=>100)); ?>
            <?php echo $form->textFieldRow($model,'phoneOffice',array('class'=>'input-block-level','size'=>60,'maxlength'=>25)); ?>
            <?php echo $form->textFieldRow($model,'phoneResidence',array('class'=>'input-block-level','size'=>60,'maxlength'=>25)); ?>
            <?php echo $form->dropDownListRow($model, 'townId', CHtml::listData(Towns::model()->findAll('disabled = 0'), 'id','town'), array('multiple'=>false, 'class'=>'input-block-level','prompt'=>'Select Area')); ?>
            <?php echo $form->dropDownListRow($model, 'distributor', CHtml::listData(Distributors::model()->findAll('status = 1',array('order'=>'distName asc')), 'distId','distName'), array('multiple'=>false, 'class'=>'input-block-level','prompt'=>'Select Area')); ?>

        </div>
        <div class="span6">
            <?php echo $form->textFieldRow($model,'contactPerson',array('class'=>'input-block-level','size'=>60,'maxlength'=>25)); ?>
            <?php echo $form->textAreaRow($model,'address1',array('class'=>'input-block-level','row'=>5)); ?>
            <?php echo $form->textAreaRow($model,'address2',array('class'=>'input-block-level','row'=>5)); ?>
            <?php echo $form->textAreaRow($model,'address3',array('class'=>'input-block-level','row'=>5)); ?>
            <?php echo $form->dropDownListRow($model, 'status', DealersEdit::itemAlias('ItemStatus'), array('multiple'=>false, 'class'=>'input-block-level')); ?>
        </div>
    </div>

    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>$model->isNewRecord ? Yii::t('messages', 'Create') : Yii::t('messages', 'Save'),
        )); ?>

        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'label'=>Yii::t('messages', 'Cancel'),
            'type'=>'info',
            'url'=>Yii::app()->createUrl('dealersEdit/LatestComments', array('statusnc'=>0))
        )); ?>
    </div>


<?php $this->endWidget(); ?>

</div><!-- form -->