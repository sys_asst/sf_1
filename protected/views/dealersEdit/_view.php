<?php
/* @var $this DealersEditController */
/* @var $data DealersEdit */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dealer_id')); ?>:</b>
	<?php echo CHtml::encode($data->dealer_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dealerCode')); ?>:</b>
	<?php echo CHtml::encode($data->dealerCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dealerName')); ?>:</b>
	<?php echo CHtml::encode($data->dealerName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('phoneOffice')); ?>:</b>
	<?php echo CHtml::encode($data->phoneOffice); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contactPerson')); ?>:</b>
	<?php echo CHtml::encode($data->contactPerson); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('phoneResidence')); ?>:</b>
	<?php echo CHtml::encode($data->phoneResidence); ?>
	<br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('townId')); ?>:</b>
    <?php echo CHtml::encode($data->town->town); ?>
    <br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('creditDays')); ?>:</b>
	<?php echo CHtml::encode($data->creditDays); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('creditAmount')); ?>:</b>
	<?php echo CHtml::encode($data->creditAmount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('address1')); ?>:</b>
	<?php echo CHtml::encode($data->address1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('address2')); ?>:</b>
	<?php echo CHtml::encode($data->address2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('address3')); ?>:</b>
	<?php echo CHtml::encode($data->address3); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('latitude')); ?>:</b>
	<?php echo CHtml::encode($data->latitude); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('longitude')); ?>:</b>
	<?php echo CHtml::encode($data->longitude); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('distributor')); ?>:</b>
	<?php echo CHtml::encode($data->distributor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('addedOn')); ?>:</b>
	<?php echo CHtml::encode($data->addedOn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lastUpdatedOn')); ?>:</b>
	<?php echo CHtml::encode($data->lastUpdatedOn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('disabled')); ?>:</b>
	<?php echo CHtml::encode($data->disabled); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('disabled_by')); ?>:</b>
	<?php echo CHtml::encode($data->disabled_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('disabled_on')); ?>:</b>
	<?php echo CHtml::encode($data->disabled_on); ?>
	<br />

	*/ ?>

</div>