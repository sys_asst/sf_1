<?php
/* @var $this DsdivisionsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Dsdivisions',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>Dsdivisions</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
