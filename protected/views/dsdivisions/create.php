<?php
/* @var $this DsdivisionsController */
/* @var $model Dsdivisions */

$this->breadcrumbs=array(
	'Dsdivisions'=>array('index'),
	'Create',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>Create Dsdivisions</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>