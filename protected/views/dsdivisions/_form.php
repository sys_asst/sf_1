<?php
/* @var $this DsdivisionsController */
/* @var $model Dsdivisions */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'dsdivisions-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
    'type'=>'horizontal',
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

    <div class="row-fluid">
        <div class="span6">
            <?php echo $form->dropDownListRow($model,'district_id',CHtml::listData(District::model()->findAll('status=1'),'id','description'),array('prompt'=>'Select District')); ?>
            <?php echo $form->textFieldRow($model,'code',array('size'=>10,'maxlength'=>10)); ?>
            <?php echo $form->dropDownListRow($model,'status',Dsdivisions::itemAlias('ItemStatus')); ?>
        </div>
        <div class="span6">
            <?php echo $form->textFieldRow($model,'name',array('size'=>10,'maxlength'=>255)); ?>
            <?php echo $form->dropDownListRow($model,'user_defined',Dsdivisions::itemAlias('UserDef')); ?>
        </div>
    </div>


    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>$model->isNewRecord ? Yii::t('messages', 'Create') : Yii::t('messages', 'Save'),
        )); ?>

        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'label'=>Yii::t('messages', 'Cancel'),
            'type'=>'info',
            'url'=>Yii::app()->createUrl('dsdivisions/admin')
        )); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->