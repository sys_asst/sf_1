<?php
/* @var $this DsdivisionsController */
/* @var $model Dsdivisions */

$this->breadcrumbs=array(
	'Dsdivisions'=>array('index'),
	'Manage',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#dsdivisions-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Dsdivisions</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
	'id'=>'dsdivisions-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
        array(
            'name'=>'district_id',
            'value'=>'$data->district->description',
            'filter' => CHtml::listData(District::model()->findAll(), 'id','description'),
        ),
        /*array(
            'name'=>'code',
            'value'=>'strlen($data->code) ? $data->code : "n/a"'
        ),*/
		'name',
        array(
            'name'=>'status',
            'value'=>'Dsdivisions::itemAlias("ItemStatus",$data->status)',
            'filter' => Dsdivisions::itemAlias("ItemStatus"),
        ),
        array(
            'name'=>'user_defined',
            'value'=>'Dsdivisions::itemAlias("UserDef",$data->user_defined)',
            'filter' => Dsdivisions::itemAlias("UserDef"),
        ),
		/*
		'added_on',UserDef
		'added_by',
		'updated_on',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'template'=>'{view} {update}',
		),
	),
)); ?>
