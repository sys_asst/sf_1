<?php
/* @var $this DsdivisionsController */
/* @var $model Dsdivisions */

$this->breadcrumbs=array(
	'Dsdivisions'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>Update Ds divisions <?php echo $model->name; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>