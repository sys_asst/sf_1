<?php
/* @var $this DsdivisionsController */
/* @var $model Dsdivisions */

$this->breadcrumbs=array(
	'Dsdivisions'=>array('index'),
	$model->name,
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));

$records = Towns::getTowns($model->id);
if(sizeof($records))
{
    $data = '<table class="items table table-striped table-bordered table-condensed"><thead><tr><th style="text-align: center">Town</th></tr></thead><tbody>';
    foreach($records as $rec)
    {
        $data .= '<tr><td>'.$rec['town'].'</td></tr>';
    }
    $data .= '</tbody></table>';
} else {
    $data .= 'no data found!';
}
?>

<h1>View Ds Divisions <?php echo $model->name; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
        array(
            'name'=>'district_id',
            'value'=>CHtml::encode($model->district->description)
        ),
        array(
            'name'=>'code',
            'value'=>CHtml::encode(strlen($model->code) ? $model->code : 'n/a')
        ),
		'name',
        array(
            'name'=>'status',
            'value'=>CHtml::encode(Dsdivisions::itemAlias('ItemStatus', $model->status))
        ),
        array(
            'name'=>'user_defined',
            'value'=>CHtml::encode(Dsdivisions::itemAlias('UserDef', $model->user_defined))
        ),
        array(
            'name'=>'added_by',
            'value'=>CHtml::encode($model->addedBy->username)
        ),
		'added_on',
		'updated_on',
        array(
            'label' =>'Towns',
            'type' => 'raw',
            'value' => $data
        )
	),
)); ?>
