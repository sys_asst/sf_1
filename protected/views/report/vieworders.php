<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Randika
 * Date: 6/10/14
 * Time: 2:05 PM
 * To change this template use File | Settings | File Templates.
 */



$this->breadcrumbs=array(
    'Report'=>array('index'),
    'Create',
);

//$baseScriptUrl=Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets'));

//Yii::app()->clientScript->registerCssFile($baseScriptUrl.'/gridview/styles.css');
?>

    <h1><?php echo OrderForm::itemAlias('Type',strlen($model->rpttype) ? $model->rpttype : 1) ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model,'data'=>$data)); ?>