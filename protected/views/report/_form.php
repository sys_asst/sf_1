<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Randika
 * Date: 6/10/14
 * Time: 2:09 PM
 * To change this template use File | Settings | File Templates.
 */
?>

<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'order-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>false,
    'type'=>'horizontal',

));
//echo Tk::a(Yii::app()->user->getState('src')->designation_id);
$dist = User::getDistributor();
$temp = array();
$distributor = '';
Yii::app()->getModule('user')->isAdmin() == false ? $temp = User::getDistributor() : null;
(!is_array($temp)) ? $temp = explode(',', $temp) : '';
if(sizeof($temp) > 1)
{
    $distributor = "'".implode("','", $temp)."'";
} elseif(sizeof($temp) == 1) {
    $distributor = "'{$temp[0]}'";
} else {
    $distributor = "''";
}

$type = OrderForm::itemAlias('Type');
//Tk::a(Yii::app()->user->src);
//Tk::a(Yii::app()->user->src['srDesignation']);//'srDesignation '
if(Yii::app()->user->profile->src == 1)
{
    $leaflevelattrbutes = CHtml::listData(Salesreps::model()->findAll(array('condition'=>'status=1 AND srDesignation="Regional Customer Manager"','order'=>'srName')), 'id','srName');
} elseif(Yii::app()->user->profile->src == 2)
{
    $leaflevelattrbutes = CHtml::listData(Salesreps::model()->findAll(array('condition'=>'status=1 AND srDesignation="Regional Customer Manager" AND srEmpno="'.Yii::app()->user->name.'"','order'=>'srName')), 'id','srName');

    $sql = "SELECT 0 FROM `salesreps` AS S WHERE S.`status` = '1' AND S.`srEmpNo` = '".Yii::app()->user->name."' AND S.`srDesignation` = 'Sales Manager' OR S.`designation_id` = '2'";
    $temp = Tk::sql($sql);

    if(sizeof($temp))
    {
        $leaflevelattrbutes = CHtml::listData(Salesreps::model()->findAll(array('condition'=>'status=1 AND srDesignation="Regional Customer Manager"','order'=>'srName')), 'id','srName');
    }
    if(Yii::app()->user->src['srDesignation'] == 'Regional Customer Manager')
    {
        $leaflevelattrbutes = CHtml::listData(Salesreps::model()->findAll(array('condition'=>'status=1 AND srEmpNo="'.Yii::app()->user->name.'"','order'=>'srName')), 'id','srName');
    }

} elseif(Yii::app()->user->profile->src == 3)
{
    unset($type[4]);
    $leaflevelattrbutes = CHtml::listData(Salesreps::model()->findAll(array('condition'=>'status=1 AND srDesignation="Regional Customer Manager"','order'=>'srName')), 'id','srName');
}
//Tk::a($leaflevelattrbutes);
?>

<p class="note">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>
    <div class="row-fluid">
        <div class="span6">
            <?php echo $form->dropDownListRow($model,'rpttype',$type); ?>
            <?php
                if($model->rpttype == 4)
                {
                    //$leaflevelattrbutes = CHtml::listData(Salesreps::model()->findAll(array('condition'=>'status=1 AND srDesignation="Regional Customer Manager"','order'=>'srName')), 'id','srName');

                    echo $form->dropDownListRow($model,'rcm',$leaflevelattrbutes,array('prompt'=>'Select Regional Customer Manager'));
                } else {
                    echo $form->dropDownListRow($model,'distributor',CHtml::listData(Distributors::model()->findAll(array('condition'=>'status=:active'.(Yii::app()->getModule('user')->isAdmin() == false ? " AND distId IN (".$distributor.")" : ''), 'order'=>'distName', 'params'=>array(':active'=>1))),'distId','distName'), array('multiple'=>false, 'class'=>'','prompt'=>'Select Distributor'));
                }
            ?>
        </div>
        <div class="span6">
            <div class="control-group ">
                <label class="control-label " for="fromdate">
                    <?php echo Yii::t('messages', 'From Date');//$attributeLabels['publishDate']?>

                </label>
                <div class="controls">
                    <?php
                    $calLang = '';//Yii::app()->toolKit->getComponenetSpecificLangIdentifier('juiDateTimePicker');
                    echo $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'=>$model,
                            'attribute'=>'fromdate',
                            //'language' => 'ja',
                            // 'i18nScriptFile' => 'jquery.ui.datepicker-ja.js', (#2)
                            'htmlOptions' => array(
                                'id' => 'fromdate_search',
                                'size' => '10',
                            ),
                            'options' => array(
                                'dateFormat' => 'yy-mm-dd',
                                'showButtonPanel' => true,
                                'showOn' => 'focus'
                            )
                        ),
                        true);
                    ?>
                    <?php echo $form->error($model, 'srJoinedDate'); ?>
                </div>
            </div>
            <div class="control-group ">
                <label class="control-label " for="tomdate">
                    <?php echo Yii::t('messages', 'To Date');//$attributeLabels['publishDate']?>

                </label>
                <div class="controls">
                    <?php
                    $calLang = '';//Yii::app()->toolKit->getComponenetSpecificLangIdentifier('juiDateTimePicker');
                    echo $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'=>$model,
                            'attribute'=>'todate',
                            //'language' => 'ja',
                            // 'i18nScriptFile' => 'jquery.ui.datepicker-ja.js', (#2)
                            'htmlOptions' => array(
                                'id' => 'todate_search',
                                'size' => '10',
                            ),
                            'options' => array(
                                'dateFormat' => 'yy-mm-dd',
                                'showButtonPanel' => true,
                                'showOn' => 'focus'
                            )
                        ),
                        true);
                    ?>
                    <?php echo $form->error($model, 'todate'); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>'Get Report',
        )); ?>

        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'label'=>Yii::t('messages', 'Cancel'),
            'type'=>'info',
            'url'=>Yii::app()->createUrl('report/vieworders')
        )); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<div id="orders-grid" class="grid-view">
<?php

    if(sizeof($data))
    {
        if($model->rpttype == 1)
        {
            $totalorders = sizeof($data);
            $ordertotal = 0;
            $class = 'odd';
            echo '<table class="items table table-striped table-bordered table-condensed"><thead>';
            echo '<tr><th>Order Id</th><th>Dealer</th><th>Order Placed Date</th><th>Delivery Date</th><th>Type</th><th>Distributor</th><th>Sales Rep</th><th style="text-align: right">Order Total</th></tr></thead><tbody>';
            foreach($data as $row)
            {
                $class = ($class == 'odd') ? 'even': 'odd';
                echo "<tr class=\"{$class}\"><td>".CHtml::link($row['ordeno'],Yii::app()->baseUrl.'/index.php/orders/vieworder/id/'.$row['id'],array('target'=>'_blank'))."</td><td>{$row['dealerName']}</td>
                <td>{$row['orderPlacedTime']}</td><td>{$row['deliveryDate']}</td><td>{$row['orderType']}</td><td>{$row['distName']}</td><td>{$row['srName']}</td>
                <td style=\"text-align: right\">".Yii::app()->numberFormatter->formatCurrency($row['orderTotalValue'],'')."</td></tr>";
                $ordertotal += $row['orderTotalValue'];
            }
            echo "<tr class=\"{$class}\"><th>Total Orders</th><th>{$totalorders}</th><th colspan=\"6\" style='text-align: right'>".Yii::app()->numberFormatter->formatCurrency($ordertotal,'')."</th></tr>";
            echo '</tbody></table>';
        } elseif($model->rpttype == 2) {
            $class = 'odd';
            echo '<table class="items table table-striped table-bordered table-condensed"><thead>';
            echo '<tr><th>Id</th><th>Sales Rep</th><th style="">Dealer</th><th style="">Distributor</th><th>Comment</th><th style="">Commend Date</th><th style="">Reason</th></tr></thead><tbody>';
            foreach($data as $row)
            {
                $class = ($class == 'odd') ? 'even': 'odd';
                echo "<tr class=\"{$class}\"><td>".CHtml::link($row['commentno'],Yii::app()->baseUrl.'/index.php/dealercomments/view?id='.$row['id'],array('target'=>'_blank'))."</td><td>{$row['salesRef']}</td>
                <td>{$row['dealerName']}</td>
                <td>{$row['distName']}</td><td>{$row['comment']}</td><td>{$row['commentDate']}</td><td>{$row['reason']}</td></tr>";

            }
            echo "<tr><th colspan=\"2\" >Total Comments</th><th colspan=\"4\" >".sizeof($data)."</th></tr>";
            echo '</tbody></table>';
        } elseif($model->rpttype == 3) {
            $class = 'odd';
            echo '<table class="items table table-striped table-bordered table-condensed"><thead>';
            echo '<tr><th>Dealer Name</th><th style="width: 3em">Office Phone</th><th>Residence Phone</th><th style="width: 5em">Contact Person</th>
            <th>Address </th><th>Town </th><th>Route </th><th>Status</th><th>Sales Rep</th><th>Added On</th></tr></thead><tbody>';
            foreach($data as $row)
            {
                $class = ($class == 'odd') ? 'even': 'odd';
                echo "<tr class=\"{$class}\"><td>".CHtml::link($row['dealerName'],Yii::app()->baseUrl.'/index.php/dealers/view?id='.$row['id'],array('target'=>'_blank'))."</td><td>
                {$row['phoneOffice']}</td><td>{$row['phoneResidence']}</td><td>{$row['contactPerson']}</td><td>{$row['address1']} {$row['address2']} {$row['address3']} </td>
                <td>{$row['town']}</td><td>".Towns::getRoute($row['townId'])."</td><td>".Dealers::itemAlias('ItemStatus', $row['disabled'])."</td><td>{$row['srName']}</td>
                <td>{$row['addedOn']}</td></tr>";

            }
            echo "<tr><th colspan=\"2\" >Total Dealers</th><th colspan=\"11\" >".sizeof($data)."</th></tr>";
            echo '</tbody></table>';
        } elseif($model->rpttype == 4)
        {
            $filename = SYS_TMP_PATH.str_replace(' ', '_',OrderForm::itemAlias('Type', $model->rpttype)).'.xlsx';
            //$this->writeExcel($model, $data, $filename);

            //echo CHtml::link('',Yii::app()->baseUrl.'/index.php/report/download/type/'.$model->rpttype,array('target'=>'_blank', 'class'=>'fa fa-file-excel-o fa-3x', 'title'=>'Download as Excel'));

            $class = 'odd';
            $chart_data = array();
            $chart_data[] = array('Sales Rep', 'Sales Order', 'Dealer Comments', 'Total Schedule');
            echo '<table class="items table table-striped table-bordered table-condensed"><thead>';
            echo '<tr><th>RCM Name</th><th>Rep Name</th><th>Total Schedule</th><th>Sales Orders</th><th>Dealer Comments</th></tr></thead><tbody>';

            foreach($data as $row)
            {
                $class = ($class == 'odd') ? 'even': 'odd';
                $no_schedules = RouteMap::getScheduledDealers($row['rmap_id']);
                $get_visits = Orders::getOrderBySR($row['rep_code'], $model->fromdate, $model->todate);
                $no_comments = Dealercomments::getNoofComments($row['rep_code'], $model->fromdate, $model->todate);
                $chart_data[] = array($row['rep_name'], (int)$get_visits, (int)$no_comments, (int)$no_schedules);
                echo "<tr class=\"{$class}\"><td>".$row['rcm_name']."</td><td>".$row['rep_name']."</td><td style=\"text-align:center;\">
                {$no_schedules}</td><td style=\"text-align:center;\">{$get_visits}</td><td style=\"text-align:center;\">{$no_comments}</td></tr>";

            }
            //echo "<tr><th colspan=\"2\" >Total Dealers</th><th colspan=\"11\" >".sizeof($data)."</th></tr>";
            echo '</tbody></table>';

            ?>
            <div class="row">
                <div class="span6" >
                    <?php
                    $chart_data_header = array('Sales Rep', 'Sales Order', 'Dealer Comments', 'Total Schedule');
                    //very useful google chart
                    $this->widget('ext.Hzl.google.HzlVisualizationChart', array('visualization' => 'ColumnChart',
                        'packages'=>'corechart',
                        'data' =>
                            $chart_data,

                        'options' => array('title' => OrderForm::itemAlias('Type', $model->rpttype),
                            'titleTextStyle' => array('fontSize' => '25'),
                            'width' => 1000,
                            'height' => 600,
                            'isStacked'=>true,
                            'seriesType'=>'bars',
                            'series'=> array(2=>array('type'=> 'line'))
                        )));
                    ?>

                </div>
            </div>
            <?php
        }

    } else {
        echo 'no data found';
    }
?>
</div>