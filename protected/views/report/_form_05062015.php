<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Randika
 * Date: 6/10/14
 * Time: 2:09 PM
 * To change this template use File | Settings | File Templates.
 */
?>

<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'order-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>false,
    'type'=>'horizontal',

));
//echo Tk::a(Yii::app()->user->getState('src')->designation_id);
$dist = User::getDistributor();
//Tk::a($dist);exit;
$dist = User::getDistributor();
$temp = array();
$distributor = '';
Yii::app()->getModule('user')->isAdmin() == false ? $temp = User::getDistributor() : null;
(!is_array($temp)) ? $temp = explode(',', $temp) : '';
if(sizeof($temp) > 1)
{
    $distributor = "'".implode("','", $temp)."'";
} elseif(sizeof($temp) == 1) {
    $distributor = "'{$temp[0]}'";
} else {
    $distributor = "''";
}

?>

<p class="note">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>
    <div class="row-fluid">
        <div class="span6">
            <?php echo $form->dropDownListRow($model,'rpttype',OrderForm::itemAlias('Type')); ?>
            <?php echo $form->dropDownListRow($model,'distributor',CHtml::listData(Distributors::model()->findAll(array('condition'=>'status=:active'.(Yii::app()->getModule('user')->isAdmin() == false ? " AND distId IN (".$distributor.")" : ''), 'order'=>'distName', 'params'=>array(':active'=>1))),'distId','distName'), array('multiple'=>false, 'class'=>'','prompt'=>'Select Distributor')); ?>
        </div>
        <div class="span6">
            <div class="control-group ">
                <label class="control-label " for="fromdate">
                    <?php echo Yii::t('messages', 'From Date');//$attributeLabels['publishDate']?>

                </label>
                <div class="controls">
                    <?php
                    $calLang = '';//Yii::app()->toolKit->getComponenetSpecificLangIdentifier('juiDateTimePicker');
                    echo $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'=>$model,
                            'attribute'=>'fromdate',
                            //'language' => 'ja',
                            // 'i18nScriptFile' => 'jquery.ui.datepicker-ja.js', (#2)
                            'htmlOptions' => array(
                                'id' => 'fromdate_search',
                                'size' => '10',
                            ),
                            'options' => array(
                                'dateFormat' => 'yy-mm-dd',
                                'showButtonPanel' => true,
                                'showOn' => 'focus'
                            )
                        ),
                        true);
                    ?>
                    <?php echo $form->error($model, 'srJoinedDate'); ?>
                </div>
            </div>
            <div class="control-group ">
                <label class="control-label " for="tomdate">
                    <?php echo Yii::t('messages', 'To Date');//$attributeLabels['publishDate']?>

                </label>
                <div class="controls">
                    <?php
                    $calLang = '';//Yii::app()->toolKit->getComponenetSpecificLangIdentifier('juiDateTimePicker');
                    echo $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'=>$model,
                            'attribute'=>'todate',
                            //'language' => 'ja',
                            // 'i18nScriptFile' => 'jquery.ui.datepicker-ja.js', (#2)
                            'htmlOptions' => array(
                                'id' => 'todate_search',
                                'size' => '10',
                            ),
                            'options' => array(
                                'dateFormat' => 'yy-mm-dd',
                                'showButtonPanel' => true,
                                'showOn' => 'focus'
                            )
                        ),
                        true);
                    ?>
                    <?php echo $form->error($model, 'todate'); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>'Get Report',
        )); ?>

        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'label'=>Yii::t('messages', 'Cancel'),
            'type'=>'info',
            'url'=>Yii::app()->createUrl('report/vieworders')
        )); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<div id="orders-grid" class="grid-view">
<?php

    if(sizeof($data))
    {
        if($model->rpttype == 1)
        {
            $totalorders = sizeof($data);
            $ordertotal = 0;
            $class = 'odd';
            echo '<table class="items table table-striped table-bordered table-condensed"><thead>';
            echo '<tr><th>Order Id</th><th>Dealer</th><th>Order Placed Date</th><th>Delivery Date</th><th>Type</th><th>Distributor</th><th>Sales Rep</th><th style="text-align: right">Order Total</th></tr></thead><tbody>';
            foreach($data as $row)
            {
                $class = ($class == 'odd') ? 'even': 'odd';
                echo "<tr class=\"{$class}\"><td>".CHtml::link($row['ordeno'],Yii::app()->baseUrl.'/index.php/orders/vieworder/id/'.$row['id'],array('target'=>'_blank'))."</td><td>{$row['dealerName']}</td><td>{$row['orderPlacedTime']}</td><td>{$row['deliveryDate']}</td><td>{$row['orderType']}</td><td>{$row['distName']}</td><td>{$row['srName']}</td><td style=\"text-align: right\">".Yii::app()->numberFormatter->formatCurrency($row['orderTotalValue'],'')."</td></tr>";
                $ordertotal += $row['orderTotalValue'];
            }
            echo "<tr class=\"{$class}\"><th>Total Orders</th><th>{$totalorders}</th><th colspan=\"6\" style='text-align: right'>".Yii::app()->numberFormatter->formatCurrency($ordertotal,'')."</th></tr>";
            echo '</tbody></table>';
        } elseif($model->rpttype == 2) {
            $class = 'odd';
            echo '<table class="items table table-striped table-bordered table-condensed"><thead>';
            echo '<tr><th>Id</th><th>Sales Rep</th><th style="width: 20em">Dealer</th><th>Comment</th><th style="width: 10em">Commend Date</th><th style="width: 20em">Reason</th></tr></thead><tbody>';
            foreach($data as $row)
            {
                $class = ($class == 'odd') ? 'even': 'odd';
                echo "<tr class=\"{$class}\"><td>".CHtml::link($row['commentno'],Yii::app()->baseUrl.'/index.php/dealercomments/view?id='.$row['id'],array('target'=>'_blank'))."</td><td>{$row['salesRef']}</td><td>{$row['dealerName']}</td><td>{$row['comment']}</td><td>{$row['commentDate']}</td><td>{$row['reason']}</td></tr>";

            }
            echo "<tr><th colspan=\"2\" >Total Comments</th><th colspan=\"4\" >".sizeof($data)."</th></tr>";
            echo '</tbody></table>';
        } elseif($model->rpttype == 3) {
        $class = 'odd';
        echo '<table class="items table table-striped table-bordered table-condensed"><thead>';
        echo '<tr><th>Dealer Name</th><th style="width: 3em">Office Phone</th><th>Residence Phone</th><th style="width: 5em">Contact Person</th><th style="width: 4em">E-mail</th>
        <th>Address </th><th>Town </th><th>Route </th><th style="width: 20em">Distributor </th><th>Status</th><th>Sales Rep</th><th>Added On</th></tr></thead><tbody>';
        foreach($data as $row)
        {
            $class = ($class == 'odd') ? 'even': 'odd';
            echo "<tr class=\"{$class}\"><td>".CHtml::link($row['dealerName'],Yii::app()->baseUrl.'/index.php/dealers/view?id='.$row['id'],array('target'=>'_blank'))."</td><td>
            {$row['phoneOffice']}</td><td>{$row['phoneResidence']}</td><td>{$row['contactPerson']}</td><td>{$row['email']}</td><td>{$row['address1']} {$row['address2']} {$row['address3']} </td><td>{$row['town']}</td><td>".Towns::getRoute($row['townId'])."</td><td>{$row['distName']}</td><td>".Dealers::itemAlias('ItemStatus', $row['disabled'])."</td><td>{$row['srcode']}</td><td>{$row['addedOn']}</td></tr>";

        }
        echo "<tr><th colspan=\"2\" >Total Dealers</th><th colspan=\"11\" >".sizeof($data)."</th></tr>";
        echo '</tbody></table>';
    }

    } else {
        echo 'no data found';
    }
?>
</div>