<?php
/* @var $this RouteController */
/* @var $model Route */

$this->breadcrumbs=array(
	'Routes'=>array('index'),
	'Manage',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#route-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Routes</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
	'id'=>'route-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
        array(
            'name'=>'area_id',
            'headerHtmlOptions' => array('style' => 'width: 40px'),
            'value'=>'$data->area->name',
            'filter' => CHtml::listData(Area::model()->findAll(array('condition'=>'status=1','order'=>'name')),'id','name'),
        ),
		//'code',
		//'name',
        array(
            'class' => 'editable.EditableColumn',
            'name' => 'name',
            'headerHtmlOptions' => array('style' => 'width: 200px'),
            'editable' => array(
                'url'        => $this->createUrl('route/UpdateRoute'),
                'placement'  => 'right',
                'inputclass' => 'span3',
            )
        ),
        array(
            'name'=>'status',
            'class' => 'editable.EditableColumn',
            //'type'=> 'select',
            'value'=>'Route::itemAlias("ItemStatus",$data->status)',
            'headerHtmlOptions' => array('style' => 'width: 30px'),
            'filter' => Route::itemAlias("ItemStatus"),
            'editable' => array(
                'type'     => 'select',
                'url'      => $this->createUrl('route/UpdateRoute'),
                'source'   => Route::itemAlias("ItemStatus"),
                /*'options'  => array(    //custom display
                    'display' => 'js: function(value, sourceData) {
                          var selected = $.grep(sourceData, function(o){ return value == o.value; }),
                              colors = {1: "green", 2: "blue", 3: "red", 4: "gray"};
                          $(this).text(selected[0].text).css("color", colors[value]);
                      }'
                ),*/
            )
            //'filter' => Route::itemAlias("ItemStatus"),
        ),
        array(
            'name'=>'added_by',
            'value'=>'$data->addedBy->username',
            'headerHtmlOptions' => array('style' => 'width: 30px'),
            'filter' => CHtml::listData(User::model()->findAll(array('condition'=>'status=1','order'=>'username')),'id','username'),
        ),
		/*
		'added_on',
		'updated_by',
		'updated_on',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'template'=>'{view}{update}'
		),
	),
)); ?>
