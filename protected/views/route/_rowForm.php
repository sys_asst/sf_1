<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Randika
 * Date: 7/1/14
 * Time: 4:47 PM
 * To change this template use File | Settings | File Templates.
 */

$row_id = "sladetail-" . $key ?>

<div class='row-fluid' id="<?php echo $row_id ?>">
    <?php
    $route_id = $model->route_id;
    echo $form->hiddenField($model, "[$key]route_id");
    echo $form->updateTypeField($model, $key, "route_id", array('key' => $key));
    //print_r($_POST);

    if(!isset($filter))
    {
        $sql = "SELECT RT.`area_id` FROM `ms_route` AS RT WHERE RT.`id` = '{$route_id}'";
        $records = Tk::sql($sql);
        if(isset($records))
        {
            $filter = $records[0]['area_id'];
        }
    }

    ?>

    <div class="span6">
        <?php
        echo $form->labelEx($model, "[$key]gndiv_id");
        //$leaflevelattrbutes = CHtml::listData(Dsdivisions::model()->findAll('status > 0 AND id NOT IN(SELECT DISTINCT(DR.`gndiv_id`) FROM `d_route` AS DR) '.(strlen($filter) && $filter > 0 ? ' AND district_id IN (SELECT DA.`district_id` FROM `d_area` AS DA WHERE DA.`area_id` = "'.$filter.'" GROUP BY DA.`district_id`) ' : '').'ORDER BY name'), 'id','name');
        $leaflevelattrbutes = CHtml::listData(Dsdivisions::model()->findAll('status > 0 '.(strlen($filter) && $filter > 0 ? ' AND district_id IN (SELECT DA.`district_id` FROM `d_area` AS DA WHERE DA.`area_id` = "'.$filter.'" GROUP BY DA.`district_id`) ' : '').'ORDER BY name'), 'id','name');
        echo $form->dropDownList($model, "[$key]gndiv_id",$leaflevelattrbutes,array('prompt'=>'Select DS Divisions','onchange'=>"js:getdata($(this).val(), {$key})"));
        echo $form->error($model, "[$key]gndiv_id");
        //echo $form->textAreaRow($model,"[$key]towns",array('rows'=>6, 'cols'=>50, 'class'=>'ds_towns'));
        ?><div class="form-actions inline"></div>
    </div>

    <div class="span3">
        <?php
        echo $form->labelEx($model, "[$key]off_day");
        echo $form->dropDownList($model, "[$key]off_day",Route::itemAlias('weekdays'));
        echo $form->error($model, "[$key]off_day");
        ?>
    </div>


    <div class="span2">

        <?php echo $form->deleteRowButton($row_id, $key); ?>
    </div>

</div>