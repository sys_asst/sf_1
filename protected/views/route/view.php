<?php
/* @var $this RouteController */
/* @var $model Route */

$this->breadcrumbs=array(
	'Routes'=>array('index'),
	$model->name,
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
$sql = "SELECT D.`dealerName`, D.`address1`, D.`dealerCode` FROM `dealers` AS D WHERE D.`disabled` = '".Dealers::STATUS_ACTIVE."' AND D.`distributor` IN(SELECT DISTINCT(DT.`distId`)
FROM (`distributors` AS DT INNER JOIN `ms_route` AS R ON DT.`area_id` = R.`area_id` AND R.`id` = '{$model->id}')) ORDER BY D.`dealerCode`";

$records = Route::getDealers($model->id);//Tk::sql($sql);
$dealers = '';
if(sizeof($records))
{
    $dealers = '<table class="items table table-striped table-bordered table-condensed"><thead><tr><th style="text-align: center">Dealer Code</th><th style="text-align: center">Dealer Name</th><th style="text-align: center">Town</th></tr></thead><tbody>';
    foreach($records as $rec)
    {
        $dealers .= '<tr><td>'.$rec['dealerCode'].'</td><td>'.$rec['dealerName'].'</td ><td>'.$rec['town'].'</td ></tr>';
    }
    $dealers .= '</tbody></table>';
} else {
    $dealers .= 'no data found!';
}

//$records = Route::getDealers($model->id);
//Tk::a($records);
?>

<h1>View Route <?php echo $model->name; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
        array(
            'name'=>'area_id',
            'value'=>CHtml::encode($model->area->name)
        ),
		'code',
		'name',
        array(
            'label'=>'GN Divisions',
            'value'=>CHtml::encode($model->getGNDivisions($model->id))
        ),
        array(
            'name'=>'status',
            'value'=>CHtml::encode(Route::itemAlias('ItemStatus', $model->status))
        ),
        array(
            'name'=>'added_by',
            'value'=>CHtml::encode($model->addedBy->username)
        ),
        'added_on',
        array(
            'name'=>'updated_by',
            'value'=>CHtml::encode(sizeof($model->updatedBy) ? $model->updatedBy->username : 'n/a')
        ),
        'updated_on',
        array(
            'label' =>'Dealers',
            'type' => 'raw',
            'value' => $dealers
        )
	),
));


?>
