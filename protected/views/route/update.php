<?php
/* @var $this RouteController */
/* @var $model Route */

$this->breadcrumbs=array(
	'Routes'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>Update Route <?php echo $model->name; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model,'details'=>$details)); ?>