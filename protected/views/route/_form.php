<?php
/* @var $this RouteController */
/* @var $model Route */
/* @var $form CActiveForm */
?>

<div class="form">

<?php //$form=$this->beginWidget('CActiveForm', array(
	//'id'=>'route-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	//'enableAjaxValidation'=>false,
//)); ?>

    <?php $form=$this->beginWidget('DynamicTabularForm', array(
        'defaultRowView'=>'_rowForm',
    )); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
    <div class="row-fluid">
        <div class="span6">
            <?php echo $form->dropDownListRow($model,'area_id',CHtml::listData(Area::model()->findAll(array('condition'=>'status=1','order'=>'name')),'id','name'),array('class'=>'input-block-level','prompt'=>'Select Area')); ?>
            <?php echo $form->textFieldRow($model,'name',array('class'=>'input-block-level', 'size'=>60,'maxlength'=>255)); ?>
        </div>

        <div class="span6">
            <?php echo $form->textFieldRow($model,'code',array('size'=>10,'maxlength'=>10)); ?>
            <?php echo $form->dropDownListRow($model,'status',Route::itemAlias('ItemStatus'), array('')); ?>
        </div>
    </div>


    <fieldset class="label_data" style="display: <?php echo ($model->scenario == 'update')? 'block' : 'block';?>">
        <legend>Route Details</legend>
        <?php
        //echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save');
        $max_leafs = 25;
        echo $form->rowForm($details, null, array(), $max_leafs, 'Route_area_id');
        ?>
    </fieldset>

    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>$model->isNewRecord ? Yii::t('messages', 'Create') : Yii::t('messages', 'Save'),
        )); ?>

        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'label'=>Yii::t('messages', 'Cancel'),
            'type'=>'info',
            'url'=>Yii::app()->createUrl('route/admin')
        )); ?>
    </div>

<?php $this->endWidget(); 

$url = Yii::app()->createUrl('/route/GetTowns');
echo "<script type=\"text/javascript\">
        function getdata(val, key){
            $.ajax({
                type:'POST',
                url:'{$url}',
                data:{parentid:val},
                success: function(response){
                    //$('#content_'+ind).html(response);
                    $('#DRoute_'+key+'_gndiv_id').siblings('.inline').html(response);
                }
            });
        }
    </script>";
?>

</div><!-- form -->