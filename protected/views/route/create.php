<?php
/* @var $this RouteController */
/* @var $model Route */

$this->breadcrumbs=array(
	'Routes'=>array('index'),
	'Create',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>Create Route</h1>

<?php $this->renderPartial('_form', array('model'=>$model,'details'=>$details)); ?>