<?php
/* @var $this RouteController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Routes',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>Routes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
