<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div class="span-19">
    <?php
    $this->widget('bootstrap.widgets.TbAlert', array(
        'id'=>'statusMsg',
        'block'=>false, // display a larger alert block?
        'fade'=>true, // use transitions?
        'closeText'=>'x', // close link text - if set to false, no close link is displayed
        'alerts'=>array(// configurations per alert type
            'success'=>array('block'=>false, 'fade'=>true, 'closeText'=>'&times;'), // success, info, warning, error or danger
            'error'=>array('block'=>false, 'fade'=>true, 'closeText'=>'&times;'),
            'info'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'),
            'warning'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'),
        ),
    ));
    ?>
	<div id="content" style="width:1100px;">

		<?php echo $content; ?>
	</div><!-- content -->
</div>
    <div class="row-fluid">
	<div id="sidebar">
	<?php
		/*$this->beginWidget('zii.widgets.CPortlet', array(
			'title'=>'Operations',
		));
		$this->widget('zii.widgets.CMenu', array(
			'items'=>$this->menu,
			'htmlOptions'=>array('class'=>'operations'),
		));
		$this->endWidget();*/

	?>
    <?php if (null != $this->menu) {
        $this->widget('bootstrap.widgets.TbMenu', array(
            'type'=>'pills',
            'stacked'=>false,
            'items'=>$this->menu,
            'encodeLabel'=>false,
            'htmlOptions'=>array('class'=>'pull-right'),
        ));
    }
    ?>
	</div>
    <!-- sidebar -->
</div>
<?php $this->endContent(); ?>