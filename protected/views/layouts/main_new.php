<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

<?php
//Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/js/bootstrap/css/bootstrap.min.css');
//Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/jquery.dataTables.css');
//Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/bootstrap/js/bootstrap.min.js');
//Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.dataTables.min.js');
?>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <?php echo Yii::app()->bootstrap->init();?>

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-awesome-4.3.0/css/font-awesome.min.css" />
</head>

<body>

<div class="container page-content" id="page">

	<div id="header">
		<!--<div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>-->
        <div id="logo"><?php //echo CHtml::image(Yii::app()->request->baseUrl."/images/logo.png","Anton Sales Force"); ?></div>
	</div><!-- header -->

	<div id="mainMbMenu">
		<?php
//Tk::a(Yii::app()->user->checkAccess('Salesreps.*'));
        //$role = Rights::getAssignedRoles(Yii::app() -> user -> Id);
        //$curuser = Yii::app()->user->getId();
        //$auth = Yii::app()->AuthManager;
/*echo Yii::app()->user->checkAccess('Dealercomments.*') .' < Dealercomments.* '. Yii::app()->user->checkAccess('Dealercommentreasons.*') .' < Dealercommentreasons.* '.
    Yii::app()->user->checkAccess('Dealers.Admin') .' < Dealers.Admin '. Yii::app()->user->checkAccess('Dealercomments.Admin') .' < Dealercomments.Admin '. Yii::app()->user->checkAccess('Dealercommentreasons.Admin') .' < Dealercommentreasons.Admin '.
    Yii::app()->user->checkAccess('DealersEdit.*').' < DealersEdit.* ';*/
        /*echo Yii::app()->user->checkAccess('Distributors.*')  .' < Distributors.* '.  Yii::app()->user->checkAccess('LeafLevelAttributeLabels.*')  .' < LeafLevelAttributeLabels.* '.  Yii::app()->user->checkAccess('Unit.*')  .' < Unit.* '.  Yii::app()->user->checkAccess('UnitCategory.*')  .' < UnitCategory.* '.
            Yii::app()->user->checkAccess('Categories.Admin')  .' < Categories.Admin '.  Yii::app()->user->checkAccess('LeafLevelAttributeLabels.Admin')  .' < LeafLevelAttributeLabels.Admin '.  Yii::app()->user->checkAccess('UnitCategory.Admin')
            .' < UnitCategory.Admin '.  Yii::app()->user->checkAccess('Unit.Admin')  .' < Unit.Admin '.  Yii::app()->user->checkAccess('Products.View')  .' < Products.View '.  Yii::app()->user->checkAccess('Products.Admin').' < Products.Admin';*/
                //echo Tk::getRole(Yii::app() -> user -> Id);
//Tk::a(Yii::app()->user->checkAccess('RouteMap.*'));
//Tk::a(Yii::app()->getAuthManager());
        $this->widget('bootstrap.widgets.TbNavbar', array(
            'type'=>'inverse', // null or 'inverse'
            //'brand'=>'Project name',
            'brand'=>CHtml::image(Yii::app()->getBaseUrl().'/images/logo_small.png'),
            'brandUrl'=>'/',
            'collapse'=>true, // requires bootstrap-responsive.css

            'items'=>array(
                array(
                    'class'=>'bootstrap.widgets.TbMenu',
                    'encodeLabel'=>false,
                    'items'=>array(
                        array('url'=>array(''), 'label'=>Yii::app()->fa->getIcon('ship',"Distributors"),
                            'items'=>array(
                                array('label'=>Yii::app()->fa->getIcon('truck', Yii::app()->getModule('user')->t(' Manage Distributors')), 'url'=>array(''),
                                    'items'=>array(
                                        array('label'=>Yii::app()->fa->getIcon('list', Yii::app()->getModule('user')->t(' List Distributors')), 'url'=>array('/distributors/admin'),'visible'=>Yii::app()->user->checkAccess('Distributors.Admin') || Yii::app()->user->checkAccess('Distributors.Admin')),
                                        array('label'=>Yii::app()->fa->getIcon('plus', Yii::app()->getModule('user')->t(' Create Distributors')), 'url'=>array('/distributors/create'),'visible'=>Yii::app()->user->checkAccess('Distributors.Create') || Yii::app()->user->checkAccess('Distributors.Admin')),
                                    ),
                                    'visible' => Yii::app()->user->checkAccess('Distributors.Admin') || Yii::app()->user->checkAccess('Distributors.Create')),
                            ),
                            'visible'=> Yii::app()->user->checkAccess('Distributors.*') || Yii::app()->user->checkAccess('Distributors.Admin') || Yii::app()->user->checkAccess('Distributors.Create')),
                        array('url'=>array(''), 'label'=>Yii::app()->fa->getIcon('shopping-cart',"Products"),
                            'items'=>array(
                                array('label'=>Yii::app()->fa->getIcon('tags', Yii::app()->getModule('user')->t(' Manage Category')), 'url'=>array(''),
                                    'items'=>array(
                                        array('label'=>Yii::app()->fa->getIcon('list', Yii::app()->getModule('user')->t(' List Category')), 'url'=>array('/categories/admin'),'visible'=>Yii::app()->user->checkAccess('Categories.Admin') || Yii::app()->user->checkAccess('Categories.*')),
                                        array('label'=>Yii::app()->fa->getIcon('plus', Yii::app()->getModule('user')->t(' Create Category')), 'url'=>array('/categories/create'),'visible'=>Yii::app()->getModule('user')->isAdmin()),
                                    ),
                                    'visible'=>Yii::app()->user->checkAccess('Categories.Admin')),
                                array('label'=>Yii::app()->fa->getIcon('tag', Yii::app()->getModule('user')->t(' Manage Labels')), 'url'=>array('/LeafLevelAttributeLabels/admin'),
                                    'items'=>array(
                                        array('label'=>Yii::app()->fa->getIcon('list', Yii::app()->getModule('user')->t(' List Labels')), 'url'=>array('/LeafLevelAttributeLabels/admin'),'visible'=>Yii::app()->user->checkAccess('LeafLevelAttributeLabels.Admin') || Yii::app()->user->checkAccess('LeafLevelAttributeLabels.*')),
                                        array('label'=>Yii::app()->fa->getIcon('plus', Yii::app()->getModule('user')->t(' Create Labels')), 'url'=>array('/LeafLevelAttributeLabels/create'),'visible'=>Yii::app()->getModule('user')->isAdmin()),
                                    ),
                                    'visible'=>Yii::app()->user->checkAccess('LeafLevelAttributeLabels.Admin')),
                                array('label'=>Yii::app()->fa->getIcon('umbrella', Yii::app()->getModule('user')->t(' Manage Products')), 'url'=>array(''),
                                    'items'=>array(
                                        array('label'=>Yii::app()->fa->getIcon('list', Yii::app()->getModule('user')->t(' List Products')), 'url'=>array('/products/admin'),'visible'=>Yii::app()->user->checkAccess('Products.Admin') || Yii::app()->user->checkAccess('Products.*')),
                                        array('label'=>Yii::app()->fa->getIcon('plus', Yii::app()->getModule('user')->t(' Create Products')), 'url'=>array('/products/create'),'visible'=>Yii::app()->getModule('user')->isAdmin()),
                                    ),
                                    'visible'=>Yii::app()->user->checkAccess('Products.Admin')),
                                array('label'=>Yii::app()->fa->getIcon('cogs', Yii::app()->getModule('user')->t(' Manage Unit Category')), 'url'=>array(''),
                                    'items'=>array(
                                        array('label'=>Yii::app()->fa->getIcon('list', Yii::app()->getModule('user')->t(' List Unit Category')), 'url'=>array('/unitCategory/admin'),'visible'=>Yii::app()->user->checkAccess('UnitCategory.Admin') || Yii::app()->user->checkAccess('UnitCategory.*')),
                                        array('label'=>Yii::app()->fa->getIcon('plus', Yii::app()->getModule('user')->t(' Create Unit Category')), 'url'=>array('/unitCategory/create'),'visible'=>Yii::app()->getModule('user')->isAdmin()),
                                    ),
                                    'visible'=>Yii::app()->user->checkAccess('UnitCategory.Admin')),
                                array('label'=>Yii::app()->fa->getIcon('rocket', Yii::app()->getModule('user')->t(' Manage Units')), 'url'=>array('/unit/admin'),
                                    'items'=>array(
                                        array('label'=>Yii::app()->fa->getIcon('list', Yii::app()->getModule('user')->t(' List Unit')), 'url'=>array('/unit/admin'),'visible'=>Yii::app()->user->checkAccess('Unit.Admin') || Yii::app()->user->checkAccess('Unit.*')),
                                        array('label'=>Yii::app()->fa->getIcon('plus', Yii::app()->getModule('user')->t(' Create Unit')), 'url'=>array('/unit/create'),'visible'=>Yii::app()->getModule('user')->isAdmin()),
                                    ),
                                    'visible'=>Yii::app()->user->checkAccess('Unit.Admin')),
                                '---',
                                array('label'=>Yii::app()->fa->getIcon('upload', Yii::app()->getModule('user')->t(' Price Update')), 'url'=>array('/products/priceupdate'),'visible'=>Yii::app()->getModule('user')->isAdmin()),
                            ),
                            'visible'=>Yii::app()->user->checkAccess('Categories.*') || Yii::app()->user->checkAccess('LeafLevelAttributeLabels.*') || Yii::app()->user->checkAccess('Unit.*') || Yii::app()->user->checkAccess('UnitCategory.*') ||
                                Yii::app()->user->checkAccess('Categories.Admin') || Yii::app()->user->checkAccess('LeafLevelAttributeLabels.Admin') || Yii::app()->user->checkAccess('UnitCategory.Admin')
                                || Yii::app()->user->checkAccess('Unit.Admin') || Yii::app()->user->checkAccess('Products.View') || Yii::app()->user->checkAccess('Products.Admin')
                        ),
                        array('url'=>array(''), 'label'=>Yii::app()->fa->getIcon('user-md',"Sales Team"),
                            'items'=>array(
                                //array('label'=>Yii::app()->getModule('user')->t('Add Sales Refs'), 'url'=>array('/salesreps/create')),
                                array('label'=>Yii::app()->fa->getIcon('motorcycle',Yii::app()->getModule('user')->t(' Manage Sales Team')), 'url'=>array('/salesreps/admin'),
                                    'items'=>array(
                                        array('label'=>Yii::app()->fa->getIcon('list', Yii::app()->getModule('user')->t(' List Sales Team')), 'url'=>array('/salesreps/admin'),'visible'=>Yii::app()->user->checkAccess('Salesreps.Admin') || Yii::app()->user->checkAccess('Salesreps.*')),
                                        array('label'=>Yii::app()->fa->getIcon('plus', Yii::app()->getModule('user')->t(' Create Sales Team')), 'url'=>array('/salesreps/create'),'visible'=>Yii::app()->getModule('user')->isAdmin()),
                                    ),
                                    'visible' =>Yii::app()->user->checkAccess('Salesreps.Admin') || Yii::app()->user->checkAccess('Salesreps.*')),
                                array('label'=>Yii::app()->fa->getIcon('sitemap', Yii::app()->getModule('user')->t(' Manage Designations')), 'url'=>array(''),
                                    'items'=>array(
                                        array('label'=>Yii::app()->fa->getIcon('list', Yii::app()->getModule('user')->t(' List Designations')), 'url'=>array('/designations/admin'),'visible'=>Yii::app()->user->checkAccess('Designations.Admin') || Yii::app()->user->checkAccess('Designations.*')),
                                        array('label'=>Yii::app()->fa->getIcon('plus', Yii::app()->getModule('user')->t(' Create Designations')), 'url'=>array('/designations/create'),'visible'=>Yii::app()->getModule('user')->isAdmin()),
                                    ),
                                    'visible'=>Yii::app()->user->checkAccess('Designations.Admin'))
                            ),
                            'visible'=> Yii::app()->user->checkAccess('Salesreps.*') || Yii::app()->user->checkAccess('Designations.*') ||
                                Yii::app()->user->checkAccess('Designations.Admin') || Yii::app()->user->checkAccess('Salesreps.Admin')),//rights/assignment/view
                        array('url'=>array('/orders/admin'), 'label'=>Yii::app()->fa->getIcon('trophy', " Orders"),
                            'items'=>array(
                                array('label'=>Yii::app()->fa->getIcon('inbox', Yii::app()->getModule('user')->t(' View Orders')), 'url'=>array('/orders/admin')),
                            ),
                            'visible'=>Yii::app()->user->checkAccess('Orders.Admin')),
                        array('url'=>array(''), 'label'=>Yii::app()->fa->getIcon('university', " Dealers"),
                            'items'=>array(
                                //array('label'=>Yii::app()->getModule('user')->t('Add Sales Refs'), 'url'=>array('/salesreps/create')),dealercomments/admin
                                array('label'=>Yii::app()->fa->getIcon('flag', Yii::app()->getModule('user')->t(' Manage Dealers')), 'url'=>array(''),
                                    'items'=>array(
                                        array('label'=>Yii::app()->fa->getIcon('list', Yii::app()->getModule('user')->t(' List Dealers')), 'url'=>array('/dealers/admin'),'visible'=>Yii::app()->user->checkAccess('Dealers.Admin') || Yii::app()->user->checkAccess('Dealers.*')),
                                        array('label'=>Yii::app()->fa->getIcon('plus', Yii::app()->getModule('user')->t(' Create Dealers')), 'url'=>array('/dealers/create'),'visible'=>Yii::app()->user->checkAccess('Dealers.Create')),
                                    ),
                                    'visible' => Yii::app()->user->checkAccess('Dealers.Admin') || Yii::app()->user->checkAccess('Dealers.Create')),
                                array('label'=>Yii::app()->fa->getIcon('comments', Yii::app()->getModule('user')->t(' Dealer Comments')), 'url'=>array('/dealercomments/admin'),
                                    'items'=>array(
                                        array('label'=>Yii::app()->fa->getIcon('list', Yii::app()->getModule('user')->t(' List Dealer Comments')), 'url'=>array('/dealercomments/admin'),'visible'=>Yii::app()->user->checkAccess('Dealercomments.Admin') || Yii::app()->user->checkAccess('Dealercomments.*')),
                                        array('label'=>Yii::app()->fa->getIcon('plus', Yii::app()->getModule('user')->t(' Create Dealer Comments')), 'url'=>array('/dealercomments/create'),'visible'=>Yii::app()->user->checkAccess('Dealercomments.Create') || Yii::app()->user->checkAccess('Dealercomments.*')),
                                    ),
                                    'visible' => Yii::app()->user->checkAccess('Dealercomments.Admin') || Yii::app()->user->checkAccess('Dealercomments.Create') || Yii::app()->user->checkAccess('Dealercomments.*')),
                                array('label'=>Yii::app()->fa->getIcon('comments-o', Yii::app()->getModule('user')->t(' Comments Reasons')), 'url'=>array(''),
                                    'items'=>array(
                                        array('label'=>Yii::app()->fa->getIcon('list', Yii::app()->getModule('user')->t(' List Comments Reasons')), 'url'=>array('/dealercommentreasons/admin'),'visible'=>Yii::app()->user->checkAccess('Dealercommentreasons.Admin') || Yii::app()->user->checkAccess('Dealercommentreasons.*')),
                                        array('label'=>Yii::app()->fa->getIcon('plus', Yii::app()->getModule('user')->t(' Create Comments Reasons')), 'url'=>array('/dealercommentreasons/create'),'visible'=>Yii::app()->user->checkAccess('Dealercommentreasons.Create') || Yii::app()->user->checkAccess('Dealercommentreasons.*')),
                                    ),
                                    'visible' => Yii::app()->user->checkAccess('Dealercommentreasons.Admin') || Yii::app()->user->checkAccess('Dealercommentreasons.Create') || Yii::app()->user->checkAccess('Dealercommentreasons.*')),
                                array('label'=>Yii::app()->fa->getIcon('envelope', Yii::app()->getModule('user')->t(' View Dealer Update Request')), 'url'=>array('/dealersEdit/LatestComments?statusnc=0'),
                                    'visible' => Yii::app()->user->checkAccess('DealersEdit.*') || Yii::app()->user->checkAccess('DealersEdit.LatestComments'))
                            ),
                            'visible'=>(Yii::app()->user->checkAccess('Dealers.*') || Yii::app()->user->checkAccess('Dealercomments.*') || Yii::app()->user->checkAccess('Dealercommentreasons.*') ||
                                Yii::app()->user->checkAccess('Dealers.Admin') || Yii::app()->user->checkAccess('Dealercomments.Admin') || Yii::app()->user->checkAccess('Dealercommentreasons.Admin') ||
                                Yii::app()->user->checkAccess('DealersEdit.*'))
                        ),
                        array('url'=>array(''), 'label'=>Yii::app()->fa->getIcon('bar-chart',"Reports"),
                            'items'=>array(
                                //array('label'=>Yii::app()->getModule('user')->t('Create Distributors'), 'url'=>array('/distributors/create')),
                                array('label'=>Yii::app()->fa->getIcon('search', Yii::app()->getModule('user')->t(' Daily Orders')), 'url'=>array('/report/vieworders')),
                                array('label'=>Yii::app()->fa->getIcon('wechat', Yii::app()->getModule('user')->t(' Dealer Comments')), 'url'=>array('/report/vieworders/rpttype/2')),
                                array('label'=>Yii::app()->fa->getIcon('flag', Yii::app()->getModule('user')->t(' Dealers')), 'url'=>array('/report/vieworders/rpttype/3')),
                                array('label'=>Yii::app()->fa->getIcon('motorcycle', Yii::app()->getModule('user')->t(' Itinerary Achievement')), 'url'=>array('/report/vieworders/rpttype/4'),
                                    'visible'=>isset(Yii::app()->user->profile) ? Yii::app()->user->profile->src != 3 : 0),
                            ),
                            'visible'=>Yii::app()->user->checkAccess('Report.*') || Yii::app()->user->checkAccess('Report.ViewOrders')),
                        array('url'=>'', 'label'=>Yii::app()->fa->getIcon('road',Yii::app()->getModule('user')->t("Areas")),//Yii::app()->getModule('user')->profileUrl
                            'items'=>array(
                                array('label'=>Yii::app()->fa->getIcon('road', Yii::app()->getModule('user')->t(' Mapping Routes')), 'url'=>array(''),
                                    'items'=>array(
                                        array('label'=>Yii::app()->fa->getIcon('list', Yii::app()->getModule('user')->t(' List Route Map')), 'url'=>array('/routeMap/admin'),'visible'=>Yii::app()->user->checkAccess('RouteMap.Admin') || Yii::app()->user->checkAccess('RouteMap.*')),
                                        array('label'=>Yii::app()->fa->getIcon('plus', Yii::app()->getModule('user')->t(' Create Route Map')), 'url'=>array('/routeMap/create'),'visible'=>Yii::app()->user->checkAccess('RouteMap.Create') || Yii::app()->user->checkAccess('RouteMap.*')),
                                    ),
                                    'visible' =>Yii::app()->user->checkAccess('RouteMap.Admin') || Yii::app()->user->checkAccess('RouteMap.Create') || Yii::app()->user->checkAccess('RouteMap.*')),
                                array('label'=>Yii::app()->fa->getIcon('chain',Yii::app()->getModule('user')->t(' Manage Regions')), 'url'=>array('/regions/admin'),
                                    'items'=>array(
                                        array('label'=>Yii::app()->fa->getIcon('list', Yii::app()->getModule('user')->t(' List Regions')), 'url'=>array('/regions/admin'),'visible'=>Yii::app()->user->checkAccess('Regions.Admin') || Yii::app()->user->checkAccess('RouteMap.*')),
                                        array('label'=>Yii::app()->fa->getIcon('plus', Yii::app()->getModule('user')->t(' Create Regions')), 'url'=>array('/regions/create'),'visible'=>Yii::app()->user->checkAccess('Regions.Create')),
                                    ),
                                    'visible' => Yii::app()->user->checkAccess('Regions.Admin') || Yii::app()->user->checkAccess('Regions.Create')),
                                array('label'=>Yii::app()->fa->getIcon('gears', Yii::app()->getModule('user')->t(' Manage Areas')), 'url'=>array(''),
                                    'items'=>array(
                                        array('label'=>Yii::app()->fa->getIcon('list', Yii::app()->getModule('user')->t(' List Areas')), 'url'=>array('/area/admin'),'visible'=>Yii::app()->user->checkAccess('Area.Admin') || Yii::app()->user->checkAccess('Area.*')),
                                        array('label'=>Yii::app()->fa->getIcon('plus', Yii::app()->getModule('user')->t(' Create Areas')), 'url'=>array('/area/create'),'visible'=>Yii::app()->user->checkAccess('Area.Create') || Yii::app()->user->checkAccess('Area.*')),
                                    ),
                                    'visible' => Yii::app()->user->checkAccess('Area.Admin') || Yii::app()->user->checkAccess('Area.Create') || Yii::app()->user->checkAccess('Area.*')),
                                array('label'=>Yii::app()->fa->getIcon('arrows', Yii::app()->getModule('user')->t(' Manage Routes')), 'url'=>array(''),
                                    'items'=>array(
                                        array('label'=>Yii::app()->fa->getIcon('list', Yii::app()->getModule('user')->t(' List Routes')), 'url'=>array('/route/admin'),'visible'=>Yii::app()->user->checkAccess('Route.Admin') || Yii::app()->user->checkAccess('Route.*')),
                                        array('label'=>Yii::app()->fa->getIcon('plus', Yii::app()->getModule('user')->t(' Create Routes')), 'url'=>array('/route/create'),'visible'=>Yii::app()->user->checkAccess('Route.Create') || Yii::app()->user->checkAccess('Route.*')),
                                    ),
                                    'visible' => Yii::app()->user->checkAccess('Route.Admin') || Yii::app()->user->checkAccess('Route.Create') || Yii::app()->user->checkAccess('Route.*')),
                                array('label'=>Yii::app()->fa->getIcon('eye', Yii::app()->getModule('user')->t(' View Towns')), 'url'=>array(''),
                                    'items'=>array(
                                        array('label'=>Yii::app()->fa->getIcon('list', Yii::app()->getModule('user')->t(' List Towns')), 'url'=>array('/towns/admin'),'visible'=>Yii::app()->user->checkAccess('Towns.Admin') || Yii::app()->user->checkAccess('Towns.*')),
                                        array('label'=>Yii::app()->fa->getIcon('plus', Yii::app()->getModule('user')->t(' Create Towns')), 'url'=>array('/towns/create'),'visible'=>Yii::app()->user->checkAccess('Towns.Create') || Yii::app()->user->checkAccess('Towns.*')),
                                    ),
                                    'visible' => Yii::app()->user->checkAccess('Towns.Admin') || Yii::app()->user->checkAccess('Towns.Create')),
                                array('label'=>Yii::app()->fa->getIcon('flag-checkered', Yii::app()->getModule('user')->t(' View Ds Divisions')), 'url'=>array('/dsdivisions/admin'),
                                    'items'=>array(
                                        array('label'=>Yii::app()->fa->getIcon('list', Yii::app()->getModule('user')->t(' List Ds Divisions')), 'url'=>array('/dsdivisions/admin'),'visible'=>Yii::app()->user->checkAccess('DSdivisions.Admin') || Yii::app()->user->checkAccess('DSdivisions.*')),
                                        array('label'=>Yii::app()->fa->getIcon('plus', Yii::app()->getModule('user')->t(' Create Ds Divisions')), 'url'=>array('/dsdivisions/create'),'visible'=>Yii::app()->user->checkAccess('DSdivisions.Create') || Yii::app()->user->checkAccess('DSdivisions.*')),
                                    ),
                                    'visible' => Yii::app()->user->checkAccess('DSdivisions.Admin') || Yii::app()->user->checkAccess('DSdivisions.Create') || Yii::app()->user->checkAccess('DSdivisions.*')),
                                array('label'=>Yii::app()->fa->getIcon('tablet', Yii::app()->getModule('user')->t(' View Districts')), 'url'=>array('/district/admin'),'visible'=>Yii::app()->getModule('user')->isAdmin()),
                                array('label'=>Yii::app()->fa->getIcon('star', Yii::app()->getModule('user')->t(' View Provinces')), 'url'=>array('/provinces/admin'),'visible'=>Yii::app()->getModule('user')->isAdmin()),
                                array('label'=>Yii::app()->fa->getIcon('globe', Yii::app()->getModule('user')->t(' View Country')), 'url'=>array('/country/admin'),'visible'=>Yii::app()->getModule('user')->isAdmin()),
                            ),
                            'visible'=>Yii::app()->user->checkAccess('RouteMap.Admin') || Yii::app()->user->checkAccess('Regions.Admin') || Yii::app()->user->checkAccess('Area.Admin')
                                || Yii::app()->user->checkAccess('Towns.Admin') || Yii::app()->user->checkAccess('DSdivisions.Admin') || Yii::app()->user->checkAccess('RouteMap.*') ||
                                Yii::app()->user->checkAccess('Regions.*') || Yii::app()->user->checkAccess('Area.*')
                                || Yii::app()->user->checkAccess('Towns.*') || Yii::app()->user->checkAccess('DSdivisions.*')),
                        array('url'=>array('/rights/authItem'), 'label'=>Yii::app()->fa->getIcon('user-secret',"Rights"), 'visible'=>Yii::app()->getModule('user')->isAdmin()),
                        //array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
                        //array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),v
                        array('url'=>Yii::app()->getModule('user')->loginUrl, 'label'=>Yii::app()->fa->getIcon('lock',Yii::app()->getModule('user')->t("Login")), 'visible'=>Yii::app()->user->isGuest),
                        //array('url'=>Yii::app()->getModule('user')->registrationUrl, 'label'=>Yii::app()->getModule('user')->t("Register"), 'visible'=>Yii::app()->user->isGuest),
                        array('url'=>Yii::app()->getModule('user')->profileUrl, 'label'=>Yii::app()->fa->getIcon('user',Yii::app()->getModule('user')->t("User")),
                            'items'=>array(
                                array('label'=>Yii::app()->fa->getIcon('user-plus',Yii::app()->getModule('user')->t(' Create Users')), 'url'=>array('/user/admin/create')),
                                array('label'=>Yii::app()->fa->getIcon('list',Yii::app()->getModule('user')->t(' List Users')), 'url'=>array('/user')),
                                array('label'=>Yii::app()->fa->getIcon('users', Yii::app()->getModule('user')->t(' Manage Users')), 'url'=>array('/user/admin')),
                            ),
                            'visible'=>Yii::app()->getModule('user')->isAdmin()),
                        //array('url'=>Yii::app()->getModule('user')->logoutUrl, 'label'=>Yii::app()->getModule('user')->t("Logout").' ('.Yii::app()->user->name.')', 'visible'=>!Yii::app()->user->isGuest),
                    //),
                        /*array('label'=>'Dropdown', 'url'=>'#', 'items'=>array(
                            array('label'=>'Action', 'url'=>'#'),
                            array('label'=>'Another action', 'url'=>'#'),
                            array('label'=>'Something else here', 'url'=>'#'),
                            '---',
                            array('label'=>'NAV HEADER'),
                            array('label'=>'Separated link', 'url'=>'#'),
                            array('label'=>'One more separated link', 'url'=>'#'),
                        )),*/
                    ),
                ),
                //'<form class="navbar-search pull-left" action=""><input type="text" class="search-query span2" placeholder="Search"></form>',
                array(
                    'class'=>'bootstrap.widgets.TbMenu',
                    'htmlOptions'=>array('class'=>'pull-right'),
                    'encodeLabel'=>false,
                    'items'=>array(
                        //array('label'=>'Link', 'url'=>'#'),
                        array('label'=>Yii::app()->fa->getIcon('wrench',' System'), 'url'=>'#', 'items'=>array(
                            array('url'=>Yii::app()->getModule('user')->logoutUrl, 'label'=>Yii::app()->fa->getIcon('power-off', Yii::app()->getModule('user')->t(" Logout")).
                                ' ('.Yii::app()->user->name.' - '.(isset(Yii::app()->user->profile) ? Yii::app()->user->profile->firstname : '') .')', 'visible'=>!Yii::app()->user->isGuest),
                            /*array('label'=>'Action', 'url'=>'#'),
                            array('label'=>'Another action', 'url'=>'#'),
                            array('label'=>'Something else here', 'url'=>'#'),
                            '---',
                            array('label'=>'Separated link', 'url'=>'#'),*/
                        )),
                    ),
                ),
            ),
        ));
        ?>
	</div><!-- mainmenu -->
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

    <?php
    //$flashMessages = Yii::app()->user->getFlashes();

    /*if ($flashMessages) {
        /*echo '<div class="flashes" style="display: inline">';
        foreach($flashMessages as $key => $message) {
            echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
        }
        echo '</div>';*/




    //}*/
    ?>
    <?php
    Yii::app()->clientScript->registerScript(
        'myHideEffect',
        '$("#statusMsg .alert").animate({opacity: 1.0}, 30000).fadeOut("slow");',
        CClientScript::POS_READY
    );
    ?>

	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer">
		<?php echo Yii::app()->params->copyRight;?>
		<?php //echo Yii::powered(); ?>
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>
