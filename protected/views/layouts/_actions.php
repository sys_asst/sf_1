<?php
/* @var $this CController */
/* @var $model CActiveRecord */


$module = is_object(Yii::app()->controller->module) ? Yii::app()->controller->module : '';
$controller = ucfirst(Yii::app()->controller->id);
$action = $this->getAction()->getId();

$view = array(
    'label'=>Fa::getIcon('eye', Yii::t('messages', 'View')),
    'url'=>array('view', 'id'=>!empty($model->id) ? $model->id : 0),
    'active'=>true,
    'visible' => Yii::app()->user->checkAccess("{$controller}.View"),
);

$create = array(
	'label'=>Fa::getIcon('plus', Yii::t('messages', 'Create')),
    'url'=>isset($_REQUEST['ptid']) ? array('create','ptid'=>$_REQUEST['ptid']) :  array('create'),//.(isset($_REQUEST['indi']) ? '?indi=indi/'.$_REQUEST['indi'] : "")
    'active'=>true,
    'visible' => ($controller == 'Records' || $controller == 'SysMailNotifications') ? false : Yii::app()->user->checkAccess("{$controller}.Create"),
);

$update = array(
    'label'=>Fa::getIcon('pencil', Yii::t('messages', 'Update')),
    'url'=>array('update', 'id'=>!empty($model->id) ? $model->id : 0),
    'active'=>true,
    'visible' => Yii::app()->user->checkAccess("{$controller}.Update"),
);

$delete = array(
    'label'=>Fa::getIcon('trash-o', Yii::t('messages', 'Delete')),
    'url'=>array('delete'),
    'linkOptions'=>array(
        'submit'=>array('delete', 'id'=>!empty($model->id) ? $model->id : 0),
        'confirm' => Yii::t('messages', 'Are you sure you want to delete this item?'),
    ),
    'active'=>true,
    'visible' => Yii::app()->user->checkAccess("{$controller}.Delete"),
);

$admin = array(
    'label'=>Fa::getIcon('table', Yii::t('messages', 'Manage')),
    'url'=>isset($_REQUEST['ptid']) ? array('admin','ptid'=>$_REQUEST['ptid']) :  array('admin'),
    'active'=>true,
    'visible' => Yii::app()->user->checkAccess("{$controller}.Admin"),
);

$this->menu = array();
switch ($this->action->id) {
    case 'view':
        $this->menu = array($create, $update, $delete, $admin);
        break;
    case 'vieworder':
        $this->menu = array($create, $update, $delete, $admin);
        break;
    case 'create':
        $this->menu = array($admin);
        break;
    case 'update':
        $this->menu = array($create, $view, $admin);
        break;
    case 'admin':
        $this->menu = array($create);
        break;
    case 'LatestComments':
        $this->menu = array($create);
        break;
}