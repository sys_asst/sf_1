<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="container" id="page">

	<div id="header">
		<!--<div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>-->
        <div id="logo"><?php echo CHtml::image(Yii::app()->request->baseUrl."/images/logo.png","Anton Sales Force"); ?></div>
	</div><!-- header -->

	<div id="mainMbMenu">
		<?php $this->widget('application.extensions.mbmenu.MbMenu',array(
			'items'=>array(
				//array('label'=>'Home', 'url'=>array('/site/index')),
				//array('label'=>'About', 'url'=>array('/site/page', 'view'=>'about')),
				//array('label'=>'Contact', 'url'=>array('/site/contact')),
                array('url'=>array('/distributors/admin'), 'label'=>"Distributors",
                    'items'=>array(
                        //array('label'=>Yii::app()->getModule('user')->t('Create Distributors'), 'url'=>array('/distributors/create')),
                        array('label'=>Yii::app()->getModule('user')->t('Manage Distributors'), 'url'=>array('/distributors/admin')),
                    ),
                    'visible'=>Yii::app()->getModule('user')->isAdmin()),
                array('url'=>array('/products/admin'), 'label'=>"Products",
                    'items'=>array(
                        //array('label'=>Yii::app()->getModule('user')->t('Add Products'), 'url'=>array('/products/create')),
                        array('label'=>Yii::app()->getModule('user')->t('Manage Category'), 'url'=>array('/categories/admin')),
                        //array('label'=>Yii::app()->getModule('user')->t('Assign Labels to Web Category'), 'url'=>array('/leavesLabels/admin')),
                        array('label'=>Yii::app()->getModule('user')->t('Manage Labels'), 'url'=>array('/LeafLevelAttributeLabels/admin')),
                        array('label'=>Yii::app()->getModule('user')->t('Manage Products'), 'url'=>array('/products/admin')),
                        array('label'=>Yii::app()->getModule('user')->t('Manage Unit Category'), 'url'=>array('/unitCategory/admin')),
                        array('label'=>Yii::app()->getModule('user')->t('Manage Units'), 'url'=>array('/unit/admin')),
                    ),
                    'visible'=>Yii::app()->getModule('user')->isAdmin()),
                array('url'=>array('/salesreps/admin'), 'label'=>"Sales Team",
                    'items'=>array(
                        //array('label'=>Yii::app()->getModule('user')->t('Add Sales Refs'), 'url'=>array('/salesreps/create')),
                        array('label'=>Yii::app()->getModule('user')->t('Manage Sales Team'), 'url'=>array('/salesreps/admin')),
                    ),
                    'visible'=>Yii::app()->getModule('user')->isAdmin()),//rights/assignment/view
                array('url'=>array('/orders/admin'), 'label'=>"Orders",
                    'items'=>array(
                        //array('label'=>Yii::app()->getModule('user')->t('Add Sales Refs'), 'url'=>array('/salesreps/create')),
                        array('label'=>Yii::app()->getModule('user')->t('View Orders'), 'url'=>array('/orders/admin')),
                    ),
                    'visible'=>!Yii::app()->user->isGuest),
                array('url'=>array('/dealers/admin'), 'label'=>"Dealers",
                    'items'=>array(
                        //array('label'=>Yii::app()->getModule('user')->t('Add Sales Refs'), 'url'=>array('/salesreps/create')),dealercomments/admin
                        array('label'=>Yii::app()->getModule('user')->t('Manage Dealers'), 'url'=>array('/dealers/admin')),
                        array('label'=>Yii::app()->getModule('user')->t('Dealer Comments'), 'url'=>array('/dealercomments/admin')),
                        array('label'=>Yii::app()->getModule('user')->t('Comments Reasons'), 'url'=>array('/dealercommentreasons/admin')),
                        array('label'=>Yii::app()->getModule('user')->t('View Dealer Update Request'), 'url'=>array('/dealersEdit/admin'))
                    ),
                    'visible'=>!Yii::app()->user->isGuest),
                array('url'=>array('/report/vieworders'), 'label'=>"Reports",
                    'items'=>array(
                        //array('label'=>Yii::app()->getModule('user')->t('Create Distributors'), 'url'=>array('/distributors/create')),
                        array('label'=>Yii::app()->getModule('user')->t('Daily Orders'), 'url'=>array('/report/vieworders')),
                        array('label'=>Yii::app()->getModule('user')->t('Dealer Comments'), 'url'=>array('/report/vieworders/rpttype/2')),
                        array('label'=>Yii::app()->getModule('user')->t('Dealers'), 'url'=>array('/report/vieworders/rpttype/3')),
                    ),
                    'visible'=>!Yii::app()->user->isGuest),
                array('url'=>Yii::app()->getModule('user')->profileUrl, 'label'=>Yii::app()->getModule('user')->t("Areas"),
                    'items'=>array(
                        array('label'=>Yii::app()->getModule('user')->t('Mapping Routes'), 'url'=>array('/routeMap/admin')),
                        array('label'=>Yii::app()->getModule('user')->t('Manage Regions'), 'url'=>array('/regions/admin')),
                        array('label'=>Yii::app()->getModule('user')->t('Manage Areas'), 'url'=>array('/area/admin')),
                        array('label'=>Yii::app()->getModule('user')->t('Manage Routes'), 'url'=>array('/route/admin')),
                        array('label'=>Yii::app()->getModule('user')->t('View Towns'), 'url'=>array('/towns/admin')),
                        array('label'=>Yii::app()->getModule('user')->t('View Ds Divisions'), 'url'=>array('/dsdivisions/admin')),
                        array('label'=>Yii::app()->getModule('user')->t('View Districts'), 'url'=>array('/district/admin')),
                        array('label'=>Yii::app()->getModule('user')->t('View Provinces'), 'url'=>array('/provinces/admin')),
                        array('label'=>Yii::app()->getModule('user')->t('View Country'), 'url'=>array('/country/admin')),
                    ),
                    'visible'=>Yii::app()->getModule('user')->isAdmin()),
                array('url'=>array('/rights/authItem'), 'label'=>"Rights", 'visible'=>Yii::app()->getModule('user')->isAdmin()),
				//array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
				//array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),v
                array('url'=>Yii::app()->getModule('user')->loginUrl, 'label'=>Yii::app()->getModule('user')->t("Login"), 'visible'=>Yii::app()->user->isGuest),
                //array('url'=>Yii::app()->getModule('user')->registrationUrl, 'label'=>Yii::app()->getModule('user')->t("Register"), 'visible'=>Yii::app()->user->isGuest),
                array('url'=>Yii::app()->getModule('user')->profileUrl, 'label'=>Yii::app()->getModule('user')->t("Profile"),
                    'items'=>array(
                        array('label'=>Yii::app()->getModule('user')->t('Create Users'), 'url'=>array('/user/admin/create')),
                        array('label'=>Yii::app()->getModule('user')->t('List Users'), 'url'=>array('/user')),
                        array('label'=>Yii::app()->getModule('user')->t('Manage Users'), 'url'=>array('/user/admin')),
                    ),
                    'visible'=>Yii::app()->getModule('user')->isAdmin()),
                array('url'=>Yii::app()->getModule('user')->logoutUrl, 'label'=>Yii::app()->getModule('user')->t("Logout").' ('.Yii::app()->user->name.')', 'visible'=>!Yii::app()->user->isGuest),
			),
		)); ?>
	</div><!-- mainmenu -->
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

    <?php
    $flashMessages = Yii::app()->user->getFlashes();
    if ($flashMessages) {
        echo '<div class="flashes" style="display: inline">';
        foreach($flashMessages as $key => $message) {
            echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
        }
        echo '</div>';
    }
    ?>
    <?php
    Yii::app()->clientScript->registerScript(
        'myHideEffect',
        '$(".flashes").animate({opacity: 1.0}, 3000).fadeOut("slow");',
        CClientScript::POS_READY
    );
    ?>

	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> by Saks Globals.<br/>
		All Rights Reserved.<br/>
		<?php //echo Yii::powered(); ?>
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>
