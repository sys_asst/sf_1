<?php
/* @var $this UnitController */
/* @var $model Unit */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
        'id'=>'unit-form',
        'enableAjaxValidation'=>false,
        'type'=>'horizontal',
    ));
    ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

    <div class="row-fluid">
        <div class="span6">
            <?php echo $form->dropDownListRow($model, 'cat_id', $model->getUnitCategoryOptions(), array('multiple'=>false, 'class'=>'input-block-level','prompt'=>'Select Category')); ?>
            <?php echo $form->textFieldRow($model,'name',array('class'=>'input-block-level','maxlength'=>4,'size'=>4)); ?>
            <?php echo $form->dropDownListRow($model,'status',Unit::itemAlias('ItemStatus'),array('class'=>'input-block-level')); ?>
            <?php echo $form->hiddenField($model,'addedby',array('value'=>Yii::app()->user->getId())); ?>
        </div>
        <div class="span6">
            <?php echo $form->textFieldRow($model,'abbreviation',array('class'=>'input-block-level','size'=>10,'maxlength'=>10)); ?>
            <?php echo $form->textFieldRow($model,'code',array('class'=>'input-block-level','size'=>10,'maxlength'=>10)); ?>
        </div>
    </div>

    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>$model->isNewRecord ? Yii::t('messages', 'Create') : Yii::t('messages', 'Save'),
        )); ?>

        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'label'=>Yii::t('messages', 'Cancel'),
            'type'=>'info',
            'url'=>Yii::app()->createUrl('unit/admin')
        )); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->