<?php
/* @var $this UnitController */
/* @var $model Unit */

$this->breadcrumbs=array(
	'Units'=>array('index'),
	$model->name,
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>View Unit <?php echo $model->name; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'cat_id',
		'code',
		'name',
        array(
            'name'=>'status',
            'value'=>CHtml::encode(Unit::itemAlias('ItemStatus', $model->status))
        ),
		'addedon',
        array(
            'name'=>'addedby',
            'value'=>CHtml::encode(isset($model->addeduser->username) ? $model->addeduser->username : 'n/a')
        ),
		'updatedon',
	),
)); ?>
