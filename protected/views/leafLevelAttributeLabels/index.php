<?php
/* @var $this LeafLevelAttributeLabelsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Leaf Level Attribute Labels',
);

$this->menu=array(
	array('label'=>'Create Leaf Level Labels', 'url'=>array('create')),
	array('label'=>'Manage Leaf Level Labels', 'url'=>array('admin')),
);
?>

<h1>Leaf Level Attribute Labels</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
