<?php
/* @var $this LeafLevelAttributeLabelsController */
/* @var $model LeafLevelAttributeLabels */

$this->breadcrumbs=array(
	'Leaf Level Attribute Labels'=>array('index'),
	$model->label,
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>View Leaf Level Labels <?php echo $model->label; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'label',
        array(
            'name'=>'status',
            'value'=>CHtml::encode(LeafLevelAttributeLabels::itemAlias('ItemStatus', $model->status))
        ),
	),
)); ?>
