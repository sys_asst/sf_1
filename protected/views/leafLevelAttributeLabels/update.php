<?php
/* @var $this LeafLevelAttributeLabelsController */
/* @var $model LeafLevelAttributeLabels */

$this->breadcrumbs=array(
	'Leaf Level Attribute Labels'=>array('index'),
	$model->label=>array('view','id'=>$model->id),
	'Update',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>Update Leaf Level Labels <?php echo $model->label; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>