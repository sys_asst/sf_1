<?php
/* @var $this LeafLevelAttributeLabelsController */
/* @var $model LeafLevelAttributeLabels */

$this->breadcrumbs=array(
	'Leaf Level Attribute Labels'=>array('index'),
	'Create',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));
?>

<h1>Create Leaf Level Labels</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>