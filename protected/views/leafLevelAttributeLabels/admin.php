<?php
/* @var $this LeafLevelAttributeLabelsController */
/* @var $model LeafLevelAttributeLabels */

$this->breadcrumbs=array(
	'Leaf Level Attribute Labels'=>array('index'),
	'Manage',
);

$this->renderPartial('../layouts/_actions', array('model'=>$model));

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#leaf-level-attribute-labels-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Labels</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
	'id'=>'leaf-level-attribute-labels-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
		'label',
        array(
            'name'=>'unit_cat_id',
            'value'=>'$data->unitCat->name',
            'filter' => CHtml::listData(UnitCategory::model()->findAll('status = 1'), 'id','name'),
        ),
        /*array(
            'name'=>'added_by',
            'value'=>'$data->added_by',
            'filter' => CHtml::listData(User::model()->findAll(), 'id','username'),
        ),*/
        array(
            'name'=>'status',
            'value'=>'Categories::itemAlias("ItemStatus",$data->status)',
            'filter' => Categories::itemAlias("ItemStatus"),
        ),
        'addedon',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'template'=>'{view}{update}'
		),
	),
)); ?>
