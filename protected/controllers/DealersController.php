<?php

class DealersController extends RController
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
//    public $layout='//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'rights', // perform access control for CRUD operations
//            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'NewDealer'=>array(
                'class'=>'CWebServiceAction',
            ),
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
//    public function accessRules()
//    {
//        return array(
//            array('allow',  // allow all users to perform 'index' and 'view' actions
//                'actions'=>array('index','view','list','ComentsReason','NewDealer','NewDealerComment'),
//                'users'=>array('*'),
//            ),
//            array('allow', // allow authenticated user to perform 'create' and 'update' actions
//                'actions'=>array('create','update'),
//                'users'=>array('@'),
//            ),
//            array('allow', // allow admin user to perform 'admin' and 'delete' actions
//                'actions'=>array('admin','delete'),
//                'users'=>array('admin'),
//            ),
//            array('deny',  // deny all users
//                'users'=>array('*'),
//            ),
//        );
//    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view',array(
            'model'=>$this->loadModel($id),
        ));
    }

    /**
     * Return Delaers list according to the given srCode and timestamp
     * @return Jason string
     */
    public function actionList()
    {
        $response = $data = array();

        $timestamp = isset($_REQUEST['timestamp']) ? $_REQUEST['timestamp'] : 0;
        $srEmpNo = isset($_REQUEST['srCode']) ? $_REQUEST['srCode'] : 0;

        if(isset($_REQUEST['srCode']))
        {
            $srCode = ' AND s.srEmpNo=:srEmpNo';
        } else {
            $srCode = ' AND 0=:srEmpNo';
        }
        if(isset($_REQUEST['timestamp']))
        {
            $timestampstr = ' AND UNIX_TIMESTAMP(d.lastUpdatedOn) > :timestamp';
        } else {
            $timestampstr = ' AND 0=:timestamp';
        }

        $data = Yii::app()->db->createCommand()
            ->select('d.id,d.dealerCode,d.dealerName,d.phoneOffice,d.phoneResidence,d.creditDays,d.creditAmount,d.address1,d.email,d.contactPerson,d.address2,d.address3,d.townId,d.latitude,d.longitude,d.distributor,d.lastUpdatedOn')
            ->from('salesreps s')
            ->join('dealers d', 'd.distributor=s.distributor')
            ->where(' s.status=1 AND d.disabled = 0 '.$timestampstr.$srCode, array(':srEmpNo'=>$srEmpNo,':timestamp'=>$timestamp))//'13491'
            ->queryAll();

        $response['status'] = true;
        $response['data']['dealers'] = $data;
        //$response['data']['message'] = "Successfully run the request";
        $json = new Dealers();
        return  $json->jsonEncode($response);
        Yii::app()->end();
    }

    /**
     * Return DelaersComentReason list according to the given srCode and timestamp
     * @return Jason string
     */
    public function actionComentsReason()
    {
        $response = $data = array();
        $request = Yii::app()->request;
        //print_r($_GET);exit;
        //echo $_REQUEST['timestamp'];exit;

        if(isset($_REQUEST['timestamp']) && strlen($_REQUEST['timestamp']))
        {
            $timestamp = ' AND UNIX_TIMESTAMP(lastUpdatedOn) > '.$_REQUEST['timestamp'];
        } else {
            $timestamp = ' AND 0=0';
        }

        $data = Yii::app()->db->createCommand()
            ->select('*')
            ->from('dealercommentreasons')
            //->join('dealers d', 'd.distributor=s.distributor')
            ->where(' 0=0 '.$timestamp)//'13491'
            ->queryAll();

        $response['status'] = true;
        $response['data']['dealerCommentReasons'] = $data;
        //$response['data']['message'] = "Successfully run the request";
        $json = new Dealers();
        return  $json->jsonEncode($response);
        Yii::app()->end();
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
     public function actionCreate()
    {
        $model=new Dealers;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Dealers']))
        {
            $model->attributes=$_POST['Dealers'];
            if($model->save())
                Yii::app()->user->setFlash('success','Your have been successfully added a record!');
                $this->redirect(array('view','id'=>$model->id));
        }

        $this->render('create',array(
            'model'=>$model,
        ));
    }

    public function actionNewDealer()
    {
        $response = $data = $returnarray = array();
        $jsonstring = isset($_REQUEST['dealer']) ? $_REQUEST['dealer'] : '';
        $srcode = isset($_REQUEST['srCode']) ? $_REQUEST['srCode'] : '';

        //$arrData = explode('&',$GLOBALS['HTTP_RAW_POST_DATA']);
        //Yii::app()->request->getQuery();
        //print_r($_REQUEST);exit;
        //print_r(Yii::app()->request->getPost());
        //exit;
        /*if(sizeof($arrData))
        {
            strlen($arrData[0]) ? list($a,$srcode) = explode('=', $arrData[0]) : $srcode = '';
            strlen($arrData[1]) ? list($a,$jsonstring) = explode('=', $arrData[1]) : $jsonstring = '';
        }*/


        if(strlen($srcode) && strlen($jsonstring))
        {
            $model=new Dealers;

            $jsonobject = json_decode(urldecode($jsonstring), true);

            //print_r($jsonobject);exit;

            $data['dealerName'] = $jsonobject['dealerName'];
            $data['phoneOffice'] = $jsonobject['phoneOffice'];
            $data['phoneResidence'] = $jsonobject['phoneResidence'];
            $data['email'] = $jsonobject['email'];
            $data['contactPerson'] = $jsonobject['contactPersonName'];
            $data['address1'] = $jsonobject['address1'];
            $data['address2'] = $jsonobject['address2'];
            $data['address3'] = $jsonobject['address3'];
            $data['townId'] = $jsonobject['townId'];
            $location_array = $jsonobject['location'];
            unset($jsonobject['location']);
            $data['latitude'] = $location_array['latitude'];
            $data['longitude'] = $location_array['longitude'];
            $data['lastUpdatedOn'] = date('Y-m-d H:i:s');
            $data['addedOn'] = date('Y-m-d H:i:s');
            $data['srcode'] = $srcode;

            /**
             * Get distributor against given srCode
             */
            $dealers = CHtml::listData(Salesreps::model()->findAll('status = 1 AND srEmpNo=:srcode',array(':srcode'=>$srcode)),'status','distributor');
            if(sizeof($dealers))
            {
                $data['distributor'] = $dealers[1];
            } else {
                $data['distributor'] = 0;
            }

            /**
             * Get new dealerCode
             */
            $dealercodePrefix = strtoupper(Yii::app()->params['dealsercodeprefix']);// As app parameter

            $dealercode = Yii::app()->db->createCommand()
                ->select("CONCAT('{$dealercodePrefix}',(MAX(id)+1)) AS `newCode`")
                ->from('dealers')
                ->queryAll();
            //print_r($dealercode);exit;

            $data['dealerCode'] = $dealercode[0]['newCode'];

            $model->attributes=$data;

            if($model->save())
            {
                $status = true;
                $message = 'Successfully added dealer';
                $returnarray = array('dealerCode' => $data['dealerCode'], 'id' => $model->id);
            } else {
                $status = false;
                $message = 'Fail to add dealer';
                $returnarray = array('dealerCode' => '', 'id' => '');
            }
            //print_r($model);

        } else {
            $status = false;
            $message = 'srcode and dealer details are blank';
        }

        $response['status'] = $status;
        $response['data']['dealer'] = $returnarray;
        $response['data']['message'] = $message;

        $json = new Dealers();
        return  $json->jsonEncode($response);
        Yii::app()->end();
    }

    public function actionNewDealerComment()
    {
        $response = $data = $returnarray = array();
        $jsonstring = isset($_REQUEST['comment']) ? $_REQUEST['comment'] : '';
        $srcode = isset($_REQUEST['srCode']) ? $_REQUEST['srCode'] : '';
        //print_r($GLOBALS);exit;

        //$arrData = explode('&',$GLOBALS['HTTP_RAW_POST_DATA']);


        /*if(sizeof($arrData))
        {
            strlen($arrData[0]) ? list($a,$srcode) = explode('=', $arrData[0]) : $srcode = '';
            strlen($arrData[1]) ? list($a,$jsonstring) = explode('=', $arrData[1]) : $jsonstring = '';
        }*/

        if(strlen($srcode) && strlen($jsonstring))
        {
            $model=new Dealercomments;

            //$comment_array = json_decode($jsonstring, true);
            $comment_array = json_decode(urldecode($jsonstring), true);

            $data['srCode'] = $srcode;
            $location_array = $comment_array['location'];
            unset($comment_array['location']);
            $data['latitude'] = $location_array['latitude'];
            $data['longitude'] = $location_array['longitude'];
            $data['dealerId'] = $comment_array['dealerId'];
            $data['reasonId'] = $comment_array['reasonId'];
            $data['comment'] = $comment_array['comment'];
            $data['commentDate'] = $comment_array['commentDate'];//date('Y-m-d H:i:s');

            $model->attributes=$data;
            $model->distributor_id = Dealers::getDealerDistributor($comment_array['dealerId']);

            if($model->save())
            {
                $status = true;
                $message = 'Successfully added data';
            } else {
                $status = false;
                $message = 'Fail to add data';
            }
            //print_r($model);exit;
        } else {
            $status = false;
            $message = 'srCode and dealer details are blank';
        }

        $response['status'] = $status;
        $response['data']['dealerComment']['id'] = isset($model->id) ? $model->id : -1;
        $response['data']['message'] = $message;

        $json = new Dealers();
        return  $json->jsonEncode($response);
        Yii::app()->end();
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Dealers']))
        {
            $model->attributes=$_POST['Dealers'];
            if($model->save())
                Yii::app()->user->setFlash('success','Your have been successfully updated the data!');
                $this->redirect(array('view','id'=>$model->id));
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    /*public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }*/

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider=new CActiveDataProvider('Dealers');
        $this->render('index',array(
            'dataProvider'=>$dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model=new Dealers('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Dealers']))
            $model->attributes=$_GET['Dealers'];

        $this->render('admin',array(
            'model'=>$model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Dealers the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=Dealers::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Dealers $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='dealers-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionCheckedIn()
    {
        $response = $data = $returnarray = array();
        $jsonstring = isset($_REQUEST['checkedIn']) ? $_REQUEST['checkedIn'] : '';
        $srcode = isset($_REQUEST['srCode']) ? $_REQUEST['srCode'] : '';
        //print_r($_REQUEST);exit;

        //$arrData = explode('&',$GLOBALS['HTTP_RAW_POST_DATA']);

        /*if(sizeof($arrData))
        {
            strlen($arrData[0]) ? list($a,$srcode) = explode('=', $arrData[0]) : $srcode = '';
            strlen($arrData[1]) ? list($a,$jsonstring) = explode('=', $arrData[1]) : $jsonstring = '';
        }*/

        if(strlen($srcode) && strlen($jsonstring))
        {
            $model=new Checkedin;

            //$comment_array = json_decode($jsonstring, true);
            $comment_array = json_decode(urldecode($jsonstring), true);

            $data['srCode'] = $srcode;
            $data['checkedInTime']=$comment_array['checkedInTime'];
            $location_array = $comment_array['location'];
            unset($comment_array['location']);
            $data['latitude'] = $location_array['latitude'];
            $data['longitude'] = $location_array['longitude'];
            $data['dealerId'] = $comment_array['dealerId'];
            $data['addedon'] = date('Y-m-d H:i:s');

            $model->attributes=$data;

            if($model->save())
            {
                $status = true;
                $message = 'Successfully added data';
            } else {
                $status = false;
                $message = 'Fail to add data';
            }
        } else {
            $status = false;
            $message = 'srCode and checkedin details are blank';
        }

        $response['status'] = $status;
        $response['data']['nodeId'] = isset($model->id) ? $model->id : -1;
        $response['data']['message'] = $message;

        $json = new Dealers();
        return  $json->jsonEncode($response);
        Yii::app()->end();
    }

}
