<?php

class ProductsController extends RController {
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
//    public $layout='//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'rights', // perform access control for CRUD operations
//            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
//    public function accessRules()
//    {
//        return array(
//            array('allow',  // allow all users to perform 'index' and 'view' actions
//                'actions'=>array('index','view','list'),
//                'users'=>array('*'),
//            ),
//            array('allow', // allow authenticated user to perform 'create' and 'update' actions
//                'actions'=>array('create','update','getcategories','getleaflevels','list'),
//                'users'=>array('@'),
//            ),
//            array('allow', // allow admin user to perform 'admin' and 'delete' actions
//                'actions'=>array('admin','delete','priceupdate'),
//                'users'=>array('admin'),
//            ),
//            array('deny',  // deny all users
//                'users'=>array('*'),
//            ),
//        );
//    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */

    public function actionSql() {

        $results = Products::model()->findAll(array('condition' => 'id < 800'));

        foreach ($results as $value) {

            $name = Products::getProductName($value->id);
            if ($name =='' && isset($value->description)) {
                $name = $value->description;
            }

            $q[] = Products::model()->updateAll(array('name' => $name), 'id = ' . $value->id);
        }
    }
    
    

    public function actionCreate() {
        $model = new Products;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Products'])) {
            $model->attributes = $_POST['Products'];
//            $model->lastUpdatedOn = date('Y-m-d H:i:s');
            $model->createdby = Yii::app()->user->getId();
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);
        //$details = array(new ProductsLabels);
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Products'])) {
            $model->attributes = $_POST['Products'];

            $model->lastUpdatedOn = date('Y-m-d H:i:s');
            $model->updatedby = Yii::app()->user->getId();
            //$model->save();
            // $this->redirect(array('view','id'=>$model->id));

            $model->mapped = 0;
            if (!empty($model->main_cat_fk_id) && !empty($model->sub_prod_cat_fk_id) && !empty($model->category_id)) {
                $model->mapped = 1;
            }

            $cat = Categories::model()->findByPk($model->category_id, array('select' => 'category'));
            $catName = "";
            if (isset($cat->category)) {
                $catName = $cat->category;
            }

            if (isset($_POST['ProductsLabels'])) {
                $unitName = array();
                foreach ($_POST['ProductsLabels'] as $k => $post) {
                    $detail = new ProductsLabels('batchSave');
                    $detail->attributes = $post;

                    $details[] = $detail;
                }

                // set product name .....
                $lbl = $_POST['ProductsLabels'];

                $unit_value = isset($lbl[0]['value']) ? $lbl[0]['value'] : '';
                $unit_name = isset($lbl[0]['unit_name']) ? $lbl[0]['unit_name'] : '';
                $type_value = isset($lbl[1]['value']) ? $lbl[1]['value'] : '';
//                $value = isset($lbl[0]['value']) ? $lbl[0]['value'] : '';

                $model->name = $catName . " " . $unit_value . $unit_name . " " . $type_value;

                $transaction = $model->getDbConnection()->beginTransaction();

                try {

                    $model->save();
                    $model->refresh();
                    ProductsLabels::model()->deleteAll(array('condition' => 'product_id=:product_id', 'params' => array(':product_id' => $model->id)));

                    foreach ($details as $detail) {
                        $detail->product_id = $model->id;
                        $detail->addedby = Yii::app()->user->getId();
                        $detail->addedon = date('Y-m-d');
                        $detail->save();
                    }
                    $transaction->commit();
                    Yii::app()->user->setFlash('success', 'Your have been successfully updated the data!');
                } catch (Exception $e) {
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error', 'The data not saved! Please try again!');
                }
            } else {
                $model->name = $catName;
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', 'Your have been successfully updated the data!');
                }
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    /* public function actionDelete($id)
      {
      $this->loadModel($id)->delete();

      // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
      if(!isset($_GET['ajax']))
      $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
      } */

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Products');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Products('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Products']))
            $model->attributes = $_GET['Products'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Products the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Products::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Products $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'products-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Get categories from the id
     */
    public function actionGetCategories() {
        $products = new Products;
        if ($_POST['parentid']) {
            $parentid = (int) $_POST['parentid'];
        }
        $bypk = (isset($_POST['level']) && $_POST['level'] == 'web') ? 1 : 0;

        $category = $products->getCategory($parentid, $bypk);
        //return $category;
        echo "<option value=''>Select Category</option>";
        foreach ($category as $value => $content)
            echo CHtml::tag('option', array('value' => $value), CHtml::encode($content), true);
    }

    /**
     * Get web level category details from web category
     */
    public function actionGetLeafLevels() {

        if ($_POST['catid']) {
            $catid = (int) $_POST['catid'];
        }
        if ($_POST['prodid']) {
            $prodid = $_POST['prodid'];
            $products = $model = $this->loadModel($prodid);
        }
        $productsLabels = ProductsLabels::model()->findAll(array('condition' => 'product_id=:product_id', 'params' => array(':product_id' => $prodid)));

        $leaflevels = Categories::getLeafValues($catid);

        $this->renderPartial('_webcate', array('model' => $products, 'leaflevels' => $leaflevels, 'values' => $productsLabels, 'catid' => $catid));
    }

    /**
     * Return productCategory list according to the given srCode and timestamp
     * @return Jason string
     */
    public function actionList() {
        $response = $data = array();
        $request = Yii::app()->request;

        if (isset($_REQUEST['id']) && strlen($_REQUEST['id'])) {
            $id = ' AND id=' . $_REQUEST['id'];
            $Id = $_REQUEST['id'];
        } else {
            $id = '';
            $Id = 0;
        }
        if (isset($_REQUEST['category_id']) && strlen($_REQUEST['category_id'])) {
            $category_id = ' AND category_id=' . $_REQUEST['category_id'];
        } else {
            $category_id = '';
        }

        if (isset($_REQUEST['timestamp']) && strlen($_REQUEST['timestamp'])) {
            $timestamp = ' AND UNIX_TIMESTAMP(lastUpdatedOn) > ' . $_REQUEST['timestamp'];
        } else {
            $timestamp = '';
        }

        $data = Yii::app()->db->createCommand()
                ->select('*')
                ->from('products')
                ->where('status=1 AND category_id > 0' . $timestamp . $category_id . $id)
                ->queryAll();

        $response['status'] = true;
        $response['data']['products'] = $data;
        //$response['data']['message'] = "Successfully run the request";
        $json = new Dealers();
        return $json->jsonEncode($response);
        Yii::app()->end();
    }

    public function actionPriceupdate() {
        $model = new PriceUpdate();

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['PriceUpdate'])) {
            $model->attributes = $_POST['PriceUpdate'];

            $tempSave = CUploadedFile::getInstance($model, 'prdsrc');
            $prd_src = SYS_TMP_PATH . Yii::app()->params->prdsrc;
            $product = new Products('search');

            if (isset($tempSave) && $tempSave->saveAs($prd_src)) {
                //$tempSave->saveAs($prd_src);
                PriceUpdate::updateProducts();
                Yii::app()->user->setFlash('success', 'Your have been successfully updated product prices!');
                $this->redirect('admin');
            } else {
                Yii::app()->user->setFlash('error', 'The data not saved! Please try again!');
            }
        }

        $this->render('priceupdate', array(
            'model' => $model,
        ));
    }

}
