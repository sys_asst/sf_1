<?php

class CategoriesController extends RController
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout='//layouts/column2';

    public function actions() {
        return array(
            'getRowForm' => array(
                'class' => 'application.extensions.dynamictabularform.actions.GetRowForm',
                'view' => '_rowForm',
                'modelClass' => 'LeavesLabels'
            ),
        );
    }


    public function filters()
    {
        return array(
            'rights', // perform access control for CRUD operations
//			'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * @return array action filters
     */
//    public function filters()
//    {
//        return array(
//            'accessControl', // perform access control for CRUD operations
//            'postOnly + delete', // we only allow deletion via POST request
//        );
//    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
//    public function accessRules()
//    {
//        return array(
//            array('allow',  // allow all users to perform 'index' and 'view' actions
//                'actions'=>array('index','view','getchildcategory','getrowform','list','t'),
//                'users'=>array('*'),
//            ),
//            array('allow', // allow authenticated user to perform 'create' and 'update' actions
//                'actions'=>array('create','update'),
//                'users'=>array('@'),
//            ),
//            array('allow', // allow admin user to perform 'admin' and 'delete' actions
//                'actions'=>array('admin','delete'),
//                'users'=>array('admin'),
//            ),
//            array('deny',  // deny all users
//                'users'=>array('*'),
//            ),
//        );
//    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view',array(
            'model'=>$this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model=new Categories;
        $sladetails = array(new LeavesLabels);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        /* old
		if(isset($_POST['Categories']))
		{
			$model->attributes=$_POST['Categories'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));*/

        if(isset($_POST['Categories']))
        {
            $model->attributes=$_POST['Categories'];

            //if($model->save())
            //$this->redirect(array('view','id'=>$model->id));

            if (isset($_POST['LeavesLabels'])) {
                $sladetails = array();
                foreach ($_POST['LeavesLabels'] as $key => $value) {
                    /*
                     * sladetail needs a scenario wherein the fk sla_id
                     * is not required because the ID can only be
                     * linked after the sla has been saved
                     */
                    $sladetail = new LeavesLabels('batchSave');
                    $sladetail->attributes = $value;
                    $sladetails[] = $sladetail;
                }
            }

            $transaction = $model->getDbConnection()->beginTransaction();

            try {
                $model->save();
                $model->refresh();

                foreach ($sladetails as $sladetail) {
                    $sladetail->leaf_level_id_fk = $model->id;

                    $sladetail->save();
                }
                $transaction->commit();
                //print_r($sladetail);exit;
                //print_r($sladetail);exit;
                Yii::app()->user->setFlash('success','Your have been successfully added a record!');

                $this->redirect(array('view','id'=>$model->id));
            } catch (Exception $e) {
                $transaction->rollback();
                Yii::app()->user->setFlash('error','Please try again!');
            }
        }

        $this->render('create',array(
            'model'=>$model,
            'details'=>$sladetails
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);
        $details = LeavesLabels::model()->findAll('leaf_level_id_fk='.$id);
        if(sizeof($details) < 1)
        {
            $details = array(new LeavesLabels);
        }
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Categories']))
        {
            $model->attributes=$_POST['Categories'];
            /*if($model->save())
                $this->redirect(array('view','id'=>$model->id));*/
            if (isset($_POST['LeavesLabels'])) {
                $sladetails = array();
                foreach ($_POST['LeavesLabels'] as $key => $value) {
                    /*
                     * sladetail needs a scenario wherein the fk sla_id
                     * is not required because the ID can only be
                     * linked after the sla has been saved
                     */
                    $sladetail = new LeavesLabels('batchSave');
                    $sladetail->attributes = $value;
                    $sladetails[] = $sladetail;
                }
            }

            $transaction = $model->getDbConnection()->beginTransaction();

            try {
                $model->save();
                $model->refresh();
                LeavesLabels::model()->deleteAll(array('condition'=>'leaf_level_id_fk=:leafid', 'params'=>array(':leafid'=>$model->id)));
                foreach ($sladetails as $sladetail) {
                    $sladetail->leaf_level_id_fk = $model->id;

                    $sladetail->save();
                }
                $transaction->commit();

                Yii::app()->user->setFlash('success','Your have been successfully updated the data!');
                $this->redirect(array('view','id'=>$model->id));
            } catch (Exception $e) {
                $transaction->rollback();
                Yii::app()->user->setFlash('error','Please try again!');
            }
        }

        $this->render('update',array(
            'model'=>$model,
            'details'=>$details
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    /*
     * public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }*/

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider=new CActiveDataProvider('Categories');
        $this->render('index',array(
            'dataProvider'=>$dataProvider,
        ));
    }

    public function actionT()
    {
        PriceUpdate::updateProducts();
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model=new Categories('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Categories']))
            $model->attributes=$_GET['Categories'];

        $this->render('admin',array(
            'model'=>$model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Categories the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=Categories::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Categories $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='categories-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Get Category by level
     */
    public function actionGetCategoriesByLevel()
    {
        $products = new Products;
        if ($_POST['levelid'])
        {
            $levelid = (int) $_POST['levelid'];
        }
        if($levelid == 2)
        {
            $text = 'Segment';
        } elseif($levelid == 3) {
            $text = 'Category';
        }

        $category = CHtml::listData(Categories::model()->findAll(array('condition'=>'disabled=:disabled AND category_level=:levelid', 'order'=>'category', 'params'=>array(':disabled'=>0,':levelid'=>$levelid-1))), 'id','category');
        //return $category;
        echo "<option value=''>Select {$text}</option>";
        if($category)
        {
            foreach($category as $value=>$content)
                echo CHtml::tag('option', array('value'=>$value),CHtml::encode($content),true);
        } else{
            echo '<option value="">no matching category</option>';
        }

    }

    public function actionGetChildCategory() {
        if ($_POST['parentid'])
        {
            $parentId = (int) $_POST['parentid'];
        }
        $category = Categories::getChildCategory($parentId);

        echo "<option value=''>Select Category</option>";
        if($category)
        {
            foreach($category as $value=>$content)
                echo CHtml::tag('option', array('value'=>$value),CHtml::encode($content),true);
        } else{
            echo '<option value="">no matching category</option>';
        }
    }

    /**
     * Return productCategory list according to the given srCode and timestamp
     * @return Jason string
     */
    public function actionList()
    {
        $response = $data = array();
        $time = isset($_REQUEST['timestamp']) ? $_REQUEST['timestamp'] : '';
        $request = Yii::app()->request;

        if(strlen($time))
        {
            $timestamp = ' UNIX_TIMESTAMP(lastUpdatedOn) > '.$time;

        } else {
            $timestamp = '0=0';
            $time = 0;
        }

        $data = Yii::app()->db->createCommand()
            ->select('id, category, IFNULL(parent_cat_id, -1) AS parent_cat_id, IFNULL(category_level, -1) AS category_level, status')
            ->from('categories')
            ->where('status=1 AND disabled = 0 AND '.$timestamp)
            ->queryAll();

        $response['status'] = true;
        $response['data']['productCategories'] = $data;
        //$response['data']['message'] = "Successfully run the request";
        $json = new Dealers();
        return $json->jsonEncode($response);
        Yii::app()->end();
    }
}
