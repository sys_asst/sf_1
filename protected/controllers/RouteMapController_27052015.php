<?php

class RouteMapController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

    public function actions() {
        return array(
            'getRowForm' => array(
                'class' => 'application.extensions.dynamictabularform.actions.GetRowForm',
                'view' => '_rowForm',
                'modelClass' => 'DRouteMap'
            ),
        );
    }

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','getRowForm','CreateShedule','GetDealers'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new RouteMap;
        $sladetails = array(new DRouteMap);
        $entered = array();
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['RouteMap']))
		{
			$model->attributes=$_POST['RouteMap'];
            $model->assigned_by = Yii::app()->user->id;

            if (isset($_POST['DRouteMap'])) {
                $sladetails = array();
                $entered = $_POST['DRouteMap'];
                foreach ($_POST['DRouteMap'] as $key => $value) {
                    //Tk::a($value);
                    if(isset($value['route_id']) && sizeof($value['route_id']))
                    {
                        foreach($value['route_id'] as $route)
                        {
                            $sladetail = new DRouteMap('batchSave');
                            $sladetail->added_date = $value['added_date'];
                            $sladetail->route_id = $route;
                            //$sladetail->attributes = $value;
                            $sladetails[] = $sladetail;
                        }
                    } else {
                        $sladetail = new DRouteMap('batchSave');
                        $sladetail->attributes = $value;
                        //print_r($value);
                        //print_r($sladetail->attributes);
                        $sladetails[] = $sladetail;
                    }
                }
            }
            //Tk::a($sladetails);
//exit;
            $transaction = $model->getDbConnection()->beginTransaction();

            try {
                $valid = $model->validate();
                if($valid)
                {
                    $model->save();
                    $model->refresh();

                    foreach ($sladetails as $sladetail) {
                        $sladetail->map_id = $model->id;
                        $sladetail->validate();
                        $sladetail->save();
                        //print_r($sladetail);exit;
                    }

                    $transaction->commit();
                    Yii::app()->user->setFlash('success','Your have been successfully added a record!');

                    $this->redirect(array('view','id'=>$model->id));
                }

            } catch (Exception $e) {
                $transaction->rollback();
                Yii::app()->user->setFlash('error','Please try again!');
            }
			//if($model->save())
				//$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
            'details'=>$sladetails,
            'entered'=>$entered
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
        $entered = array();
        $model=$this->loadModel($id);
        $details = DRouteMap::model()->findAll('map_id='.$id);
        //Tk::a($details);exit;
        $added = $routes = array();
        if(sizeof($details))
        {
            foreach($details as $s_data)
            {
                $id = (date('d', strtotime($s_data['added_date']))-1);
                $sladetail = new DRouteMap('batchSave');

                $sladetail->added_date = $s_data->added_date ;
                $routes[$id][] = $s_data->route_id ;
                $sladetail->route_id = $routes[$id];
                $added[$id] = $sladetail;
            }
        }
        //Tk::a($added);
        //Tk::a($details);exit;
        if(sizeof($details) < 1)
        {
            $details = array(new DRouteMap);
        }
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['RouteMap']))
		{
			$model->attributes=$_POST['RouteMap'];
            if (isset($_POST['DRouteMap'])) {
                $sladetails = array();
                foreach ($_POST['DRouteMap'] as $key => $value) {
                    //Tk::a($value);
                    /**$sladetail = new DRouteMap('batchSave');
                    $sladetail->attributes = $value;
                    //print_r($value);
                    //print_r($sladetail->attributes);
                    $sladetails[] = $sladetail;*/
                    if(isset($value['route_id']) && sizeof($value['route_id']))
                    {
                        foreach($value['route_id'] as $route)
                        {
                            $sladetail = new DRouteMap('batchSave');
                            $sladetail->added_date = $value['added_date'];
                            $sladetail->route_id = $route;
                            //$sladetail->attributes = $value;
                            $sladetails[] = $sladetail;
                        }
                    } else {
                        $sladetail = new DRouteMap('batchSave');
                        $sladetail->attributes = $value;
                        //print_r($value);
                        //print_r($sladetail->attributes);
                        $sladetails[] = $sladetail;
                    }
                }
            }
            //exit;
            //print_r($sladetails);exit;
            $transaction = $model->getDbConnection()->beginTransaction();

            try {
                $valid = $model->validate();
                if($valid)
                {
                    $model->save();
                    $model->refresh();
                    DRouteMap::model()->deleteAll(array('condition'=>'map_id=:leafid', 'params'=>array(':leafid'=>$model->id)));
                    foreach ($sladetails as $sladetail) {
                        $sladetail->map_id = $model->id;
                        //print_r($sladetail);
                        $sladetail->validate();
                        $sladetail->save();
                        //print_r($sladetail);exit;
                    }
                    $transaction->commit();

                    Yii::app()->user->setFlash('success','Your have been successfully updated the data!');
                    $this->redirect(array('view','id'=>$model->id));
                }

            } catch (Exception $e) {
                $transaction->rollback();
                Yii::app()->user->setFlash('error','Please try again!');
            }
			//if($model->save())
				//$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
            'details'=>$added,
            'entered'=>$entered
		));
	}

    public function actionCreateShedule()
    {
        $month = isset($_REQUEST['month']) && strlen($_REQUEST['month']) > 0 ? $_REQUEST['month'] : date('Y-m-01');
        $id = isset($_REQUEST['id']) && $_REQUEST['id'] > 0 ? $_REQUEST['id'] : 0;//Tk::a($_REQUEST);
        $rep_id = isset($_REQUEST['rep_id']) && $_REQUEST['rep_id'] > 0 ? $_REQUEST['rep_id'] : 0;//Tk::a($_REQUEST);

        $details = DRouteMap::model()->findAll(array('condition'=>"map_id=:map_id AND DATE_FORMAT(`added_date`, '%y-%m') = DATE_FORMAT(:month, '%y-%m')", 'params'=>array(':map_id'=>$id,':month'=>$month)));

        $this->renderPartial('_schedule', array('id'=>$id, 'month'=>$month, 'rep_id'=>$rep_id, 'data'=>$details));
    }

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('RouteMap');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new RouteMap('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['RouteMap']))
			$model->attributes=$_GET['RouteMap'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return RouteMap the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=RouteMap::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param RouteMap $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='route-map-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    public function actionGetDealers()
    {

        if ($_POST['rout_id'])
        {
            $route_id = $_POST['rout_id'];
        }

        $records = Route::getDealers($route_id);

        echo sizeof($records);
    }
}
