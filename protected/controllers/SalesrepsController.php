<?php

class SalesrepsController extends RController
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'rights',//'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    //public function filters() { return array( 'rights', ); }
    public function allowedActions()
    {
        return 'index';
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {

        $model = $this->loadModel($id);


        $distributors = $areas = $records = array();


        if ($model->designation_id == 1) {


            $sql = 'SELECT A.`name` FROM `d_regions` AS DR INNER JOIN `ms_area` AS A ON A.`id` = DR.`area_id` WHERE DR.`region_id` = "' . $model->region_id . '" ORDER BY A.`name`';
            $records = Tk::sql($sql);

            if (sizeof($records)) {
                foreach ($records as $record) {
//                    $areas .= (strlen($areas)) ? ', '.$record['name'] : $record['name'] ;
                    $areas[] = $record['name'];
                }
            }

            $sql = 'SELECT D.`distName` AS `name` FROM `d_regions` AS DR INNER JOIN `distributors` AS D ON D.`area_id` = DR.`area_id` WHERE DR.`region_id` = "' . $model->region_id . '" ORDER BY D.`distName`';
            $records = Tk::sql($sql);

            if (sizeof($records)) {
                foreach ($records as $record) {
//                    $distributors .= (strlen($distributors)) ? ', '.$record['name'] : $record['name'] ;
                    $distributors[] .= $record['name'];
                }
            }
            $areas = implode(" <br/> ", $areas);
            $distributors = implode(" <br/> ", $distributors);
        }else{
            $areas =   isset($model->area->name) ? $model->area->name : '';
            $distributors =  Distributors::getDistributor($model->distributor,1);
        }




        $this->render('view', array('model' => $model, 'distributors' => $distributors, 'areas' => $areas));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    /*
    *
     * Removed by asn */
    public function actionCreate()
    {
        $model = new Salesreps;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Salesreps'])) {
            $model->attributes = $_POST['Salesreps'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Salesreps'])) {

            print_r($_POST['Salesreps']);

            $model->attributes = $_POST['Salesreps'];
            if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->id));
            }

        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('Salesreps');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new Salesreps('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Salesreps']))
            $model->attributes = $_GET['Salesreps'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Salesreps the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Salesreps::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Salesreps $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'salesreps-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Create new user from sales ref
     */
    public function actionAddUser($id)
    {
        //$user_model=Yii::app()->getModule('user');
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Salesreps'])) {
            $model->attributes = $_POST['Salesreps'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('adduser', array(
            'model' => $model,
        ));
    }
}
