<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Anura
 * Date: 5/30/14
 * Time: 10:02 AM
 * To change this template use File | Settings | File Templates.
 */

class AppController extends RController
{
    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha'=>array(
                'class'=>'CCaptchaAction',
                'backColor'=>0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page'=>array(
                'class'=>'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
        $this->render('index');
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if($error=Yii::app()->errorHandler->error)
        {
            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    public function actionSrLogin() {
        $response = array();

        //$arrData = explode('&',$GLOBALS['HTTP_RAW_POST_DATA']);
        $srcode = (isset($_REQUEST['srCode'])) ? $_REQUEST['srCode'] : '';
        $password = (isset($_REQUEST['password'])) ? $_REQUEST['password'] : '';
        //print_r($GLOBALS);exit;

        /*if(sizeof($arrData))
        {
            strlen($arrData[0]) ? list($a,$srcode) = explode('=', $arrData[0]) : $srcode = '';
            strlen($arrData[1]) ? list($a,$password) = explode('=', $arrData[1]) : $jsonstring = '';
        }*/

        //print_r($arrData);exit;

        if (Yii::app()->user->isGuest) {

            if(strlen($srcode) && strlen($password))
            {
                $password = addslashes($password);
                $hash = Yii::app()->getModule('user')->hash;

                if ($hash=="md5")
                {
                    $password = md5($password);
                } elseif($hash=="sha1") {
                    $password = sha1($password);
                } else {
                    $password = hash($hash,$password);
                }

                $username = addslashes($srcode);

                $data = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from('users')
                    ->where('status = 1 AND username=:username AND password=:password',array(':username'=>$username, ':password'=>$password))
                    ->queryAll();

                if(sizeof($data))
                {
                    $message = 'login success';
                    $status = true;
                    $key = 'data';
                } else {
                    $message = 'login fail';
                    $status = false;
                    $key = 'error';
                }

                $response['status'] = $status;
                $response[$key]['message'] = $message;
            } else {
                $response['status'] = false;
                $response['error']['message'] = "srCode and password is can not be blank";
            }
            $json = new Dealers();
            return $json->jsonEncode($response);
            Yii::app()->end();
        } else {
            $response['status'] = true;
            $response['data']['message'] = "already logged";
            $json = new Dealers();
            return $json->jsonEncode($response);
            Yii::app()->end();
        }
    }

    /**
     * Return DelaersComentReason list according to the given srCode and timestamp
     * @return Jason string
     */
    public function actionComentsReason()
    {
        $response = $data = array();
        $request = Yii::app()->request;
        //print_r($_GET);exit;
        //echo $_REQUEST['timestamp'];exit;

        if(isset($_REQUEST['timestamp']) && strlen($_REQUEST['timestamp']))
        {
            $timestamp = ' AND UNIX_TIMESTAMP(lastUpdatedOn) > '.$_REQUEST['timestamp'];
        } else {
            $timestamp = ' AND 0=0';
        }

        $data = Yii::app()->db->createCommand()
            ->select('*')
            ->from('dealercommentreasons')
            //->join('dealers d', 'd.distributor=s.distributor')
            ->where(' 0=0 '.$timestamp)//'13491'
            ->queryAll();

        $response['status'] = true;
        $response['data']['dealerCommentReasons'] = $data;
        //$response['data']['message'] = "Successfully run the request";
        $json = new Dealers();
        return  $json->jsonEncode($response);
        Yii::app()->end();
    }

    public function actionlastUpdated ()
    {
        $response = $data = array();
        $tableNames = array('products', 'dealers', 'distributors', 'categories', 'dealercommentreasons', 'areas', 'routes', 'salesreps', 'schedule', 'towns');

        $tempLastUpdatedOn = 0;

        foreach ($tableNames as $tableName)
        {
            $lastUpdatedOn = Yii::app()->db->createCommand()
                ->select('(MAX(UNIX_TIMESTAMP(lastUpdatedOn))) AS lastUpdatedOn')
                ->from($tableName)
                ->queryRow();
            //print_r($lastUpdatedOn);
            $lastUpdatedOn = $lastUpdatedOn['lastUpdatedOn'];
            if($tempLastUpdatedOn < $lastUpdatedOn)
            {
                $tempLastUpdatedOn = $lastUpdatedOn;
            }
        }

        $response['status'] = true;
        $response['data']['timestamp']['lastUpdatedOn'] = $tempLastUpdatedOn;
        //$response['data']['message'] = "Successfully run the request";
        $json = new Dealers();
        return $json->jsonEncode($response);
        Yii::app()->end();
    }

    public function actionCheckedIn()
    {
        $response = $data = $returnarray = array();
        $jsonstring = isset($_REQUEST['checkedIn']) ? $_REQUEST['checkedIn'] : '';
        $srcode = isset($_REQUEST['srCode']) ? $_REQUEST['srCode'] : '';
        //print_r($_REQUEST);exit;

        //$arrData = explode('&',$GLOBALS['HTTP_RAW_POST_DATA']);


        /*if(sizeof($arrData))
        {
            strlen($arrData[0]) ? list($a,$srcode) = explode('=', $arrData[0]) : $srcode = '';
            strlen($arrData[1]) ? list($a,$jsonstring) = explode('=', $arrData[1]) : $jsonstring = '';
        }*/

        if(strlen($srcode) && strlen($jsonstring))
        {
            $model=new Checkedin;

            //$comment_array = json_decode($jsonstring, true);
            $comment_array = json_decode(urldecode($jsonstring), true);

            $data['srCode'] = $srcode;
            $data['checkedInTime']=$comment_array['checkedInTime'];
            $location_array = $comment_array['location'];
            unset($comment_array['location']);
            $data['latitude'] = $location_array['latitude'];
            $data['longitude'] = $location_array['longitude'];
            $data['dealerId'] = $comment_array['dealerId'];
            $data['addedon'] = date('Y-m-d H:i:s');

            $model->attributes=$data;

            if($model->save())
            {
                $status = true;
                $message = 'Successfully added data';
            } else {
                $status = false;
                $message = 'Fail to add data';
            }
        } else {
            $status = false;
            $message = 'srCode and checkedin details are blank';
        }

        $response['status'] = $status;
        $response['data']['nodeId'] = isset($model->id) ? $model->id : -1;
        $response['data']['message'] = $message;

        $json = new Dealers();
        return  $json->jsonEncode($response);
        Yii::app()->end();
    }





}
