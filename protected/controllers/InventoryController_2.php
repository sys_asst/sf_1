<?php

class InventoryController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
//	public $layout='//layouts/column2';

    /**
     * @return array action filters
     */
//	public function filters()
//	{
//		return array(
//			'accessControl', // perform access control for CRUD operations
//			'postOnly + delete', // we only allow deletion via POST request
//		);
//	}

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {

//        print_r($_POST);

        $model = new Inventory;

        if (isset($_POST['Inventory']) && isset($_POST['InventoryItems'])) {

//                        if (isset($_POST['InventoryItems']['products_id'])) {



            if(!isset(Yii::app()->session['inventory_id'])){

                $model->attributes = $_POST['Inventory'];

                $model->inventory_type_id = 1; //test
                $model->status_id = 1; //test
                $model->createdby = Yii::app()->user->getId();
                $model->createdtime = date('Y-m-d H:i:s');

                $inventorySave = $model->save();
            }else{
                $inventorySave = true;
            }



            if ($inventorySave) {

                $model1 = new InventoryItems;
//                                if (isset($_POST['InventoryItems'])) {

                if(!isset(Yii::app()->session['inventory_id'])){
                    Yii::app()->session['inventory_id'] = $model->id;
                }

                $model1->attributes = $_POST['InventoryItems'];
                $model1->inventory_id = Yii::app()->session['inventory_id'];
                $model1->createdby = Yii::app()->user->getId();
                $model1->createdtime = date('Y-m-d H:i:s');

                if ($model1->save()){

                    $this->redirect(array('view', 'id' => $model1->id));
                }

//                                }

            }
//                        }
        }


        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

//        $transaction = $model->getDbConnection()->beginTransaction();

//        try {

//            if (isset($_POST['Inventory']) && isset($_POST['InventoryItems'])) {
//
//                if (isset($_POST['InventoryItems']['product_id'])) {
//
//                    $model->attributes = $_POST['Inventory'];
//
//                    $model->inventory_type_id = 1; //test
//                    $model->status_id = 1; //test
//                    $model->createdby = Yii::app()->user->getId();
//                    $model->createdtime = date('Y-m-d H:i:s');
//
//
//                    if ($model->save()) {
//                        $model->refresh();
//
//        $inventoryItems = new InventoryItems;

//                        $transaction1 = $inventoryItems->getDbConnection()->beginTransaction();
//
//                        try {
//                             $model->attributes=$_POST['InventoryItems'];
//                            $inventoryItems->attributes = $_POST['InventoryItems'];

//                            $inventoryItems->inventory_id = 12;
//                            $inventoryItems->products_id = 12;
//                            $inventoryItems->updatedby = 12;
//                            $inventoryItems->inventory_id = 12;
//
////                    inventory_id, products_id, quantity, createdby, createdtime, updatedby, updatedtime
//
//                            $inventoryItems->createdby = Yii::app()->user->getId();
//                            $inventoryItems->createdtime = date('Y-m-d H:i:s');
//                            $inventoryItems->updatedtime = date('Y-m-d H:i:s');
//
////                        $inventoryItems->validate();
//                            if ($inventoryItems->save()) {
////                                $transaction1->commit();
//                                Yii::app()->user->setFlash('error', 'wow!');
////                                $this->redirect(array('view', 'id' => $model->id));
//                            }else{
//                                Yii::app()->user->setFlash('error', 'why!');
//                            }


//                        } catch (Exception $e1) {
//                            //Tk::a($e);exit;
//                            echo "<br/><br/>";
//                            print_r($_POST['InventoryItems']);
//                            print_r( $inventoryItems->product_id);
//                            echo "<br/><br/>";
//                            $transaction1->rollback();
//
//                            print_r($e1->getMessage());
//                            Yii::app()->user->setFlash('error', 'Please try again!');
//                        }
//                    }

//                } else {
//                    // to do
//                }

//            }
//            $transaction->commit();
//        }catch (Exception $e) {
//            //Tk::a($e);exit;
//            $transaction->rollback();
//         print_r($e);
//            Yii::app()->user->setFlash('error','Please try again!');
//        }


//        $product=new Products('search');
//        $product->unsetAttributes();
//
//        if(isset($_GET['Products']))
//            $product->attributes=$_GET['Products'];

        $inventoryItems = new InventoryItems('search');
//        $inventoryItems->inventory_id = 27;
        $inventoryItems->inventory_id = Yii::app()->session['inventory_id'];

//        echo Yii::app()->session['inventory_id'];


//        if(isset($_GET['InventoryItems']))
//            $model->attributes=$_GET['InventoryItems'];

        $this->render('create', array('model' => $model, 'inventoryItems'=>$inventoryItems));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Inventory'])) {
            $model->attributes = $_POST['Inventory'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('Inventory');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new Inventory('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Inventory']))
            $model->attributes = $_GET['Inventory'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Inventory the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Inventory::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Inventory $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'inventory-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }


    public function actionGetProducts()
    {

        $request = trim($_GET['term']);
        if ($request != '') {
            $model = Categories::model()->findAll(array("condition" => "category like '$request%' and status = '1' and category_level='3'"));
            $data = array();

            foreach ($model as $get) {

//                $data[] = array('value'=>$get->category, 'name'=>$get->id);
                $data[] = $get->category;
//                $data[]['value'][]=$get->id;
            }
            $this->layout = 'empty';
            echo json_encode($data);
        }
    }


    public function actionGetProductsName()
    {

        $request = trim($_GET['term']);
        $web_cat = urldecode($_GET['web_cat']);

        if ($request != '') {
//        if($request!='' && $request != ''){


            if ($web_cat != '') {

                $categories = Categories::model()->findAll(array("select" => "id", "condition" => "category = '$web_cat' and status = '1' and category_level='3' limit 1"));

                if (isset($categories[0]['id'])) {

                    $cat_id = $categories[0]['id'];

                    $products = Products::model()->findAll(array("condition" => "category_id = '$cat_id' and description like '$request%' limit 10"));

                }

            } else {
                $products = Products::model()->findAll(array("condition" => "description like '$request%' limit 10"));
            }


//            if(isset($categories[0]['id']))
//            {
//
//              $cat_id = "";
//
//            }
//
//
            $data = array();

            foreach ($products as $get) {

                $data[] = array('value' => $get->description, 'price' => $get->price, 'uniqueId' => $get->uniqueId, 'id' => $get->id);
//                $data[]= $get->description;
//                $data[]['value'][]=$get->id;
            }
            $this->layout = 'empty';
            echo json_encode($data);
        }
    }
}
