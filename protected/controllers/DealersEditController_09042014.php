<?php

class DealersEditController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'UpdateDealer'=>array(
                'class'=>'CWebServiceAction',
            ),
        );
    }

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','updatedealer','LatestComments'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','compare','confirm'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new DealersEdit;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['DealersEdit']))
		{
			$model->attributes=$_POST['DealersEdit'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['DealersEdit']))
		{
			$model->attributes=$_POST['DealersEdit'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

    public function actionCompare($id)
    {
        $model=$this->loadModel($id);
        $this->render('viewcompare',array(
            'model'=>$this->loadModel($model->id),
            'dealer'=>Dealers::model()->findByPk($model->dealer_id)
        ));
    }

    public function actionConfirm($id)
    {
        $model=$this->loadModel($id);
        $dealer = Dealers::model()->findByPk($model->dealer_id);
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        $dealer->attributes = $model->attributes;

        if($dealer->save())
        {
            $model->status = DealersEdit::STATUS_ACTIVE;
            $model->save();
            Yii::app()->user->setFlash('success','Your have been successfully updated a dealer!');
            $this->redirect(array('/dealers/view','id'=>$dealer->id));
        }
        /*if(isset($_POST['DealersEdit']))
        {
            $model->attributes=$_POST['DealersEdit'];
            if($model->save())
                $this->redirect(array('view','id'=>$model->id));
        }*/

        $this->render('update',array(
            'model'=>$model,
        ));
    }

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('DealersEdit');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new DealersEdit('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['DealersEdit']))
			$model->attributes=$_GET['DealersEdit'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
        public function actionLatestComments()
       {
            $model=new DealersEdit('searchLatest');
            $model->unsetAttributes();  // clear any default values

            $model->status = Tk::get('statusnc');
            if(isset($_GET['DealersEdit']))
                $model->attributes=$_GET['DealersEdit'];

                $this->render('admin',array(
                     'model'=>$model,
               ));
        }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return DealersEdit the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=DealersEdit::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param DealersEdit $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='dealers-edit-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    /**
     * Save UpdateDealer Request
     *
     * @return JsonResponse
     */
    public function actionUpdateDealer()
    {
        $response = $data = $returnarray = array();
        $jsonstring = isset($_REQUEST['dealer']) ? $_REQUEST['dealer'] : '';
        $srcode = isset($_REQUEST['srCode']) ? $_REQUEST['srCode'] : '';

        //print_r($GLOBALS);exit;
        //$arrData = explode('&',$GLOBALS['HTTP_RAW_POST_DATA']);
        //Yii::app()->request->getQuery();
        //print_r($_REQUEST);exit;
        //print_r(Yii::app()->request->getPost());
        //exit;
        /*if(sizeof($arrData))
        {
            strlen($arrData[0]) ? list($a,$srcode) = explode('=', $arrData[0]) : $srcode = '';
            strlen($arrData[1]) ? list($a,$jsonstring) = explode('=', $arrData[1]) : $jsonstring = '';
        }*/

        if(strlen($srcode) && strlen($jsonstring))
        {
            $model=new DealersEdit;

            $jsonobject = json_decode(urldecode($jsonstring), true);

            $data['dealer_id'] = $jsonobject['dealerId'];
            $data['dealerName'] = $jsonobject['dealerName'];
            $data['phoneOffice'] = $jsonobject['phoneOffice'];
            $data['phoneResidence'] = $jsonobject['phoneResidence'];
            $data['email'] = $jsonobject['email'];
            $data['contactPerson'] = $jsonobject['contactPersonName'];
            $data['address1'] = $jsonobject['address1'];
            $data['address2'] = $jsonobject['address2'];
            $data['address3'] = $jsonobject['address3'];
            $data['townId'] = $jsonobject['townId'];
            $location_array = $jsonobject['location'];
            unset($jsonobject['location']);
            $data['latitude'] = $location_array['latitude'];
            $data['longitude'] = $location_array['longitude'];
            $data['lastUpdatedOn'] = date('Y-m-d H:i:s');

            /**
             * Get distributor against given srCode
             */
            $dealers = CHtml::listData(Salesreps::model()->findAll('status = 1 AND srEmpNo=:srcode',array(':srcode'=>$srcode)),'status','distributor');
            if(sizeof($dealers))
            {
                $data['distributor'] = $dealers[1];
            } else {
                $data['distributor'] = 0;
            }

            /**
             * Get new dealerCode
             */
            $dealercodePrefix = strtoupper(Yii::app()->params['dealsercodeprefix']);// As app parameter

            $dealercode = Yii::app()->db->createCommand()
                ->select("CONCAT('{$dealercodePrefix}',(MAX(id)+1)) AS `newCode`")
                ->from('dealers')
                ->queryAll();

            $data['dealerCode'] = $dealercode[0]['newCode'];

            $model->attributes=$data;

            if($model->save())
            {
                $status = true;
                $message = 'Successfully added dealer';
                $returnarray = array('dealerCode' => $data['dealerCode'], 'id' => $model->id);
            } else {
                $status = false;
                $message = 'Fail to add dealer';
                $returnarray = array('dealerCode' => '', 'id' => '');
            }

        } else {
            $status = false;
            $message = 'srcode and deler details are blank';
        }

        $response['status'] = $status;
        $response['data']['dealer'] = $returnarray;
        $response['data']['message'] = $message;

        $json = new Dealers();
        return  $json->jsonEncode($response);
        Yii::app()->end();
    }
}
