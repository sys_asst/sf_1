<?php

class RegionsController extends RController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
//	public $layout='//layouts/column2';

    public function actions() {
        return array(
            'getRowForm' => array(
                'class' => 'application.extensions.dynamictabularform.actions.GetRowForm',
                'view' => '_rowForm',
                'modelClass' => 'DRegions'
            ),
        );
    }
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'rights', // perform access control for CRUD operations
//			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
//	public function accessRules()
//	{
//		return array(
//			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('index','view','getRowForm'),
//				'users'=>array('*'),
//			),
//			array('allow', // allow authenticated user to perform 'create' and 'update' actions
//				'actions'=>array('create','update'),
//				'users'=>array('@'),
//			),
//			array('allow', // allow admin user to perform 'admin' and 'delete' actions
//				'actions'=>array('admin','delete'),
//				'users'=>array('admin'),
//			),
//			array('deny',  // deny all users
//				'users'=>array('*'),
//			),
//		);
//	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Regions;
        $sladetails = array(new DRegions);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Regions']))
		{
			$model->attributes=$_POST['Regions'];
            $model->added_by = Yii::app()->user->id;

            if (isset($_POST['DRegions'])) {
                $sladetails = array();
                foreach ($_POST['DRegions'] as $key => $value) {
                    $sladetail = new DRegions('batchSave');
                    $sladetail->attributes = $value;
                    $sladetails[] = $sladetail;
                }
            }

            $transaction = $model->getDbConnection()->beginTransaction();

            try {
                $valid = $model->validate();
                if($valid)
                {
                    $model->save();
                    $model->refresh();

                    foreach ($sladetails as $sladetail) {
                        $sladetail->region_id = $model->id;
                        $sladetail->validate();
                        $sladetail->save();
                    }
                    $transaction->commit();
                    Yii::app()->user->setFlash('success','Your have been successfully added a record!');

                    $this->redirect(array('view','id'=>$model->id));
                }

            } catch (Exception $e) {
                $transaction->rollback();
                Yii::app()->user->setFlash('error','Please try again!');
            }
			//if($model->save())
				//$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
            'details'=>$sladetails
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
        $details = DRegions::model()->findAll('region_id='.$id);
        if(sizeof($details) < 1)
        {
            $details = array(new DRegions);
        }

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Regions']))
		{
			$model->attributes=$_POST['Regions'];
            $model->updated_by = Yii::app()->user->id;

            if (isset($_POST['DRegions'])) {
                $sladetails = array();
                foreach ($_POST['DRegions'] as $key => $value) {
                    $sladetail = new DRegions('batchSave');
                    $sladetail->attributes = $value;
                    $sladetails[] = $sladetail;
                }
            }

            $transaction = $model->getDbConnection()->beginTransaction();

            try {
                $valid = $model->validate();
                if($valid)
                {
                    $model->save();
                    $model->refresh();
                    DRegions::model()->deleteAll(array('condition'=>'region_id=:leafid', 'params'=>array(':leafid'=>$model->id)));
                    foreach ($sladetails as $sladetail) {
                        $sladetail->region_id = $model->id;
                        $sladetail->validate();
                        $sladetail->save();
                    }
                    $transaction->commit();

                    Yii::app()->user->setFlash('success','Your have been successfully updated the data!');
                    $this->redirect(array('view','id'=>$model->id));
                }

            } catch (Exception $e) {
                $transaction->rollback();
                Yii::app()->user->setFlash('error','Please try again!');
            }

			//if($model->save())
				//$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
            'details'=>$details
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Regions');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Regions('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Regions']))
			$model->attributes=$_GET['Regions'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Regions the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Regions::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Regions $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='regions-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
