<?php

class InventoryItemsController extends RController {
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
//	public $layout='//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'rights', // perform access control for CRUD operations
//            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
//	public function accessRules()
//	{
//		return array(
//			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('index','view'),
//				'users'=>array('*'),
//			),
//			array('allow', // allow authenticated user to perform 'create' and 'update' actions
//				'actions'=>array('create','update'),
//				'users'=>array('@'),
//			),
//			array('allow', // allow admin user to perform 'admin' and 'delete' actions
//				'actions'=>array('admin','delete'),
//				'users'=>array('admin'),
//			),
//			array('deny',  // deny all users
//				'users'=>array('*'),
//			),
//		);
//	}

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate2() {
        $model = new InventoryItems;
        // Ajax Validation enabled
        $this->performAjaxValidation($model);
        // Flag to know if we will render the form or try to add 
        // new jon.
        $flag = true;
        if (isset($_POST['InventoryItems'])) {
            $flag = false;
            $model->attributes = $_POST['InventoryItems'];

            if ($model->save()) {
                //Return an <option> and select it
                $this->redirect(array('view', 'id' => $model->id));
            }
        }
        if ($flag) {
            Yii::app()->clientScript->scriptMap['jquery.js'] = false;
            $this->renderPartial('create', array('model' => $model,), false, true);
        }

//                	$this->render('create',array('model'=>$model,));
    }

    public function actionCreate1() {
        $model = new InventoryItems;
        
//           $this->performAjaxValidation($model);
        if (isset($_POST['InventoryItems'])) {
            $model->attributes = $_POST['InventoryItems'];
            if ($model->save()) {
                if (Yii::app()->request->isAjaxRequest) {
                    // Stop jQuery from re-initialization
                    Yii::app()->clientScript->scriptMap['jquery.js'] = false;

                    echo CJSON::encode(array(
                        'status' => 'success',
                        'content' => 'ModelName successfully created',
                    ));
                    exit;
                } else
                    $this->redirect(array('view', 'id' => $model->id));
            }
        }

        if (Yii::app()->request->isAjaxRequest) {
            // Stop jQuery from re-initialization
            Yii::app()->clientScript->scriptMap['jquery.js'] = false;

            echo CJSON::encode(array(
                'status' => 'failure',
                'content' => $this->renderPartial('_form_1', array(
                    'model' => $model), true, true),
            ));
            exit;
        } else
            $this->render('create', array('model' => $model));
    }

    public function actionCreate2test() {
        $model = new InventoryItems;
        $model->unsetAttributes();
        // Ajax Validation enabled
        $this->performAjaxValidation($model);
        // Flag to know if we will render the form or try to add 
        // new jon.
//        print_r($_POST);

        $flag = true;
        if (isset($_POST['InventoryItems'])) {
            $flag = false;
            $model->attributes = $_POST['InventoryItems'];

            if ($model->save()) {
                $model->unsetAttributes();
                unset($_POST['InventoryItems']);
//                Yii::app()->user->setFlash('error', 'test');
//                        $this->redirect(array('create'));
                echo CJSON::encode(array('status' => 'success'));


//                     if (Yii::app()->request->isAjaxRequest)
//        {
//            echo CJSON::encode(array('status'=>'success','divcreate'=>$this->renderPartial('create', array('model'=>$model), true)));
//            Yii::app()->end();
//        }
//                
//                 echo array(
//                                  'status'=>'success',
//                                  'div'=>"Data saved"
//                                );
//                 echo CJSON::encode(array(
//                                  'status'=>'success',
//                                  'div'=>"Data saved"
//                                ));
//              echo   CHtml::encode(array('success'=>true));
//                $this->redirect(array('create'));
//                  echo CHtml::tag('option',array ('value'=>$model->id,'selected'=>true),CHtml::encode($model->id),true);
//                $this->redirect(array('view','id'=>$model->id));
                //Return an <option> and select it
//                            echo CHtml::tag('option',array (
//                                'value'=>$model->id,
//                                'selected'=>true
//                            ),CHtml::encode($model->id),true);
            } else {
                echo CJSON::encode(array('status' => 'fail'));
            }
        }

//                if($flag) {
//                    Yii::app()->clientScript->scriptMap['jquery.js'] = false;
//                    $this->renderPartial('_form_1',array('model'=>$model,),false,true);
//                }
    }

    public function actionCreate() {
        $model = new InventoryItems;
        $model->unsetAttributes();
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['InventoryItems'])) {
            $model->attributes = $_POST['InventoryItems'];

            $model->createdby = Yii::app()->user->getId();
            $model->createdtime = date('Y-m-d H:i:s');

            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['InventoryItems'])) {
            $model->attributes = $_POST['InventoryItems'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    public function actionDelCart($id) {


        if ($this->loadModel($id)->delete()) {

            $itemsCount = InventoryItems::model()->countByAttributes(array('inventory_id' => Yii::app()->session['inventory_id']));

            if ($itemsCount == 0) {

                Yii::app()->session['inventory_id'];
                if (Inventory::model()->deleteAll("id = " . Yii::app()->session['inventory_id'])) {

                    unset(Yii::app()->session['inventory_id'], Yii::app()->session['inventoryData']);
                    $this->redirect(array('inventory/create'));
                }
            } else {

                if (!isset($_GET['ajax'])) {

                    Yii::app()->session['cart_count'] = $itemsCount;

                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('inventory/create'));
                }
            }

//            $this->redirect(array('inventory/create'));
//            $this->refresh();
        }


        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
//        if(!isset($_GET['ajax'])){
//            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
//        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('InventoryItems');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new InventoryItems('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['InventoryItems']))
            $model->attributes = $_GET['InventoryItems'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return InventoryItems the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = InventoryItems::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param InventoryItems $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'inventory-items-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
