<?php

class RouteController extends RController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

    public function actions() {
        return array(
            'getRowForm' => array(
                'class' => 'application.extensions.dynamictabularform.actions.GetRowForm',
                'view' => '_rowForm',
                'modelClass' => 'DRoute'
            ),
        );
    }

    /**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'rights', // perform access control for CRUD operations
//			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
//	public function accessRules()
//	{
//		return array(
//			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('index','view','getRowForm','UpdateRoute','GetTowns'),
//				'users'=>array('*'),
//			),
//			array('allow', // allow authenticated user to perform 'create' and 'update' actions
//				'actions'=>array('create','update'),
//				'users'=>array('@'),
//			),
//			array('allow', // allow admin user to perform 'admin' and 'delete' actions
//				'actions'=>array('admin','delete'),
//				'users'=>array('admin'),
//			),
//			array('deny',  // deny all users
//				'users'=>array('*'),
//			),
//		);
//	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Route;
        $sladetails = array(new DRoute);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Route']))
		{
			$model->attributes=$_POST['Route'];
            $model->added_by = Yii::app()->user->id;

            if (isset($_POST['DRoute'])) {
                $sladetails = array();
                foreach ($_POST['DRoute'] as $key => $value) {
                    $sladetail = new DRoute('batchSave');
                    $sladetail->attributes = $value;
                    $sladetails[] = $sladetail;
                }
            }

            $transaction = $model->getDbConnection()->beginTransaction();

            try {
                $valid = $model->validate();
                if($valid)
                {
                    $model->save();
                    $model->refresh();

                    foreach ($sladetails as $sladetail) {
                        $sladetail->route_id = $model->id;
                        $sladetail->validate();
                        $sladetail->save();
                    }
                    $transaction->commit();
                    Yii::app()->user->setFlash('success','Your have been successfully added a record!');

                    $this->redirect(array('view','id'=>$model->id));
                }

            } catch (Exception $e) {
                //Tk::a($e);exit;
                $transaction->rollback();
                Yii::app()->user->setFlash('error','Please try again!');
            }

			//if($model->save())
				//$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
            'details'=>$sladetails
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
        $details = DRoute::model()->findAll('route_id='.$id);
        if(sizeof($details) < 1)
        {
            $details = array(new DRoute);
        }

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Route']))
		{
			$model->attributes=$_POST['Route'];
            $model->updated_by = Yii::app()->user->id;

            if (isset($_POST['DRoute'])) {
                $sladetails = array();
                foreach ($_POST['DRoute'] as $key => $value) {
                    $sladetail = new DRoute('batchSave');
                    //$sladetail->attributes = $value;
                    //$sladetails[] = $sladetail;
                    if($value['route_id'] != 'delete')
                    {
                        $sladetail->attributes = $value;
                        $sladetails[] = $sladetail;
                    }
                }
            }

            $transaction = $model->getDbConnection()->beginTransaction();

            try {
                $valid = $model->validate();
                if($valid)
                {
                    $model->save();
                    $model->refresh();
                    DRoute::model()->deleteAll(array('condition'=>'route_id=:leafid', 'params'=>array(':leafid'=>$model->id)));
                    
                    if(sizeof($sladetails))
                    {
                        foreach ($sladetails as $sladetail) {
                            $sladetail->route_id = $model->id;
                            $sladetail->validate();
                            $sladetail->save();
                        }
                    }
                    $transaction->commit();

                    Yii::app()->user->setFlash('success','Your have been successfully updated the data!');
                    $this->redirect(array('view','id'=>$model->id));
                }

            } catch (Exception $e) {
                $transaction->rollback();
                Yii::app()->user->setFlash('error','Please try again!');
            }
			//if($model->save())
				//$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
            'details'=>$details
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Route');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Route('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Route']))
			$model->attributes=$_GET['Route'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Route the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Route::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Route $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='route-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

        public function actionUpdateRoute()
    {
        $es = new EditableSaver('Route');  //'User' is name of model to be updated
        //$es->update();
        $es->onBeforeUpdate = function($event) {
            if(Yii::app()->user->isGuest) {
                $event->sender->error('You are not allowed to update data');
            }
        };
        $es->onBeforeUpdate = function($event) {
            $event->sender->setAttribute('updated_on', date('Y-m-d H:i:s'));
        };
        $es->update();
    }

    /**
     * Get assigned routes and towns for a give DS Division
     */
    public function actionGetTowns()
    {
        if(isset($_POST['parentid']))
        {
            $parent_id = $_POST['parentid'];
        }

        $records = Towns::getTowns($parent_id);

        if(sizeof($records))
        {
            $towns = $routes = '';
            $sql = "SELECT DISTINCT(DR.`gndiv_id`), DR.`route_id`, R.`name` FROM `d_route` AS DR INNER JOIN `ms_route` AS R ON R.`id` = DR.`route_id` AND DR.`gndiv_id` = '{$parent_id}'";
            $route = Tk::sql($sql);
            if(sizeof($route))
            {
                foreach($route as $route_data)
                {
                    $recordLink = CHtml::link(Yii::t('messages',' '.$route_data['name'].''),array('route/view/?id='.$route_data['route_id']),array('target'=>'_blank', 'style'=>'color:#eee'));
                    $routes .= strlen($routes) ? ', '.$recordLink : $recordLink;
                }
                $routes = '<b>This DS Division assigned for route:</b> <span class="label label-warning">'.$routes.'</span><br />';
            }

            foreach($records as $rec)
            {
                $towns .= (strlen($towns)) ? ', '.$rec['town'] : $rec['town'];
            }
            echo $routes.' <b>Assigned towns:</b><br />'.$towns;
        } else {
            echo 'no towns added!';
        }
    }
}
