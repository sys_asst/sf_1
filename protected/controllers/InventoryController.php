<?php

class InventoryController extends RController {
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
//	public $layout='//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'rights', // perform access control for CRUD operations
//            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionViewSum($id) {

        /////////////////////////////

        $model = new Inventory('view');
        $model->unsetAttributes();  // clear any default values

        $model->ref_no = $id;



        /////////////////////////
//        $model = $this->loadModel($id);
//        Inventory::model()

        $inventories = Inventory::model()->findAllByAttributes(array('ref_no' => $id));
//
        $ref_no = array();
        foreach ($inventories as $inventory) {
            $ref_no[] = $inventory->id;
        }

        if (empty($ref_no)) {
            $ref_no = -1;
        }

//
        $model1 = new InventoryItems('search');
        $model1->unsetAttributes();  // clear any default values
//        $model1->id = array(implode(",",$ref_no));
        $model1->inventory_id = $ref_no;

//        if(isset($_GET['InventoryItems']))
//            $model1->attributes=$_GET['InventoryItems'];
//        $inventoryItems = InventoryItems::model()->findAllByAttributes(array('inventory_id'=>$id));
//        print_r(implode(",",$ref_no));

        $this->render('view', array('model' => $model, 'inventoryItems' => $model1));
    }
    
    
      public function actionView($id) {
          
            $inventoryItems = new InventoryItems('search');
        $inventoryItems->unsetAttributes();  // clear any default values

            $inventoryItems->inventory_id = $id;
            
         $inventoryItems1 = InventoryItems::model()->findAll('inventory_id='.$id);
        
        $this->render('view', array('model' => $this->loadModel($id),'inventoryItems'=>$inventoryItems,'inventoryItems1'=>$inventoryItems1));
    }


    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
            
        $model = new Inventory;

        $model1 = new InventoryItems;
        $inventorySave = false;
        
        if (isset($_POST['close'])) {

            unset(Yii::app()->session['inventory_id'], Yii::app()->session['inventoryData']);

            $this->redirect(array('admin'));
        } else if (isset($_POST['Inventory']) && isset($_POST['InventoryItems'])) {

            $model->attributes = $_POST['Inventory'];

            $model->quantity = $_POST['InventoryItems']['quantity'];
            $model->products_id = $_POST['Inventory']['products_id'];
            $model->add_minus = $_POST['Inventory']['add_minus'];
            $model->remarks = $_POST['Inventory']['remarks'];
            $model->createdtime = $_POST['Inventory']['createdtime'];

            $model->product_name = $_POST['Inventory']['product_name'];
            $model->web_category = $_POST['Inventory']['web_category'];


            if (!isset(Yii::app()->session['inventory_id'])) {

                $model->inventory_type_id = 1; //test
                $model->status_id = 1; //test
                $model->createdby = Yii::app()->user->getId();

                $sql = "SELECT inventory.invoice_no FROM inventory WHERE inventory.createdby = '" . Yii::app()->user->id . "' ORDER BY id DESC limit 1";
                $InventoryLast = Tk::sql($sql);
                
                $invoiceNo = 0;
                if (isset($InventoryLast[0]['invoice_no'])) {

                    $invoiceNo = substr($InventoryLast[0]['invoice_no'], 3);
                }

                $invoice_no = parent::setDocumentNo($invoiceNo, "PSV");

                $model->invoice_no = $invoice_no;


                $distId = 0;
                if (Yii::app()->tk->getRole(Yii::app()->user->getId()) == 'Distributor') {
                    $distId = Yii::app()->user->name;
                }

                $model->distId = $distId;

                $inventorySave = $model->save();
            } else {

                if ($model->validate()) {
                    $inventorySave = true;
                } else {
                    $inventorySave = false;
                }
            }

            if ($inventorySave == true) {

                unset($_POST['InventoryItems']['uniqueId'], $_POST['InventoryItems']['price']);

                if (!isset(Yii::app()->session['inventory_id'])) {

                    Yii::app()->session['inventory_id'] = $model->id;
                    Yii::app()->session['inventoryData'] = $_POST['Inventory'];
                }

                $model1->attributes = $_POST['InventoryItems'];
                $model1->products_id = $_POST['Inventory']['products_id'];

                $products = Products::model()->findByPk($model1->products_id);
//                $products = Products::model()->findAll(array("condition" => "description like '$request%' limit 10"));

                if (isset($products)) {

                    $model1->unit_price = $products->price;
                    $model1->total_price = ($products->price * $model1->quantity) * (100 - $model1->discount) / 100;
                }

                $model1->quantity = abs($model1->quantity);

                if ($_POST['Inventory']['add_minus'] == 1) {
                    $model1->quantity = "-" . $model1->quantity;
                }

//                $model1->quantity = -$model1->quantity;

              
                $model1->inventory_id = Yii::app()->session['inventory_id'];
                $model1->createdby = Yii::app()->user->getId();
                $model1->createdtime = date('Y-m-d H:i:s');

                $model1->validate();

                if ($model1->save()) {
//                    $transaction->commit();
                    $this->redirect(array('create'));
                    Yii::app()->user->setFlash('success', 'Successfully added');



//                    $this->redirect(array('view', 'id' => $model1->id));
                } else {
//                    console.lg("hi");
                    Yii::app()->user->setFlash('error', 'Error');
                }
            }
        }

        ///////////////////////////


        $model3 = new InventoryItems('search');
        $model3->unsetAttributes();  // clear any default values

        $inventory_id = -1;

        if (isset(Yii::app()->session['inventory_id']))
            $inventory_id = Yii::app()->session['inventory_id'];

        $model3->inventory_id = $inventory_id;


        $itemsCount = InventoryItems::model()->countByAttributes(array('inventory_id' => $inventory_id));

        if (isset(Yii::app()->session['inventoryData'])) {
//            $model->attributes = Yii::app()->session['inventoryData'];

            if (isset(Yii::app()->session['inventoryData']['ref_no'])) {
                $model->ref_no = Yii::app()->session['inventoryData']['ref_no'];
            }

            if (isset(Yii::app()->session['inventoryData']['remarks'])) {
                $model->remarks = Yii::app()->session['inventoryData']['remarks'];
            }

            if (isset(Yii::app()->session['inventoryData']['transaction_date'])) {
                $model->transaction_date = Yii::app()->session['inventoryData']['transaction_date'];
            }
            $readonly = true;
        } else {
            $readonly = false;
        }

        $this->render('create', array('readonly' => $readonly, 'model' => $model, 'inventoryItems' => $model3, 'itemsCount' => $itemsCount, 'inventoryData' => Yii::app()->session['inventoryData']));
    }

    public function actionCreate2() {



        $model = new Inventory;


        if (isset($_POST['close'])) {

            unset(Yii::app()->session['inventory_id']);
            unset(Yii::app()->session['inventoryData']);

            $this->redirect(array('inventoryitems/admin'));
        } else if (isset($_POST['Inventory']) && isset($_POST['InventoryItems'])) {


            if (!isset(Yii::app()->session['inventory_id'])) {

                $model->attributes = $_POST['Inventory'];

                $model->inventory_type_id = 1; //test
                $model->status_id = 1; //test
                $model->createdby = Yii::app()->user->getId();
//                $model->createdtime = date('Y-m-d H:i:s');

                $inventorySave = $model->save();
            } else {
                $inventorySave = true;
            }

            if ($inventorySave) {

                $model1 = new InventoryItems;
//                                if (isset($_POST['InventoryItems'])) {
//                $transaction = $model1->getDbConnection()->beginTransaction();
//                try {

                if (!isset(Yii::app()->session['inventory_id'])) {

                    Yii::app()->session['inventory_id'] = $model->id;
                    Yii::app()->session['inventoryData'] = $_POST['Inventory'];
                }

                $model1->attributes = $_POST['InventoryItems'];

                $products = Products::model()->findByAttributes(array('id' => $model1->products_id));
//                $products = Products::model()->findAll(array("condition" => "description like '$request%' limit 10"));

                if (isset($products)) {

                    $model1->unit_price = $products->price;
                    $model1->total_price = $products->price * $model1->quantity;
                }



                $model1->inventory_id = Yii::app()->session['inventory_id'];
                $model1->createdby = Yii::app()->user->getId();
                $model1->createdtime = date('Y-m-d H:i:s');



                if ($model1->save()) {
//                    $transaction->commit();
                    Yii::app()->user->setFlash('success', 'Successfully added');
                    $this->redirect(array('create'));


//                    $this->redirect(array('view', 'id' => $model1->id));
                } else {
//                    console.lg("hi");
                    Yii::app()->user->setFlash('error', 'test');
                }


//                 } catch (Exception $e) {
////                    echo $e->getMessage();
//                //Tk::a($e);exit;
//                $transaction->rollback();
//                Yii::app()->user->setFlash('error',$e->getMessage());
//            }
//                else{
//                    $this->redirect(array('view22'));
//                }
//                                }
            }
//                        }
//            Yii::app()->user->setFlash('error','test123');
        }


//        echo Yii::app()->session['inventory_id'];
//        if(isset($_GET['InventoryItems']))
//            $model->attributes=$_GET['InventoryItems'];
//        $this->render('create', array('model' => $model));




        $model3 = new InventoryItems('search');
        $model3->unsetAttributes();  // clear any default values

        $inventory_id = -1;

        if (isset(Yii::app()->session['inventory_id']))
            $inventory_id = Yii::app()->session['inventory_id'];

        $model3->inventory_id = $inventory_id;


        $itemsCount = InventoryItems::model()->countByAttributes(array('inventory_id' => $inventory_id));

        if (isset(Yii::app()->session['inventoryData'])) {
//            $model->attributes = Yii::app()->session['inventoryData'];

            if (isset(Yii::app()->session['inventoryData']['ref_no'])) {
                $model->ref_no = Yii::app()->session['inventoryData']['ref_no'];
            }

            if (isset(Yii::app()->session['inventoryData']['remarks'])) {
                $model->remarks = Yii::app()->session['inventoryData']['remarks'];
            }

            if (isset(Yii::app()->session['inventoryData']['transaction_date'])) {
                $model->transaction_date = Yii::app()->session['inventoryData']['transaction_date'];
            }
            $readonly = true;
        } else {
            $readonly = false;
        }

//        $model->remarks = 'ttttt';
//        print_r($model->attributes);

        $this->render('create2', array('readonly' => $readonly, 'model' => $model, 'inventoryItems' => $model3, 'itemsCount' => $itemsCount, 'inventoryData' => Yii::app()->session['inventoryData']));
    }

    public function actionCreate3() {



        $model = new Inventory;


        if (isset($_POST['close'])) {

            unset(Yii::app()->session['inventory_id']);
            unset(Yii::app()->session['inventoryData']);

            $this->redirect(array('inventoryitems/admin'));
        } else if (isset($_POST['Inventory']) && isset($_POST['InventoryItems'])) {


            if (!isset(Yii::app()->session['inventory_id'])) {

                $model->attributes = $_POST['Inventory'];

                $model->inventory_type_id = 1; //test
                $model->status_id = 1; //test
                $model->createdby = Yii::app()->user->getId();
//                $model->createdtime = date('Y-m-d H:i:s');

                $inventorySave = $model->save();
            } else {
                $inventorySave = true;
            }

            if ($inventorySave) {

                $model1 = new InventoryItems;
//                                if (isset($_POST['InventoryItems'])) {
//                $transaction = $model1->getDbConnection()->beginTransaction();
//                try {


                unset($_POST['InventoryItems']['uniqueId'], $_POST['InventoryItems']['price']);

                if (!isset(Yii::app()->session['inventory_id'])) {

                    Yii::app()->session['inventory_id'] = $model->id;
                    Yii::app()->session['inventoryData'] = $_POST['Inventory'];
                }

                $model1->attributes = $_POST['InventoryItems'];

                $products = Products::model()->findByAttributes(array('id' => $model1->products_id));
//                $products = Products::model()->findAll(array("condition" => "description like '$request%' limit 10"));

                if (isset($products)) {

                    $model1->unit_price = $products->price;
                    $model1->total_price = $products->price * $model1->quantity;
                }



                $model1->inventory_id = Yii::app()->session['inventory_id'];
                $model1->createdby = Yii::app()->user->getId();
                $model1->createdtime = date('Y-m-d H:i:s');



                if ($model1->save()) {
//                    $transaction->commit();
                    Yii::app()->user->setFlash('success', 'Successfully added');
                    $this->redirect(array('create'));


//                    $this->redirect(array('view', 'id' => $model1->id));
                } else {
//                    console.lg("hi");
                    Yii::app()->user->setFlash('error', 'test');
                }


//                 } catch (Exception $e) {
////                    echo $e->getMessage();
//                //Tk::a($e);exit;
//                $transaction->rollback();
//                Yii::app()->user->setFlash('error',$e->getMessage());
//            }
//                else{
//                    $this->redirect(array('view22'));
//                }
//                                }
            }
//                        }
//            Yii::app()->user->setFlash('error','test123');
        }


//        echo Yii::app()->session['inventory_id'];
//        if(isset($_GET['InventoryItems']))
//            $model->attributes=$_GET['InventoryItems'];
//        $this->render('create', array('model' => $model));




        $model3 = new InventoryItems('search');
        $model3->unsetAttributes();  // clear any default values

        $inventory_id = -1;

        if (isset(Yii::app()->session['inventory_id']))
            $inventory_id = Yii::app()->session['inventory_id'];

        $model3->inventory_id = $inventory_id;


        $itemsCount = InventoryItems::model()->countByAttributes(array('inventory_id' => $inventory_id));

        if (isset(Yii::app()->session['inventoryData'])) {
//            $model->attributes = Yii::app()->session['inventoryData'];

            if (isset(Yii::app()->session['inventoryData']['ref_no'])) {
                $model->ref_no = Yii::app()->session['inventoryData']['ref_no'];
            }

            if (isset(Yii::app()->session['inventoryData']['remarks'])) {
                $model->remarks = Yii::app()->session['inventoryData']['remarks'];
            }

            if (isset(Yii::app()->session['inventoryData']['transaction_date'])) {
                $model->transaction_date = Yii::app()->session['inventoryData']['transaction_date'];
            }
            $readonly = true;
        } else {
            $readonly = false;
        }

//        $model->remarks = 'ttttt';
//        print_r($model->attributes);

        $this->render('create3', array('readonly' => $readonly, 'model' => $model, 'inventoryItems' => $model3, 'itemsCount' => $itemsCount, 'inventoryData' => Yii::app()->session['inventoryData']));
    }

    public function actionCreate4() {

        $model = new Inventory;

        if (isset($_POST['close'])) {

            unset(Yii::app()->session['inventory_id']);
            unset(Yii::app()->session['inventoryData']);

            $this->redirect(array('inventory/admin'));
        } else if (isset($_POST['Inventory']) && isset($_POST['InventoryItems'])) {


            if (!isset(Yii::app()->session['inventory_id'])) {

                $model->attributes = $_POST['Inventory'];

                $model->inventory_type_id = 1; //test
                $model->status_id = 1; //test
                $model->createdby = Yii::app()->user->getId();
//                $model->createdtime = date('Y-m-d H:i:s');

                $inventorySave = $model->save();
            } else {
                $inventorySave = true;
            }

            if ($inventorySave) {

                $model1 = new InventoryItems;
//                                if (isset($_POST['InventoryItems'])) {
//                $transaction = $model1->getDbConnection()->beginTransaction();
//                try {


                unset($_POST['InventoryItems']['uniqueId'], $_POST['InventoryItems']['price']);

                if (!isset(Yii::app()->session['inventory_id'])) {

                    Yii::app()->session['inventory_id'] = $model->id;
                    Yii::app()->session['inventoryData'] = $_POST['Inventory'];
                }

                $model1->attributes = $_POST['InventoryItems'];

                $products = Products::model()->findByAttributes(array('id' => $model1->products_id));
//                $products = Products::model()->findAll(array("condition" => "description like '$request%' limit 10"));

                if (isset($products)) {

                    $model1->unit_price = $products->price;
                    $model1->total_price = $products->price * $model1->quantity;
                }



                $model1->inventory_id = Yii::app()->session['inventory_id'];
                $model1->createdby = Yii::app()->user->getId();
                $model1->createdtime = date('Y-m-d H:i:s');



                if ($model1->save()) {
//                    $transaction->commit();
                    Yii::app()->user->setFlash('success', 'Successfully added');
                    $this->redirect(array('create'));


//                    $this->redirect(array('view', 'id' => $model1->id));
                } else {
//                    console.lg("hi");
                    Yii::app()->user->setFlash('error', 'test');
                }


//                 } catch (Exception $e) {
////                    echo $e->getMessage();
//                //Tk::a($e);exit;
//                $transaction->rollback();
//                Yii::app()->user->setFlash('error',$e->getMessage());
//            }
//                else{
//                    $this->redirect(array('view22'));
//                }
//                                }
            }
//                        }
//            Yii::app()->user->setFlash('error','test123');
        }


//        echo Yii::app()->session['inventory_id'];
//        if(isset($_GET['InventoryItems']))
//            $model->attributes=$_GET['InventoryItems'];
//        $this->render('create', array('model' => $model));




        $model3 = new InventoryItems('search');
        $model3->unsetAttributes();  // clear any default values

        $inventory_id = -1;

        if (isset(Yii::app()->session['inventory_id']))
            $inventory_id = Yii::app()->session['inventory_id'];

        $model3->inventory_id = $inventory_id;


        $itemsCount = InventoryItems::model()->countByAttributes(array('inventory_id' => $inventory_id));

        if (isset(Yii::app()->session['inventoryData'])) {
//            $model->attributes = Yii::app()->session['inventoryData'];

            if (isset(Yii::app()->session['inventoryData']['ref_no'])) {
                $model->ref_no = Yii::app()->session['inventoryData']['ref_no'];
            }

            if (isset(Yii::app()->session['inventoryData']['remarks'])) {
                $model->remarks = Yii::app()->session['inventoryData']['remarks'];
            }

            if (isset(Yii::app()->session['inventoryData']['transaction_date'])) {
                $model->transaction_date = Yii::app()->session['inventoryData']['transaction_date'];
            }
            $readonly = true;
        } else {
            $readonly = false;
        }

//        $model->remarks = 'ttttt';
//        print_r($model->attributes);

        $this->render('create4', array('readonly' => $readonly, 'model' => $model, 'inventoryItems' => $model3, 'itemsCount' => $itemsCount, 'inventoryData' => Yii::app()->session['inventoryData']));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Inventory'])) {
            $model->attributes = $_POST['Inventory'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Inventory');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Inventory('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Inventory']))
            $model->attributes = $_GET['Inventory'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Inventory the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Inventory::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Inventory $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'inventory-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionGetProducts() {

        $request = trim($_GET['term']);
        if ($request != '') {
            $model = Categories::model()->findAll(array("condition" => "category like '%$request%' and status = '1' and category_level='3' limit 10"));
            $data = array();

            foreach ($model as $get) {
                $data[] = $get->category;
            }
            $this->layout = 'empty';
            echo json_encode($data);
        }
    }

    public function actionGetProductsName() {

        $request = isset($_GET['term']) ? trim($_GET['term']) : '';
        $web_cat = isset($_GET['web_cat']) ? urldecode($_GET['web_cat']) : '';
        $id = isset($_GET['id']) ? urldecode($_GET['id']) : '';

        if (($request != '') || ($id != '' )) {

            if ($web_cat != '') {

                $categories = Categories::model()->findAll(array("select" => "id", "condition" => "category = '$web_cat' and status = '1' and category_level='3' limit 1"));

                if (isset($categories[0]['id'])) {

                    $cat_id = $categories[0]['id'];

                    $products = Products::model()->findAll(array("condition" => "category_id = '$cat_id' and name like '%$request%' limit 10"));
                }
            } else if ($id != '') {
                $products = Products::model()->findAll(array("condition" => "id ='$id' limit 1"));
            } else if ($request != '') {
                $products = Products::model()->findAll(array("condition" => "name like '%$request%' limit 10"));
            }


            $data = array();
            $product = new Products;

            foreach ($products as $get) {

                $av_qty = Inventory::totQty($get->id);

                $sub_id = $product->getCategory($get->sub_prod_cat_fk_id, $get->id);
                
                $cat = Categories::model()->find(array("condition" => "id = ".$get->category_id." and status = '1' and category_level='3'"));
                $sub_cat = isset($sub_id[$get->sub_prod_cat_fk_id]) ? $sub_id[$get->sub_prod_cat_fk_id] : '';
                $category = isset($cat->category) ? $cat->category: '';

                $data[] = array('value' => $get->name, 'discount'=>$get->discount1, 'price' => $get->price, 'uniqueId' => $get->uniqueId, 'id' => $get->id, 'av_qty' => $av_qty, 'sub_cat' => $sub_cat , 'cat'=>$category);
            }
            $this->layout = 'empty';
            echo json_encode($data);
        }
    }

//    public function actionGetProductsName() {
//
//        
//        $request = isset($_GET['term']) ? trim($_GET['term']) : '';
//        $web_cat = isset($_GET['web_cat']) ? urldecode($_GET['web_cat']) : '';
//        $id = isset($_GET['id']) ? urldecode($_GET['id']) : '';
//
//        if (($request != '') || ($id != '' )) {
//
//            if ($web_cat != '') {
//
//                $categories = Categories::model()->findAll(array("select" => "id", "condition" => "category = '$web_cat' and status = '1' and category_level='3' limit 1"));
//
//                if (isset($categories[0]['id'])) {
//
//                    $cat_id = $categories[0]['id'];
//
//                    $products = Products::model()->findAll(array("condition" => "category_id = '$cat_id' and description like '$request%' limit 10"));
//                }
//            } else if($id != '') {
//                $products = Products::model()->findAll(array("condition" => "id ='$id' limit 1"));
//            } else if($request != ''){
//                $products = Products::model()->findAll(array("condition" => "description like '$request%' limit 10"));
//            }
//
//
//            $data = array();
//
//            foreach ($products as $get) {
//
//                $av_qty = Inventory::totQty($get->id);
//                $data[] = array('value' => $get->description, 'price' => $get->price, 'uniqueId' => $get->uniqueId, 'id' => $get->id, 'av_qty' => $av_qty);
//            }
//            $this->layout = 'empty';
//            echo json_encode($data);
//        }
//    }
}
