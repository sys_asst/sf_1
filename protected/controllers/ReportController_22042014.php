<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Anura
 * Date: 6/10/14
 * Time: 1:51 PM
 * To change this template use File | Settings | File Templates.
 */

class ReportController extends RController
{
    public $layout = '//layouts/column2';

    public function filters()
    {
        return array(
            'rights',//'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function allowedActions()
    {
        return ;
    }

    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha'=>array(
                'class'=>'CCaptchaAction',
                'backColor'=>0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page'=>array(
                'class'=>'CViewAction',
            ),
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('index','view','ViewOrders'),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('create','update','ViewOrders'),
                'users'=>array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('admin','delete'),
                'users'=>array('admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionError()
    {
        if($error=Yii::app()->errorHandler->error)
        {
            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    public function actionViewOrders()
    {
        $model = new OrderForm;
        $data = array();
        $model->rpttype = isset($_REQUEST['rpttype']) ? $_REQUEST['rpttype'] : 1;
        if(isset($_POST['OrderForm']))
        {
            $model->attributes=$_POST['OrderForm'];
            //print_r($_POST);exit;
            (strlen($model->fromdate) < 1) ? $model->fromdate = date('Y-m-d') : '';
            (strlen($model->todate) < 1) ? $model->todate = date('Y-m-d') : '';
            Yii::app()->getModule('user')->isAdmin() == false ? $model->distributor = User::getDistributor() : '';
            //echo $model->distributor.'AAAAAAAA';

            if($model->rpttype == 1)
            {
                $data = Yii::app()->db->createCommand()
                    ->select('IFNULL((SELECT ODN.`OrderNo` FROM `order_nos` AS ODN WHERE ODN.`orderId` = O.id), O.id) AS ordeno, O.id, O.orderType, O.orderTotalValue, O.orderPlacedTime, D.dealerName, O.deliveryDate,B.distName,S.srName')
                    ->from('orders O')
                    ->join('dealers D', 'D.id=O.dealerId')
                    ->join('salesreps S', 'O.srcode=S.srEmpNo AND S.status = "1"')
                    ->join('distributors B', 'B.distId=D.distributor')
                    //->where('D.disabled = 0 AND O.orderPlacedTime BETWEEN ":fromdate" AND ":todate"', array(':fromdate'=>$model->fromdate,':todate'=>((strlen($model->todate)) ? "{$model->todate}" : "NOW()")))//'13491'
                    ->where("D.disabled = 0 ".(strlen($model->distributor) ? " AND D.distributor IN({$model->distributor})" : "")." AND O.orderPlacedTime BETWEEN '{$model->fromdate} 00:00:00' AND ".((strlen($model->todate)) ? "DATE_FORMAT('{$model->todate}', '%Y-%m-%d 23:59:59')" : "NOW()"))
                    ->order('O.orderPlacedTime DESC')
                    ->queryAll();//SELECT DR.`area_id` FROM `ms_regions` AS R INNER JOIN `d_regions` AS DR ON DR.`region_id` = R.`id` WHERE R.`rcm_id`
            } elseif($model->rpttype == 2) {
                $data = Yii::app()->db->createCommand()
                    ->select('IFNULL((SELECT ODN.`commentNo` FROM `comments_no` AS ODN WHERE ODN.`commentId` = DC.id), DC.id) AS commentno,
                       IFNULL(DC.comment,"n/a") AS comment, DC.id, DC.commentDate, D.dealerName,
                       IFNULL((SELECT R.reason FROM dealercommentreasons AS R WHERE R.id = DC.reasonId), \'n/a\') AS reason,
                       IFNULL((SELECT S.srName FROM salesreps AS S WHERE S.srEmpNo = DC.srCode AND S.status = "1"), \'n/a\') AS salesRef')
                    ->from('dealercomments DC')
                    ->join('dealers D', 'D.id = DC.dealerId')
                    //->where('D.disabled = 0 AND O.orderPlacedTime BETWEEN ":fromdate" AND ":todate"', array(':fromdate'=>$model->fromdate,':todate'=>((strlen($model->todate)) ? "{$model->todate}" : "NOW()")))//'13491'
                    ->where((strlen($model->distributor) ? " D.distributor IN({$model->distributor}) AND " : "")." DC.commentDate BETWEEN '{$model->fromdate} 00:00:00' AND ".((strlen($model->todate)) ? "DATE_FORMAT('{$model->todate}', '%Y-%m-%d 23:59:59')" : "NOW()"))
                    ->queryAll();
            } elseif($model->rpttype == 3) {
                $data = Yii::app()->db->createCommand()
                    ->select('D.id, D.dealerName, D.phoneOffice, D.phoneResidence,D.contactPerson, D.email, D.address1, D.address2, D.address3, T.town, DT.distName')
                    ->from('dealers D')
                    ->join('distributors DT', 'DT.distId = D.distributor')
                    ->join('towns T', 'T.id = D.townId')
                    //->where('D.disabled = 0 AND O.orderPlacedTime BETWEEN ":fromdate" AND ":todate"', array(':fromdate'=>$model->fromdate,':todate'=>((strlen($model->todate)) ? "{$model->todate}" : "NOW()")))//'13491'
                    ->where((strlen($model->distributor) ? " D.distributor IN({$model->distributor}) AND " : "")." D.lastUpdatedOn BETWEEN '{$model->fromdate} 00:00:00' AND ".((strlen($model->todate)) ? "DATE_FORMAT('{$model->todate}', '%Y-%m-%d 23:59:59')" : "NOW()"))
                    ->queryAll();
            }

        }

        $this->render('vieworders',array(
            'model'=>$model,'data'=>$data
        ));
    }
}