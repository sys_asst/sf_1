<?php

class SiteController extends RController
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout(true);
		$this->redirect(Yii::app()->homeUrl);
	}

    public function actionSrLogin() {
        $response = array();
        if (Yii::app()->user->isGuest) {

            if(isset($_REQUEST['srCode']) && isset($_REQUEST['password']))
            {
                $password = addslashes($_REQUEST['password']);
                $hash = Yii::app()->getModule('user')->hash;

                if ($hash=="md5")
                {
                    $password = md5($password);
                } elseif($hash=="sha1") {
                    $password = sha1($password);
                } else {
                    $password = hash($hash,$password);
                }

                $username = addslashes($_REQUEST['srCode']);

                $data = Yii::app()->db->createCommand()
                    ->select('*')
                    ->from('users')
                    ->where('status = 1 AND username=:username AND password=:password',array(':username'=>$username, ':password'=>$password))
                    ->queryAll();

                if(sizeof($data))
                {
                    $message = 'login success';
                    $status = true;
                } else {
                    $message = 'login fail';
                    $status = false;
                }

                $response['status'] = $status;
                $response['data']['message'] = $message;
            } else {
                $response['status'] = false;
                $response['data']['message'] = "srCode and password is can not be blank";
            }
            $json = new Dealers();
            return $json->jsonEncode($response);
            Yii::app()->end();
        } else {
            $response['status'] = true;
            $response['data']['message'] = "already logged";
            $json = new Dealers();
            return $json->jsonEncode($response);
            Yii::app()->end();
        }
    }
}
