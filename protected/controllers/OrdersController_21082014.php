<?php

class OrdersController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','list','lastUpdated','NewOrder','vieworder','admin'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

    public function actionVieworder($id)
    {
        $this->render('vieworder',array(
            'model'=>$this->loadModel($id),
        ));
    }
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Orders;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Orders']))
		{
			$model->attributes=$_POST['Orders'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Orders']))
		{
			$model->attributes=$_POST['Orders'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Orders');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
        $model=new Orders('search');
        /*if(Yii::app()->user->name=='admin')
        {
            $model=new Orders('search');
        } else {
            //$model = new CActiveDataProvider(Orders::model()->owner()->findAll());
            $model=new Orders('search');
        }*/

		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Orders']))
			$model->attributes=$_GET['Orders'];
        //print_r($model);exit;

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Orders the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Orders::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Orders $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='orders-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    /**
     * Return productCategory list according to the given srCode and timestamp
     * @return Jason string
     */
    public function actionList()
    {
        $response = $data = $dataArray = array();

        if(isset($_REQUEST['id']) && strlen($_REQUEST['id']))
        {
            $id = ' AND id='.$_REQUEST['id'];
        } else {
            $id = '';
        }
        if(isset($_REQUEST['dealer'])  && strlen($_REQUEST['dealer']))
        {
            $dealerID = ' AND dealerID='.$_REQUEST['dealer'];
        } else {
            $dealerID = '';
        }
        if(isset($_REQUEST['sr'])  && strlen($_REQUEST['sr']))
        {
            $srCode= ' AND srCode='.$_REQUEST['sr'];
        } else {
            $srCode = '';
        }
        if(isset($_REQUEST['timestamp']) && strlen($_REQUEST['timestamp']))
        {
            $timestamp = ' AND UNIX_TIMESTAMP(orderPlacedTime) > '.$_REQUEST['timestamp'];
        } else {
            $timestamp = '';
        }
        if(isset($_REQUEST['to']) && isset($_REQUEST['from']) && strlen($_REQUEST['to']) && strlen($_REQUEST['from']))
        {
            $fromdate = date_create_from_format('Ymd',$_REQUEST['from']);
            $todate = date_create_from_format('Ymd',$_REQUEST['to']);

            $daterange = " AND orderPlacedTime BETWEEN '".$fromdate->format('Y-m-d')." 00:00:00' AND '".$todate->format('Y-m-d')." 23:59:59'";
        } else {
            $daterange = '';
        }

        $data = Yii::app()->db->createCommand()
            ->select('id,DATE_FORMAT(deliveryDate,"%Y-%m-%d 00:00:00") AS deliveryDate,orderInitTime,orderPlacedTime,orderType,dealerId,orderTotalValue,paymentType,srCode,latitude,longitude')
            ->from('orders')
            ->where('0=0 '. $timestamp . $dealerID . $id . $srCode . $daterange)
            ->queryAll();

        if(sizeof($data))
        {
            foreach($data as $data_row)
            {
                $lineitems = array();
                if($data_row['id'])
                {
                    $lineitems = Yii::app()->db->createCommand()
                        ->select('*')
                        ->from('orderitems')
                        ->where('orderId=:orderid ',array(':orderid'=>$data_row['id']))
                        ->queryAll();
                }
                //print_r($lineitems);exit;
                $data_row['lineItems'] = $lineitems;
                $data_row['location'] = array('latitude' => $data_row['latitude'], 'longitude' => $data_row['longitude']);

                unset($data_row['latitude']);
                unset($data_row['longitude']);
                array_push($dataArray, $data_row);
            }
        }

        $response['status'] = true;
        $response['data']['orders'] = $dataArray;
        //$response['data']['message'] = "Successfully run the request";
        $json = new Dealers();
        return $json->jsonEncode($response);
        Yii::app()->end();
    }

    public function actionlastUpdated ()
    {
        $response = $data = array();
        $tableNames = array('products', 'dealers', 'distributors', 'categories', 'dealerCommentReasons', 'areas', 'routes', 'salesreps', 'schedule', 'towns');

        $tempLastUpdatedOn = 0;

        foreach ($tableNames as $tableName)
        {
            $lastUpdatedOn = Yii::app()->db->createCommand()
                ->select('(MAX(UNIX_TIMESTAMP(lastUpdatedOn))) AS lastUpdatedOn')
                ->from($tableName)
                ->queryRow();
                //print_r($lastUpdatedOn);
            $lastUpdatedOn = $lastUpdatedOn['lastUpdatedOn'];
            if($tempLastUpdatedOn < $lastUpdatedOn)
            {
                $tempLastUpdatedOn = $lastUpdatedOn;
            }
        }

        $response['status'] = true;
        $response['data']['timestamp']['lastUpdatedOn'] = $tempLastUpdatedOn;
        //$response['data']['message'] = "Successfully run the request";
        $json = new Dealers();
        return $json->jsonEncode($response);
        Yii::app()->end();
    }

    public function actionNewOrder()
    {
        $response = $data = $returnarray = $details = array();
        $jsonstring = isset($_REQUEST['order']) ? $_REQUEST['order'] : '';
        $srcode = isset($_REQUEST['srCode']) ? $_REQUEST['srCode'] : '';

        if(strlen($srcode) && strlen($jsonstring))
        {
            $model=new Orders;

            $orderinfo_array = json_decode($jsonstring, true);

            $orderinfo_array['srCode'] = $srcode;
            $lineitems = $orderinfo_array['lineItems'];
            $location_array = $orderinfo_array['location'];
            unset($orderinfo_array['lineItems']);
            unset($orderinfo_array['location']);
            $orderinfo_array['latitude'] = $location_array['latitude'];
            $orderinfo_array['longitude'] = $location_array['longitude'];

            $model->attributes=$orderinfo_array;

            if(sizeof($orderinfo_array))
            {
                foreach($lineitems as $lineitem)
                {
                    $detail = new Orderitems('batchSave');
                    $detail->attributes = $lineitem;
                    $details[] = $detail;
                }
                $transaction = $model->getDbConnection()->beginTransaction();

                try {
                    //print_r($model);exit;
                    $model->save();
                    $model->refresh();
                    Orderitems::model()->deleteAll(array('condition'=>'orderId=:orderId', 'params'=>array(':orderId'=>$model->id)));
                    foreach ($details as $detail) {
                        $detail->orderId = $model->id;
                        $detail->save();
                    }
                    $transaction->commit();
                    $status = true;
                    $message = 'Successfully added data';
                } catch (Exception $e) {
                    $transaction->rollback();
                    $status = false;
                    $message = 'Fail to add data';
                }
            } else {
                $status = false;
                $message = 'Empty data';
            }

        } else {
            $status = false;
            $message = 'srCode and order details are blank';
        }

        $response['status'] = $status;
        $response['data']['orderId'] = strlen($model->id) ? $model->id : -1;
        $response['data']['message'] = $message;

        $json = new Dealers();
        return  $json->jsonEncode($response);
        Yii::app()->end();
    }
}
